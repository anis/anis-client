/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { NavigationGuard } from './core/navigation.guard';
import { NotFoundPageComponent } from './core/containers/not-found-page.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    {
        path: 'search',
        loadChildren: () => import('./search/search.module').then(m => m.SearchModule),
        canActivate: [NavigationGuard]
    },
    {
        path: 'search-multiple',
        loadChildren: () => import('./search-multiple/search-multiple.module').then(m => m.SearchMultipleModule),
        canActivate: [NavigationGuard]
    },
    {
        path: 'detail/:dname/:objectSelected',
        loadChildren: () => import('./detail/detail.module').then(m => m.DetailModule)
    },
    {
        path: 'documentation',
        loadChildren: () => import('./documentation/documentation.module').then(m => m.DocumentationModule),
        canActivate: [NavigationGuard]
    },
    { path: '**', component: NotFoundPageComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }

export const routedComponents = [
    NotFoundPageComponent
];
