import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { ResultComponent } from './result.component';
import * as fromSearch from '../store/search.reducer';
import * as searchActions from '../store/search.action';
import { Criterion, SearchQueryParams } from '../store/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import { Attribute, Category, Dataset, Family } from '../../metamodel/model';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { Pagination, PaginationOrder } from '../../shared/datatable/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';
import { RouterLinkDirectiveStub } from '../../../settings/test-data/router-link-directive-stub';

describe('[Search] Container: ResultComponent', () => {
    @Component({ selector: 'app-result-download', template: '' })
    class DownloadSectionStubComponent {
        @Input() datasetName: string;
        @Input() datasetList: Dataset[];
        @Input() dataLength: number;
        @Input() isConeSearchAdded: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() criteriaList: Criterion[];
        @Input() outputList: number[];
    }

    @Component({ selector: 'app-reminder', template: '' })
    class ReminderStubComponent {
        @Input() datasetAttributeList: Attribute[];
        @Input() isConeSearchAdded: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() criteriaList: Criterion[];
        @Input() criteriaFamilyList: Family[];
        @Input() outputFamilyList: Family[];
        @Input() categoryList: Category[];
        @Input() outputList: number[];
    }

    @Component({ selector: 'app-result-url-display', template: '' })
    class UrlDisplaySectionStubComponent {
        @Input() datasetList: Dataset[];
        @Input() datasetName: string;
        @Input() isConeSearchAdded: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() criteriaList: Criterion[];
        @Input() outputList: number[];
    }

    @Component({ selector: 'app-datatable-tab', template: '' })
    class DatatableTabStubComponent {
        @Input() datasetName: string;
        @Input() datasetList: Dataset[];
        @Input() queryParams: SearchQueryParams;
        @Input() datasetAttributeList: Attribute[];
        @Input() outputList: number[];
        @Input() searchData: any[];
        @Input() dataLength: number;
        @Input() selectedData: any[];
        @Input() processWip: boolean;
        @Input() processDone: boolean;
        @Input() processId: string;
    }

    let scrollTopServiceStub: Partial<ScrollTopService> = {
        setScrollTop() {}
    };

    let component: ResultComponent;
    let fixture: ComponentFixture<ResultComponent>;
    let store: MockStore;
    const initialState = {
        search: { ...fromSearch.initialState },
        metamodel: { ...fromMetamodel },
        coneSearch: { ...fromConeSearch }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                ResultComponent,
                DownloadSectionStubComponent,
                ReminderStubComponent,
                UrlDisplaySectionStubComponent,
                DatatableTabStubComponent,
                RouterLinkDirectiveStub
            ],
            providers: [
                provideMockStore({ initialState }),
                { provide: ScrollTopService, useValue: scrollTopServiceStub }
            ]
        });
        fixture = TestBed.createComponent(ResultComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        component.datasetName = of('toto');
        const loadDatasetSearchMetaAction = new datasetActions.LoadDatasetSearchMetaAction();
        const changeStepAction = new searchActions.ChangeStepAction('result');
        const resultChecked = new searchActions.ResultChecked();
        const initSearchByUrl = new searchActions.InitSearchByUrl();
        const spy = spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(4);
            expect(spy).toHaveBeenCalledWith(loadDatasetSearchMetaAction);
            expect(spy).toHaveBeenCalledWith(changeStepAction);
            expect(spy).toHaveBeenCalledWith(resultChecked);
            expect(spy).toHaveBeenCalledWith(initSearchByUrl);
            done();
        });
    });

    it('#getDataLength() should dispatch GetDataLengthAction', () => {
        const getDataLengthAction = new searchActions.GetDataLengthAction();
        const spy = spyOn(store, 'dispatch');
        component.getDataLength();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(getDataLengthAction);
    });

    it('#getSearchData() should dispatch RetrieveDataAction', () => {
        const pagination: Pagination = { dname: 'toto', nbItems: 10, page: 1, sortedCol: 1, order: PaginationOrder.a };
        const retrieveDataAction = new searchActions.RetrieveDataAction(pagination);
        const spy = spyOn(store, 'dispatch');
        component.getSearchData(pagination);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(retrieveDataAction);
    });

    it('#addSearchData() should dispatch AddSelectedDataAction', () => {
        const addSelectedDataAction = new searchActions.AddSelectedDataAction('toto');
        const spy = spyOn(store, 'dispatch');
        component.addSearchData('toto');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(addSelectedDataAction);
    });

    it('#deleteSearchData() should dispatch DeleteSelectedDataAction', () => {
        const deleteSelectedDataAction = new searchActions.DeleteSelectedDataAction('toto');
        const spy = spyOn(store, 'dispatch');
        component.deleteSearchData('toto');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(deleteSelectedDataAction);
    });

    it('#executeProcess() should dispatch ExecuteProcessAction', () => {
        const executeProcessAction = new searchActions.ExecuteProcessAction('toto');
        const spy = spyOn(store, 'dispatch');
        component.executeProcess('toto');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(executeProcessAction);
    });

    it('#ngOnDestroy() should dispatch DestroyResultsAction', () => {
        const destroyResultsAction = new searchActions.DestroyResultsAction();
        const spy = spyOn(store, 'dispatch');
        component.ngOnDestroy();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(destroyResultsAction);
    });
});
