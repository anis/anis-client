/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromSearch from '../store/search.reducer';
import * as searchActions from '../store/search.action';
import * as searchSelector from '../store/search.selector';
import { Criterion, SearchQueryParams } from '../store/model';
import * as fromAuth from '../../auth/auth.reducer';
import * as authSelector from '../../auth/auth.selector';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as attributeActions from '../../metamodel/action/attribute.action';
import * as criteriaActions from '../../metamodel/action/criteria.action';
import * as outputActions from '../../metamodel/action/output.action';
import * as metamodelSelector from '../../metamodel/selectors';
import { Survey, Family, Dataset, Attribute, Category } from '../../metamodel/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    auth: fromAuth.State;
    metamodel: fromMetamodel.State;
    search: fromSearch.State;
}

@Component({
    selector: 'app-dataset',
    templateUrl: 'dataset.component.html'
})
/**
 * @class
 * @classdesc Search dataset container.
 *
 * @implements OnInit
 */
export class DatasetComponent implements OnInit {
    public isUserAuthenticated: Observable<boolean>;
    public datasetSearchMetaIsLoading: Observable<boolean>;
    public datasetSearchMetaIsLoaded: Observable<boolean>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public surveyList: Observable<Survey[]>;
    public datasetList: Observable<Dataset[]>;
    public datasetFamilyList: Observable<Family[]>;
    public attributeList: Observable<Attribute[]>;
    public datasetAttributeList: Observable<Attribute[]>;
    public currentStep: Observable<string>;
    public datasetName: Observable<string>;
    public criteriaFamilyList: Observable<Family[]>;
    public criteriaList: Observable<Criterion[]>;
    public outputFamilyList: Observable<Family[]>;
    public categoryList: Observable<Category[]>;
    public outputList: Observable<number[]>;
    public queryParams: Observable<SearchQueryParams>;

    constructor(private store: Store<StoreState>, private scrollTopService: ScrollTopService) {
        this.isUserAuthenticated = store.select(authSelector.isAuthenticated);
        this.datasetSearchMetaIsLoading = store.select(metamodelSelector.getDatasetSearchMetaIsLoading);
        this.datasetSearchMetaIsLoading = store.select(metamodelSelector.getDatasetSearchMetaIsLoading);
        this.datasetSearchMetaIsLoaded = store.select(metamodelSelector.getDatasetSearchMetaIsLoaded);
        this.surveyList = store.select(metamodelSelector.getSurveyList);
        this.datasetList = store.select(metamodelSelector.getDatasetList);
        this.datasetFamilyList = store.select(metamodelSelector.getDatasetFamilyList);
        this.currentStep = store.select(searchSelector.getCurrentStep);
        this.datasetName = store.select(searchSelector.getDatasetName);
        this.criteriaFamilyList = this.store.select(metamodelSelector.getCriteriaFamilyList);
        this.criteriaList = this.store.select(searchSelector.getCriteriaList);
        this.outputFamilyList = this.store.select(metamodelSelector.getOutputFamilyList);
        this.categoryList = this.store.select(metamodelSelector.getCategoryList);
        this.outputList = this.store.select(searchSelector.getOutputList);
        this.queryParams = this.store.select(searchSelector.getQueryParams);
    }

    ngOnInit() {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.ChangeStepAction('dataset')));
        this.store.dispatch(new datasetActions.LoadDatasetSearchMetaAction());
        this.scrollTopService.setScrollTop();
    }

    /**
     * Dispatches actions to:
     *      - launch new search with the given dataset name.
     *      - retrieve dataset attribute list.
     *      - retrieve criteria metadata.
     *      - retrieve output metadata.
     *
     * @param  {string} datasetName - The dataset name.
     */
    selectDataset(datasetName: string): void {
        // Create a micro tasks that is processed after the current synchronous code
        // This micro task prevent the expression has changed after it was checked
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.NewSearchAction(datasetName)));
        this.store.dispatch(new attributeActions.LoadAttributeListAction(datasetName));
        this.store.dispatch(new criteriaActions.LoadCriteriaSearchMetaAction(datasetName));
        this.store.dispatch(new outputActions.LoadOutputSearchMetaAction(datasetName));
        this.attributeListIsLoading = this.store.select(metamodelSelector.getAttributeListIsLoading, { dname: datasetName });
        this.attributeListIsLoaded = this.store.select(metamodelSelector.getAttributeListIsLoaded, { dname: datasetName });
        this.attributeList = this.store.select(metamodelSelector.getAttributeList, { dname: datasetName });
    }
}
