/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromSearch from '../store/search.reducer';
import * as searchActions from '../store/search.action';
import * as searchSelector from '../store/search.selector';
import { Criterion, SearchQueryParams } from '../store/model';
import { Dataset, Family, Attribute, Category } from '../../metamodel/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as metamodelSelector from '../../metamodel/selectors';
import * as coneSearchSelector from '../../shared/cone-search/store/cone-search.selector';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    search: fromSearch.State;
    metamodel: fromMetamodel.State;
}

@Component({
    selector: 'app-criteria',
    templateUrl: 'criteria.component.html'
})
/**
 * @class
 * @classdesc Search criteria container.
 *
 * @implements OnInit
 */
export class CriteriaComponent implements OnInit {
    public criteriaSearchMetaIsLoading: Observable<boolean>;
    public criteriaSearchMetaIsLoaded: Observable<boolean>;
    public datasetSearchMetaIsLoading: Observable<boolean>;
    public datasetSearchMetaIsLoaded: Observable<boolean>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public datasetName: Observable<string>;
    public currentStep: Observable<string>;
    public datasetList: Observable<Dataset[]>;
    public criteriaFamilyList: Observable<Family[]>;
    public attributeList: Observable<Attribute[]>;
    public criteriaList: Observable<Criterion[]>;
    public isValidConeSearch: Observable<boolean>;
    public isConeSearchAdded: Observable<boolean>;
    public coneSearch: Observable<ConeSearch>;
    public outputFamilyList: Observable<Family[]>;
    public categoryList: Observable<Category[]>;
    public outputList: Observable<number[]>;
    public queryParams: Observable<SearchQueryParams>;

    constructor(private store: Store<StoreState>, private scrollTopService: ScrollTopService) {
        this.criteriaSearchMetaIsLoading = store.select(metamodelSelector.getCriteriaSearchMetaIsLoading);
        this.criteriaSearchMetaIsLoaded = store.select(metamodelSelector.getCriteriaSearchMetaIsLoaded);
        this.datasetSearchMetaIsLoading = store.select(metamodelSelector.getDatasetSearchMetaIsLoading);
        this.datasetSearchMetaIsLoaded = store.select(metamodelSelector.getDatasetSearchMetaIsLoaded);
        this.datasetName = store.select(searchSelector.getDatasetName);
        this.currentStep = store.select(searchSelector.getCurrentStep);
        this.datasetList = store.select(metamodelSelector.getDatasetList);
        this.criteriaFamilyList = this.store.select(metamodelSelector.getCriteriaFamilyList);
        this.criteriaList = this.store.select(searchSelector.getCriteriaList);
        this.isValidConeSearch = this.store.select(coneSearchSelector.getIsValidConeSearch);
        this.isConeSearchAdded = this.store.select(searchSelector.getIsConeSearchAdded);
        this.coneSearch = this.store.select(coneSearchSelector.getConeSearch);
        this.outputFamilyList = this.store.select(metamodelSelector.getOutputFamilyList);
        this.categoryList = this.store.select(metamodelSelector.getCategoryList);
        this.outputList = this.store.select(searchSelector.getOutputList);
        this.queryParams = this.store.select(searchSelector.getQueryParams);
    }

    ngOnInit() {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.InitSearchByUrl()));
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.ChangeStepAction('criteria')));
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.CriteriaChecked()));
        this.store.dispatch(new datasetActions.LoadDatasetSearchMetaAction());
        this.scrollTopService.setScrollTop();
        this.datasetName.subscribe(datasetName => {
            if (datasetName) {
                this.attributeListIsLoading = this.store.select(metamodelSelector.getAttributeListIsLoading, { dname: datasetName });
                this.attributeListIsLoaded = this.store.select(metamodelSelector.getAttributeListIsLoaded, { dname: datasetName });
                this.attributeList = this.store.select(metamodelSelector.getAttributeList, { dname: datasetName });
            }
        });
    }

    /**
     * Dispatches action to toggle cone search added status.
     *
     * @param  {boolean} coneSearchAdded - Is cone search added.
     */
    coneSearchAdded(coneSearchAdded: boolean): void {
        this.store.dispatch(new searchActions.IsConeSearchAddedAction(coneSearchAdded));
    }

    /**
     * Dispatches action to add the given criterion to the search.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    addCriterion(criterion: Criterion): void {
        this.store.dispatch(new searchActions.AddCriterionAction(criterion));
    }

    /**
     * Dispatches action to remove the given criterion ID to the search.
     *
     * @param  {number} id - The criterion ID.
     */
    deleteCriterion(id: number): void {
        this.store.dispatch(new searchActions.DeleteCriterionAction(id));
    }
}
