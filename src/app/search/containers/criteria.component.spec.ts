import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { CriteriaComponent } from './criteria.component';
import * as fromSearch from '../store/search.reducer';
import * as searchActions from '../store/search.action';
import { Criterion, FieldCriterion, SearchQueryParams } from '../store/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import { Dataset, Attribute, Family, Category } from '../../metamodel/model';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';
import { RouterLinkDirectiveStub } from '../../../settings/test-data/router-link-directive-stub';

describe('[Search] Container: CriteriaComponent', () => {
    @Component({ selector: 'app-cone-search-tab', template: '' })
    class ConeSearchTabStubComponent {
        @Input() datasetName: string;
        @Input() datasetList: Dataset[];
        @Input() isConeSearchAdded: boolean;
        @Input() isValidConeSearch: boolean;
    }

    @Component({ selector: '<app-criteria-tabs', template: '' })
    class CriteriaTabsStubComponent {
        @Input() criteriaFamilyList: Family[];
        @Input() datasetAttributeList: Attribute[];
        @Input() criteriaList: Criterion[];
    }

    @Component({ selector: 'app-summary', template: '' })
    class SummaryStubComponent {
        @Input() currentStep: string;
        @Input() datasetName: string;
        @Input() datasetList: Dataset[];
        @Input() isConeSearchAdded: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() criteriaFamilyList: Family[];
        @Input() criteriaList: Criterion[];
        @Input() datasetAttributeList: Attribute[];
        @Input() outputFamilyList: Family[];
        @Input() categoryList: Category[];
        @Input() outputList: number[];
        @Input() outputListEmpty: boolean;
        @Input() queryParams: SearchQueryParams;
    }

    let scrollTopServiceStub: Partial<ScrollTopService> = {
        setScrollTop() {}
    };

    let component: CriteriaComponent;
    let fixture: ComponentFixture<CriteriaComponent>;
    let store: MockStore;
    const initialState = {
        search: { ...fromSearch.initialState },
        metamodel: { ...fromMetamodel },
        coneSearch: { ...fromConeSearch }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                CriteriaComponent,
                ConeSearchTabStubComponent,
                CriteriaTabsStubComponent,
                SummaryStubComponent,
                RouterLinkDirectiveStub
            ],
            providers: [
                provideMockStore({ initialState }),
                { provide: ScrollTopService, useValue: scrollTopServiceStub }
            ]
        });
        fixture = TestBed.createComponent(CriteriaComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        component.datasetName = of('toto');
        const loadDatasetSearchMetaAction = new datasetActions.LoadDatasetSearchMetaAction();
        const initSearchByUrl = new searchActions.InitSearchByUrl();
        const changeStepAction = new searchActions.ChangeStepAction('criteria');
        const criteriaChecked = new searchActions.CriteriaChecked();
        const spy = spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(4);
            expect(spy).toHaveBeenCalledWith(loadDatasetSearchMetaAction);
            expect(spy).toHaveBeenCalledWith(initSearchByUrl);
            expect(spy).toHaveBeenCalledWith(changeStepAction);
            expect(spy).toHaveBeenCalledWith(criteriaChecked);
            done();
        });
    });

    it('#coneSearchAdded() should dispatch IsConeSearchAddedAction', () => {
        const isConeSearchAddedAction = new searchActions.IsConeSearchAddedAction(true);
        const spy = spyOn(store, 'dispatch');
        component.coneSearchAdded(true);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(isConeSearchAddedAction);
    });

    it('#addCriterion() should dispatch AddCriterionAction', () => {
        const criterion = { id: 1, type: 'field', operator: 'eq', value: 'test' } as FieldCriterion;
        const addCriterionAction = new searchActions.AddCriterionAction(criterion);
        const spy = spyOn(store, 'dispatch');
        component.addCriterion(criterion);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(addCriterionAction);
    });

    it('#deleteCriterion() should dispatch DeleteCriterionAction', () => {
        const deleteCriterionAction = new searchActions.DeleteCriterionAction(1);
        const spy = spyOn(store, 'dispatch');
        component.deleteCriterion(1);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(deleteCriterionAction);
    });
});
