/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as searchReducer from '../store/search.reducer';
import * as searchSelector from '../store/search.selector';
import { SearchQueryParams } from '../store/model';
import * as searchMultipleActions from '../../search-multiple/store/search-multiple.action';
import * as coneSearchActions from '../../shared/cone-search/store/cone-search.action';

@Component({
    selector: 'app-search',
    templateUrl: 'search.component.html'
})
/**
 * @class
 * @classdesc Search container.
 */
export class SearchComponent {
    public currentStep: Observable<string>;
    public datasetName: Observable<string>;
    public criteriaStepChecked: Observable<boolean>;
    public outputStepChecked: Observable<boolean>;
    public resultStepChecked: Observable<boolean>;
    public queryParams: Observable<SearchQueryParams>;
    public outputListEmpty: Observable<boolean>;

    constructor(private store: Store<searchReducer.State>) {
        this.currentStep = store.select(searchSelector.getCurrentStep);
        this.datasetName = store.select(searchSelector.getDatasetName);
        this.criteriaStepChecked = store.select(searchSelector.getCriteriaStepChecked);
        this.outputStepChecked = store.select(searchSelector.getOutputStepChecked);
        this.resultStepChecked = store.select(searchSelector.getResultStepChecked);
        this.queryParams = this.store.select(searchSelector.getQueryParams);
        this.outputListEmpty = this.store.select(searchSelector.getOutputListEmpty);
        this.store.dispatch(new searchMultipleActions.ResetSearchAction());
        this.store.dispatch(new coneSearchActions.DeleteConeSearchAction());
    }
}
