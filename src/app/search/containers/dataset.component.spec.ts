import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { DatasetComponent } from './dataset.component';
import * as fromSearch from '../store/search.reducer';
import * as searchActions from '../store/search.action';
import { Criterion, SearchQueryParams } from '../store/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as attActions from '../../metamodel/action/attribute.action';
import * as criteriaActions from '../../metamodel/action/criteria.action';
import * as outputActions from '../../metamodel/action/output.action';
import { Dataset, Attribute, Survey, Family, Category } from '../../metamodel/model';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';
import { RouterLinkDirectiveStub } from '../../../settings/test-data/router-link-directive-stub';

describe('[Search] Container: DatasetComponent', () => {
    @Component({ selector: 'app-dataset-tabs', template: '' })
    class DatasetTabsStubComponent {
        @Input() surveyList: Survey[];
        @Input() datasetList: Dataset[];
        @Input() datasetFamilyList: Family[];
        @Input() datasetSelected: string;
    }

    @Component({ selector: 'app-summary', template: '' })
    class SummaryStubComponent {
        @Input() currentStep: string;
        @Input() datasetName: string;
        @Input() datasetList: Dataset[];
        @Input() isConeSearchAdded: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() criteriaFamilyList: Family[];
        @Input() criteriaList: Criterion[];
        @Input() datasetAttributeList: Attribute[];
        @Input() outputFamilyList: Family[];
        @Input() categoryList: Category[];
        @Input() outputList: number[];
        @Input() outputListEmpty: boolean;
        @Input() queryParams: SearchQueryParams;
    }

    let scrollTopServiceStub: Partial<ScrollTopService> = {
        setScrollTop() {}
    };

    let component: DatasetComponent;
    let fixture: ComponentFixture<DatasetComponent>;
    let store: MockStore;
    const initialState = {
        search: { ...fromSearch.initialState },
        metamodel: { ...fromMetamodel },
        coneSearch: { ...fromConeSearch }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetComponent,
                DatasetTabsStubComponent,
                SummaryStubComponent,
                RouterLinkDirectiveStub
            ],
            providers: [
                provideMockStore({ initialState }),
                { provide: ScrollTopService, useValue: scrollTopServiceStub }
            ]
        });
        fixture = TestBed.createComponent(DatasetComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        const loadDatasetSearchMetaAction = new datasetActions.LoadDatasetSearchMetaAction();
        const changeStepAction = new searchActions.ChangeStepAction('dataset');
        const spy = spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(2);
            expect(spy).toHaveBeenCalledWith(loadDatasetSearchMetaAction);
            expect(spy).toHaveBeenCalledWith(changeStepAction);
            done();
        });
    });

    it('#selectDataset() should dispatch loadAttributeListAction, LoadCriteriaSearchMetaAction, ' +
        'LoadOutputSearchMetaAction and NewSearchAction', (done) => {
        const newSearchAction = new searchActions.NewSearchAction('toto');
        const loadAttributeListAction = new attActions.LoadAttributeListAction('toto');
        const loadCriteriaSearchMetaAction = new criteriaActions.LoadCriteriaSearchMetaAction('toto');
        const loadOutputSearchMetaAction = new outputActions.LoadOutputSearchMetaAction('toto');
        const spy = spyOn(store, 'dispatch');
        component.selectDataset('toto');
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(4);
            expect(spy).toHaveBeenCalledWith(loadAttributeListAction);
            expect(spy).toHaveBeenCalledWith(loadCriteriaSearchMetaAction);
            expect(spy).toHaveBeenCalledWith(loadOutputSearchMetaAction);
            expect(spy).toHaveBeenCalledWith(newSearchAction);
            done();
        });
    });
});
