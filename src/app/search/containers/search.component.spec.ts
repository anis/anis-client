import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { SearchComponent } from './search.component';
import * as fromSearch from '../store/search.reducer';
import { SearchQueryParams } from '../store/model';

describe('[Search] Container: SearchComponent', () => {
    @Component({ selector: 'app-progress-bar', template: '' })
    class ProgressBarStubComponent {
        @Input() currentStep: string;
        @Input() datasetName: string;
        @Input() criteriaStepChecked: boolean;
        @Input() outputStepChecked: boolean;
        @Input() resultStepChecked: boolean;
        @Input() queryParams: SearchQueryParams;
        @Input() outputListEmpty: boolean;
    }

    @Component({ selector: 'router-outlet', template: '' })
    class RouterOutletStubComponent { }

    let component: SearchComponent;
    let fixture: ComponentFixture<SearchComponent>;
    let store: MockStore;
    const initialState = { documentation: { ...fromSearch.initialState } };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                SearchComponent,
                ProgressBarStubComponent,
                RouterOutletStubComponent
            ],
            providers: [provideMockStore({ initialState })]
        });
        fixture = TestBed.createComponent(SearchComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});

