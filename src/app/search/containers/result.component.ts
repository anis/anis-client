/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as sampActions from '../../samp/store/samp.action';
import * as sampSelector from '../../samp/store/samp.selector';
import * as fromSearch from '../store/search.reducer';
import * as searchActions from '../store/search.action';
import * as searchSelector from '../store/search.selector';
import { Criterion, SearchQueryParams } from '../store/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as metamodelSelector from '../../metamodel/selectors';
import { Dataset, Attribute, Family, Category } from '../../metamodel/model';
import * as coneSearchSelector from '../../shared/cone-search/store/cone-search.selector';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { Pagination } from '../../shared/datatable/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    search: fromSearch.State;
    metamodel: fromMetamodel.State;
}

@Component({
    selector: 'app-result',
    templateUrl: 'result.component.html'
})
/**
 * @class
 * @classdesc Search result container.
 *
 * @implements OnInit
 * @implements OnDestroy
 */
export class ResultComponent implements OnInit, OnDestroy {
    public datasetSearchMetaIsLoading: Observable<boolean>;
    public datasetSearchMetaIsLoaded: Observable<boolean>;
    public datasetName: Observable<string>;
    public currentStep: Observable<string>;
    public datasetList: Observable<Dataset[]>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public attributeList: Observable<Attribute[]>;
    public criteriaFamilyList: Observable<Family[]>;
    public criteriaList: Observable<Criterion[]>;
    public isConeSearchAdded: Observable<boolean>;
    public coneSearch: Observable<ConeSearch>;
    public outputFamilyList: Observable<Family[]>;
    public categoryList: Observable<Category[]>;
    public outputList: Observable<number[]>;
    public searchData: Observable<any[]>;
    public dataLengthIsLoaded: Observable<boolean>;
    public dataLength: Observable<number>;
    public selectedData: Observable<number[] | string[]>;
    public queryParams: Observable<SearchQueryParams>;
    public processWip: Observable<boolean>;
    public processDone: Observable<boolean>;
    public processId: Observable<string>;
    public sampRegistered: Observable<boolean>;

    constructor(private store: Store<StoreState>, private scrollTopService: ScrollTopService) {
        this.datasetSearchMetaIsLoading = store.select(metamodelSelector.getDatasetSearchMetaIsLoading);
        this.datasetSearchMetaIsLoaded = store.select(metamodelSelector.getDatasetSearchMetaIsLoaded);
        this.datasetName = store.select(searchSelector.getDatasetName);
        this.currentStep = store.select(searchSelector.getCurrentStep);
        this.datasetList = store.select(metamodelSelector.getDatasetList);
        this.criteriaFamilyList = this.store.select(metamodelSelector.getCriteriaFamilyList);
        this.criteriaList = this.store.select(searchSelector.getCriteriaList);
        this.isConeSearchAdded = this.store.select(searchSelector.getIsConeSearchAdded);
        this.coneSearch = this.store.select(coneSearchSelector.getConeSearch);
        this.outputFamilyList = this.store.select(metamodelSelector.getOutputFamilyList);
        this.categoryList = this.store.select(metamodelSelector.getCategoryList);
        this.outputList = this.store.select(searchSelector.getOutputList);
        this.searchData = this.store.select(searchSelector.getSearchData);
        this.dataLengthIsLoaded = this.store.select(searchSelector.getDataLengthIsLoaded);
        this.dataLength = this.store.select(searchSelector.getDataLength);
        this.selectedData = this.store.select(searchSelector.getSelectedData);
        this.queryParams = this.store.select(searchSelector.getQueryParams);
        this.processWip = this.store.select(searchSelector.getProcessWip);
        this.processDone = this.store.select(searchSelector.getProcessDone);
        this.processId = this.store.select(searchSelector.getProcessId);
        this.sampRegistered = this.store.select(sampSelector.getRegistered);
    }

    ngOnInit() {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.ChangeStepAction('result')));
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.ResultChecked()));
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.InitSearchByUrl()));
        this.store.dispatch(new datasetActions.LoadDatasetSearchMetaAction());
        this.scrollTopService.setScrollTop();
        this.datasetName.subscribe(datasetName => {
            if (datasetName) {
                this.attributeListIsLoading = this.store.select(metamodelSelector.getAttributeListIsLoading, { dname: datasetName });
                this.attributeListIsLoaded = this.store.select(metamodelSelector.getAttributeListIsLoaded, { dname: datasetName });
                this.attributeList = this.store.select(metamodelSelector.getAttributeList, { dname: datasetName });
            }
        });
    }

    sampRegister() {
        this.store.dispatch(new sampActions.RegisterAction());
    }

    sampUnregister() {
        this.store.dispatch(new sampActions.UnregisterAction());
    }

    broadcastVotable(url: string) {
        this.store.dispatch(new sampActions.BroadcastVotableAction(url));
    }

    /**
     * Dispatches action to retrieve result number.
     */
    getDataLength(): void {
        this.store.dispatch(new searchActions.GetDataLengthAction());
    }

    /**
     * Dispatches action to retrieve data with the given pagination.
     *
     * @param  {Pagination} params - The pagination parameters.
     */
    getSearchData(params: Pagination): void {
        this.store.dispatch(new searchActions.RetrieveDataAction(params));
    }

    /**
     * Dispatches action to add the given data ID to the selected data.
     *
     * @param  {number | string} data - The data ID to add to the data selection.
     */
    addSearchData(data: number | string): void {
        this.store.dispatch(new searchActions.AddSelectedDataAction(data));
    }

    /**
     * Dispatches action to remove the given data ID to the selected data.
     *
     * @param  {number | string} data - The data ID to remove to the data selection.
     */
    deleteSearchData(data: number | string): void {
        this.store.dispatch(new searchActions.DeleteSelectedDataAction(data));
    }

    /**
     * Dispatches action to execute the given process.
     *
     * @param  {string} typeProcess - The data ID to add to the data selection.
     */
    executeProcess(typeProcess: string): void {
        this.store.dispatch(new searchActions.ExecuteProcessAction(typeProcess));
    }

    /**
     * Dispatches action to destroy search results.
     */
    ngOnDestroy() {
        this.store.dispatch(new searchActions.DestroyResultsAction());
    }
}
