import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { OutputComponent } from './output.component';
import * as fromSearch from '../store/search.reducer';
import * as searchActions from '../store/search.action';
import { Criterion, SearchQueryParams } from '../store/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import { Dataset, Attribute, Family, Category } from '../../metamodel/model';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';
import { RouterLinkDirectiveStub } from '../../../settings/test-data/router-link-directive-stub';

describe('[Search] Container: OutputComponent', () => {
    @Component({ selector: 'app-output-tabs', template: '' })
    class OutputTabsStubComponent {
        @Input() outputFamilyList: Family[];
        @Input() categoryList: Category[];
        @Input() datasetAttributeList: Attribute[];
        @Input() outputList: number[];
    }

    @Component({ selector: 'app-summary', template: '' })
    class SummaryStubComponent {
        @Input() currentStep: string;
        @Input() datasetName: string;
        @Input() datasetList: Dataset[];
        @Input() isConeSearchAdded: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() criteriaFamilyList: Family[];
        @Input() criteriaList: Criterion[];
        @Input() datasetAttributeList: Attribute[];
        @Input() outputFamilyList: Family[];
        @Input() categoryList: Category[];
        @Input() outputList: number[];
        @Input() outputListEmpty: boolean;
        @Input() queryParams: SearchQueryParams;
    }

    let scrollTopServiceStub: Partial<ScrollTopService> = {
        setScrollTop() {}
    };

    let component: OutputComponent;
    let fixture: ComponentFixture<OutputComponent>;
    let store: MockStore;
    const initialState = {
        search: { ...fromSearch.initialState },
        metamodel: { ...fromMetamodel },
        coneSearch: { ...fromConeSearch }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputComponent,
                OutputTabsStubComponent,
                SummaryStubComponent,
                RouterLinkDirectiveStub
            ],
            providers: [
                provideMockStore({ initialState }),
                { provide: ScrollTopService, useValue: scrollTopServiceStub }
            ]
        });
        fixture = TestBed.createComponent(OutputComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        component.datasetName = of('toto');
        const loadDatasetSearchMetaAction = new datasetActions.LoadDatasetSearchMetaAction();
        const changeStepAction = new searchActions.ChangeStepAction('output');
        const outputChecked = new searchActions.OutputChecked();
        const initSearchByUrl = new searchActions.InitSearchByUrl();
        const spy = spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(4);
            expect(spy).toHaveBeenCalledWith(loadDatasetSearchMetaAction);
            expect(spy).toHaveBeenCalledWith(changeStepAction);
            expect(spy).toHaveBeenCalledWith(outputChecked);
            expect(spy).toHaveBeenCalledWith(initSearchByUrl);
            done();
        });
    });

    it('#updateOutputList() should dispatch UpdateOutputListAction', () => {
        const updateOutputListAction = new searchActions.UpdateOutputListAction([1, 2, 3]);
        const spy = spyOn(store, 'dispatch');
        component.updateOutputList([1, 2, 3]);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(updateOutputListAction);
    });
});
