/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromSearch from '../store/search.reducer';
import * as searchActions from '../store/search.action';
import * as searchSelector from '../store/search.selector';
import { Criterion, SearchQueryParams } from '../store/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as metamodelSelector from '../../metamodel/selectors';
import { Dataset, Family, Category, Attribute } from '../../metamodel/model';
import * as coneSearchSelector from '../../shared/cone-search/store/cone-search.selector';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    search: fromSearch.State;
    metamodel: fromMetamodel.State;
}

@Component({
    selector: 'app-output',
    templateUrl: 'output.component.html',
    styleUrls: ['output.component.css']
})
/**
 * @class
 * @classdesc Search output container.
 *
 * @implements OnInit
 */
export class OutputComponent implements OnInit {
    public outputSearchMetaIsLoading: Observable<boolean>;
    public outputSearchMetaIsLoaded: Observable<boolean>;
    public datasetSearchMetaIsLoading: Observable<boolean>;
    public datasetSearchMetaIsLoaded: Observable<boolean>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public datasetName: Observable<string>;
    public currentStep: Observable<string>;
    public datasetList: Observable<Dataset[]>;
    public attributeList: Observable<Attribute[]>;
    public criteriaFamilyList: Observable<Family[]>;
    public criteriaList: Observable<Criterion[]>;
    public isConeSearchAdded: Observable<boolean>;
    public coneSearch: Observable<ConeSearch>;
    public outputFamilyList: Observable<Family[]>;
    public categoryList: Observable<Category[]>;
    public outputListEmpty: Observable<boolean>;
    public outputList: Observable<number[]>;
    public queryParams: Observable<SearchQueryParams>;

    constructor(private store: Store<StoreState>, private scrollTopService: ScrollTopService) {
        this.outputSearchMetaIsLoading = store.select(metamodelSelector.getOutputSearchMetaIsLoading);
        this.outputSearchMetaIsLoaded = store.select(metamodelSelector.getOutputSearchMetaIsLoaded);
        this.datasetSearchMetaIsLoading = store.select(metamodelSelector.getDatasetSearchMetaIsLoading);
        this.datasetSearchMetaIsLoaded = store.select(metamodelSelector.getDatasetSearchMetaIsLoaded);
        this.datasetName = store.select(searchSelector.getDatasetName);
        this.currentStep = store.select(searchSelector.getCurrentStep);
        this.datasetList = store.select(metamodelSelector.getDatasetList);
        this.criteriaFamilyList = this.store.select(metamodelSelector.getCriteriaFamilyList);
        this.criteriaList = this.store.select(searchSelector.getCriteriaList);
        this.isConeSearchAdded = this.store.select(searchSelector.getIsConeSearchAdded);
        this.coneSearch = this.store.select(coneSearchSelector.getConeSearch);
        this.outputFamilyList = this.store.select(metamodelSelector.getOutputFamilyList);
        this.categoryList = this.store.select(metamodelSelector.getCategoryList);
        this.outputListEmpty = this.store.select(searchSelector.getOutputListEmpty);
        this.outputList = this.store.select(searchSelector.getOutputList);
        this.queryParams = this.store.select(searchSelector.getQueryParams);
    }

    ngOnInit() {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.ChangeStepAction('output')));
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.OutputChecked()));
        Promise.resolve(null).then(() => this.store.dispatch(new searchActions.InitSearchByUrl()));
        this.store.dispatch(new datasetActions.LoadDatasetSearchMetaAction());
        this.scrollTopService.setScrollTop();
        this.datasetName.subscribe(datasetName => {
            if (datasetName) {
                this.attributeListIsLoading = this.store.select(metamodelSelector.getAttributeListIsLoading, { dname: datasetName });
                this.attributeListIsLoaded = this.store.select(metamodelSelector.getAttributeListIsLoaded, { dname: datasetName });
                this.attributeList = this.store.select(metamodelSelector.getAttributeList, { dname: datasetName });
            }
        });
    }

    /**
     * Dispatches action to update output list selection with the given updated output list.
     *
     * @param  {number[]} outputList - The updated output list.
     */
    updateOutputList(outputList: number[]): void {
        this.store.dispatch(new searchActions.UpdateOutputListAction(outputList));
    }
}
