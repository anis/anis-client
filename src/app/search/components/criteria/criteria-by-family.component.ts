/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Criterion } from '../../store/model';
import { Attribute, Option } from '../../../metamodel/model';
import { sortByDisplay } from '../../../shared/utils';

@Component({
    selector: 'app-criteria-by-family',
    templateUrl: 'criteria-by-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search criteria by family component.
 */
export class CriteriaByFamilyComponent {
    @Input() attributeList: Attribute[];
    @Input() criteriaList: Criterion[];
    @Output() addCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    advancedForm = false;

    /**
     * Returns attribute list sorted by criteria display.
     *
     * @return Attribute[]
     */
    getAttributeListSortedByDisplay(): Attribute[] {
        return this.attributeList
            .sort((a, b) => a.criteria_display - b.criteria_display);
    }

    /**
     * Returns options for the given attribute ID.
     *
     * @param  {number} idAttribute - The attribute ID.
     *
     * @return Option[]
     */
    getOptions(idAttribute: number): Option[] {
        return [...this.attributeList.find(attribute => attribute.id === idAttribute).options]
            .sort(sortByDisplay);
    }

    /**
     * Returns criterion for the given criterion ID.
     *
     * @param  {number} id - The criterion ID.
     *
     * @return Criterion
     */
    getCriterion(id: number): Criterion {
        return this.criteriaList.find(criterion => criterion.id === id);
    }

    /**
     * Emits event to add the given criterion to the criteria list.
     *
     * @param  {Criterion} criterion - The criterion.
     *
     * @fires EventEmitter<Criterion>
     */
    emitAdd(criterion: Criterion): void {
        this.addCriterion.emit(criterion);
    }

    /**
     * Emits event to remove the given criterion ID from the criteria list.
     *
     * @param  {number} id - The criterion ID.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(id: number): void {
        this.deleteCriterion.emit(id);
    }
}
