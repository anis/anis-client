/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Dataset } from '../../../metamodel/model';

@Component({
    selector: 'app-cone-search-tab',
    templateUrl: 'cone-search-tab.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search cone search tab component.
 */
export class ConeSearchTabComponent {
    @Input() datasetName: string;
    @Input() datasetList: Dataset[];
    @Input() isConeSearchAdded: boolean;
    @Input() isValidConeSearch: boolean;
    @Output() coneSearchAdded: EventEmitter<boolean> = new EventEmitter();

    /**
     * Checks if cone search is enabled.
     *
     * @return boolean
     */
    coneSearchEnabled(): boolean {
        const config = this.datasetList.find(d => d.name === this.datasetName).config;
        return config.cone_search.enabled;
    }

    /**
     * Checks if accordion is opened.
     *
     * @return boolean
     */
     coneSearchOpened(): boolean {
        const config = this.datasetList.find(d => d.name === this.datasetName).config;
        return config.cone_search.opened;
    }
}
