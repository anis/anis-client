import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { CriteriaTabsComponent } from './criteria-tabs.component';
import { Criterion, FieldCriterion } from '../../store/model';
import { Attribute, Family } from '../../../metamodel/model';
import { CRITERIA_FAMILY_LIST, ATTRIBUTE_LIST, CRITERIA_LIST } from '../../../../settings/test-data';

describe('[Search][Criteria] Component: CriteriaTabsComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-criteria-tabs
                [criteriaFamilyList]="criteriaFamilyList"
                [attributeList]="attributeList"
                [criteriaList]="criteriaList">
            </app-criteria-tabs>
        `
    })
    class TestHostComponent {
        @ViewChild(CriteriaTabsComponent, { static: false })
        public testedComponent: CriteriaTabsComponent;
        public criteriaFamilyList: Family[] = CRITERIA_FAMILY_LIST;
        public attributeList: Attribute[] = ATTRIBUTE_LIST;
        public criteriaList: Criterion[] = CRITERIA_LIST;
    }

    @Component({ selector: 'app-criteria-by-family', template: '' })
    class CriteriaByFamilyStubComponent {
        @Input() attributeList: Attribute[];
        @Input() criteriaList: Criterion[];
        @Output() addCriterion: EventEmitter<Criterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: CriteriaTabsComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                CriteriaTabsComponent,
                TestHostComponent,
                CriteriaByFamilyStubComponent
            ],
            imports: [AccordionModule.forRoot(), BrowserAnimationsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getAttributeByFamily(idFamily) should filter attibutes by idFamily', () => {
        const filteredAttributeList: Attribute[] = testedComponent.getAttributeByFamily(1);
        expect(filteredAttributeList.length).toBe(1);
        expect(filteredAttributeList[0].id).toBe(1);
    });

    it('raises the add criterion event when clicked', () => {
        const criterion = { id: 1, type: 'field', operator: 'eq', value: 'test' } as FieldCriterion;
        testedComponent.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(criterion));
        testedComponent.emitAdd(criterion);
    });

    it('raises the delete criterion event when clicked', () => {
        const criterionId = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete(criterionId);
    });
});

