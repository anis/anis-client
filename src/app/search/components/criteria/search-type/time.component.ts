/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Criterion, FieldCriterion } from '../../../store/model';

@Component({
    selector: 'app-time',
    templateUrl: 'time.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Time search type component.
 */
export class TimeComponent {
    @Input() id: number;
    @Input() operator: string;
    @Input() label: string;
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Input() advancedForm: boolean;
    @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    hours: string[] = this.initTime(24);
    minutes: string[] = this.initTime(60);
    hh = new FormControl();
    mm = new FormControl();
    disabledOperator: boolean;

    /**
     * Modifies operator with the given one.
     *
     * @param  {operator} operator - The operator.
     */
    changeOperator(operator: string): void {
        this.operator = operator;
    }

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<FieldCriterion>
     */
    emitAdd(): void {
        const time = {id: this.id, type: 'field', operator: this.operator, value: this.hh.value + ':' + this.mm.value};
        this.addCriterion.emit(time);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.hh.reset();
            this.hh.enable();
            this.mm.reset();
            this.mm.enable();
            this.disabledOperator = false;
        } else {
            const c = criterion as FieldCriterion;
            this.hh.setValue(c.value.slice(0, 2));
            this.hh.disable();
            this.mm.setValue(c.value.slice(3, 5));
            this.mm.disable();
            this.disabledOperator = true;
        }
    }

    /**
     * Returns string array to represent the given time.
     *
     * @param  {number} time - The number max to represent time.
     *
     * @return string[]
     */
    initTime(time: number): string[] {
        const array: string[] = [];
        for (let i = 0; i < time; i++) {
            const t = ('0' + i).slice(-2);
            array.push(t);
        }
        return array;
    }
}
