/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-operator',
    templateUrl: 'operator.component.html',
    styleUrls: ['operator.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Operator component.
 */
export class OperatorComponent {
    @Input() operator: string;
    @Input() searchType: string;
    @Input() advancedForm: boolean;
    @Input() disabled: boolean;
    @Output() changeOperator: EventEmitter<string> = new EventEmitter();

    operators = [
        { value: 'eq', label: '=' },
        { value: 'neq', label: '≠' },
        { value: 'gt', label: '>' },
        { value: 'gte', label: '>=' },
        { value: 'lt', label: '<' },
        { value: 'lte', label: '<=' },
        { value: 'lk', label: 'like' },
        { value: 'nlk', label: 'not like' },
        { value: 'in', label: 'in' },
        { value: 'nin', label: 'not in' }
    ];

    /**
     * Emits event to change operator with the given one.
     *
     * @fires EventEmitter<string>
     */
    emitChange(operator: string): void {
        this.changeOperator.emit(operator);
    }

    /**
     * Return form label for the given operator.
     *
     * @param  {operator} operator - The operator.
     *
     * @return string
     */
    getLabel(operator: string): string {
        return this.operators.find(o => o.value === operator).label;
    }
}
