/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Criterion, FieldCriterion } from '../../../store/model';

@Component({
    selector: 'app-datetime',
    templateUrl: 'datetime.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Datetime search type component.
 */
export class DatetimeComponent {
    @Input() id: number;
    @Input() operator: string;
    @Input() label: string;
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Input() advancedForm: boolean;
    @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    hours: string[] = this.initTime(24);
    minutes: string[] = this.initTime(60);
    date = new FormControl('');
    hh = new FormControl();
    mm = new FormControl();
    isValidFields = false;
    datetime: Date = new Date();

    disabledOperator: boolean;

    /**
     * Modifies operator with the given one.
     *
     * @param  {operator} operator - The operator.
     */
    changeOperator(operator: string): void {
        this.operator = operator;
    }

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<FieldCriterion>
     */
    emitAdd(): void {
        const month = ('0' + (this.datetime.getMonth() + 1)).slice(-2);
        const day = ('0' + (this.datetime.getDate())).slice(-2);
        const date = this.datetime.getFullYear() + '-' + month + '-' + day;
        const time = this.hh.value + ':' + this.mm.value;
        const fd = {id: this.id, type: 'field', operator: this.operator, value: date + ' ' + time};
        this.addCriterion.emit(fd);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.date.reset();
            this.date.enable();
            this.hh.reset();
            this.hh.enable();
            this.mm.reset();
            this.mm.enable();
            this.isValidFields = false;
            this.disabledOperator = false;
        } else {
            const c = criterion as FieldCriterion;
            const [d, t] = c.value.split(' ');
            const [h, m] = t.split(':');
            this.date.setValue(new Date(d));
            this.date.disable();
            this.hh.setValue(h);
            this.hh.disable();
            this.mm.setValue(m);
            this.mm.disable();
            this.isValidFields = true;
            this.disabledOperator = true;
        }
    }

    /**
     * Returns string array to represent the given time.
     *
     * @param  {number} time - The number max to represent time.
     *
     * @return string[]
     */
    initTime(time: number): string[] {
        const array: string[] = [];
        for (let i = 0; i < time; i++) {
            const t = ('0' + i).slice(-2);
            array.push(t);
        }
        return array;
    }

    /**
     * Reconstructs datetime from form inputs.
     */
    change(): void {
        if (!this.date.value || !this.hh.value || !this.mm.value) {
            this.isValidFields = false;
        } else {
            this.isValidFields = true;
            this.datetime.setFullYear(
                this.date.value.getFullYear(),
                this.date.value.getMonth(),
                this.date.value.getDate());
            this.datetime.setHours(
                this.hh.value,
                this.mm.value);
            this.datetime.setSeconds(0, 0);
        }
    }
}
