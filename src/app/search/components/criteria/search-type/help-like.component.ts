/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';

@Component({
    selector: 'app-help-like',
    templateUrl: 'help-like.component.html'
})
/**
 * @class
 * @classdesc Help like operator component.
 */
export class HelpLikeComponent { }
