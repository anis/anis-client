import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { ListComponent } from './list.component';
import { ListCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: ListComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-list [criterion]='criterion'></app-list>`
    })
    class TestHostComponent {
        @ViewChild(ListComponent, { static: false })
        public testedComponent: ListComponent;
        public criterion: ListComponent = undefined;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: ListComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                ListComponent,
                TestHostComponent
            ],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.list.value).toBeNull();
        expect(testedComponent.list.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const values = ['1', '2'];
        const criterion = { id: 1, type: 'list', values } as ListCriterion;
        const expectedListValues = values.join('\n');
        testedComponent.getDefault(criterion);
        expect(testedComponent.list.value).toBe(expectedListValues);
        expect(testedComponent.list.disabled).toBeTruthy();
    });

    it('#getPlaceholder() should fill the placeholder if defined', () => {
        const placeholder = 'placeholder';
        testedComponent.placeholder = placeholder;
        expect(testedComponent.getPlaceholder()).toBe(placeholder);
    });

    it('#getPlaceholder() should not fill the placeholder if not defined', () => {
        expect(testedComponent.getPlaceholder()).toBe('');
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const values = '1\n2';
        testedComponent.list = new FormControl(values);
        const expectedCriterion = { id: testedComponent.id, type: 'list', values: ['1', '2'] } as ListCriterion;
        testedComponent.addCriterion.subscribe((event: ListCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });
});
