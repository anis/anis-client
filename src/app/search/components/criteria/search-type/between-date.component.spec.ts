import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { BetweenDateComponent } from './between-date.component';
import { BetweenCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: BetweenDateComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-between-date [criterion]='criterion'></app-between-date>`
    })
    class TestHostComponent {
        @ViewChild(BetweenDateComponent, { static: false })
        public testedComponent: BetweenDateComponent;
        public criterion: BetweenCriterion = undefined;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: BetweenDateComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [BetweenDateComponent, TestHostComponent],
            imports: [BsDatepickerModule.forRoot(), FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDateString() should format a datetime object into a string', () => {
        const dateObject = new Date('February 08, 2019 15:47:00');
        const expectedDateString = '2019-02-08';
        const formatedDate = testedComponent.getDateString(dateObject);
        expect(formatedDate).toEqual(expectedDateString);
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.field.value).toBeNull();
        expect(testedComponent.field.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const criterion = { id: 1, type: 'between', min: '2019-02-08', max: '2019-02-17' } as BetweenCriterion;
        const expectedFieldValue = [new Date(criterion.min), new Date(criterion.max)];
        testedComponent.getDefault(criterion);
        expect(testedComponent.field.value).toEqual(expectedFieldValue);
        expect(testedComponent.field.disabled).toBeTruthy();
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const dateMin = '2019-02-08';
        const dateMax = '2019-02-17';
        testedComponent.field = new FormControl([new Date(dateMin), new Date(dateMax)]);
        const expectedCriterion = { id: testedComponent.id, type: 'between', min: dateMin, max: dateMax } as BetweenCriterion;
        testedComponent.addCriterion.subscribe((event: BetweenCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
