import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgSelectModule } from '@ng-select/ng-select';

import { DatetimeComponent } from './datetime.component';
import { FieldCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: DatetimeComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-datetime [criterion]='criterion'></app-datetime>`
    })
    class TestHostComponent {
        @ViewChild(DatetimeComponent, { static: false })
        public testedComponent: DatetimeComponent;
        public criterion: FieldCriterion = undefined;
    }

    @Component({ selector: 'app-operator', template: '' })
    class OperatorStubComponent {
        @Input() operator: string;
        @Input() searchType: string;
        @Input() advancedForm: boolean;
        @Input() disabled: boolean;
        @Output() changeOperator: EventEmitter<string> = new EventEmitter();
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: DatetimeComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatetimeComponent,
                TestHostComponent,
                OperatorStubComponent
            ],
            imports: [BsDatepickerModule.forRoot(), NgSelectModule, FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.date.value).toBeNull();
        expect(testedComponent.date.enabled).toBeTruthy();
        expect(testedComponent.hh.value).toBeNull();
        expect(testedComponent.hh.enabled).toBeTruthy();
        expect(testedComponent.mm.value).toBeNull();
        expect(testedComponent.mm.enabled).toBeTruthy();
        expect(testedComponent.isValidFields).toBeFalsy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const value = '2019-02-17 15:47';
        const criterion = { id: 1, type: 'field', operator: 'eq', value } as FieldCriterion;
        const expectedDate = new Date('2019-02-17');
        const expectedHour = '15';
        const expectedMinute = '47';
        testedComponent.getDefault(criterion);
        expect(testedComponent.date.value).toEqual(expectedDate);
        expect(testedComponent.date.disabled).toBeTruthy();
        expect(testedComponent.hh.value).toEqual(expectedHour);
        expect(testedComponent.hh.disabled).toBeTruthy();
        expect(testedComponent.mm.value).toEqual(expectedMinute);
        expect(testedComponent.mm.disabled).toBeTruthy();
        expect(testedComponent.isValidFields).toBeTruthy();
    });

    it('#initTime(t) should return an array of string with 2 digits from 0 to t', () => {
        const n = 10;
        expect(testedComponent.initTime(n).length).toEqual(n);
        expect(testedComponent.initTime(n)[5]).toEqual('05');
    });

    it('#change() should set #isValidFields to false if one or more fields are not defined', () => {
        testedComponent.change();
        expect(testedComponent.isValidFields).toBeFalsy();
        testedComponent.hh = new FormControl('15');
        testedComponent.change();
        expect(testedComponent.isValidFields).toBeFalsy();
        testedComponent.mm = new FormControl('47');
        testedComponent.change();
        expect(testedComponent.isValidFields).toBeFalsy();
        testedComponent.date = new FormControl(new Date('2019-02-17'));
        testedComponent.change();
        expect(testedComponent.isValidFields).toBeTruthy();
    });

    it('#change() should set #datetime with fields value when defined', () => {
        testedComponent.date = new FormControl(new Date('2019-02-17'));
        testedComponent.hh = new FormControl('15');
        testedComponent.mm = new FormControl('47');
        testedComponent.change();
        expect(testedComponent.datetime).toEqual(new Date('2019-02-17 15:47'));
    });

    it('#changeOperator() should change the operator', () => {
        expect(testedComponent.operator).toBeUndefined();
        testedComponent.changeOperator('toto');
        expect(testedComponent.operator).toBe('toto');
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const operator = '=';
        testedComponent.operator = operator;
        testedComponent.datetime = new Date('2019-02-17 15:47');
        testedComponent.hh = new FormControl('15');
        testedComponent.mm = new FormControl('47');
        const expectedCriterion = { id: testedComponent.id, type: 'field', operator, value: '2019-02-17 15:47' } as FieldCriterion;
        testedComponent.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
