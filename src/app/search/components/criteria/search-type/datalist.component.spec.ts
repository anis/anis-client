import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { DatalistComponent } from './datalist.component';
import { FieldCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: DatalistComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-datalist [criterion]='criterion'></app-datalist>`
    })
    class TestHostComponent {
        @ViewChild(DatalistComponent, { static: false })
        public testedComponent: DatalistComponent;
        public criterion: FieldCriterion = undefined;
    }

    @Component({ selector: 'app-operator', template: '' })
    class OperatorStubComponent {
        @Input() operator: string;
        @Input() searchType: string;
        @Input() advancedForm: boolean;
        @Input() disabled: boolean;
        @Output() changeOperator: EventEmitter<string> = new EventEmitter();
    }

    @Component({ selector: 'app-help-like', template: '' })
    class HelpLikeStubComponent { }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: DatalistComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatalistComponent,
                TestHostComponent,
                OperatorStubComponent,
                HelpLikeStubComponent
            ],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.field.value).toBeNull();
        expect(testedComponent.field.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const value = '10';
        const criterion = { id: 1, type: 'field', operator: 'eq', value } as FieldCriterion;
        const expectedFieldValue = value;
        testedComponent.getDefault(criterion);
        expect(testedComponent.field.value).toEqual(expectedFieldValue);
        expect(testedComponent.field.disabled).toBeTruthy();
    });

    it('#getPlaceholder() should fill the placeholder if defined', () => {
        const placeholder = '10';
        testedComponent.placeholder = placeholder;
        expect(testedComponent.getPlaceholder()).toEqual(placeholder);
    });

    it('#getPlaceholder() should not fill the placeholder if not defined', () => {
        expect(testedComponent.getPlaceholder()).toEqual('');
    });

    it('#getDatalistId() should return an id', () => {
        testedComponent.id = 1;
        expect(testedComponent.getDatalistId()).toEqual('datalist_' + 1);
    });

    it('#changeOperator() should change the operator', () => {
        expect(testedComponent.operator).toBeUndefined();
        testedComponent.changeOperator('toto');
        expect(testedComponent.operator).toBe('toto');
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const operator = '=';
        const value = '10';
        testedComponent.field = new FormControl(value);
        testedComponent.operator = operator;
        const expectedCriterion = { id: testedComponent.id, type: 'field', operator, value } as FieldCriterion;
        testedComponent.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
