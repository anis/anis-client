/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Criterion, SelectMultipleCriterion } from '../../../store/model';
import { Option } from '../../../../metamodel/model';

@Component({
    selector: 'app-select-multiple',
    templateUrl: 'select-multiple.component.html',
    styleUrls: ['operator.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Select multiple search type component.
 */
export class SelectMultipleComponent {
    @Input() id: number;
    @Input() label: string;
    @Input() options: Option[];
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Output() addCriterion: EventEmitter<SelectMultipleCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    ms = new FormControl();

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<SelectMultipleCriterion>
     */
    emitAdd(): void {
        const options = this.ms.value.map(optionValue => this.options.find(o => o.value === optionValue));
        const ms = {id: this.id, type: 'multiple', options};
        this.addCriterion.emit(ms);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.ms.reset();
            this.ms.enable();
        } else {
            const c = criterion as SelectMultipleCriterion;
            this.ms.setValue(c.options.map(o => o.value));
            this.ms.disable();
        }
    }
}
