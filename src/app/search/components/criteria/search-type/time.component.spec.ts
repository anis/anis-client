import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { NgSelectModule } from '@ng-select/ng-select';

import { TimeComponent } from './time.component';
import { FieldCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: TimeComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-time [criterion]='criterion'></app-time>`
    })
    class TestHostComponent {
        @ViewChild(TimeComponent, { static: false })
        public testedComponent: TimeComponent;
        public criterion: FieldCriterion = undefined;
    }

    @Component({ selector: 'app-operator', template: '' })
    class OperatorStubComponent {
        @Input() operator: string;
        @Input() searchType: string;
        @Input() advancedForm: boolean;
        @Input() disabled: boolean;
        @Output() changeOperator: EventEmitter<string> = new EventEmitter();
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: TimeComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                TimeComponent,
                TestHostComponent,
                OperatorStubComponent
            ],
            imports: [NgSelectModule, FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.hh.value).toBeNull();
        expect(testedComponent.hh.enabled).toBeTruthy();
        expect(testedComponent.mm.value).toBeNull();
        expect(testedComponent.mm.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const criterion = { id: 1, type: 'field', operator: 'eq', value: '15:47' } as FieldCriterion;
        const expectedHour = '15';
        const expectedMinute = '47';
        testedComponent.getDefault(criterion);
        expect(testedComponent.hh.value).toEqual(expectedHour);
        expect(testedComponent.hh.disabled).toBeTruthy();
        expect(testedComponent.mm.value).toEqual(expectedMinute);
        expect(testedComponent.mm.disabled).toBeTruthy();
    });

    it('#initTime(t) should return an array of string with 2 digits from 0 to t', () => {
        const n = 10;
        expect(testedComponent.initTime(n).length).toEqual(n);
        expect(testedComponent.initTime(n)[5]).toEqual('05');
    });

    it('#changeOperator() should change the operator', () => {
        expect(testedComponent.operator).toBeUndefined();
        testedComponent.changeOperator('toto');
        expect(testedComponent.operator).toBe('toto');
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const operator = 'eq';
        testedComponent.operator = operator;
        testedComponent.hh = new FormControl('15');
        testedComponent.mm = new FormControl('47');
        const expectedCriterion = { id: testedComponent.id, type: 'field', operator, value: '15:47' } as FieldCriterion;
        testedComponent.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
