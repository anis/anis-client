/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { JsonCriterion, Criterion } from '../../../store/model';

@Component({
    selector: 'app-json-criteria',
    templateUrl: 'json.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc JSON search type component.
 */
export class JsonComponent {
    @Input() id: number;
    @Input() label: string;
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Output() addCriterion: EventEmitter<JsonCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    jsonForm = new FormGroup({
        path: new FormControl(),
        operator: new FormControl(),
        value: new FormControl()
    });

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<JsonCriterion>
     */
    emitAdd(): void {
        const js = {id: this.id, type: 'json', path: this.jsonForm.value.path, operator: this.jsonForm.value.operator, value: this.jsonForm.value.value};
        this.addCriterion.emit(js);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.jsonForm.reset();
            this.jsonForm.enable();
        } else {
            const c = criterion as JsonCriterion;
            this.jsonForm.controls.path.setValue(c.path);
            this.jsonForm.controls.operator.setValue(c.operator);
            this.jsonForm.controls.value.setValue(c.value);
            this.jsonForm.disable();
        }
    }
}
