import { FieldComponent } from './field.component';
import { BetweenComponent } from './between.component';
import { SelectComponent } from './select.component';
import { SelectMultipleComponent } from './select-multiple.component';
import { DatalistComponent } from './datalist.component';
import { ListComponent } from './list.component';
import { RadioComponent } from './radio.component';
import { CheckboxComponent } from './checkbox.component';
import { DateComponent } from './date.component';
import { BetweenDateComponent } from './between-date.component';
import { TimeComponent } from './time.component';
import { DatetimeComponent } from './datetime.component';
import { JsonComponent } from './json.component';
import { OperatorComponent } from './operator.component';
import { HelpLikeComponent } from './help-like.component';

export const SearchTypeComponents = [
    FieldComponent,
    BetweenComponent,
    SelectComponent,
    SelectMultipleComponent,
    DatalistComponent,
    ListComponent,
    RadioComponent,
    CheckboxComponent,
    DateComponent,
    BetweenDateComponent,
    TimeComponent,
    DatetimeComponent,
    JsonComponent,
    OperatorComponent,
    HelpLikeComponent
];
