/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Criterion, BetweenCriterion } from '../../../store/model';

@Component({
    selector: 'app-between-date',
    templateUrl: 'between-date.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Between date search type component.
 */
export class BetweenDateComponent {
    @Input() id: number;
    @Input() operator: string;
    @Input() label: string;
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Output() addCriterion: EventEmitter<BetweenCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    field = new FormControl('');

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<BetweenCriterion>
     */
    emitAdd(): void {
        const dateMin = this.getDateString(this.field.value[0]);
        const dateMax = this.getDateString(this.field.value[1]);
        const fd: BetweenCriterion = {id: this.id, type: 'between', min: dateMin, max: dateMax};
        this.addCriterion.emit(fd);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.field.reset();
            this.field.enable();
        } else {
            const c = criterion as BetweenCriterion;
            this.field.setValue([new Date(c.min), new Date(c.max)]);
            this.field.disable();
        }
    }

    /**
     * Stringifies the given date.
     *
     * @param  {Date} date - The date.
     *
     * @return string
     */
    getDateString(date: Date): string {
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = ('0' + (date.getDate())).slice(-2);
        return date.getFullYear() + '-' + month + '-' + day;
    }
}
