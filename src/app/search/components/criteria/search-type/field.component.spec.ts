import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { FieldComponent } from './field.component';
import { FieldCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: FieldComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-field [criterion]='criterion'></app-field>`
    })
    class TestHostComponent {
        @ViewChild(FieldComponent, { static: false })
        public testedComponent: FieldComponent;
        public criterion: FieldCriterion = undefined;
    }

    @Component({ selector: 'app-operator', template: '' })
    class OperatorStubComponent {
        @Input() operator: string;
        @Input() searchType: string;
        @Input() advancedForm: boolean;
        @Input() disabled: boolean;
        @Output() changeOperator: EventEmitter<string> = new EventEmitter();
    }

    @Component({ selector: 'app-help-like', template: '' })
    class HelpLikeStubComponent { }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: FieldComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                FieldComponent,
                TestHostComponent,
                OperatorStubComponent,
                HelpLikeStubComponent
            ],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.field.value).toBeNull();
        expect(testedComponent.field.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const value = 'test';
        const criterion = { id: 1, type: 'field', operator: 'eq', value } as FieldCriterion;
        const expectedFieldValue = value;
        testedComponent.getDefault(criterion);
        expect(testedComponent.field.value).toEqual(expectedFieldValue);
        expect(testedComponent.field.disabled).toBeTruthy();
    });

    it('#getType() should return `number` if criterion is a number type', () => {
        testedComponent.attributeType = 'smallint';
        expect(testedComponent.getType()).toEqual('number');
        testedComponent.attributeType = 'integer';
        expect(testedComponent.getType()).toEqual('number');
        testedComponent.attributeType = 'decimal';
        expect(testedComponent.getType()).toEqual('number');
        testedComponent.attributeType = 'float';
        expect(testedComponent.getType()).toEqual('number');
    });

    it('#getType() should return `text` if criterion is not a number type', () => {
        testedComponent.attributeType = 'char';
        expect(testedComponent.getType()).toEqual('text');
    });

    it('#getPlaceholder() should fill the placeholder if defined', () => {
        const placeholder = 'placeholder';
        testedComponent.placeholder = placeholder;
        expect(testedComponent.getPlaceholder()).toEqual(placeholder);
    });

    it('#getPlaceholder() should not fill the placeholder if not defined', () => {
        expect(testedComponent.getPlaceholder()).toEqual('');
    });

    it('#changeOperator() should change the operator', () => {
        expect(testedComponent.operator).toBeUndefined();
        testedComponent.changeOperator('toto');
        expect(testedComponent.operator).toBe('toto');
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const operator = '=';
        testedComponent.operator = operator;
        const value = 'test';
        testedComponent.field = new FormControl(value);
        const expectedCriterion = { id: testedComponent.id, type: 'field', operator, value } as FieldCriterion;
        testedComponent.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
