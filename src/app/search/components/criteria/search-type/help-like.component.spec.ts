import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpLikeComponent } from './help-like.component';

describe('[Search][Criteria][SearchType] Component: HelpLikeComponent', () => {
    let component: HelpLikeComponent;
    let fixture: ComponentFixture<HelpLikeComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [HelpLikeComponent]
        });
        fixture = TestBed.createComponent(HelpLikeComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
