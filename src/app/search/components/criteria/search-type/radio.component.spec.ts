import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { RadioComponent } from './radio.component';
import { FieldCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: RadioComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-radio [criterion]='criterion'></app-radio>`
    })
    class TestHostComponent {
        @ViewChild(RadioComponent, { static: false })
        public testedComponent: RadioComponent;
        public criterion: FieldCriterion = undefined;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: RadioComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [RadioComponent, TestHostComponent],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.radio.value).toBeNull();
        expect(testedComponent.radio.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const operator = '=';
        const value = 'test';
        const criterion = { id: 1, type: 'field', operator, value } as FieldCriterion;
        testedComponent.getDefault(criterion);
        expect(testedComponent.radio.value).toEqual(value);
        expect(testedComponent.radio.disabled).toBeTruthy();
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const operator = '=';
        testedComponent.operator = operator;
        const value = 'three';
        testedComponent.radio = new FormControl(value);
        testedComponent.options = [
            { label: 'One', value: 'one', display: 1 },
            { label: 'Two', value: 'two', display: 2 },
            { label: 'Three', value: 'three', display: 3 }
        ];
        const expectedCriterion = { id: testedComponent.id, type: 'field', operator, value } as FieldCriterion;
        testedComponent.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
