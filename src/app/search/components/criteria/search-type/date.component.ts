/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Criterion, FieldCriterion } from '../../../store/model';

@Component({
    selector: 'app-date',
    templateUrl: 'date.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Date search type component.
 */
export class DateComponent {
    @Input() id: number;
    @Input() operator: string;
    @Input() label: string;
    @Input() placeholder: string;
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Input() advancedForm: boolean;
    @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    field = new FormControl('');
    disabledOperator: boolean;

    /**
     * Modifies operator with the given one.
     *
     * @param  {operator} operator - The operator.
     */
    changeOperator(operator: string): void {
        this.operator = operator;
    }

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<FieldCriterion>
     */
    emitAdd(): void {
        const fd = {id: this.id, type: 'field', operator: this.operator, value: this.getDateString(this.field.value)};
        this.addCriterion.emit(fd);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete():void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.field.reset();
            this.field.enable();
            this.disabledOperator = false;
        } else {
            const c = criterion as FieldCriterion;
            this.field.setValue(new Date(c.value));
            this.field.disable();
            this.disabledOperator = true;
        }
    }

    /**
     * Returns placeholder.
     *
     * @return string
     */
    getPlaceholder(): string {
        if (!this.placeholder) {
            return '';
        } else {
            return this.placeholder;
        }
    }

    /**
     * Stringifies the given date.
     *
     * @param  {Date} date - The date.
     *
     * @return string
     */
    getDateString(date: Date): string {
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = ('0' + (date.getDate())).slice(-2);
        return date.getFullYear() + '-' + month + '-' + day;
    }
}
