import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { BetweenComponent } from './between.component';
import { BetweenCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: BetweenComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-between [criterion]='criterion'></app-between>`
    })
    class TestHostComponent {
        @ViewChild(BetweenComponent, { static: false })
        public testedComponent: BetweenComponent;
        public criterion: BetweenCriterion = undefined;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: BetweenComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [BetweenComponent, TestHostComponent],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.fieldMin.value).toBeNull();
        expect(testedComponent.fieldMin.enabled).toBeTruthy();
        expect(testedComponent.fieldMax.value).toBeNull();
        expect(testedComponent.fieldMax.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const min = '10';
        const max = '20';
        const criterion = { id: 1, type: 'between', min, max } as BetweenCriterion;
        const expectedFieldMinValue = min;
        const expectedFieldMaxValue = max;
        testedComponent.getDefault(criterion);
        expect(testedComponent.fieldMin.value).toEqual(expectedFieldMinValue);
        expect(testedComponent.fieldMin.disabled).toBeTruthy();
        expect(testedComponent.fieldMax.value).toEqual(expectedFieldMaxValue);
        expect(testedComponent.fieldMax.disabled).toBeTruthy();
    });

    it('#getPlaceholderMin() should fill the placeholder if defined', () => {
        const placeholder = '10';
        testedComponent.placeholderMin = placeholder;
        expect(testedComponent.getPlaceholderMin()).toEqual(placeholder);
    });

    it('#getPlaceholderMin() should not fill the placeholder if not defined', () => {
        expect(testedComponent.getPlaceholderMin()).toEqual('');
    });

    it('#getPlaceholderMax() should fill the placeholder if defined', () => {
        const placeholder = '10';
        testedComponent.placeholderMax = placeholder;
        expect(testedComponent.getPlaceholderMax()).toEqual(placeholder);
    });

    it('#getPlaceholderMax() should not fill the placeholder if not defined', () => {
        expect(testedComponent.getPlaceholderMax()).toEqual('');
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const min = '10';
        const max = '20';
        testedComponent.fieldMin = new FormControl(min);
        testedComponent.fieldMax = new FormControl(max);
        const expectedCriterion = { id: testedComponent.id, type: 'between', min, max } as BetweenCriterion;
        testedComponent.addCriterion.subscribe((event: BetweenCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
