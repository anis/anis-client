import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl, FormArray } from '@angular/forms';

import { CheckboxComponent } from './checkbox.component';
import { SelectMultipleCriterion } from '../../../store/model';
import { Option } from '../../../../metamodel/model';

describe('[Search][Criteria][SearchType] Component: CheckboxComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-checkbox [options]='options' [criterion]='criterion'></app-checkbox>`
    })
    class TestHostComponent {
        @ViewChild(CheckboxComponent, { static: false })
        public testedComponent: CheckboxComponent;
        public criterion: SelectMultipleCriterion = undefined;
        public options: Option[] = [
            { label: 'One', value: 'one', display: 1 },
            { label: 'Two', value: 'two', display: 2 },
            { label: 'Three', value: 'three', display: 3 }
        ];
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: CheckboxComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [CheckboxComponent, TestHostComponent],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should not fill and enable form if criterion not defined', () => {
        expect(testedComponent.checkboxes.length).toEqual(3);
        expect(testedComponent.isChecked()).toBeFalsy();
        expect(testedComponent.checkboxes.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const options: Option[] = [
            { label: 'One', value: 'one', display: 1 },
            { label: 'Two', value: 'two', display: 2 }
        ];
        const criterion = { id: 1, type: 'multiple', options} as SelectMultipleCriterion;
        testedComponent.getDefault(criterion);
        expect(testedComponent.isChecked()).toBeTruthy();
        const values = testedComponent.checkboxes.value as boolean[];
        expect(values.filter(v => v).length).toEqual(2);
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.checkboxes = new FormArray([
            new FormControl(false),
            new FormControl(true),
            new FormControl(true)
        ])
        const expectedCriterion = {
            id: 1,
            type: 'multiple',
            options: [
                { label: 'Two', value: 'two', display: 2 },
                { label: 'Three', value: 'three', display: 3 }
            ]
        };
        testedComponent.addCriterion.subscribe((event: SelectMultipleCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
