/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { FieldCriterion, Criterion } from '../../../store/model';

@Component({
    selector: 'app-field',
    templateUrl: 'field.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
/**
 * @class
 * @classdesc Field search type component.
 */
export class FieldComponent {
    @Input() id: number;
    @Input() operator: string;
    @Input() label: string;
    @Input() placeholder: string;
    @Input() attributeType: string;
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Input() advancedForm: boolean;
    @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    field = new FormControl('');
    disabledOperator: boolean;

    /**
     * Modifies operator with the given one.
     *
     * @param  {operator} operator - The operator.
     */
    changeOperator(operator: string): void {
        this.operator = operator;
    }

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<FieldCriterion>
     */
    emitAdd(): void {
        const fd = {id: this.id, type: 'field', operator: this.operator, value: this.field.value};
        this.addCriterion.emit(fd);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.field.reset();
            this.field.enable();
            this.disabledOperator = false;
        } else {
            const c = criterion as FieldCriterion;
            this.field.setValue(c.value);
            this.field.disable();
            this.disabledOperator = true;
        }
    }

    /**
     * Return field type.
     *
     * @return string
     */
    getType(): string {
        const numberTypeList = ['smallint', 'integer', 'decimal', 'float'];
        if (this.operator === 'in' || this.operator === 'nin' || !numberTypeList.includes(this.attributeType)) {
            return 'text';
        } else {
            return 'number';
        }
    }

    /**
     * Return placeholder.
     *
     * @return string
     */
    getPlaceholder(): string {
        if (!this.placeholder) {
            return '';
        } else {
            return this.placeholder;
        }
    }
}
