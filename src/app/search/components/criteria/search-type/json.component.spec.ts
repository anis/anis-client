import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { JsonComponent } from './json.component';
import { JsonCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: JsonComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-json-criteria [criterion]='criterion'></app-json-criteria>`
    })
    class TestHostComponent {
        @ViewChild(JsonComponent, { static: false })
        public testedComponent: JsonComponent;
        public criterion: JsonCriterion = undefined;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: JsonComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [JsonComponent, TestHostComponent],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.jsonForm.controls.path.value).toBeNull();
        expect(testedComponent.jsonForm.controls.operator.value).toBeNull();
        expect(testedComponent.jsonForm.controls.value.value).toBeNull();
        expect(testedComponent.jsonForm.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const path = 'path';
        const operator = '=';
        const value = 'test';
        const criterion = { id: 1, type: 'json', path, operator, value } as JsonCriterion;
        testedComponent.getDefault(criterion);
        expect(testedComponent.jsonForm.controls.path.value).toEqual(path);
        expect(testedComponent.jsonForm.controls.operator.value).toEqual(operator);
        expect(testedComponent.jsonForm.controls.value.value).toEqual(value);
        expect(testedComponent.jsonForm.disabled).toBeTruthy();
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const path = 'path';
        const operator = '=';
        const value = 'test';
        testedComponent.jsonForm.controls.path.setValue(path);
        testedComponent.jsonForm.controls.operator.setValue(operator);
        testedComponent.jsonForm.controls.value.setValue(value);
        const expectedCriterion = { id: testedComponent.id, type: 'json', path, operator, value } as JsonCriterion;
        testedComponent.addCriterion.subscribe((event: JsonCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
