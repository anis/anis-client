/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Criterion, FieldCriterion } from '../../../store/model';
import { Option } from '../../../../metamodel/model';

@Component({
    selector: 'app-select',
    templateUrl: 'select.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Select search type component.
 */
export class SelectComponent {
    @Input() id: number;
    @Input() operator: string;
    @Input() label: string;
    @Input() options: Option[];
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Input() advancedForm: boolean;
    @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    se = new FormControl();
    disabledOperator: boolean;

    /**
     * Modifies operator with the given one.
     *
     * @param  {operator} operator - The operator.
     */
    changeOperator(operator: string): void {
        this.operator = operator;
    }

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<FieldCriterion>
     */
    emitAdd(): void {
        const option = this.options.find(o => o.value === this.se.value);
        const se = {id: this.id, type: 'field', operator: this.operator, value: option.value};
        this.addCriterion.emit(se);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.se.reset();
            this.se.enable();
            this.disabledOperator = false;
        } else {
            const c = criterion as FieldCriterion;
            this.se.setValue(c.value);
            this.se.disable();
            this.disabledOperator = true;
        }
    }
}
