/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Criterion, BetweenCriterion } from '../../../store/model';

@Component({
    selector: 'app-between',
    templateUrl: 'between.component.html',
    styleUrls: ['operator.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Between search type component.
 */
export class BetweenComponent {
    @Input() id: number;
    @Input() label: string;
    @Input() placeholderMin: string;
    @Input() placeholderMax: string;
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Output() addCriterion: EventEmitter<BetweenCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    fieldMin = new FormControl('');
    fieldMax = new FormControl('');

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<BetweenCriterion>
     */
    emitAdd(): void {
        const fd = {id: this.id, type: 'between', min: this.fieldMin.value, max: this.fieldMax.value};
        this.addCriterion.emit(fd);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.fieldMin.reset();
            this.fieldMax.reset();
            this.fieldMin.enable();
            this.fieldMax.enable();
        } else {
            const c = criterion as BetweenCriterion;
            this.fieldMin.setValue(c.min);
            this.fieldMax.setValue(c.max);
            this.fieldMin.disable();
            this.fieldMax.disable();
        }
    }

    /**
     * Returns placeholder for the minimum value.
     *
     * @return string
     */
    getPlaceholderMin(): string {
        if (!this.placeholderMin) {
            return '';
        } else {
            return this.placeholderMin;
        }
    }

    /**
     * Returns placeholder for the maximum value.
     *
     * @return string
     */
    getPlaceholderMax(): string {
        if (!this.placeholderMax) {
            return '';
        } else {
            return this.placeholderMax;
        }
    }
}
