/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Criterion, FieldCriterion } from '../../../store/model';
import { Option } from '../../../../metamodel/model';

@Component({
    selector: 'app-radio',
    templateUrl: 'radio.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Radio search type component.
 */
export class RadioComponent {
    @Input() id: number;
    @Input() operator: string;
    @Input() label: string;
    @Input() options: Option[];
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    radio = new FormControl('');

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<FieldCriterion>
     */
    emitAdd(): void {
        const option = this.options.find(o => o.value === this.radio.value);
        const cb = {id: this.id, type: 'field', operator: this.operator, value: option.value};
        this.addCriterion.emit(cb);
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.radio.reset();
            this.radio.enable();
        } else {
            const c = criterion as FieldCriterion;
            this.radio.setValue(c.value);
            this.radio.disable();
        }
    }
}
