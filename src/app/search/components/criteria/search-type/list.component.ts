/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { ListCriterion, Criterion } from '../../../store/model';

@Component({
    selector: 'app-list',
    templateUrl: 'list.component.html',
    styleUrls: ['operator.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
/**
 * @class
 * @classdesc List search type component.
 */
export class ListComponent {
    @Input() id: number;
    @Input() label: string;
    @Input() placeholder: string;
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: Criterion) {
        this.getDefault(criterion);
    }
    @Output() addCriterion: EventEmitter<ListCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    list = new FormControl('');

    /**
     * Fills form with the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     */
    getDefault(criterion: Criterion): void {
        if (!criterion) {
            this.list.reset();
            this.list.enable();
        } else {
            const c = criterion as ListCriterion;
            this.list.setValue(c.values.join('\n'));
            this.list.disable();
        }
    }

    /**
     * Returns placeholder.
     *
     * @return string
     */
    getPlaceholder(): string {
        if (!this.placeholder) {
            return '';
        } else {
            return this.placeholder;
        }
    }

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<ListCriterion>
     */
    emitAdd(): void {
        const ls = { id: this.id, type: 'list', values: this.list.value.split('\n') };
        this.addCriterion.emit(ls);
    }
}
