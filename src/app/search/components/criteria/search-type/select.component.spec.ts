import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { NgSelectModule } from '@ng-select/ng-select';

import { SelectComponent } from './select.component';
import { FieldCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: SelectComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-select [criterion]='criterion'></app-select>`
    })
    class TestHostComponent {
        @ViewChild(SelectComponent, { static: false })
        public testedComponent: SelectComponent;
        public criterion: FieldCriterion = undefined;
    }

    @Component({ selector: 'app-operator', template: '' })
    class OperatorStubComponent {
        @Input() operator: string;
        @Input() searchType: string;
        @Input() advancedForm: boolean;
        @Input() disabled: boolean;
        @Output() changeOperator: EventEmitter<string> = new EventEmitter();
    }

    @Component({ selector: 'app-help-like', template: '' })
    class HelpLikeStubComponent { }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: SelectComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                SelectComponent, 
                TestHostComponent,
                OperatorStubComponent,
                HelpLikeStubComponent
            ],
            imports: [NgSelectModule, FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.se.value).toBeNull();
        expect(testedComponent.se.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const operator = '=';
        const value = 'test';
        const criterion = { id: 1, type: 'field', operator, value } as FieldCriterion;
        testedComponent.getDefault(criterion);
        expect(testedComponent.se.value).toEqual(value);
        expect(testedComponent.se.disabled).toBeTruthy();
    });

    it('#changeOperator() should change the operator', () => {
        expect(testedComponent.operator).toBeUndefined();
        testedComponent.changeOperator('toto');
        expect(testedComponent.operator).toBe('toto');
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const operator = '=';
        testedComponent.operator = operator;
        const value = 'three';
        testedComponent.se = new FormControl(value);
        testedComponent.options = [
            { label: 'One', value: 'one', display: 1 },
            { label: 'Two', value: 'two', display: 2 },
            { label: 'Three', value: 'three', display: 3 }
        ];
        const expectedCriterion = { id: testedComponent.id, type: 'field', operator, value } as FieldCriterion;
        testedComponent.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
