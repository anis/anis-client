import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { NgSelectModule } from '@ng-select/ng-select';

import { SelectMultipleComponent } from './select-multiple.component';
import { SelectMultipleCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: SelectMultipleComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-select-multiple [criterion]='criterion'></app-select-multiple>`
    })
    class TestHostComponent {
        @ViewChild(SelectMultipleComponent, { static: false })
        public testedComponent: SelectMultipleComponent;
        public criterion: SelectMultipleCriterion = undefined;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: SelectMultipleComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [SelectMultipleComponent, TestHostComponent],
            imports: [NgSelectModule, FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.ms.value).toBeNull();
        expect(testedComponent.ms.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const options = [
            { label: 'One', value: 'one', display: 1 },
            { label: 'Two', value: 'two', display: 2 },
            { label: 'Three', value: 'three', display: 3 }
        ];
        const criterion = { id: 1, type: 'multiple', options} as SelectMultipleCriterion;
        testedComponent.getDefault(criterion);
        expect(testedComponent.ms.value).toEqual(['one', 'two', 'three']);
        expect(testedComponent.ms.disabled).toBeTruthy();
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.options = [
            { label: 'One', value: 'one', display: 1 },
            { label: 'Two', value: 'two', display: 2 },
            { label: 'Three', value: 'three', display: 3 }
        ];
        const value = ['three'];
        testedComponent.ms = new FormControl(value);
        const expectedValue = [
            { label: 'Three', value: 'three', display: 3 }
        ];
        const expectedCriterion = { id: testedComponent.id, type: 'multiple', options: expectedValue } as SelectMultipleCriterion;
        testedComponent.addCriterion.subscribe((event: SelectMultipleCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
