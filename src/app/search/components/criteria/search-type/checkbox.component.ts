/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormArray } from '@angular/forms';

import { SelectMultipleCriterion } from '../../../store/model';
import { Option } from '../../../../metamodel/model';

@Component({
    selector: 'app-checkbox',
    templateUrl: 'checkbox.component.html',
    styleUrls: ['checkbox.component.css', 'operator.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Checkbox search type component.
 */
export class CheckboxComponent {
    @Input() id: number;
    @Input() label: string;
    @Input() options: Option[];
    /**
     * Calls getDefault function for the given criterion.
     *
     * @param  {SelectMultipleCriterion} criterion - The criterion.
     */
    @Input()
    set criterion(criterion: SelectMultipleCriterion) {
        this.getDefault(criterion);
    }
    @Output() addCriterion: EventEmitter<SelectMultipleCriterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    checkboxes: FormArray;

    /**
     * Emits event to add criterion to the criteria list.
     *
     * @fires EventEmitter<SelectMultipleCriterion>
     */
    emitAdd(): void {
        // Filter options to keep only the checked checkboxes and emit the new criterion
        const selected = this.checkboxes.value;
        const values = [...this.options.filter((option, index) => selected[index])];
        this.addCriterion.emit({id: this.id, type: 'multiple', options: values});
    }

    /**
     * Emits event to remove criterion ID from the criteria list.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(): void {
        // Delete the criterion into the state to reactivate the checkboxes
        this.deleteCriterion.emit(this.id);
    }

    /**
     * Fills form with the given criterion.
     *
     * @param  {SelectMultipleCriterion} criterion - The criterion.
     */
    getDefault(criterion: SelectMultipleCriterion): void {
        // Initialization of checkboxes (1 per option)
        const formControls: FormControl[] = [];
        for (let i = 0; i < this.options.length; i++) {
            formControls.push(new FormControl());
        }

        // Store checkboxes in an array
        this.checkboxes = new FormArray(formControls);

        // If the criterion is already used
        // Set to true only the checkboxes present in the criterion
        if (criterion) {
            for (let i = 0; i < this.options.length; i++) {
                if (criterion.options.find(o => o.label === this.options[i].label)) {
                    this.checkboxes.controls[i].setValue(true);
                }
            }
            this.checkboxes.disable();
        }
    }

    /**
     * Checks if one of the checkboxes is checked.
     *
     * @return boolean
     */
    isChecked(): boolean {
        // If one of the checkboxes is checked returns true else returns false
        return this.checkboxes.controls.filter(formControl => formControl.value).length > 0;
    }
}
