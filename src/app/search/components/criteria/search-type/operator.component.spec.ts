import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OperatorComponent } from './operator.component';

describe('[Search][Criteria][SearchType] Component: OperatorComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-operator
                [operator]='operator'
                [searchType]='searchType'
                [advancedForm]='advancedForm'
                [disabled]='disabled'>
            </app-operator>`
    })
    class TestHostComponent {
        @ViewChild(OperatorComponent, { static: false })
        testedComponent: OperatorComponent;
        operator = 'eq';
        searchType: string = undefined;
        advancedForm: boolean = undefined;
        disabled: boolean = undefined;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: OperatorComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [OperatorComponent, TestHostComponent],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getLabel() should return the correct operator form label', () => {
        expect(testedComponent.getLabel('eq')).toBe('=');
        expect(testedComponent.getLabel('neq')).toBe('≠');
        expect(testedComponent.getLabel('gt')).toBe('>');
        expect(testedComponent.getLabel('gte')).toBe('>=');
        expect(testedComponent.getLabel('lt')).toBe('<');
        expect(testedComponent.getLabel('lte')).toBe('<=');
        expect(testedComponent.getLabel('lk')).toBe('like');
        expect(testedComponent.getLabel('nlk')).toBe('not like');
    });

    it('raises the changeOperator event when the value change', () => {
        testedComponent.changeOperator.subscribe((event: string) => expect(event).toEqual('eq'));
        testedComponent.emitChange('eq');
    });

    it('should display the select box when it\'s an enabled advanced form', () => {
        testHostComponent.advancedForm = true;
        testHostComponent.disabled = false;
        testHostFixture.detectChanges();
        const template = testHostFixture.nativeElement;
        expect(template.querySelector('select')).toBeTruthy();
        expect(template.querySelector('.readonly')).toBeFalsy();
    });

    it('should display the readonly field when it\'s not an advanced form or not enabled advanced form', () => {
        testHostComponent.advancedForm = true;
        testHostComponent.disabled = true;
        testHostFixture.detectChanges();
        const template = testHostFixture.nativeElement;
        expect(template.querySelector('select')).toBeFalsy();
        expect(template.querySelector('.readonly')).toBeTruthy();
        testHostComponent.advancedForm = true;
        testHostFixture.detectChanges();
        expect(template.querySelector('select')).toBeFalsy();
        expect(template.querySelector('.readonly')).toBeTruthy();
    });
});
