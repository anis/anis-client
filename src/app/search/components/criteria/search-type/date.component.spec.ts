import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { DateComponent } from './date.component';
import { FieldCriterion } from '../../../store/model';

describe('[Search][Criteria][SearchType] Component: DateComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-date [criterion]='criterion'></app-date>`
    })
    class TestHostComponent {
        @ViewChild(DateComponent, { static: false })
        public testedComponent: DateComponent;
        public criterion: FieldCriterion = undefined;
    }

    @Component({ selector: 'app-operator', template: '' })
    class OperatorStubComponent {
        @Input() operator: string;
        @Input() searchType: string;
        @Input() advancedForm: boolean;
        @Input() disabled: boolean;
        @Output() changeOperator: EventEmitter<string> = new EventEmitter();
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: DateComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DateComponent, 
                TestHostComponent,
                OperatorStubComponent
            ],
            imports: [BsDatepickerModule.forRoot(), FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDefault() should enable and not fill form as criterion not defined in host component', () => {
        expect(testedComponent.field.value).toBeNull();
        expect(testedComponent.field.enabled).toBeTruthy();
    });

    it('#getDefault() should fill and disable form if criterion is defined', () => {
        const value = '2019-02-17';
        const criterion = { id: 1, type: 'field', operator: 'eq', value } as FieldCriterion;
        const expectedFieldValue = new Date(value);
        testedComponent.getDefault(criterion);
        expect(testedComponent.field.value).toEqual(expectedFieldValue);
        expect(testedComponent.field.disabled).toBeTruthy();
    });

    it('#getPlaceholder() should fill the placeholder if defined', () => {
        const placeholder = '2019-02-17';
        testedComponent.placeholder = placeholder;
        expect(testedComponent.getPlaceholder()).toEqual(placeholder);
    });

    it('#getPlaceholder() should not fill the placeholder if not defined', () => {
        expect(testedComponent.getPlaceholder()).toEqual('');
    });

    it('#getDateString() should return a date as string', () => {
        const dateString = '2019-02-17';
        const date = new Date(dateString);
        expect(testedComponent.getDateString(date)).toEqual(dateString);
    });

    it('#changeOperator() should change the operator', () => {
        expect(testedComponent.operator).toBeUndefined();
        testedComponent.changeOperator('toto');
        expect(testedComponent.operator).toBe('toto');
    });

    it('raises the add criterion event when clicked', () => {
        testedComponent.id = 1;
        const operator = '=';
        testedComponent.operator = operator;
        const date = '2019-02-17';
        testedComponent.field = new FormControl(new Date(date));
        const expectedCriterion = { id: testedComponent.id, type: 'field', operator, value: date } as FieldCriterion;
        testedComponent.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(expectedCriterion));
        testedComponent.emitAdd();
    });

    it('raises the delete criterion event when clicked', () => {
        testedComponent.id = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete();
    });
});
