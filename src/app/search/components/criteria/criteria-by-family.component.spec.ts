import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { CriteriaByFamilyComponent } from './criteria-by-family.component';
import { Criterion, FieldCriterion, BetweenCriterion, SelectMultipleCriterion, JsonCriterion } from '../../store/model';
import { Attribute, Option } from '../../../metamodel/model';
import { ATTRIBUTE_LIST, CRITERIA_LIST } from '../../../../settings/test-data';

describe('[Search][Criteria] Component: CriteriaByFamilyComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-criteria-by-family [attributeList]='attributeList' [criteriaList]='criteriaList'></app-criteria-by-family>`
    })
    class TestHostComponent {
        @ViewChild(CriteriaByFamilyComponent, { static: false })
        public testedComponent: CriteriaByFamilyComponent;
        private attributeList: Attribute[] = ATTRIBUTE_LIST;
        private criteriaList: Criterion[] = CRITERIA_LIST;
    }

    @Component({ selector: 'app-field', template: '' })
    class FieldStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() placeholder: string;
        @Input() attributeType: string;
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
        @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-between', template: '' })
    class BetweenStubComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() placeholderMin: string;
        @Input() placeholderMax: string;
        @Input() criterion: Criterion;
        @Output() addCriterion: EventEmitter<BetweenCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-select', template: '' })
    class SelectStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
        @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-select-multiple', template: '' })
    class SelectMultipleStubComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
        @Output() addCriterion: EventEmitter<SelectMultipleCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-datalist', template: '' })
    class DatalistStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() placeholder: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
        @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-radio', template: '' })
    class RadioStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
        @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-checkbox', template: '' })
    class CheckboxStubComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() options: Option[];
        @Input() criterion: Criterion;
        @Output() addCriterion: EventEmitter<SelectMultipleCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-date', template: '' })
    class DateStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() placeholder: string;
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
        @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-between-date', template: '' })
    class BetweenDateStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() criterion: Criterion;
        @Output() addCriterion: EventEmitter<BetweenCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-time', template: '' })
    class TimeStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
        @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-datetime', template: '' })
    class DatetimeStubComponent {
        @Input() id: number;
        @Input() operator: string;
        @Input() label: string;
        @Input() criterion: Criterion;
        @Input() advancedForm: boolean;
        @Output() addCriterion: EventEmitter<FieldCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    @Component({ selector: 'app-json-criteria', template: '' })
    class JsonStubComponent {
        @Input() id: number;
        @Input() label: string;
        @Input() criterion: Criterion;
        @Output() addCriterion: EventEmitter<JsonCriterion> = new EventEmitter();
        @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: CriteriaByFamilyComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                CriteriaByFamilyComponent,
                TestHostComponent,
                FieldStubComponent,
                BetweenStubComponent,
                SelectStubComponent,
                SelectMultipleStubComponent,
                DatalistStubComponent,
                RadioStubComponent,
                CheckboxStubComponent,
                DateStubComponent,
                BetweenDateStubComponent,
                TimeStubComponent,
                DatetimeStubComponent,
                JsonStubComponent
            ]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getAttributeListSortedByDisplay() should sort attributeList by criteria_display', () => {
        const sortedAttributeList: Attribute[] = testedComponent.getAttributeListSortedByDisplay();
        expect(sortedAttributeList[0].id).toBe(2);
        expect(sortedAttributeList[1].id).toBe(1);
    });

    it('#getOptions(attribute) should return sorted options of attribute', () => {
        const attributeId = 1;
        const options: Option[] = testedComponent.getOptions(attributeId);
        expect(options.length).toBe(3);
        expect(options[0].value).toBe('one');
        expect(options[2].value).toBe('three');
    });

    it('#getCriterion(criterionId) should return correct crition', () => {
        const criterionId = 1;
        const criterion = testedComponent.getCriterion(criterionId) as FieldCriterion;
        expect(criterion.value).toBe('fd_crit_1');
    });

    it('raises the add criterion event when clicked', () => {
        const criterion = { id: 1, type: 'field', operator: 'eq', value: 'test' } as FieldCriterion;
        testedComponent.addCriterion.subscribe((event: FieldCriterion) => expect(event).toEqual(criterion));
        testedComponent.emitAdd(criterion);
    });

    it('raises the delete criterion event when clicked', () => {
        const criterionId = 1;
        testedComponent.deleteCriterion.subscribe((event: number) => expect(event).toEqual(1));
        testedComponent.emitDelete(criterionId);
    });
});

