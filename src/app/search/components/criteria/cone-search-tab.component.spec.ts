import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, ViewChild, Input } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { ConeSearchTabComponent } from './cone-search-tab.component';
import { Dataset } from '../../../metamodel/model';
import { DATASET, DATASET_LIST } from '../../../../settings/test-data';

describe('[Search][Criteria] Component: ConeSearchTabComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-cone-search-tab
                [datasetName]='datasetName' 
                [datasetList]='datasetList' 
                [isConeSearchAdded]='isConeSearchAdded' 
                [isValidConeSearch]='isValidConeSearch'>
            </app-cone-search-tab>`
    })
    class TestHostComponent {
        @ViewChild(ConeSearchTabComponent, { static: false })
        testedComponent: ConeSearchTabComponent;
        datasetName: string = DATASET.name;
        datasetList: Dataset[] = DATASET_LIST;
        isConeSearchAdded: boolean = false;
        isValidConeSearch: boolean = false;
    }

    @Component({ selector: 'app-cone-search', template: '' })
    class ConeSearchStubComponent {
        @Input() disabled: boolean;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: ConeSearchTabComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConeSearchTabComponent,
                TestHostComponent,
                ConeSearchStubComponent
            ],
            imports: [
                AccordionModule.forRoot(),
                BrowserAnimationsModule,
                ReactiveFormsModule
            ]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#coneSearchEnabled() should return if cone search is enabled or not', () => {
        expect(testedComponent.coneSearchEnabled()).toBeTruthy();
        testedComponent.datasetName = 'cat_2';
        expect(testedComponent.coneSearchEnabled()).toBeFalsy();
    });
});
