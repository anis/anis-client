/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Criterion } from '../../store/model';
import { Family, Attribute } from '../../../metamodel/model';

@Component({
    selector: 'app-criteria-tabs',
    templateUrl: 'criteria-tabs.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search criteria tabs component.
 */
export class CriteriaTabsComponent {
    @Input() criteriaFamilyList: Family[];
    @Input() attributeList: Attribute[];
    @Input() criteriaList: Criterion[];
    @Output() addCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    /**
     * Returns attribute list for the given criteria family ID.
     *
     * @param  {number} idFamily - The criteria family ID.
     *
     * @return Attribute[]
     */
    getAttributeByFamily(idFamily: number): Attribute[] {
        return this.attributeList
            .filter(attribute => attribute.id_criteria_family === idFamily);
    }

    /**
     * Emits event to add the given criterion to the criteria list.
     *
     * @param  {Criterion} criterion - The criterion.
     *
     * @fires EventEmitter<Criterion>
     */
    emitAdd(criterion: Criterion): void {
        this.addCriterion.emit(criterion);
    }

    /**
     * Emits event to remove the given criterion ID to the criteria list.
     *
     * @param  {number} id - The criterion ID.
     *
     * @fires EventEmitter<number>
     */
    emitDelete(id: number): void {
        this.deleteCriterion.emit(id);
    }
}
