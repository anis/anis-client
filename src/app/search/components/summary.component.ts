/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';

import { Criterion, SearchQueryParams } from '../store/model';
import { Attribute, Category, Dataset, Family } from '../../metamodel/model';
import { printCriterion as print, sortByDisplay } from '../../shared/utils'
import { ConeSearch } from '../../shared/cone-search/store/model';

@Component({
    selector: 'app-summary',
    templateUrl: 'summary.component.html',
    styleUrls: ['summary.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
/**
 * @class
 * @classdesc Search summary component.
 */
export class SummaryComponent {
    @Input() currentStep: string;
    @Input() datasetName: string;
    @Input() datasetList: Dataset[];
    @Input() isConeSearchAdded: boolean;
    @Input() coneSearch: ConeSearch;
    @Input() criteriaFamilyList: Family[];
    @Input() criteriaList: Criterion[];
    @Input() attributeList: Attribute[];
    @Input() outputFamilyList: Family[];
    @Input() categoryList: Category[];
    @Input() outputList: number[];
    @Input() outputListEmpty: boolean;
    @Input() queryParams: SearchQueryParams;

    accordionFamilyIsOpen = true;

    /**
     * Returns dataset selected for the search.
     *
     * @return Dataset
     */
    getDataset(): Dataset {
        return this.datasetList.find(dataset => dataset.name === this.datasetName);
    }

    /**
     * Checks if there is no criteria added to the search.
     *
     * @return boolean
     */
    noCriteria(): boolean {
        if (this.isConeSearchAdded || this.criteriaList.length > 0) {
            return false
        }
        return true;
    }

    /**
     * Returns attribute for the given attribute ID.
     *
     * @param  {number} id - The attribute ID.
     *
     * @return Dataset[]
     */
    getAttribute(id: number): Attribute {
        return this.attributeList.find(attribute => attribute.id === id);
    }

    /**
     * Returns pretty print of the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     *
     * @return string
     */
    printCriterion(criterion: Criterion): string {
        return print(criterion);
    }

    /**
     * Returns category list sorted by display, for the given output family ID.
     *
     * @param  {number} idFamily - The family ID.
     *
     * @return Category[]
     */
    getCategoryByFamilySortedByDisplay(idFamily: number): Category[] {
        return this.categoryList
            .filter(category => category.id_output_family === idFamily)
            .sort(sortByDisplay);
    }

    /**
     * Returns output list sorted by display, that belongs to the given category ID.
     *
     * @param  {number} idCategory - The category ID.
     *
     * @return Attribute[]
     */
    getSelectedOutputByCategory(idCategory: number): Attribute[] {
        const outputListByCategory = this.attributeList.filter(attribute => attribute.id_output_category === idCategory);
        return outputListByCategory
            .filter(attribute => this.outputList.includes(attribute.id))
            .sort((a, b) => a.output_display - b.output_display);
    }
}
