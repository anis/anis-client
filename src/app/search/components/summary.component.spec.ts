import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { SummaryComponent } from './summary.component';
import { Criterion } from '../store/model';
import { Category } from '../../metamodel/model';
import { ATTRIBUTE_LIST, CRITERIA_LIST, DATASET_LIST, CATEGORY_LIST } from '../../../settings/test-data';

describe('[Search] Component: SummaryComponent', () => {
    let component: SummaryComponent;
    let fixture: ComponentFixture<SummaryComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SummaryComponent],
            imports: [AccordionModule.forRoot(), BrowserAnimationsModule]
        });
        fixture = TestBed.createComponent(SummaryComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getDataset() should return the dataset with the given name', () => {
        component.datasetList = DATASET_LIST;
        component.datasetName = 'cat_1';
        expect(component.getDataset().description).toBe('Description of cat 1');
    });

    it('#noCriteria() should return if there is criteria or not', () => {
        component.isConeSearchAdded = false;
        component.criteriaList = [];
        expect(component.noCriteria()).toBeTruthy();
        component.criteriaList = CRITERIA_LIST;
        expect(component.noCriteria()).toBeFalsy();
        component.isConeSearchAdded = true;
        expect(component.noCriteria()).toBeFalsy();
        component.criteriaList = [];
        expect(component.noCriteria()).toBeFalsy();
    });

    it('#getAttribute() should return the attribute with the given id', () => {
        component.attributeList = ATTRIBUTE_LIST;
        expect(component.getAttribute(1).name).toBe('name_one');
    });

    it('#printCriterion() should return pretty criterion', () => {
        const criterion: Criterion = CRITERIA_LIST.find(c => c.id === 1);
        expect(component.printCriterion(criterion)).toBe('= fd_crit_1');
    });

    it('#getCategoryByFamilySortedByDisplay(idFamily) should sort by display categories belonging to idFamily', () => {
        component.categoryList = CATEGORY_LIST;
        const sortedCategoryList: Category[] = component.getCategoryByFamilySortedByDisplay(1);
        expect(sortedCategoryList.length).toBe(2);
        expect(sortedCategoryList[0].id).toBe(2);
        expect(sortedCategoryList[1].id).toBe(1);
    });

    it('#getSelectedOutputByCategory() should return selected attributes belonging to the given category', () => {
        component.attributeList = ATTRIBUTE_LIST;
        component.outputList = [1];
        expect(component.getSelectedOutputByCategory(1).length).toEqual(1);
        component.outputList = [];
        expect(component.getSelectedOutputByCategory(1).length).toEqual(0);
    });
});

