/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

import { Dataset } from '../../../metamodel/model';

@Component({
    selector: 'app-samp',
    templateUrl: 'samp.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Samp component.
 */
export class SampComponent {
    @Input() datasetName: string;
    @Input() datasetList: Dataset[];
    @Input() sampRegistered: boolean;
    @Output() sampRegister: EventEmitter<{}> = new EventEmitter();
    @Output() sampUnregister: EventEmitter<{}> = new EventEmitter();

    isSampActivated(): boolean {
        const dataset = this.datasetList.find(dataset => dataset.name === this.datasetName);
        return dataset.config.samp.enabled;
    }

    isSampOpened(): boolean {
        const dataset = this.datasetList.find(dataset => dataset.name === this.datasetName);
        return dataset.config.samp.opened;
    }

    getColor(): string {
        if (this.sampRegistered) {
            return 'green';
        } else {
            return 'red';
        }
    }
}
