/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Criterion } from '../../store/model';
import { Dataset } from '../../../metamodel/model';
import { getCriterionStr, getHost as host } from '../../../shared/utils';
import { ConeSearch } from '../../../shared/cone-search/store/model';

@Component({
    selector: 'app-result-download',
    templateUrl: 'download.component.html',
    styleUrls: ['download.component.css']
})
/**
 * @class
 * @classdesc Search result download component.
 *
 * @implements OnInit
 */
export class DownloadComponent implements OnInit {
    @Input() datasetName: string;
    @Input() datasetList: Dataset[];
    @Input() dataLengthIsLoaded: boolean;
    @Input() dataLength: number;
    @Input() isConeSearchAdded: boolean;
    @Input() coneSearch: ConeSearch;
    @Input() criteriaList: Criterion[];
    @Input() outputList: number[];
    @Input() sampRegistered: boolean;
    @Output() getDataLength: EventEmitter<null> = new EventEmitter();
    @Output() broadcast: EventEmitter<string> = new EventEmitter();

    ngOnInit() {
        this.getDataLength.emit();
    }

    /**
     * Returns dataset label.
     *
     * @return string
     */
    getDatasetLabel(): string {
        const dataset = this.datasetList.find(dataset => dataset.name === this.datasetName);
        return dataset.label;
    }

    isDownloadActivated(): boolean {
        const dataset = this.datasetList.find(dataset => dataset.name === this.datasetName);
        return dataset.config.download.enabled;
    }

    isDownloadOpened(): boolean {
        const dataset = this.datasetList.find(dataset => dataset.name === this.datasetName);
        return dataset.config.download.opened;
    }

    /**
     * Checks if the download format is allowed by Anis Admin configuration.
     *
     * @param  {string} format - The file format to download.
     *
     * @return boolean
     */
    getConfigDownloadResultFormat(format: string): boolean {
        const dataset = this.datasetList.find(dataset => dataset.name === this.datasetName);
        return dataset.config.download[format];
    }

    /**
     * Returns URL to download file for the given format.
     *
     * @param  {string} format - The file format to download.
     *
     * @return string
     */
    getUrl(format: string): string {
        let query: string = host() + '/search/' + this.datasetName + '?a=' + this.outputList.join(';');
        if (this.criteriaList.length > 0) {
            query += '&c=' + this.criteriaList.map(criterion => getCriterionStr(criterion)).join(';');
        }
        if (this.isConeSearchAdded) {
            query += '&cs=' + this.coneSearch.ra + ':' + this.coneSearch.dec + ':' + this.coneSearch.radius;
        }
        query += '&f=' + format
        return query;
    }

    getUrlArchive(): string {
        let query: string = host() + '/archive/' + this.datasetName + '?a=' + this.outputList.join(';');
        if (this.criteriaList.length > 0) {
            query += '&c=' + this.criteriaList.map(criterion => getCriterionStr(criterion)).join(';');
        }
        if (this.isConeSearchAdded) {
            query += '&cs=' + this.coneSearch.ra + ':' + this.coneSearch.dec + ':' + this.coneSearch.radius;
        }
        return query;
    }
    
    broadcastVotable(): void {
        this.broadcast.emit(this.getUrl('votable'));
    }
}
