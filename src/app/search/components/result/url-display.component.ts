/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { Criterion } from '../../store/model';
import { Dataset } from '../../../metamodel/model';
import { getCriterionStr, getHost as host } from '../../../shared/utils';
import { ConeSearch } from '../../../shared/cone-search/store/model';

@Component({
    selector: 'app-result-url-display',
    templateUrl: 'url-display.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search result URL display component.
 */
export class UrlDisplaySectionComponent {
    @Input() datasetList: Dataset[];
    @Input() dataLengthIsLoaded: boolean;
    @Input() datasetName: string;
    @Input() isConeSearchAdded: boolean;
    @Input() coneSearch: ConeSearch;
    @Input() criteriaList: Criterion[];
    @Input() outputList: number[];

    constructor(private toastr: ToastrService) { }

    /**
     * Checks if URL display is enabled.
     *
     * @return boolean
     */
    urlDisplayEnabled(): boolean {
        const config = this.datasetList.find(d => d.name === this.datasetName).config;
        return config.server_link.enabled;
    }

    urlDisplayOpened(): boolean {
        const config = this.datasetList.find(d => d.name === this.datasetName).config;
        return config.server_link.opened;
    }

    /**
     * Returns API URL to get data with user parameters.
     *
     * @return string
     */
    getUrl(): string {
        let query: string = host() + '/search/' + this.datasetName + '?a=' + this.outputList.join(';');
        if (this.criteriaList.length > 0) {
            query += '&c=' + this.criteriaList.map(criterion => getCriterionStr(criterion)).join(';');
        }
        if (this.isConeSearchAdded) {
            query += '&cs=' + this.coneSearch.ra + ':' + this.coneSearch.dec + ':' + this.coneSearch.radius;
        }
        return query;
    }

    /**
     * Copies API URL to user clipboard.
     */
    copyToClipboard(): void {
        const selBox = document.createElement('textarea');
        selBox.value = this.getUrl();
        document.body.appendChild(selBox);
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
        this.toastr.success('Copied');
    }
}
