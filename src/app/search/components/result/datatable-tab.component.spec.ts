import { Component, Input, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { DatatableTabComponent } from './datatable-tab.component';
import { Attribute, Dataset } from '../../../metamodel/model';
import { Pagination, PaginationOrder } from '../../../shared/datatable/model';
import { ATTRIBUTE_LIST, DATASET_LIST } from '../../../../settings/test-data';

describe('[Search][Result] Component: DatatableTabComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-datatable-tab
                [datasetName]='datasetName'
                [datasetList]='datasetList'
                [datasetAttributeList]='datasetAttributeList'
                [outputList]='outputList'
                [searchData]='searchData'
                [dataLengthIsLoaded]='dataLengthIsLoaded'
                [dataLength]='dataLength'
                [selectedData]='selectedData'
                [processWip]='processWip'
                [processDone]='processDone'
                [processId]='processId'>
            </app-datatable-tab>`
    })
    class TestHostComponent {
        @ViewChild(DatatableTabComponent, { static: false })
        public testedComponent: DatatableTabComponent;
        public datasetName: string;
        public datasetList: Dataset[];
        public datasetAttributeList: Attribute[] = ATTRIBUTE_LIST;
        public outputList: number[];
        public searchData: any[];
        public dataLengthIsLoaded: boolean;
        public dataLength: number;
        public selectedData: any[];
        public processWip: boolean;
        public processDone: boolean;
        public processId: string;
    }

    @Component({ selector: 'app-datatable', template: '' })
    class DatatableStubComponent {
        @Input() dataset: Dataset;
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Input() data: any[];
        @Input() dataLength: number;
        @Input() selectedData: (string | number)[];
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: DatatableTabComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatatableTabComponent,
                TestHostComponent,
                DatatableStubComponent
            ],
            imports: [AccordionModule.forRoot(), BrowserAnimationsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#isDatatableOpened() should return if datatable has to be opened or not', () => {
        testedComponent.datasetList = DATASET_LIST;
        testedComponent.datasetName = 'cat_1';
        expect(testedComponent.isDatatableOpened()).toBeTruthy();
        testedComponent.datasetName = 'cat_2';
        expect(testedComponent.isDatatableOpened()).toBeFalsy();
    });

    it('#getDataset() should return dataset object', () => {
        testedComponent.datasetList = DATASET_LIST;
        testedComponent.datasetName = 'cat_1';
        const dataset: Dataset = testedComponent.getDataset();
        expect(dataset.name).toBe('cat_1');
        expect(dataset.label).toBe('Cat 1');
    });

    it('should emit getSearchData event when dataLengthIsLoaded is true', () => {
        const pagination: Pagination = { dname: 'toto', nbItems: 10, page: 1, sortedCol: 1, order: PaginationOrder.a };
        testedComponent.getSearchData.subscribe((event: Pagination) => expect(event).toEqual(pagination));
        testHostComponent.dataLengthIsLoaded = true;
        testHostComponent.datasetName = 'toto';
        testHostFixture.detectChanges();
    });
});

