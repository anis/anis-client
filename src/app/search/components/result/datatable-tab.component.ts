/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { Attribute, Dataset } from '../../../metamodel/model';
import { Pagination, PaginationOrder } from '../../../shared/datatable/model';

@Component({
    selector: 'app-datatable-tab',
    templateUrl: 'datatable-tab.component.html',
    styleUrls: ['datatable-tab.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search result datatable tab component.
 */
export class DatatableTabComponent {
    @Input() datasetName: string;
    @Input() datasetList: Dataset[];
    @Input() datasetAttributeList: Attribute[];
    @Input() outputList: number[];
    @Input() searchData: any[];
    /**
     * Emits event to get data when result count is loaded.
     *
     * @param  {boolean} dataLengthIsLoaded - Is result count loaded.
     */
    @Input()
    set dataLengthIsLoaded(dataLengthIsLoaded: boolean) {
        this._dataLengthIsLoaded = dataLengthIsLoaded;
        if (dataLengthIsLoaded) {
            const pagination: Pagination = {
                dname: this.datasetName,
                page: 1,
                nbItems: 10,
                sortedCol: this.datasetAttributeList.find(a => a.order_by).id,
                order: PaginationOrder.a
            };
            this.getSearchData.emit(pagination);
        }
    }
    @Input() dataLength: number;
    @Input() selectedData: any[];
    @Input() processWip: boolean;
    @Input() processDone: boolean;
    @Input() processId: string;
    @Output() getSearchData: EventEmitter<Pagination> = new EventEmitter();
    @Output() addSelectedData: EventEmitter<number | string> = new EventEmitter();
    @Output() deleteSelectedData: EventEmitter<number | string> = new EventEmitter();
    @Output() executeProcess: EventEmitter<string> = new EventEmitter();

    _dataLengthIsLoaded: boolean = false;

    isDatatableEnabled(): boolean {
        const config = this.getDataset().config;
        return config.datatable.enabled;
    }

    /**
     * Checks if datatable accordion should be open.
     *
     * @return boolean
     */
    isDatatableOpened(): boolean {
        const config = this.getDataset().config;
        return config.datatable.opened;
    }

    /**
     * Returns selected dataset for the search.
     *
     * @return Dataset
     */
    getDataset(): Dataset {
        return this.datasetList.find(dataset => dataset.name === this.datasetName);
    }
}
