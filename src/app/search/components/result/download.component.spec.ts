import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadComponent } from './download.component';
import { ConeSearch } from '../../../shared/cone-search/store/model';
import { DATASET_LIST, CRITERIA_LIST } from '../../../../settings/test-data';
import { environment } from '../../../../environments/environment';

describe('[Search][Result] Component: DownloadComponent', () => {
    let component: DownloadComponent;
    let fixture: ComponentFixture<DownloadComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DownloadComponent]
        });
        fixture = TestBed.createComponent(DownloadComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should emit getDataLength event on ngOnInit lifecycle', () => {
        component.getDataLength.subscribe((event: null) => expect(event).toBeUndefined());
        component.ngOnInit();
    });

    it('#getDatasetLabel() should return the dataset label', () => {
        component.datasetList = DATASET_LIST;
        component.datasetName = 'cat_1';
        expect(component.getDatasetLabel()).toEqual('Cat 1');
    });

    it('#getConfigDownloadResultFormat() should return if download button for the given format has to be displayed', () => {
        component.datasetList = DATASET_LIST;
        component.datasetName = 'cat_1';
        expect(component.getConfigDownloadResultFormat('csv')).toBeTruthy();
        expect(component.getConfigDownloadResultFormat('ascii')).toBeFalsy();
    });

    it('#getUrl(format) should construct url with format parameter', () => {
        component.datasetName = 'dataset';
        component.outputList = [1, 2, 3];
        component.criteriaList = CRITERIA_LIST;
        component.isConeSearchAdded = true;
        component.coneSearch = { ra: 4, dec: 5, radius: 6 } as ConeSearch;
        expect(component.getUrl('csv')).toBe(environment.apiUrl + '/search/dataset?a=1;2;3&c=1::eq::fd_crit_1;2::eq::fd_crit_2&cs=4:5:6&f=csv');
        component.criteriaList = [];
        component.isConeSearchAdded = false;
        expect(component.getUrl('csv')).toBe(environment.apiUrl + '/search/dataset?a=1;2;3&f=csv');
    });
});

