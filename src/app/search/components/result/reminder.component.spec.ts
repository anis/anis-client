import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { ReminderComponent } from './reminder.component';
import { Criterion } from '../../store/model';
import { ATTRIBUTE_LIST, CATEGORY_LIST, CRITERIA_LIST } from '../../../../settings/test-data';

describe('[Search][Result] Component: ReminderComponent', () => {
    let component: ReminderComponent;
    let fixture: ComponentFixture<ReminderComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ReminderComponent],
            imports: [
                TabsModule.forRoot(),
                AccordionModule.forRoot()
            ]
        });
        fixture = TestBed.createComponent(ReminderComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#nbCriteria() should return criteria amount', () => {
        component.isConeSearchAdded = false;
        component.criteriaList = [];
        expect(component.nbCriteria()).toEqual(0);
        component.isConeSearchAdded = true;
        expect(component.nbCriteria()).toEqual(1);
    });

    it('#criteriaByFamily() should return criteria for the given family', () => {
        component.datasetAttributeList = ATTRIBUTE_LIST;
        component.criteriaList = CRITERIA_LIST;
        expect(component.criteriaByFamily(1).length).toEqual(1);
        expect(component.criteriaByFamily(1)[0].id).toEqual(1);
    });

    it('#getAttribute() should return the attribute with the given id', () => {
        component.datasetAttributeList = ATTRIBUTE_LIST;
        expect(component.getAttribute(1).name).toBe('name_one');
    });

    it('#printCriterion() should return pretty criterion', () => {
        const criterion: Criterion = CRITERIA_LIST.find(c => c.id === 1);
        expect(component.printCriterion(criterion)).toBe('= fd_crit_1');
    });

    it('#categoryListByFamily() should return output categories for the given output family', () => {
        component.categoryList = CATEGORY_LIST;
        expect(component.categoryListByFamily(1).length).toEqual(2);
        expect(component.categoryListByFamily(1)[0].id).toEqual(1);
        expect(component.categoryListByFamily(1)[1].id).toEqual(2);
    });

    it('#outputListByCategory() should return outputs for the given output category', () => {
        component.datasetAttributeList = ATTRIBUTE_LIST;
        component.outputList = [1, 2];
        expect(component.outputListByCategory(1).length).toEqual(1);
        expect(component.outputListByCategory(1)[0]).toEqual(1);
    });
});
