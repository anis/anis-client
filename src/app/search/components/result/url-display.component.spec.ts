import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { UrlDisplaySectionComponent } from './url-display.component';
import { ConeSearch } from '../../../shared/cone-search/store/model';
import { CRITERIA_LIST, DATASET_LIST } from '../../../../settings/test-data';
import { environment } from '../../../../environments/environment';

describe('[Search][Result] Component: UrlDisplayComponent', () => {
    let component: UrlDisplaySectionComponent;
    let fixture: ComponentFixture<UrlDisplaySectionComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [UrlDisplaySectionComponent],
            imports: [AccordionModule.forRoot(), BrowserAnimationsModule, ToastrModule.forRoot()],
            providers: [ToastrService]
        });
        fixture = TestBed.createComponent(UrlDisplaySectionComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#urlDisplayEnabled() should return if component is enabled or not', () => {
        component.datasetList = DATASET_LIST;
        component.datasetName = 'cat_1';
        expect(component.urlDisplayEnabled()).toBeTruthy();
        component.datasetName = 'cat_2';
        expect(component.urlDisplayEnabled()).toBeFalsy();
    });

    it('#getUrl() should construct url', () => {
        component.datasetName = 'dataset';
        component.outputList = [1, 2, 3];
        component.criteriaList = [];
        component.isConeSearchAdded = false;
        let url = component.getUrl();
        expect(url).toBe(environment.apiUrl + '/search/dataset?a=1;2;3');
        component.criteriaList = CRITERIA_LIST;
        url = component.getUrl();
        expect(url).toBe(environment.apiUrl + '/search/dataset?a=1;2;3&c=1::eq::fd_crit_1;2::eq::fd_crit_2');
        component.criteriaList = [];
        component.isConeSearchAdded = true;
        component.coneSearch = { ra: 4, dec: 5, radius: 6 } as ConeSearch;
        url = component.getUrl();
        expect(url).toBe(environment.apiUrl + '/search/dataset?a=1;2;3&cs=4:5:6');
    });
});

