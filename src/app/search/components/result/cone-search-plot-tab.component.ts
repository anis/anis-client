/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ConeSearch } from 'src/app/shared/cone-search/store/model';

import { Attribute, Dataset } from '../../../metamodel/model';

@Component({
    selector: 'app-cone-search-plot-tab',
    templateUrl: 'cone-search-plot-tab.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConeSearchPlotTabComponent implements OnChanges {
    @Input() datasetName: string;
    @Input() datasetList: Dataset[];
    @Input() datasetAttributeList: Attribute[];
    @Input() searchData: any[];
    @Input() dataLengthIsLoaded: boolean;
    @Input() dataLength: number;
    @Input() isConeSearchAdded: boolean;
    @Input() coneSearch: ConeSearch;

    backgroundList = [];
    selectedBackground = null;

    ngOnChanges(changes: SimpleChanges) {
        if (changes.datasetList) {
            if (changes.datasetList.firstChange) {
                this.setBackgroundList();
            }
        }
    }

    /**
     * Checks if datatable accordion should be open.
     *
     * @return boolean
     */
    isConeSearchOpened(): boolean {
        const config = this.getDataset().config;
        return config.cone_search.opened;
    }

    /**
     * Checks if URL display is enabled.
     *
     * @return boolean
     */
    checkDisplayPlot(): boolean {
        return this.getDataset().config.cone_search.plot_enabled;
    }

    getData() {
        const dataset = this.getDataset();
        const columnRa = this.datasetAttributeList.find(a => a.id === dataset.config.cone_search.column_ra);
        const columnDec = this.datasetAttributeList.find(a => a.id === dataset.config.cone_search.column_dec);
        const data = this.searchData.map(d => ({ "x": +d[columnRa.label], "y": +d[columnDec.label] }));
        return data;
    }

    /**
     * Returns selected dataset for the search.
     *
     * @return Dataset
     */
    getDataset(): Dataset {
        return this.datasetList.find(dataset => dataset.name === this.datasetName);
    }

    setBackgroundList() {
        let background = [];
        const dataset = this.getDataset();
        if (dataset.config.cone_search.sdss_enabled) {
            background.push({id: 0, name: "SDSS DR16", display: dataset.config.cone_search.sdss_display})
        }

        dataset.config.cone_search.background.forEach(b => {
            if (b.enabled) {
                const image = dataset.config.images.find(i => i.id === b.id);
                background.push({
                    id: b.id,
                    display: b.display,
                    ...image
                });
            }
        })

        if (background.length > 0) {
            background.sort((a, b) => a.display - b.display);
            this.backgroundList = background;
            this.selectedBackground = background[0].id;
        }
    }
}
