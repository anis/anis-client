/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';

import { Criterion } from '../../store/model';
import { Dataset, Attribute, Category, Family } from '../../../metamodel/model';
import { printCriterion as print } from '../../../shared/utils'
import { ConeSearch } from '../../../shared/cone-search/store/model';

@Component({
    selector: 'app-reminder',
    templateUrl: 'reminder.component.html',
    styleUrls: ['reminder.component.css']
})
/**
 * @class
 * @classdesc Search result reminder component.
 */
export class ReminderComponent {
    @Input() datasetName: string;
    @Input() datasetList: Dataset[];
    @Input() datasetAttributeList: Attribute[];
    @Input() dataLengthIsLoaded:boolean;
    @Input() isConeSearchAdded: boolean;
    @Input() coneSearch: ConeSearch;
    @Input() criteriaList: Criterion[];
    @Input() criteriaFamilyList: Family[];
    @Input() outputFamilyList: Family[];
    @Input() categoryList: Category[];
    @Input() outputList: number[];

    isSummaryActivated(): boolean {
        const dataset = this.datasetList.find(dataset => dataset.name === this.datasetName);
        return dataset.config.summary.enabled;
    }

    isSummaryOpened(): boolean {
        const dataset = this.datasetList.find(dataset => dataset.name === this.datasetName);
        return dataset.config.summary.opened;
    }

    /**
     * Returns total of added criteria.
     *
     * @return number
     */
    nbCriteria(): number {
        if (this.isConeSearchAdded) {
            return this.criteriaList.length + 1;
        }
        return this.criteriaList.length;
    }

    /**
     * Returns criteria list for the given criteria family ID.
     *
     * @param  {number} idFamily - The criteria family ID.
     *
     * @return Criterion[]
     */
    criteriaByFamily(idFamily: number): Criterion[] {
        const attributeListByFamily: Attribute[] = this.datasetAttributeList
            .filter(attribute => attribute.id_criteria_family === idFamily);
        return this.criteriaList
            .filter(criterion => attributeListByFamily.includes(
                this.datasetAttributeList.find(attribute => attribute.id === criterion.id))
            );
    }

    /**
     * Returns attribute for the given attribute ID.
     *
     * @param  {number} id - The attribute ID.
     *
     * @return Attribute
     */
    getAttribute(id: number): Attribute {
        return this.datasetAttributeList.find(attribute => attribute.id === id);
    }

    /**
     * Returns criterion pretty printed.
     *
     * @param  {Criterion} criterion - The criterion.
     *
     * @return string
     */
    printCriterion(criterion: Criterion): string {
        return print(criterion);
    }

    /**
     * Returns category list for the given criteria family ID.
     *
     * @param  {number} idFamily - The criteria family ID.
     *
     * @return Category[]
     */
    categoryListByFamily(idFamily: number): Category[] {
        return this.categoryList.filter(category => category.id_output_family === idFamily);
    }

    /**
     * Returns output list for the given category ID.
     *
     * @param  {number} idCategory - The output category ID.
     *
     * @return number[]
     */
    outputListByCategory(idCategory: number): number[] {
        const attributeListByCategory: Attribute[] = this.datasetAttributeList
            .filter(attribute => attribute.id_output_category === idCategory);
        return this.outputList
            .filter(output => attributeListByCategory.includes(
                this.datasetAttributeList.find(attribute => attribute.id === output))
            );
    }
}
