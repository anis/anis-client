/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { SearchQueryParams } from '../store/model';

@Component({
    selector: 'app-progress-bar',
    templateUrl: 'progress-bar.component.html',
    styleUrls: ['progress-bar.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search progress bar component.
 */
export class ProgressBarComponent {
    @Input() currentStep: string;
    @Input() datasetName: string;
    @Input() criteriaStepChecked: boolean;
    @Input() outputStepChecked: boolean;
    @Input() resultStepChecked: boolean;
    @Input() queryParams: SearchQueryParams;
    @Input() outputListEmpty: boolean;

    /**
     * Returns step class that match to the current step.
     *
     * @return string
     */
    getStepClass(): string {
        switch (this.currentStep) {
            case 'dataset':
                return 'datasetStep';
            case 'criteria':
                return 'criteriaStep';
            case 'output':
                return 'outputStep';
            case 'result':
                return 'resultStep';
            default:
                return 'datasetStep';
        }
    }
}
