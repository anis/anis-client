import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { DatasetTabsComponent } from './dataset-tabs.component';
import { Survey, Dataset, Family } from '../../../metamodel/model';
import { SURVEY_LIST, DATASET_LIST, DATASET_FAMILY_LIST, DATASET } from '../../../../settings/test-data';

describe('[Search][Dataset] Component: DatasetTabsComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-dataset-tabs
                [surveyList]="surveyList"
                [datasetList]="datasetList"
                [datasetFamilyList]="datasetFamilyList"
                [datasetSelected]="datasetSelected">
            </app-dataset-tabs>
        `
    })
    class TestHostComponent {
        @ViewChild(DatasetTabsComponent, { static: false })
        public testedComponent: DatasetTabsComponent;
        public surveyList: Survey[] = SURVEY_LIST;
        public datasetList: Dataset[] = DATASET_LIST;
        public datasetFamilyList: Family[] = DATASET_FAMILY_LIST;
        public datasetSelected = 'cat_1';
    }

    @Component({ selector: 'app-dataset-card', template: '' })
    class DatasetCardStubComponent {
        @Input() survey: Survey;
        @Input() dataset: Dataset;
        @Input() datasetSelected: string;
        @Output() select: EventEmitter<string> = new EventEmitter();
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: DatasetTabsComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetTabsComponent,
                TestHostComponent,
                DatasetCardStubComponent
            ],
            imports: [AccordionModule.forRoot(), BrowserAnimationsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getNbDatasetByFamily() should return the number of dataset for the given dataset family', () => {
        expect(testedComponent.getNbDatasetByFamily(1)).toBe(1);
    });

    it('#getDatasetListByFamily(idFamily) should filter datasets by idFamily', () => {
        const filteredDatasetList: Dataset[] = testedComponent.getDatasetListByFamily(1);
        expect(filteredDatasetList.length).toBe(1);
        expect(filteredDatasetList[0].name).toBe('cat_1');
    });

    it('#getSurvey(dataset) should return the dataset survey', () => {
        const survey = testedComponent.getSurvey(DATASET);
        expect(survey.name).toBe('survey_1');
    });
    
    it('should select dataset if only one dataset in instance', () => {
        testedComponent.select.subscribe((event: string) => expect(event).toBe('cat_1'));
        testHostComponent.datasetList = [DATASET]
        testHostComponent.datasetSelected = null;
        testHostFixture.detectChanges();
    });
});

