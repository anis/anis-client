import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';

import { PopoverModule } from 'ngx-bootstrap/popover';

import { DatasetCardComponent } from './dataset-card.component';
import { Dataset, Survey } from '../../../metamodel/model';
import { DATASET, SURVEY } from '../../../../settings/test-data';

describe('[Search][Dataset] Component: DatasetCardComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-dataset-card
                [survey]="survey"
                [dataset]="dataset"
                [datasetSelected]="datasetSelected">
            </app-dataset-card>
        `
    })
    class TestHostComponent {
        @ViewChild(DatasetCardComponent, { static: false })
        public testedComponent: DatasetCardComponent;
        public survey: Survey = SURVEY;
        public dataset: Dataset = DATASET;
        public datasetSelected = 'cat_1';
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: DatasetCardComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetCardComponent,
                TestHostComponent
            ],
            imports: [PopoverModule.forRoot()]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });
});
