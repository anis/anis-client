/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { Survey, Dataset } from '../../../metamodel/model';

@Component({
    selector: 'app-dataset-card',
    templateUrl: 'dataset-card.component.html',
    styleUrls: [ 'dataset-card.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search dataset card component.
 */
export class DatasetCardComponent {
    @Input() survey: Survey;
    @Input() dataset: Dataset;
    @Input() datasetSelected: string;
    @Output() select: EventEmitter<string> = new EventEmitter();
}
