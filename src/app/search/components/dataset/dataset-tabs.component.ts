/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Survey, Dataset, Family } from '../../../metamodel/model';
import { sortByDisplay } from '../../../shared/utils';

@Component({
    selector: 'app-dataset-tabs',
    templateUrl: 'dataset-tabs.component.html',
    styleUrls: ['dataset-tabs.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search dataset tabs component.
 */
export class DatasetTabsComponent {
    @Input() surveyList: Survey[];
    @Input() datasetList: Dataset[];
    @Input() datasetFamilyList: Family[];
    /**
     * Emits event to select automatically dataset if none selected and only one in dataset list.
     *
     * @param  {string} datasetSelected - The selected dataset name.
     */
    @Input()
    set datasetSelected(datasetSelected: string) {
        this.dnameSelected = datasetSelected;
        if (this.dnameSelected === null && this.datasetList.length === 1) {
            this.select.emit(this.datasetList[0].name);
        }
    }
    @Output() select: EventEmitter<string> = new EventEmitter();
    
    dnameSelected: string;

    /**
     * Returns number of datasets for the given dataset family ID.
     *
     * @param  {number} idFamily - The dataset family ID.
     *
     * @return number
     */
    getNbDatasetByFamily(idFamily: number): number {
        return this.getDatasetListByFamily(idFamily).length;
    }

    /**
     * Returns dataset list sorted by display, for the given dataset family ID.
     *
     * @param  {number} idFamily - The dataset family ID.
     *
     * @return Dataset[]
     */
    getDatasetListByFamily(idFamily: number): Dataset[] {
        return this.datasetList
            .filter(d => d.id_dataset_family === idFamily)
            .sort(sortByDisplay);
    }

    /**
     * Returns survey for the given dataset.
     *
     * @param  {Dataset} dataset - The dataset.
     *
     * @return Survey
     */
    getSurvey(dataset: Dataset): Survey {
        return this.surveyList.find(survey => survey.name === dataset.survey_name);
    }
}
