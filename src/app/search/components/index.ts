import { ProgressBarComponent } from './progress-bar.component';
import { SummaryComponent } from './summary.component';

import { DatasetTabsComponent } from './dataset/dataset-tabs.component';
import { DatasetCardComponent } from './dataset/dataset-card.component';

import { ConeSearchTabComponent } from './criteria/cone-search-tab.component';
import { CriteriaTabsComponent } from './criteria/criteria-tabs.component';
import { CriteriaByFamilyComponent } from './criteria/criteria-by-family.component';
import { SearchTypeComponents } from './criteria/search-type';

import { OutputTabsComponent } from './output/output-tabs.component';
import { OutputByFamilyComponent } from './output/output-by-family.component';
import { OutputByCategoryComponent } from './output/output-by-category.component';

import { DownloadComponent } from './result/download.component';
import { ReminderComponent } from './result/reminder.component';
import { UrlDisplaySectionComponent } from './result/url-display.component';
import { DatatableTabComponent } from './result/datatable-tab.component';
import { ConeSearchPlotTabComponent } from './result/cone-search-plot-tab.component';
import { SampComponent } from './result/samp.component';

export const dummiesComponents = [
    ProgressBarComponent,
    SummaryComponent,

    DatasetTabsComponent,
    DatasetCardComponent,

    ConeSearchTabComponent,
    CriteriaTabsComponent,
    CriteriaByFamilyComponent,
    SearchTypeComponents,
    
    OutputTabsComponent,
    OutputByFamilyComponent,
    OutputByCategoryComponent,
    
    DownloadComponent,
    ReminderComponent,
    UrlDisplaySectionComponent,
    DatatableTabComponent,
    ConeSearchPlotTabComponent,
    SampComponent
];
