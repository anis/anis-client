/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Family, Category, Attribute } from '../../../metamodel/model';
import { sortByDisplay } from '../../../shared/utils';

@Component({
    selector: 'app-output-by-family',
    templateUrl: 'output-by-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search output by family component.
 */
export class OutputByFamilyComponent {
    @Input() outputFamily: Family;
    @Input() categoryList: Category[];
    @Input() attributeList: Attribute[];
    @Input() outputList: number[];
    @Output() change: EventEmitter<number[]> = new EventEmitter();

    /**
     * Returns category list sorted by display, for the given output family ID.
     *
     * @param  {number} idFamily - The output family ID.
     *
     * @return Category[]
     */
    getCategoryByFamilySortedByDisplay(idFamily: number): Category[] {
        return this.categoryList
            .filter(category => category.id_output_family === idFamily)
            .sort(sortByDisplay);
    }

    /**
     * Returns output list that belongs to the given category ID.
     *
     * @param  {number} idCategory - The output category ID.
     *
     * @return Attribute[]
     */
    getAttributeByCategory(idCategory: number): Attribute[] {
        return this.attributeList
            .filter(attribute => attribute.id_output_category === idCategory);
    }

    /**
     * Checks if all outputs for the given category ID are selected.
     *
     * @param  {number} idCategory - The output category ID.
     *
     * @return boolean
     */
    getIsAllSelected(idCategory: number): boolean {
        const attributeListId = this.getAttributeByCategory(idCategory).map(a => a.id);
        const filteredOutputList = this.outputList.filter(id => attributeListId.indexOf(id) > -1);
        return attributeListId.length === filteredOutputList.length;
    }

    /**
     * Checks if all outputs for the given category ID are unselected.
     *
     * @param  {number} idCategory - The output category ID.
     *
     * @return boolean
     */
    getIsAllUnselected(idCategory: number): boolean {
        const attributeListId = this.getAttributeByCategory(idCategory).map(a => a.id);
        const filteredOutputList = this.outputList.filter(id => attributeListId.indexOf(id) > -1);
        return filteredOutputList.length === 0;
    }

    /**
     * Emits update output list event with updated sorted output list given.
     *
     * @param  {number[]} clonedOutputList - The updated output list.
     *
     * @fires EventEmitter<number[]>
     */
    emitChange(clonedOutputList: number[]): void {
        this.change.emit(
            this.attributeList
                .filter(a => clonedOutputList.indexOf(a.id) > -1)
                .sort((a, b) => a.output_display - b.output_display)
                .map(a => a.id)
        );
    }
}
