/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { Family, Category, Attribute } from '../../../metamodel/model';

@Component({
    selector: 'app-output-tabs',
    templateUrl: 'output-tabs.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search output tab component.
 */
export class OutputTabsComponent {
    @Input() outputFamilyList: Family[];
    @Input() categoryList: Category[];
    @Input() attributeList: Attribute[];
    @Input() outputList: number[];
    /**
     * Displays output list empty warning notifications.
     *
     * @param  {boolean} outputListEmpty - Is output list empty.
     */
    @Input()
    set outputListEmpty(outputListEmpty: boolean) {
        if (outputListEmpty) {
            this.toastr.warning('At least 1 output is required!');
        }
    }
    @Output() change: EventEmitter<number[]> = new EventEmitter();

    constructor(private toastr: ToastrService) { }
}
