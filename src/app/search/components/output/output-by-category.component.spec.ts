import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutputByCategoryComponent } from './output-by-category.component';
import { Attribute } from '../../../metamodel/model';
import { ATTRIBUTE_LIST } from '../../../../settings/test-data';

describe('[Search][Output] Component: OutputByCategoryComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-output-by-category
                [categoryLabel]='categoryLabel'
                [attributeList]='attributeList'
                [outputList]='outputList'
                [isAllSelected]='isAllSelected'
                [isAllUnselected]='isAllUnselected'>
            </app-output-by-category>`
    })
    class TestHostComponent {
        @ViewChild(OutputByCategoryComponent, { static: false })
        public testedComponent: OutputByCategoryComponent;
        public categoryLabel = 'Default output category';
        public attributeList: Attribute[] = ATTRIBUTE_LIST;
        public outputList: number[] = [1];
        public isAllSelected = true;
        public isAllUnselected: false;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: OutputByCategoryComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputByCategoryComponent,
                TestHostComponent
            ]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getAttributeListSortedByDisplay() should sort attributeList by output_display', () => {
        const sortedAttributeList: Attribute[] = testedComponent.getAttributeListSortedByDisplay();
        expect(sortedAttributeList[0].id).toBe(2);
        expect(sortedAttributeList[1].id).toBe(1);
    });

    it('#isSelected(idOutput) should return true if output is selected', () => {
        expect(testedComponent.isSelected(1)).toBeTruthy();
    });

    it('#isSelected(idOutput) should return false if output is not selected', () => {
        expect(testedComponent.isSelected(2)).toBeFalsy();
    });

    it('#toggleSelection(idOutput) should remove idOutput from outputList and raise change event', () => {
        const idOutput = 1;
        const expectedOutputList = [];
        testedComponent.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        testedComponent.toggleSelection(idOutput);
    });

    it('#toggleSelection(idOutput) should add idOutput to outputList and raise change event', () => {
        const idOutput = 2;
        const expectedOutputList = [1, 2];
        testedComponent.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        testedComponent.toggleSelection(idOutput);
    });

    it('#selectAll() should add all outputs to outputList and raise change event', () => {
        const expectedOutputList = [1, 2];
        testedComponent.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        testedComponent.selectAll();
    });

    it('#unselectAll() should remove all outputs to outputList and raise change event', () => {
        const expectedOutputList = [];
        testedComponent.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        testedComponent.unselectAll();
    });
});

