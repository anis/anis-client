import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { OutputTabsComponent } from './output-tabs.component';
import { Attribute, Family, Category } from '../../../metamodel/model';
import { ATTRIBUTE_LIST, OUTPUT_FAMILY_LIST , CATEGORY_LIST } from '../../../../settings/test-data';

describe('[Search][Output] Component: OutputTabsComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-output-tabs
                [outputFamilyList]='outputFamilyList'
                [categoryList]='categoryList'
                [attributeList]='attributeList'
                [outputList]='outputList'
                [outputListEmpty]='outputListEmpty'>
            </app-output-tabs>`
    })
    class TestHostComponent {
        @ViewChild(OutputTabsComponent, { static: false })
        public testedComponent: OutputTabsComponent;
        public outputFamilyList: Family[] = OUTPUT_FAMILY_LIST;
        public attributeList: Attribute[] = ATTRIBUTE_LIST;
        public categoryList: Category[] = CATEGORY_LIST;
        public outputList: number[] = [1];
        public outputListEmpty: boolean = true;
    }

    @Component({ selector: 'app-output-by-family', template: '' })
    class OutputByFamilyStubComponent {
        @Input() outputFamily: Family;
        @Input() categoryList: Category[];
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Output() change: EventEmitter<number[]> = new EventEmitter();
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: OutputTabsComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputTabsComponent,
                TestHostComponent,
                OutputByFamilyStubComponent
            ],
            imports: [AccordionModule.forRoot(), BrowserAnimationsModule, ToastrModule.forRoot()],
            providers: [ToastrService]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });
});

