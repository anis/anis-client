import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutputByFamilyComponent } from './output-by-family.component';
import { Attribute, Family, Category } from '../../../metamodel/model';
import { ATTRIBUTE_LIST, OUTPUT_FAMILY, CATEGORY_LIST } from '../../../../settings/test-data';

describe('[Search][Output] Component: OutputByFamilyComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-output-by-family
                [outputFamily]='outputFamily'
                [attributeList]='attributeList'
                [categoryList]='categoryList'
                [outputList]='outputList'>
            </app-output-by-family>`
    })
    class TestHostComponent {
        @ViewChild(OutputByFamilyComponent, { static: false })
        public testedComponent: OutputByFamilyComponent;
        public outputFamily: Family = OUTPUT_FAMILY;
        public attributeList: Attribute[] = ATTRIBUTE_LIST;
        public categoryList: Category[] = CATEGORY_LIST;
        public outputList: number[] = [1];
    }

    @Component({ selector: 'app-output-by-category', template: '' })
    class OutputByCategoryStubComponent {
        @Input() categoryLabel: string;
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Input() isAllSelected: boolean;
        @Input() isAllUnselected: boolean;
        @Output() change: EventEmitter<number[]> = new EventEmitter();
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: OutputByFamilyComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputByFamilyComponent,
                TestHostComponent,
                OutputByCategoryStubComponent
            ]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getCategoryByFamilySortedByDisplay(idFamily) should sort by display categories belonging to idFamily', () => {
        const sortedCategoryList: Category[] = testedComponent.getCategoryByFamilySortedByDisplay(1);
        expect(sortedCategoryList.length).toBe(2);
        expect(sortedCategoryList[0].id).toBe(2);
        expect(sortedCategoryList[1].id).toBe(1);
    });

    it('#getAttributeByCategory(idCategory) should return attributes belonging to idCategory', () => {
        expect(testedComponent.getAttributeByCategory(1).length).toBe(1);
        expect(testedComponent.getAttributeByCategory(1)[0].id).toBe(1);
    });

    it('#getIsAllSelected(idCategory) should return true if all outputs of idCategory are selected', () => {
        expect(testedComponent.getIsAllSelected(1)).toBeTruthy();
    });

    it('#getIsAllSelected(idCategory) should return false if not all outputs of idCategory are selected', () => {
        testedComponent.outputList = [];
        expect(testedComponent.getIsAllSelected(1)).toBeFalsy();
    });

    it('#getIsAllUnselected(idCategory) should return true if all outputs of idCategory are not selected', () => {
        testedComponent.outputList = [];
        expect(testedComponent.getIsAllUnselected(1)).toBeTruthy();
    });

    it('#getIsAllUnselected(idCategory) should return false if not all outputs of idCategory are not selected', () => {
        expect(testedComponent.getIsAllUnselected(1)).toBeFalsy();
    });

    it('#emitChange(outputList) should raise change event', () => {
        const expectedOutputList = [1];
        testedComponent.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        testedComponent.emitChange([1]);
    });
});

