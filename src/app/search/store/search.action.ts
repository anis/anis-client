/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import { Criterion } from './model';
import { Pagination } from '../../shared/datatable/model';

export const INIT_SEARCH_BY_URL = '[Search] Init Search By Url';
export const CHANGE_STEP = '[Search] Change Search Step';
export const SELECT_DATASET = '[Search] Select Dataset';
export const NEW_SEARCH = '[Search] New Search';
export const CRITERIA_CHECKED = '[Search] Criteria Checked';
export const OUTPUT_CHECKED = '[Search] Output Checked';
export const RESULT_CHECKED = '[Search] Result Checked';
export const IS_CONE_SEARCH_ADDED = '[Search] Is Cone Search Added';
export const UPDATE_CRITERIA_LIST = '[Search] Update Criteria List';
export const ADD_CRITERION = '[Search] Add Criterion';
export const DELETE_CRITERION = '[Search] Delete Criterion';
export const UPDATE_OUTPUT_LIST = '[Search] Update Output List';
export const RETRIEVE_DATA = '[Search] Retrieve Data';
export const RETRIEVE_DATA_SUCCESS = '[Search] Retrieve Data Success';
export const RETRIEVE_DATA_FAIL = '[Search] Retrieve Data Fail';
export const GET_DATA_LENGTH = '[Search] Get Data Length';
export const GET_DATA_LENGTH_SUCCESS = '[Search] Get Data Length Success';
export const GET_DATA_LENGTH_FAIL = '[Search] Get Data Length Fail';
export const ADD_SELECTED_DATA = '[Search] Add Selected Data';
export const DELETE_SELECTED_DATA = '[Search] Delete Selected Data';
export const EXECUTE_PROCESS = '[Search] Execute Process';
export const EXECUTE_PROCESS_WIP = '[Search] Execute Process WIP';
export const EXECUTE_PROCESS_SUCCESS = '[Search] Execute Process Success';
export const EXECUTE_PROCESS_FAIL = '[Search] Execute Process Fail';
export const DESTROY_RESULTS = '[Search] Destroy Results';
export const RESET_SEARCH = '[Search] Reset Search';

/**
 * @class
 * @classdesc InitSearchByUrl action.
 * @readonly
 */
export class InitSearchByUrl implements Action {
    readonly type = INIT_SEARCH_BY_URL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc ChangeStepAction action.
 * @readonly
 */
export class ChangeStepAction implements Action {
    readonly type = CHANGE_STEP;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc SelectDatasetAction action.
 * @readonly
 */
export class SelectDatasetAction implements Action {
    readonly type = SELECT_DATASET;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc NewSearchAction action.
 * @readonly
 */
export class NewSearchAction implements Action {
    readonly type = NEW_SEARCH;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc CriteriaChecked action.
 * @readonly
 */
export class CriteriaChecked implements Action {
    readonly type = CRITERIA_CHECKED;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc OutputChecked action.
 * @readonly
 */
export class OutputChecked implements Action {
    readonly type = OUTPUT_CHECKED;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc ResultChecked action.
 * @readonly
 */
export class ResultChecked implements Action {
    readonly type = RESULT_CHECKED;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc IsConeSearchAddedAction action.
 * @readonly
 */
export class IsConeSearchAddedAction implements Action {
    readonly type = IS_CONE_SEARCH_ADDED;

    constructor(public payload: boolean) { }
}

/**
 * @class
 * @classdesc UpdateCriteriaListAction action.
 * @readonly
 */
export class UpdateCriteriaListAction implements Action {
    readonly type = UPDATE_CRITERIA_LIST;

    constructor(public payload: Criterion[]) { }
}

/**
 * @class
 * @classdesc AddCriterionAction action.
 * @readonly
 */
export class AddCriterionAction implements Action {
    readonly type = ADD_CRITERION;

    constructor(public payload: Criterion) { }
}

/**
 * @class
 * @classdesc DeleteCriterionAction action.
 * @readonly
 */
export class DeleteCriterionAction implements Action {
    readonly type = DELETE_CRITERION;

    constructor(public payload: number) { }
}

/**
 * @class
 * @classdesc UpdateOutputListAction action.
 * @readonly
 */
export class UpdateOutputListAction implements Action {
    readonly type = UPDATE_OUTPUT_LIST;

    constructor(public payload: number[]) { }
}

/**
 * @class
 * @classdesc RetrieveDataAction action.
 * @readonly
 */
export class RetrieveDataAction implements Action {
    readonly type = RETRIEVE_DATA;

    constructor(public payload: Pagination) { }
}

/**
 * @class
 * @classdesc RetrieveDataSuccessAction action.
 * @readonly
 */
export class RetrieveDataSuccessAction implements Action {
    readonly type = RETRIEVE_DATA_SUCCESS;

    constructor(public payload: any[]) { }
}

/**
 * @class
 * @classdesc RetrieveDataFailAction action.
 * @readonly
 */
export class RetrieveDataFailAction implements Action {
    readonly type = RETRIEVE_DATA_FAIL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc GetDataLengthAction action.
 * @readonly
 */
export class GetDataLengthAction implements Action {
    readonly type = GET_DATA_LENGTH;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc GetDataLengthSuccessAction action.
 * @readonly
 */
export class GetDataLengthSuccessAction implements Action {
    readonly type = GET_DATA_LENGTH_SUCCESS;

    constructor(public payload: number) { }
}

/**
 * @class
 * @classdesc GetDataLengthFailAction action.
 * @readonly
 */
export class GetDataLengthFailAction implements Action {
    readonly type = GET_DATA_LENGTH_FAIL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc AddSelectedDataAction action.
 * @readonly
 */
export class AddSelectedDataAction implements Action {
    readonly type = ADD_SELECTED_DATA;

    constructor(public payload: number | string) { }
}

/**
 * @class
 * @classdesc DeleteSelectedDataAction action.
 * @readonly
 */
export class DeleteSelectedDataAction implements Action {
    readonly type = DELETE_SELECTED_DATA;

    constructor(public payload: number | string) { }
}

/**
 * @class
 * @classdesc ExecuteProcessAction action.
 * @readonly
 */
export class ExecuteProcessAction implements Action {
    readonly type = EXECUTE_PROCESS;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc ExecuteProcessWipAction action.
 * @readonly
 */
export class ExecuteProcessWipAction implements Action {
    readonly type = EXECUTE_PROCESS_WIP;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc ExecuteProcessSuccessAction action.
 * @readonly
 */
export class ExecuteProcessSuccessAction implements Action {
    readonly type = EXECUTE_PROCESS_SUCCESS;

    constructor(public payload: any) { }
}

/**
 * @class
 * @classdesc ExecuteProcessFailAction action.
 * @readonly
 */
export class ExecuteProcessFailAction implements Action {
    readonly type = EXECUTE_PROCESS_FAIL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc DestroyResultsAction action.
 * @readonly
 */
export class DestroyResultsAction implements Action {
    readonly type = DESTROY_RESULTS;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc ResetSearchAction action.
 * @readonly
 */
export class ResetSearchAction implements Action {
    readonly type = RESET_SEARCH;

    constructor(public payload: {} = null) { }
}

export type Actions
    = InitSearchByUrl
    | ChangeStepAction
    | SelectDatasetAction
    | NewSearchAction
    | CriteriaChecked
    | OutputChecked
    | ResultChecked
    | IsConeSearchAddedAction
    | UpdateCriteriaListAction
    | AddCriterionAction
    | DeleteCriterionAction
    | UpdateOutputListAction
    | RetrieveDataAction
    | RetrieveDataSuccessAction
    | RetrieveDataFailAction
    | GetDataLengthAction
    | GetDataLengthSuccessAction
    | GetDataLengthFailAction
    | AddSelectedDataAction
    | DeleteSelectedDataAction
    | ExecuteProcessAction
    | ExecuteProcessWipAction
    | ExecuteProcessSuccessAction
    | ExecuteProcessFailAction
    | DestroyResultsAction
    | ResetSearchAction;
