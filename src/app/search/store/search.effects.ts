/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Store, Action } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as fromRouter from '@ngrx/router-store';
import { of } from 'rxjs';
import { map, tap, switchMap, withLatestFrom, catchError, delay } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as fromSearch from './search.reducer';
import * as searchActions from './search.action';
import { SearchService } from './search.service';
import * as fromMetamodel from '../../metamodel/reducers';
import * as attributeActions from '../../metamodel/action/attribute.action';
import * as criteriaActions from '../../metamodel/action/criteria.action';
import * as outputActions from '../../metamodel/action/output.action';
import { Option } from '../../metamodel/model';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import * as coneSearchActions from '../../shared/cone-search/store/cone-search.action';
import { ConeSearch } from '../../shared/cone-search/store/model';
import * as utils from '../../shared/utils';
import { getCriterionStr } from '../../shared/utils';

@Injectable()
/**
 * @class
 * @classdesc Search effects.
 */
export class SearchEffects {
    constructor(
        private actions$: Actions,
        private searchService: SearchService,
        private toastr: ToastrService,
        private store$: Store<{
            router: fromRouter.RouterReducerState<utils.RouterStateUrl>,
            search: fromSearch.State,
            metamodel: fromMetamodel.State,
            coneSearch: fromConeSearch.State
        }>
    ) { }

    /**
     * Calls actions to fill store with data provided by the URL.
     */
    @Effect()
    initSearchByUrlAction$ = this.actions$.pipe(
        ofType(searchActions.INIT_SEARCH_BY_URL),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.search.pristine) {
                const datasetName = state.router.state.params.dname;
                const actions: Action[] = [
                    new attributeActions.LoadAttributeListAction(datasetName),
                    new searchActions.SelectDatasetAction(datasetName),
                    new criteriaActions.LoadCriteriaSearchMetaAction(datasetName),
                    new outputActions.LoadOutputSearchMetaAction(datasetName)
                ];
                if (state.router.state.queryParams.s[0] === '1') {
                    actions.push(new searchActions.CriteriaChecked());
                }
                if (state.router.state.queryParams.s[1] === '1') {
                    actions.push(new searchActions.OutputChecked());
                }
                if (state.router.state.queryParams.s[2] === '1') {
                    actions.push(new searchActions.ResultChecked());
                }
                return actions;
            } else {
                return of({ type: '[No Action] ' + searchActions.INIT_SEARCH_BY_URL });
            }
        })
    );

    /**
     * Calls actions to reset search.
     */
    @Effect()
    newSearchAction$ = this.actions$.pipe(
        ofType(searchActions.NEW_SEARCH),
        switchMap((action: searchActions.NewSearchAction) => [
            new searchActions.SelectDatasetAction(action.payload),
            new coneSearchActions.DeleteConeSearchAction()
        ])
    );

    /**
     * Calls actions when dataset attribute list is loaded to:
     *      - generate output list.
     *      - generate criteria list.
     *      - instantiate cone search.
     */
    @Effect()
    loadAttributeListSuccessAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_ATTRIBUTE_LIST_SUCCESS),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const loadAttributeListSuccessAction = action as attributeActions.LoadAttributeListSuccessAction;
            const datasetName = state.search.datasetName;

            if (loadAttributeListSuccessAction.payload.datasetName === datasetName) {
                const actions: Action[] = [];

                // Generate outputList
                let defaultOutputList: number[];
                if (state.router.state.queryParams.a) {
                    defaultOutputList = state.router.state.queryParams.a.split(';').map((o: string) => parseInt(o, 10));
                } else {
                    defaultOutputList = loadAttributeListSuccessAction.payload.attributeList
                        .filter(attribute => attribute.selected && attribute.id_output_category)
                        .sort((a, b) => a.output_display - b.output_display)
                        .map(attribute => attribute.id);
                }
                actions.push(new searchActions.UpdateOutputListAction(defaultOutputList));

                // Generate criteriaList
                let defaultCriteriaList: ({ id: number; type: string; value: string; operator: string } | { values: string[]; id: number; type: string } | { min: string; max: string; id: number; type: string } | { options: Option[]; id: number; type: string } | { path: string; id: number; type: string; value: string; operator: string } | null)[];
                if (state.router.state.queryParams.c) {
                    defaultCriteriaList = state.router.state.queryParams.c.split(';').map((c: string) => {
                        const params = c.split('::');
                        const attribute = loadAttributeListSuccessAction.payload.attributeList.find(a => a.id === parseInt(params[0], 10));
                        switch (attribute.search_type) {
                            case 'field':
                            case 'select':
                            case 'datalist':
                            case 'radio':
                            case 'date':
                            case 'date-time':
                            case 'time':
                                return { id: parseInt(params[0], 10), type: 'field', operator: params[1], value: params[2] };
                            case 'list':
                                return { id: parseInt(params[0], 10), type: 'list', values: params[2].split('|') };
                            case 'between':
                            case 'between-date':
                                if (params[1] === 'bw') {
                                    const bwValues = params[2].split('|');
                                    return { id: parseInt(params[0], 10), type: 'between', min: bwValues[0], max: bwValues[1] };
                                } else if (params[1] === 'gte') {
                                    return { id: parseInt(params[0], 10), type: 'between', min: params[2], max: null };
                                } else {
                                    return { id: parseInt(params[0], 10), type: 'between', min: null, max: params[2] };
                                }
                            case 'select-multiple':
                            case 'checkbox':
                                const msValues = params[2].split('|');
                                const options = attribute.options.filter(option => msValues.includes(option.value));
                                return { id: parseInt(params[0], 10), type: 'multiple', options };
                            case 'json':
                                const [path, operator, value] = params[2].split('|');
                                return { id: parseInt(params[0], 10), type: 'json', path, operator, value };
                            default:
                                return null;
                        }
                    });
                } else {
                    defaultCriteriaList = loadAttributeListSuccessAction.payload.attributeList
                        .filter(attribute => attribute.id_criteria_family && attribute.search_type && attribute.min)
                        .map(attribute => {
                            switch (attribute.search_type) {
                                case 'field':
                                case 'select':
                                case 'datalist':
                                case 'radio':
                                case 'date':
                                case 'date-time':
                                case 'time':
                                    return { id: attribute.id, type: 'field', value: attribute.min.toString(), operator: attribute.operator };
                                case 'list':
                                    return { id: attribute.id, type: 'list', values: attribute.min.toString().split('|') };
                                case 'between':
                                case 'between-date':
                                    return { id: attribute.id, type: 'between', min: attribute.min.toString(), max: attribute.max.toString() };
                                case 'select-multiple':
                                case 'checkbox':
                                    const msValues = attribute.min.toString().split('|');
                                    const options = attribute.options.filter(option => msValues.includes(option.value));
                                    return { id: attribute.id, type: 'multiple', options };
                                case 'json':
                                    const [path, operator, value] = attribute.min.toString().split('|');
                                    return { id: attribute.id, type: 'json', path, operator, value };
                                default:
                                    return null;
                            }
                        });
                }
                actions.push(new searchActions.UpdateCriteriaListAction(defaultCriteriaList));

                // Generate cone search from URL query param
                if (state.router.state.queryParams.cs) {
                    const params = state.router.state.queryParams.cs.split(':');
                    const coneSearch: ConeSearch = {
                        ra: parseFloat(params[0]),
                        dec: parseFloat(params[1]),
                        radius: parseInt(params[2], 10)
                    };
                    actions.push(new searchActions.IsConeSearchAddedAction(true));
                    actions.push(new coneSearchActions.AddConeSearchAction(coneSearch));
                }

                return actions;
            } else {
                return of({ type: '[No Action] ' + attributeActions.LOAD_ATTRIBUTE_LIST_SUCCESS });
            }
        })
    );

    /**
     * Calls actions to retrieve data with the user parameters.
     */
    @Effect()
    retrieveDataAction$ = this.actions$.pipe(
        ofType(searchActions.RETRIEVE_DATA),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const retrieveDataAction = action as searchActions.RetrieveDataAction;
            let query = retrieveDataAction.payload.dname + '?a=' + state.search.outputList.join(';');
            if (state.search.criteriaList.length > 0) {
                query += '&c=' + state.search.criteriaList.map(criterion => getCriterionStr(criterion)).join(';');
            }
            if (state.search.coneSearchAdded) {
                query += '&cs=' + state.coneSearch.coneSearch.ra + ':' + state.coneSearch.coneSearch.dec + ':' + state.coneSearch.coneSearch.radius;
            }
            query += '&p=' + retrieveDataAction.payload.nbItems + ':' + retrieveDataAction.payload.page;
            query += '&o=' + retrieveDataAction.payload.sortedCol + ':' + retrieveDataAction.payload.order;
            return this.searchService.retrieveData(query).pipe(
                map((searchData: any[]) => new searchActions.RetrieveDataSuccessAction(searchData)),
                catchError(() => of(new searchActions.RetrieveDataFailAction()))
            );
        })
    );

    /**
     * Displays retrieve data error notification.
     */
    @Effect({ dispatch: false })
    retrieveDataFailAction$ = this.actions$.pipe(
        ofType(searchActions.RETRIEVE_DATA_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'The search data loading failed'))
    );

    /**
     * Calls actions to retrieve data count with the user parameters.
     */
    @Effect()
    getDataLengthAction$ = this.actions$.pipe(
        ofType(searchActions.GET_DATA_LENGTH),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            let query = state.search.datasetName + '?a=count';
            if (state.search.criteriaList.length > 0) {
                query += '&c=' + state.search.criteriaList.map(criterion => getCriterionStr(criterion)).join(';');
            }
            if (state.search.coneSearchAdded) {
                query += '&cs=' + state.coneSearch.coneSearch.ra + ':' + state.coneSearch.coneSearch.dec + ':' + state.coneSearch.coneSearch.radius;
            }
            return this.searchService.getDataLength(query).pipe(
                map((response: { nb: number }[]) => new searchActions.GetDataLengthSuccessAction(response[0].nb)),
                catchError(() => of(new searchActions.GetDataLengthFailAction()))
            );
        })
    );

    /**
     * Displays retrieve data count error notification.
     */
    @Effect({ dispatch: false })
    getDataLengthFailAction$ = this.actions$.pipe(
        ofType(searchActions.GET_DATA_LENGTH_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'The data length loading failed'))
    );

    /**
     * Calls actions to execute process with the user parameters.
     */
    @Effect()
    executeProcessAction$ = this.actions$.pipe(
        ofType(searchActions.EXECUTE_PROCESS),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const executeProcessAction = action as searchActions.ExecuteProcessAction;
            const dname = state.search.datasetName;
            let query = '?a=' + state.search.outputList.join(';');
            query += '&c=';
            query += state.metamodel.attribute.entities[dname].attributeList.find(a => a.search_flag === 'ID').id;
            query += '::in::';
            query += state.search.selectedData.join('|');
            return this.searchService.executeProcess(executeProcessAction.payload, dname, query).pipe(
                map((res: any) => new searchActions.ExecuteProcessWipAction(res.message)),
                catchError(() => of(new searchActions.ExecuteProcessFailAction()))
            );
        })
    );

    /**
     * Checks if process execution is done.
     */
    @Effect()
    executeProcessWipAction$ = this.actions$.pipe(
        ofType(searchActions.EXECUTE_PROCESS_WIP),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const executeProcessWipAction = action as searchActions.ExecuteProcessWipAction;
            const idProcess: string = executeProcessWipAction.payload;
            return this.searchService.checkProcess(idProcess).pipe(
                map(_ => {
                    return new searchActions.ExecuteProcessSuccessAction(idProcess);
                }),
                catchError((err: HttpErrorResponse) => {
                    if (err.status === 404) {
                        return of(new searchActions.ExecuteProcessWipAction(idProcess)).pipe(delay(5000));
                    }
                    return of(new searchActions.ExecuteProcessFailAction());
                }));
        })
    );

    /**
     * Displays process execution error notification.
     */
    @Effect({ dispatch: false })
    executeProcessFailAction$ = this.actions$.pipe(
        ofType(searchActions.EXECUTE_PROCESS_FAIL),
        tap(_ => this.toastr.error('Action Failed!', 'The process failed'))
    );
}
