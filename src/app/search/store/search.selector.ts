/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as search from './search.reducer';
import { Criterion, SearchQueryParams } from './model';
import { getConeSearch } from '../../shared/cone-search/store/cone-search.selector';
import { getCriterionStr } from '../../shared/utils';
import { ConeSearch } from '../../shared/cone-search/store/model';

export const getSearchState = createFeatureSelector<search.State>('search');

export const getPristine = createSelector(
    getSearchState,
    search.getPristine
);

export const getCurrentStep = createSelector(
    getSearchState,
    search.getCurrentStep
);

export const getDatasetName = createSelector(
    getSearchState,
    search.getDatasetName
);

export const getCriteriaStepChecked = createSelector(
    getSearchState,
    search.getCriteriaStepChecked
);

export const getOutputStepChecked = createSelector(
    getSearchState,
    search.getOutputStepChecked
);

export const getResultStepChecked = createSelector(
    getSearchState,
    search.getResultStepChecked
);

export const getIsConeSearchAdded = createSelector(
    getSearchState,
    search.getIsConeSearchAdded
);

export const getCriteriaList = createSelector(
    getSearchState,
    search.getCriteriaList
);

export const getOutputList = createSelector(
    getSearchState,
    search.getOutputList
);


export const getOutputListEmpty = createSelector(
    getOutputList,
    (
        outputList: number[]) =>{
        return outputList.length === 0;
    }
);

export const getSearchData = createSelector(
    getSearchState,
    search.getSearchData
);

export const getDataLengthIsLoading = createSelector(
    getSearchState,
    search.getDataLengthIsLoading
);

export const getDataLengthIsLoaded = createSelector(
    getSearchState,
    search.getDataLengthIsLoaded
);

export const getDataLength = createSelector(
    getSearchState,
    search.getDataLength
);

export const getSelectedData = createSelector(
    getSearchState,
    search.getSelectedData
);

export const getQueryParams = createSelector(
    getIsConeSearchAdded,
    getConeSearch,
    getCriteriaList,
    getOutputList,
    getCriteriaStepChecked,
    getOutputStepChecked,
    getResultStepChecked,
    (
        isConeSearchAdded: boolean,
        coneSearch: ConeSearch,
        criteriaList: Criterion[],
        outputList: number[],
        criteriaStepChecked: boolean,
        outputStepChecked: boolean,
        resultStepChecked: boolean) => {
        let step = '';
        step += (criteriaStepChecked) ? '1' : '0';
        step += (outputStepChecked) ? '1' : '0';
        step += (resultStepChecked) ? '1' : '0';
        let queryParams: SearchQueryParams = {
            s: step,
            a: outputList.join(';')
        };
        if (isConeSearchAdded) {
            queryParams = {
                ...queryParams,
                cs: coneSearch.ra + ':' + coneSearch.dec + ':' + coneSearch.radius
            };
        }
        if (criteriaList.length > 0) {
            queryParams = {
                ...queryParams,
                c: criteriaList.map(criterion => getCriterionStr(criterion)).join(';')
            };
        }
        return queryParams;
    }
);

export const getProcessWip = createSelector(
    getSearchState,
    search.getProcessWip
);

export const getProcessDone = createSelector(
    getSearchState,
    search.getProcessDone
);

export const getProcessId = createSelector(
    getSearchState,
    search.getProcessId
);
