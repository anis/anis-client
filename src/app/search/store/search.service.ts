/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable()
/**
 * @class
 * @classdesc Search service.
 */
export class SearchService {
    API_PATH: string = environment.apiUrl;
    instanceName: string = environment.instanceName;

    constructor(private http: HttpClient) { }

    /**
     * Retrieves results for the given parameters.
     *
     * @param  {string} query - The query.
     *
     * @return Observable<any[]>
     */
    retrieveData(query: string): Observable<any[]> {
        return this.http.get<any[]>(this.API_PATH + '/search/' + query);
    }

    /**
     * Retrieves results number for the given parameters.
     *
     * @param  {string} query - The query.
     *
     * @return Observable<{ nb: number }[]>
     */
    getDataLength(query: string): Observable<{ nb: number }[]> {
        return this.http.get<{ nb: number }[]>(this.API_PATH + '/search/' + query);
    }

    /**
     * Executes process with the given parameters.
     *
     * @param  {string} typeProcess - The process type.
     * @param  {string} dname - The dataset name.
     * @param  {string} query - The query.
     *
     * @return Observable<any>
     */
    executeProcess(typeProcess: string, dname: string, query: string): Observable<any> {
        const url = this.API_PATH + '/service/' + this.instanceName + '/' + dname + '/' + typeProcess + query;
        return this.http.get<any>(url);
    }

    /**
     * Checks if process with the given ID is done.
     *
     * @param  {string} id - The process ID.
     *
     * @return Observable<any>
     */
    checkProcess(id: string): Observable<any> {
        return this.http.head<any>('http://0.0.0.0:8085/' + id + '.csv');
    }
}
