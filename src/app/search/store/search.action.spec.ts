import * as searchActions from './search.action';
import { Criterion } from './model';
import { Pagination, PaginationOrder } from '../../shared/datatable/model';
import { CRITERIA_LIST } from '../../../settings/test-data';

describe('[Search] Action', () => {
    it('should create InitSearchByUrl action', () => {
        const action = new searchActions.InitSearchByUrl();
        expect(action.type).toEqual(searchActions.INIT_SEARCH_BY_URL);
    });

    it('should create ChangeStepAction', () => {
        const action = new searchActions.ChangeStepAction('toto');
        expect(action.type).toEqual(searchActions.CHANGE_STEP);
        expect(action.payload).toEqual('toto');
    });

    it('should create SelectDatasetAction', () => {
        const action = new searchActions.SelectDatasetAction('toto');
        expect(action.type).toEqual(searchActions.SELECT_DATASET);
        expect(action.payload).toEqual('toto');
    });

    it('should create NewSearchAction', () => {
        const action = new searchActions.NewSearchAction('toto');
        expect(action.type).toEqual(searchActions.NEW_SEARCH);
        expect(action.payload).toEqual('toto');
    });

    it('should create CriteriaChecked action', () => {
        const action = new searchActions.CriteriaChecked();
        expect(action.type).toEqual(searchActions.CRITERIA_CHECKED);
    });

    it('should create OutputChecked action', () => {
        const action = new searchActions.OutputChecked();
        expect(action.type).toEqual(searchActions.OUTPUT_CHECKED);
    });

    it('should create ResultChecked action', () => {
        const action = new searchActions.ResultChecked();
        expect(action.type).toEqual(searchActions.RESULT_CHECKED);
    });

    it('should create IsConeSearchAddedAction', () => {
        const action = new searchActions.IsConeSearchAddedAction(true);
        expect(action.type).toEqual(searchActions.IS_CONE_SEARCH_ADDED);
        expect(action.payload).toBeTruthy();
    });

    it('should create UpdateCriteriaListAction', () => {
        const criteriaList: Criterion[] = CRITERIA_LIST;
        const action = new searchActions.UpdateCriteriaListAction(criteriaList);
        expect(action.type).toEqual(searchActions.UPDATE_CRITERIA_LIST);
        expect(action.payload).toEqual(criteriaList);
    });

    it('should create AddCriterionAction', () => {
        const criterion: Criterion = CRITERIA_LIST[0];
        const action = new searchActions.AddCriterionAction(criterion);
        expect(action.type).toEqual(searchActions.ADD_CRITERION);
        expect(action.payload).toEqual(criterion);
    });

    it('should create DeleteCriterionAction', () => {
        const action = new searchActions.DeleteCriterionAction(1);
        expect(action.type).toEqual(searchActions.DELETE_CRITERION);
        expect(action.payload).toEqual(1);
    });

    it('should create UpdateOutputListAction', () => {
        const action = new searchActions.UpdateOutputListAction([1, 2]);
        expect(action.type).toEqual(searchActions.UPDATE_OUTPUT_LIST);
        expect(action.payload).toEqual([1, 2]);
    });

    it('should create RetrieveDataAction', () => {
        const pagination : Pagination = { dname: 'toto', nbItems: 10, page: 1, sortedCol: 1, order: PaginationOrder.a };
        const action = new searchActions.RetrieveDataAction(pagination);
        expect(action.type).toEqual(searchActions.RETRIEVE_DATA);
        expect(action.payload).toEqual(pagination);
    });

    it('should create RetrieveDataSuccessAction', () => {
        const action = new searchActions.RetrieveDataSuccessAction(['toto']);
        expect(action.type).toEqual(searchActions.RETRIEVE_DATA_SUCCESS);
        expect(action.payload).toEqual(['toto']);
    });

    it('should create RetrieveDataFailAction', () => {
        const action = new searchActions.RetrieveDataFailAction();
        expect(action.type).toEqual(searchActions.RETRIEVE_DATA_FAIL);
    });

    it('should create GetDataLengthAction', () => {
        const action = new searchActions.GetDataLengthAction();
        expect(action.type).toEqual(searchActions.GET_DATA_LENGTH);
    });

    it('should create GetDataLengthSuccessAction', () => {
        const action = new searchActions.GetDataLengthSuccessAction(1);
        expect(action.type).toEqual(searchActions.GET_DATA_LENGTH_SUCCESS);
        expect(action.payload).toEqual(1);
    });

    it('should create GetDataLengthFailAction', () => {
        const action = new searchActions.GetDataLengthFailAction();
        expect(action.type).toEqual(searchActions.GET_DATA_LENGTH_FAIL);
    });

    it('should create AddSelectedDataAction', () => {
        const action = new searchActions.AddSelectedDataAction(1);
        expect(action.type).toEqual(searchActions.ADD_SELECTED_DATA);
        expect(action.payload).toEqual(1);
    });

    it('should create DeleteSelectedDataAction', () => {
        const action = new searchActions.DeleteSelectedDataAction(1);
        expect(action.type).toEqual(searchActions.DELETE_SELECTED_DATA);
        expect(action.payload).toEqual(1);
    });

    it('should create ExecuteProcessAction', () => {
        const action = new searchActions.ExecuteProcessAction('toto');
        expect(action.type).toEqual(searchActions.EXECUTE_PROCESS);
        expect(action.payload).toEqual('toto');
    });

    it('should create ExecuteProcessWipAction', () => {
        const action = new searchActions.ExecuteProcessWipAction('toto');
        expect(action.type).toEqual(searchActions.EXECUTE_PROCESS_WIP);
        expect(action.payload).toEqual('toto');
    });

    it('should create ExecuteProcessSuccessAction', () => {
        const action = new searchActions.ExecuteProcessSuccessAction('toto');
        expect(action.type).toEqual(searchActions.EXECUTE_PROCESS_SUCCESS);
        expect(action.payload).toEqual('toto');
    });

    it('should create ExecuteProcessFailAction', () => {
        const action = new searchActions.ExecuteProcessFailAction();
        expect(action.type).toEqual(searchActions.EXECUTE_PROCESS_FAIL);
    });

    it('should create DestroyResultsAction', () => {
        const action = new searchActions.DestroyResultsAction();
        expect(action.type).toEqual(searchActions.DESTROY_RESULTS);
    });

    it('should create ResetSearchAction', () => {
        const action = new searchActions.ResetSearchAction();
        expect(action.type).toEqual(searchActions.RESET_SEARCH);
    });
});
