import * as fromSearch from './search.reducer';
import * as searchActions from './search.action';
import { FieldCriterion } from './model';
import { CRITERIA_LIST } from '../../../settings/test-data'

describe('[Search] Reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromSearch;
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should change the currentStep', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.ChangeStepAction('toto');
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toEqual('toto');
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set the datasetName and change pristine', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.SelectDatasetAction('toto');
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeFalsy();
        expect(state.currentStep).toBeNull();
        expect(state.datasetName).toBe('toto');
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set currentStep to "dataset"', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.NewSearchAction('toto');
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toEqual('dataset');
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set criteriaStepChecked to true', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.CriteriaChecked();
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeTruthy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set outputStepChecked to true', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.OutputChecked();
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeTruthy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set resultStepChecked to true', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.ResultChecked();
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeTruthy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set coneSearchAdded to true', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.IsConeSearchAddedAction(true);
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeTruthy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });


    it('should set criteriaList', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.UpdateCriteriaListAction(CRITERIA_LIST);
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(2);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should add criterion to criteriaList', () => {
        const criterion: FieldCriterion = { id: 3, type: 'field', operator: 'eq', value: 'fd_crit_3' };
        const { initialState } = fromSearch;
        const action = new searchActions.AddCriterionAction(criterion);
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(1);
        expect(state.criteriaList[0].id).toEqual(3);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should remove criterion to criteriaList', () => {
        const criterion: FieldCriterion = { id: 2, type: 'field', operator: 'eq', value: 'fd_crit_2' };
        const initialState = { ...fromSearch.initialState, criteriaList: CRITERIA_LIST };
        const action = new searchActions.DeleteCriterionAction(criterion.id);
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(1);
        expect(state.criteriaList[0].id).toEqual(1);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set outputList', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.UpdateOutputListAction([1, 2]);
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(2);
        expect(state.outputList).toContain(1);
        expect(state.outputList).toContain(2);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set searchData', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.RetrieveDataSuccessAction(['data_1', 'data_2']);
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toBe(2);
        expect(state.searchData).toContain('data_1');
        expect(state.searchData).toContain('data_2');
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });


    it('should set dataLengthIsLoading to true', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.GetDataLengthAction();
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeTruthy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set dataLengthIsLoading to false, dataLengthIsLoaded to true and set dataLength', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.GetDataLengthSuccessAction(12);
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeTruthy();
        expect(state.dataLength).toBe(12);
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set dataLengthIsLoading to false', () => {
        const initialState = { ...fromSearch.initialState, dataLengthIsLoading: true };
        const action = new searchActions.GetDataLengthFailAction();
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should add data to selectedData', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.AddSelectedDataAction(1);
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(1);
        expect(state.selectedData).toContain(1);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should remove data to selectedData', () => {
        const initialState = { ...fromSearch.initialState, selectedData: ['data_1', 'data_2'] };
        const action = new searchActions.DeleteSelectedDataAction('data_2');
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(1);
        expect(state.selectedData).toContain('data_1');
        expect(state.selectedData).not.toContain('data_2');
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set processWip to true and processDone to false', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.ExecuteProcessAction('process_type');
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeTruthy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set processId', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.ExecuteProcessWipAction('toto');
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBe('toto');
        expect(state).not.toEqual(initialState);
    });

    it('should set processWip to false and processDone to true', () => {
        const { initialState } = fromSearch;
        const action = new searchActions.ExecuteProcessSuccessAction('toto');
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeTruthy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should unset processId and set processWip and processDone to false', () => {
        const initialState = {
            ...fromSearch.initialState,
            processId: 'toto',
            processWip: true,
            processDone: true
        };
        const action = new searchActions.ExecuteProcessFailAction();
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should unset searchData and dataLength', () => {
        const initialState = {
            ...fromSearch.initialState,
            searchData: ['toto', 'titi', 'tutu'],
            dataLength: 3
        };
        const action = new searchActions.DestroyResultsAction();
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should reset search', () => {
        const initialState = {
            ...fromSearch.initialState,
            pristine: false,
            currentStep: 'dataset',
            datasetName: 'toto',
            criteriaStepChecked: true,
            outputStepChecked: true,
            resultStepChecked: true,
            coneSearchAdded: true,
            criteriaList: CRITERIA_LIST,
            outputList: [1, 2],
            searchData: ['toto', 'titi', 'tutu'],
            dataLengthIsLoading: true,
            dataLengthIsLoaded: true,
            dataLength: 3,
            selectedData: [1],
            processWip: true,
            processDone: true,
            processId: '1'
        };
        const action = new searchActions.ResetSearchAction();
        const state = fromSearch.reducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull()
        expect(state.datasetName).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.searchData.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.selectedData.length).toEqual(0);
        expect(state.processWip).toBeFalsy();
        expect(state.processDone).toBeFalsy();
        expect(state.processId).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should get pristine', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getPristine(state)).toBeTruthy();
    });

    it('should get currentStep', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getCurrentStep(state)).toBeNull();
    });

    it('should get datasetName', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getDatasetName(state)).toBeNull();
    });

    it('should get criteriaStepChecked', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getCriteriaStepChecked(state)).toBeFalsy();
    });

    it('should get outputStepChecked', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getOutputStepChecked(state)).toBeFalsy();
    });

    it('should get resultStepChecked', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getResultStepChecked(state)).toBeFalsy();
    });

    it('should get coneSearchAdded', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getIsConeSearchAdded(state)).toBeFalsy();
    });

    it('should get criteriaList', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getCriteriaList(state).length).toEqual(0);
    });

    it('should get outputList', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getOutputList(state).length).toEqual(0);
    });

    it('should get searchData', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getSearchData(state).length).toEqual(0);
    });

    it('should get dataLengthIsLoading', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getDataLengthIsLoading(state)).toBeFalsy();
    });

    it('should get dataLengthIsLoaded', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getDataLengthIsLoaded(state)).toBeFalsy();
    });

    it('should get dataLength', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getDataLength(state)).toBeNull();
    });

    it('should get selectedData', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getSelectedData(state).length).toEqual(0);
    });

    it('should get processWip', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getProcessWip(state)).toBeFalsy();
    });

    it('should get processDone', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getProcessDone(state)).toBeFalsy();
    });

    it('should get processId', () => {
        const action = {} as searchActions.Actions;
        const state = fromSearch.reducer(undefined, action);

        expect(fromSearch.getProcessId(state)).toBeNull();
    });
});
