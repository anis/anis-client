/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Criterion } from './criterion.model';

/**
 * @class
 * @classdesc List criterion class.
 *
 * @implements Criterion
 */
export class ListCriterion implements Criterion {
    id: number;
    type: string;
    values: string[];
}
