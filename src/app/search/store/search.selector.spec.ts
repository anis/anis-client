import * as searchSelector from './search.selector';
import * as fromSearch from './search.reducer';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import { Criterion } from './model';
import { CRITERIA_LIST } from '../../../settings/test-data';
import { ConeSearch } from '../../shared/cone-search/store/model';

describe('[Search] Selector', () => {
    it('should get pristine', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getPristine(state)).toBeTruthy();
    });

    it('should get currentStep', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getCurrentStep(state)).toBeNull();
    });

    it('should get datasetName', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getDatasetName(state)).toBeNull();
    });

    it('should get criteriaStepChecked', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getCriteriaStepChecked(state)).toBeFalsy();
    });

    it('should get outputStepChecked', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getOutputStepChecked(state)).toBeFalsy();
    });

    it('should get resultStepChecked', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getResultStepChecked(state)).toBeFalsy();
    });

    it('should get coneSearchAdded', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getIsConeSearchAdded(state)).toBeFalsy();
    });

    it('should get criteriaList', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getCriteriaList(state).length).toEqual(0);
    });

    it('should get outputList', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getOutputList(state).length).toEqual(0);
    });

    it('should get outputListEmpty', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getOutputListEmpty(state)).toBeTruthy();
    });

    it('should get dataLength', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getDataLength(state)).toBeNull();
    });

    it('should get selectedData', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getSelectedData(state).length).toEqual(0);
    });

    it('should get queryParams without criteria', () => {
        const outputList: number[] = [1, 2];
        const state = {
            search: {
                ...fromSearch.initialState,
                outputList
            },
            coneSearch: { ...fromConeSearch.initialState }
        };
        const expected = { s: '000', a: '1;2' };

        expect(searchSelector.getQueryParams(state)).toEqual(expected);
    });

    it('should get queryParams with criteria', () => {
        const outputList: number[] = [1, 2];
        const criteriaList: Criterion[] = CRITERIA_LIST;
        const state = {
            search: {
                ...fromSearch.initialState,
                outputList,
                criteriaList
            },
            coneSearch: { ...fromConeSearch.initialState }
        };
        const expected = { s: '000', a: '1;2', c: '1::eq::fd_crit_1;2::eq::fd_crit_2' };

        expect(searchSelector.getQueryParams(state)).toEqual(expected);
    });

    it('should get queryParams with cone search', () => {
        const outputList: number[] = [1, 2];
        const coneSearchAdded: boolean = true;
        const coneSearch: ConeSearch = { ra: 3, dec: 4, radius: 5 };
        const state = {
            search: {
                ...fromSearch.initialState,
                outputList,
                coneSearchAdded
            },
            coneSearch: {
                ...fromConeSearch.initialState,
                coneSearch
            }
        };
        const expected = { s: '000', a: '1;2', cs: '3:4:5' };

        expect(searchSelector.getQueryParams(state)).toEqual(expected);
    });

    it('should get queryParams with checked steps', () => {
        const criteriaStepChecked: boolean = true;
        const outputStepChecked: boolean = true;
        const resultStepChecked: boolean = true;
        const outputList: number[] = [1, 2];
        const state = {
            search: {
                ...fromSearch.initialState,
                criteriaStepChecked,
                outputStepChecked,
                resultStepChecked,
                outputList
            },
            coneSearch: { ...fromConeSearch.initialState }
        };
        const expected = { s: '111', a: '1;2' };

        expect(searchSelector.getQueryParams(state)).toEqual(expected);
    });

    it('should get processWip', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getProcessWip(state)).toBeFalsy();
    });

    it('should get processDone', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getProcessDone(state)).toBeFalsy();
    });

    it('should get processId', () => {
        const state = { search: { ...fromSearch.initialState }};
        expect(searchSelector.getProcessId(state)).toBeNull();
    });
});