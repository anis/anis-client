/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from './search.action';
import { Criterion } from './model';

/**
 * Interface for search state.
 *
 * @interface State
 */
export interface State {
    pristine: boolean;
    currentStep: string;
    datasetName: string;
    criteriaStepChecked: boolean;
    outputStepChecked: boolean;
    resultStepChecked: boolean;
    coneSearchAdded: boolean;
    criteriaList: Criterion[];
    outputList: number[];
    searchData: any[];
    dataLengthIsLoading: boolean;
    dataLengthIsLoaded: boolean;
    dataLength: number;
    selectedData: any[];
    processWip: boolean;
    processDone: boolean;
    processId: string;
}

export const initialState: State = {
    pristine: true,
    currentStep: null,
    datasetName: null,
    criteriaStepChecked: false,
    outputStepChecked: false,
    resultStepChecked: false,
    coneSearchAdded: false,
    criteriaList: [],
    outputList: [],
    searchData: [],
    dataLengthIsLoading: false,
    dataLengthIsLoaded: false,
    dataLength: null,
    selectedData: [],
    processWip: false,
    processDone: false,
    processId: null
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.CHANGE_STEP:
            return {
                ...state,
                currentStep: action.payload
            };

        case actions.SELECT_DATASET:
            return {
                ...state,
                pristine: false,
                datasetName: action.payload
            };

        case actions.NEW_SEARCH:
            return {
                ...initialState,
                currentStep: 'dataset'
            };

        case actions.CRITERIA_CHECKED:
            return {
                ...state,
                criteriaStepChecked: true
            };

        case actions.OUTPUT_CHECKED:
            return {
                ...state,
                outputStepChecked: true
            };

        case actions.RESULT_CHECKED:
            return {
                ...state,
                resultStepChecked: true
            };

        case actions.IS_CONE_SEARCH_ADDED:
            return {
                ...state,
                coneSearchAdded: action.payload
            };

        case actions.UPDATE_CRITERIA_LIST:
            return {
                ...state,
                criteriaList: action.payload
            };

        case actions.ADD_CRITERION:
            return {
                ...state,
                criteriaList: [...state.criteriaList, action.payload]
            };

        case actions.DELETE_CRITERION:
            return {
                ...state,
                criteriaList: [...state.criteriaList.filter(c => c.id !== action.payload)]
            };

        case actions.UPDATE_OUTPUT_LIST:
            return {
                ...state,
                outputList: action.payload
            };

        case actions.RETRIEVE_DATA_SUCCESS:
            return {
                ...state,
                searchData: action.payload
            };

        case actions.GET_DATA_LENGTH:
            return {
                ...state,
                dataLengthIsLoading: true
            };

        case actions.GET_DATA_LENGTH_SUCCESS:
            return {
                ...state,
                dataLengthIsLoading: false,
                dataLengthIsLoaded: true,
                dataLength: action.payload
            };

        case actions.GET_DATA_LENGTH_FAIL:
            return {
                ...state,
                dataLengthIsLoading: false
            };

        case actions.ADD_SELECTED_DATA:
            return {
                ...state,
                selectedData: [...state.selectedData, action.payload]
            };

        case actions.DELETE_SELECTED_DATA:
            return {
                ...state,
                selectedData: [...state.selectedData.filter(d => d !== action.payload)]
            };

        case actions.EXECUTE_PROCESS:
            return {
                ...state,
                processWip: true,
                processDone: false
            };

        case actions.EXECUTE_PROCESS_WIP:
            return {
                ...state,
                processId: action.payload
            };

        case actions.EXECUTE_PROCESS_SUCCESS:
            return {
                ...state,
                processWip: false,
                processDone: true
            };

        case actions.EXECUTE_PROCESS_FAIL:
            return {
                ...state,
                processWip: false,
                processDone: false,
                processId: null
            };

        case actions.DESTROY_RESULTS:
            return {
                ...state,
                searchData: [],
                dataLength: null
            };

        case actions.RESET_SEARCH:
            return { ...initialState };

        default:
            return state;
    }
}

export const getPristine = (state: State) => state.pristine;
export const getCurrentStep = (state: State) => state.currentStep;
export const getDatasetName = (state: State) => state.datasetName;
export const getCriteriaStepChecked = (state: State) => state.criteriaStepChecked;
export const getOutputStepChecked = (state: State) => state.outputStepChecked;
export const getResultStepChecked = (state: State) => state.resultStepChecked;
export const getIsConeSearchAdded = (state: State) => state.coneSearchAdded;
export const getCriteriaList = (state: State) => state.criteriaList;
export const getOutputList = (state: State) => state.outputList;
export const getSearchData = (state: State) => state.searchData;
export const getDataLengthIsLoading = (state: State) => state.dataLengthIsLoading;
export const getDataLengthIsLoaded = (state: State) => state.dataLengthIsLoaded;
export const getDataLength = (state: State) => state.dataLength;
export const getSelectedData = (state: State) => state.selectedData;
export const getProcessWip = (state: State) => state.processWip;
export const getProcessDone = (state: State) => state.processDone;
export const getProcessId = (state: State) => state.processId;
