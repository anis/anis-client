/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '../shared/shared.module';
import { MetamodelModule } from '../metamodel/metamodel.module';
import { SearchEffects } from './store/search.effects';
import { SearchService } from './store/search.service';
import { SearchRoutingModule, routedComponents } from './search-routing.module';
import { dummiesComponents } from './components';
import { reducer } from './store/search.reducer';

@NgModule({
    imports: [
        SharedModule,
        MetamodelModule,
        SearchRoutingModule,
        StoreModule.forFeature('search', reducer),
        EffectsModule.forFeature([SearchEffects])
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ],
    providers: [SearchService]
})
/**
 * @class
 * @classdesc Search module.
 */
export class SearchModule { }
