/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchComponent } from './containers/search.component';
import { DatasetComponent } from './containers/dataset.component';
import { CriteriaComponent } from './containers/criteria.component';
import { OutputComponent } from './containers/output.component';
import { ResultComponent } from './containers/result.component';

const routes: Routes = [
    {
        path: '', component: SearchComponent, children: [
            { path: '', redirectTo: 'dataset', pathMatch: 'full' },
            { path: 'dataset', component: DatasetComponent },
            { path: 'criteria/:dname', component: CriteriaComponent },
            { path: 'output/:dname', component: OutputComponent },
            { path: 'result/:dname', component: ResultComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
/**
 * @class
 * @classdesc Search routing module.
 */
export class SearchRoutingModule { }

export const routedComponents = [
    SearchComponent,
    DatasetComponent,
    CriteriaComponent,
    OutputComponent,
    ResultComponent
];
