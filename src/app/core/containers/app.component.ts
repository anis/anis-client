/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromMetamodel from '../../metamodel/reducers';
import * as instanceActions from '../../metamodel/action/instance.action';
import * as metamodelSelector from '../../metamodel/selectors';
import * as fromAuth from '../../auth/auth.reducer';
import * as authActions from '../../auth/auth.action';
import * as authSelector from '../../auth/auth.selector';
import { UserProfile } from '../../auth/user-profile.model';
import { Instance } from '../../metamodel/model';
import { VERSIONS } from '../../../settings/settings';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    auth: fromAuth.State;
    metamodel: fromMetamodel.State;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    encapsulation: ViewEncapsulation.None
})
/**
 * @class
 * @classdesc App container.
 *
 * @implements OnInit
 */
export class AppComponent implements OnInit {
    public anisClientVersion: string = VERSIONS.anisClient;
    public year = (new Date()).getFullYear();
    public isAuthenticated: Observable<boolean>;
    public userProfile: Observable<UserProfile>;
    public instance: Observable<Instance>;

    constructor(private store: Store<StoreState>) {
        this.isAuthenticated = store.select(authSelector.isAuthenticated);
        this.userProfile = store.select(authSelector.getUserProfile);
        this.instance = store.select(metamodelSelector.getInstance);
    }

    ngOnInit() {
        this.store.dispatch(new instanceActions.LoadInstanceMetaAction());
    }

    /**
     * Dispatches action to log in.
     */
    login(): void {
        this.store.dispatch(new authActions.LoginAction());
    }

    /**
     * Dispatches action to log out.
     */
    logout(): void {
        this.store.dispatch(new authActions.LogoutAction());
    }

    /**
     * Dispatches action to open edit profile page.
     */
    openEditProfile(): void {
        this.store.dispatch(new authActions.OpenEditProfileAction());
    }
}
