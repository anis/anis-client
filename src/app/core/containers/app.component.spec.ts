import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';
import * as fromAuth from '../../auth/auth.reducer';
import * as authActions from '../../auth/auth.action';
import { AppComponent } from './app.component';
import * as fromMetamodel from '../../metamodel/reducers';
import * as instanceActions from '../../metamodel/action/instance.action';

describe('[Core] Container: AppComponent', () => {
    @Component({ selector: 'app-nav', template: '' })
    class NavStubComponent {
        @Input() isAuthenticated: boolean;
        @Input() userProfile: any;
    }

    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    let store: MockStore;
    const initialState = {
        auth: { ...fromAuth.initialState },
        metamodel: { ...fromMetamodel }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                AppComponent,
                NavStubComponent
            ],
            providers: [
                provideMockStore({ initialState })
            ]
        });
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', () => {
        const loadInstanceMetaAction = new instanceActions.LoadInstanceMetaAction();
        const spy = spyOn(store, 'dispatch');
        component.ngOnInit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(loadInstanceMetaAction);
    });

    it('#login() should dispatch LoginAction', () => {
        const loginAction = new authActions.LoginAction();
        const spy = spyOn(store, 'dispatch');
        component.login();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(loginAction);
    });

    it('#logout() should dispatch LogoutAction', () => {
        const logoutAction = new authActions.LogoutAction();
        const spy = spyOn(store, 'dispatch');
        component.logout();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(logoutAction);
    });

    it('#openEditProfile() should dispatch OpenEditProfileAction', () => {
        const openEditProfileAction = new authActions.OpenEditProfileAction();
        const spy = spyOn(store, 'dispatch');
        component.openEditProfile();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(openEditProfileAction);
    });
});
