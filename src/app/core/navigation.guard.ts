/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import * as fromMetamodel from '../metamodel/reducers';

@Injectable({
    providedIn: 'root'
})
/**
 * @class
 * @classdesc Navigation guard.
 *
 * @implements CanActivate
 */
export class NavigationGuard implements CanActivate {
    constructor(private store$: Store<{ metamodel: fromMetamodel.State }>) { }

    // canLoad(
    //     route: Route,
    //     segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    //     const module: string = route.path;
    //     return this.isModuleAllowed(module).pipe(take(1));
    // }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        const module: string = this.url2module(next.routeConfig.path);
        return this.isModuleAllowed(module);
    }

    /**
     * Checks if the given module is allowed.
     *
     * @param  {string} module - The module.
     *
     * @return Observable<boolean>
     */
    isModuleAllowed(module: string): Observable<boolean> {
        return this.store$.select(state => state.metamodel.instance.instance)
            .pipe(
                filter( instance => instance !== null),
                map(instance => instance.config[module].allowed)
            );
    }

    /**
     * Translates URL segment to a module name.
     *
     * @param  {string} url - The URL segment.
     *
     * @return string
     */
    url2module(url: string): string {
        return url.replace(/-/, '_');
    }
}
