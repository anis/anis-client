import { TestBed } from '@angular/core/testing';

import { provideMockStore } from '@ngrx/store/testing';

import { NavigationGuard } from './navigation.guard';
import * as fromMetamodel from '../metamodel/reducers';
import { INSTANCE } from '../../settings/test-data/instance';

describe('[Guard] NavigationGuard', () => {
    let guard: NavigationGuard;
    const initialState = { metamodel: {
        ...fromMetamodel,
        instance: { instance: INSTANCE }
    }};

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ provideMockStore({ initialState }) ]
        });
        guard = TestBed.inject(NavigationGuard);
    });

    it('should be created', () => {
        expect(guard).toBeTruthy();
    });

    // it('#isModuleAllowed() should return if module is allowed or not', () => {
    //     guard.isModuleAllowed('search').subscribe(allowed => expect(allowed).toBeTruthy());
    //     guard.isModuleAllowed('search-multiple').subscribe(allowed => expect(allowed).toBeTruthy());
    //     guard.isModuleAllowed('documentation').subscribe(allowed => expect(allowed).toBeFalsy());
    // });

    it('#url2module() should return correct module name', () => {
        expect(guard.url2module('search')).toEqual('search');
        expect(guard.url2module('search-multiple')).toEqual('search_multiple');
    });
});
