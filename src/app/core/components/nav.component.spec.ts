import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavComponent } from './nav.component';
import { INSTANCE } from '../../../settings/test-data/instance';

describe('[Core] Component: NavComponent', () => {
    let component: NavComponent;
    let fixture: ComponentFixture<NavComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [NavComponent]
        });
        fixture = TestBed.createComponent(NavComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should display the Sign In button if authentication allowed and no user logged in', () => {
        component.instance = INSTANCE;
        component.isAuthenticated = false;
        fixture.detectChanges();
        const template = fixture.nativeElement;
        expect(template.querySelector('#button-sign-in')).toBeTruthy();
    });

    it('should not display the Sign In button if user logged in', () => {
        component.instance = INSTANCE;
        component.isAuthenticated = true;
        fixture.detectChanges();
        const template = fixture.nativeElement;
        expect(template.querySelector('#button-sign-in')).toBeFalsy();
    });

    it('should not display the Sign In button if authentication not allowed', () => {
        component.instance = {
            name: 'toto',
            label: 'Toto',
            client_url: '',
            nb_dataset_families: 1,
            nb_datasets: 1,
            config: {
                authentication: {
                    allowed: false
                },
                search: {
                    allowed: false
                },
                search_multiple: {
                    allowed: false,
                    all_datasets_selected: false
                },
                documentation: {
                    allowed: false
                }
            }
        };
        component.isAuthenticated = true;
        fixture.detectChanges();
        const template = fixture.nativeElement;
        expect(template.querySelector('#button-sign-in')).toBeFalsy();
    });

    it('should not display the dropdown menu if authentication not allowed', () => {
        component.instance = {
            name: 'toto',
            label: 'Toto',
            client_url: '',
            nb_dataset_families: 1,
            nb_datasets: 1,
            config: {
                authentication: {
                    allowed: false
                },
                search: {
                    allowed: false
                },
                search_multiple: {
                    allowed: false,
                    all_datasets_selected: false
                },
                documentation: {
                    allowed: false
                }
            }
        };
        component.isAuthenticated = true;
        fixture.detectChanges();
        const template = fixture.nativeElement;
        expect(template.querySelector('#dropdown-menu')).toBeFalsy();
    });

    it('should not display the dropdown menu if no user logged in', () => {
        component.instance = INSTANCE;
        component.isAuthenticated = false;
        fixture.detectChanges();
        const template = fixture.nativeElement;
        expect(template.querySelector('#dropdown-menu')).toBeFalsy();
    });

    it('should display the dropdown menu if authentication allowed and user logged in', () => {
        component.instance = INSTANCE;
        component.isAuthenticated = true;
        fixture.detectChanges();
        const template = fixture.nativeElement;
        expect(template.querySelector('#dropdown-menu')).toBeTruthy();
    });

    it('should display search, search multiple and documentation links if instance config allows it', () => {
        component.instance = INSTANCE;
        fixture.detectChanges();
        const template = fixture.nativeElement;
        expect(template.querySelector('#search_link')).toBeTruthy();
        expect(template.querySelector('#search_multiple_link')).toBeTruthy();
        expect(template.querySelector('#documentation_link')).toBeNull();
    });

    it('should not display search, search multiple and documentation links if instance config don\'t allows it', () => {
        component.instance = {
            name: 'toto',
            label: 'Toto',
            client_url: '',
            nb_dataset_families: 1,
            nb_datasets: 1,
            config: {
                authentication: {
                    allowed: false
                },
                search: {
                    allowed: false
                },
                search_multiple: {
                    allowed: false,
                    all_datasets_selected: false
                },
                documentation: {
                    allowed: false
                }
            } };
        fixture.detectChanges();
        const template = fixture.nativeElement;
        expect(template.querySelector('#search_link')).toBeFalsy();
        expect(template.querySelector('#search_multiple_link')).toBeFalsy();
        expect(template.querySelector('#documentation_link')).toBeFalsy();
    });

    it('#getConfig() return if the given kay is allowed for the given instance configuration propriety', () => {
        component.instance = {
            name: 'toto',
            label: 'Toto',
            client_url: '',
            nb_dataset_families: 1,
            nb_datasets: 1,
            config: {
                authentication: {
                    allowed: false
                },
                search: {
                    allowed: true
                },
                search_multiple: {
                    allowed: false,
                    all_datasets_selected: false
                },
                documentation: {
                    allowed: false
                }
            }
        };
        expect(component.getConfig('search', 'allowed')).toBeTruthy();
        expect(component.getConfig('search_multiple', 'allowed')).toBeFalsy();
        expect(component.getConfig('documentation', 'allowed')).toBeFalsy();
    });
});
