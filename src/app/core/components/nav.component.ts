/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Instance } from '../../metamodel/model';
import { UserProfile } from '../../auth/user-profile.model';
import { environment } from '../../../environments/environment'

@Component({
    selector: 'app-nav',
    templateUrl: 'nav.component.html',
    styleUrls: [ 'nav.component.css' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Core nav component.
 */
export class NavComponent {
    @Input() isAuthenticated: boolean;
    @Input() userProfile: UserProfile;
    @Input() instance: Instance;
    @Output() login: EventEmitter<any> = new EventEmitter();
    @Output() logout: EventEmitter<any> = new EventEmitter();
    @Output() openEditProfile: EventEmitter<any> = new EventEmitter();

    baseHref: string = environment.baseHref;
    instanceName: string = environment.instanceName;

    /**
     * Checks if given key for the given configuration propriety is allowed.
     *
     * @param {string} prop - The configuration propriety.
     * @param {string} key - The key in configuration propriety.
     *
     * @return boolean
     */
    getConfig(prop: string, key: string): boolean {
        if (this.instance && this.instance.config[prop] && this.instance.config[prop][key]) {
            return this.instance.config[prop][key];
        }
        return false;
    }
}
