/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { StaticRoutingModule, routedComponents } from './static-routing.module';

@NgModule({
    imports: [
        SharedModule,
        StaticRoutingModule
    ],
    declarations: [routedComponents]
})
export class StaticModule { }
