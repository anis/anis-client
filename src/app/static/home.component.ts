/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';

import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-home',
    styleUrls: ['home.component.css'],
    templateUrl: 'home.component.html'
})
export class HomeComponent {
    instanceName: string = environment.instanceName;
}
