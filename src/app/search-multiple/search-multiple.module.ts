/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '../shared/shared.module';
import { MetamodelModule } from '../metamodel/metamodel.module';
import { SearchMultipleEffects } from './store/search-multiple.effects';
import { SearchMultipleService } from './store/search-multiple.service';
import { SearchMultipleRoutingModule, routedComponents } from './search-multiple-routing.module';
import { dummiesComponents } from './components';
import { reducer } from './store/search-multiple.reducer';

@NgModule({
    imports: [
        SharedModule,
        MetamodelModule,
        SearchMultipleRoutingModule,
        StoreModule.forFeature('searchMultiple', reducer),
        EffectsModule.forFeature([SearchMultipleEffects])
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ],
    providers: [SearchMultipleService]
})
/**
 * @class
 * @classdesc Search multiple module.
 */
export class SearchMultipleModule { }
