import { ProgressBarMultipleComponent } from './progress-bar-multiple.component';
import { SummaryMultipleComponent } from './summary-multiple.component';
import { DatasetListComponent } from './datasets/dataset-list.component';
import { DatasetsByFamilyComponent } from './datasets/datasets-by-family.component';
import { OverviewComponent } from './result/overview.component';
import { DatasetsResultComponent } from './result/datasets-result.component';
import { DownloadSectionComponent } from "./result/download-section.component";

export const dummiesComponents = [
    ProgressBarMultipleComponent,
    SummaryMultipleComponent,
    DatasetListComponent,
    DatasetsByFamilyComponent,
    OverviewComponent,
    DatasetsResultComponent,
    DownloadSectionComponent
];