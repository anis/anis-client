/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { SearchMultipleQueryParams } from '../store/model';

@Component({
    selector: 'app-progress-bar-multiple',
    templateUrl: 'progress-bar-multiple.component.html',
    styleUrls: ['progress-bar-multiple.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search multiple progress bar component.
 */
export class ProgressBarMultipleComponent {
    @Input() currentStep: string;
    @Input() positionStepChecked: boolean;
    @Input() datasetsStepChecked: boolean;
    @Input() resultStepChecked: boolean;
    @Input() isValidConeSearch: boolean;
    @Input() noSelectedDatasets: boolean;
    @Input() queryParams: SearchMultipleQueryParams;

    /**
     * Returns step class that match to the current step.
     *
     * @return string
     */
    getStepClass(): string {
        switch (this.currentStep) {
            case 'position':
                return 'positionStep';
            case 'datasets':
                return 'datasetsStep';
            case 'result':
                return 'resultStep';
            default:
                return 'positionStep';
        }
    }
}
