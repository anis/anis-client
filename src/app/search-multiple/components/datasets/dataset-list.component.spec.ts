import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input, ViewChild } from '@angular/core';

import { DatasetListComponent } from './dataset-list.component';
import { Dataset, Family, Survey } from '../../../metamodel/model';
import { DATASET, DATASET_FAMILY_LIST } from '../../../../settings/test-data';

describe('[SearchMultiple][Datasets] Component: DatasetListComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-dataset-list
                [datasetFamilyList]="datasetFamilyList"
                [datasetList]="datasetList"
                [selectedDatasets]="selectedDatasets">
            </app-dataset-list>`
    })
    class TestHostComponent {
        @ViewChild(DatasetListComponent, { static: false })
        public testedComponent: DatasetListComponent;
        public datasetFamilyList: Family[] = DATASET_FAMILY_LIST;
        public datasetList: Dataset[] = [DATASET];
        public selectedDatasets: string[] = [];
    }

    @Component({ selector: 'app-datasets-by-family', template: '' })
    class DatasetsByFamilyStubComponent {
        @Input() datasetFamily: Family;
        @Input() surveyList: Survey[];
        @Input() datasetList: Dataset[];
        @Input() selectedDatasets: string[];
        @Input() isAllSelected: boolean;
        @Input() isAllUnselected: boolean;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: DatasetListComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetListComponent,
                TestHostComponent,
                DatasetsByFamilyStubComponent
            ]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getDatasetFamilyList() should return sorted survey list', () => {
        const sortedDatasetFamilyList: Family[] = testedComponent.getDatasetFamilyList();
        expect(sortedDatasetFamilyList.length).toBe(1);
        expect(sortedDatasetFamilyList[0].label).toBe('Default dataset family');
    });

    it('#getDatasetsByFamily() should return dataset list for the given dataset family', () => {
        const datasetList: Dataset[] = testedComponent.getDatasetsByFamily(1);
        expect(datasetList.length).toBe(1);
        expect(datasetList[0].name).toBe('cat_1');
    });

    it('#getIsAllSelected() should return true if all datasets of the given dataset family are selected', () => {
        testHostComponent.selectedDatasets = ['cat_1'];
        testHostFixture.detectChanges();
        expect(testedComponent.getIsAllSelected(1)).toBeTruthy();
    });

    it('#getIsAllSelected() should return false if not all datasets of the given dataset family are selected', () => {
        expect(testedComponent.getIsAllSelected(1)).toBeFalsy();
    });

    it('#getIsAllUnselected() should return true if all datasets of the given dataset family are not selected', () => {
        expect(testedComponent.getIsAllUnselected(1)).toBeTruthy();
    });

    it('#getIsAllUnselected() should return false if not all datasets of the given dataset family are not selected', () => {
        testHostComponent.selectedDatasets = ['cat_1'];
        testHostFixture.detectChanges();
        expect(testedComponent.getIsAllUnselected(1)).toBeFalsy();
    });
});

