/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';

import { Dataset, Family, Survey } from '../../../metamodel/model';
import { sortByDisplay } from '../../../shared/utils';

@Component({
    selector: 'app-datasets-by-family',
    templateUrl: 'datasets-by-family.component.html',
    styleUrls: ['datasets-by-family.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
/**
 * @class
 * @classdesc Search multiple datasets by family component.
 */
export class DatasetsByFamilyComponent {
    @Input() datasetFamily: Family;
    @Input() surveyList: Survey[];
    @Input() datasetList: Dataset[];
    @Input() selectedDatasets: string[];
    @Input() isAllSelected: boolean;
    @Input() isAllUnselected: boolean;
    @Output() updateSelectedDatasets: EventEmitter<string[]> = new EventEmitter();

    /**
     * Returns dataset list sorted by display.
     *
     * @return Dataset[]
     */
    getDatasetSortedByDisplay(): Dataset[] {
        return this.datasetList.sort(sortByDisplay);
    }

    /**
     * Checks if dataset is selected fot the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return boolean
     */
    isSelected(dname: string): boolean {
        return this.selectedDatasets.filter(i => i === dname).length > 0;
    }

    /**
     * Emits event to update dataset list selection with the given updated selected dataset name list.
     *
     * @param  {string} dname - The dataset name.
     */
    toggleSelection(dname: string): void {
        const clonedSelectedDatasets = [...this.selectedDatasets];
        const index = clonedSelectedDatasets.indexOf(dname);
        if (index > -1) {
            clonedSelectedDatasets.splice(index, 1);
        } else {
            clonedSelectedDatasets.push(dname);
        }
        this.updateSelectedDatasets.emit(clonedSelectedDatasets);
    }

    /**
     * Emits event to update dataset list selection with all datasets names.
     */
    selectAll(): void {
        const clonedSelectedDatasets = [...this.selectedDatasets];
        const datasetListName = this.datasetList.map(d => d.name);
        datasetListName.filter(name => clonedSelectedDatasets.indexOf(name) === -1).forEach(name => {
            clonedSelectedDatasets.push(name);
        });
        this.updateSelectedDatasets.emit(clonedSelectedDatasets);
    }

    /**
     * Emits event to update dataset list selection with no datasets names.
     */
    unselectAll(): void {
        const clonedSelectedDatasets = [...this.selectedDatasets];
        const datasetListName = this.datasetList.map(d => d.name);
        datasetListName.filter(name => clonedSelectedDatasets.indexOf(name) > -1).forEach(name => {
            const index = clonedSelectedDatasets.indexOf(name);
            clonedSelectedDatasets.splice(index, 1);
        });
        this.updateSelectedDatasets.emit(clonedSelectedDatasets);
    }

    /**
     * Returns survey description of the given survey name.
     *
     * @param  {string} surveyName - The survey name.
     *
     * @return string
     */
    getSurveyDescription(surveyName: string): string {
        return this.surveyList.find(p => p.name === surveyName).description;
    }
}
