import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasetsByFamilyComponent } from './datasets-by-family.component';
import { Dataset } from '../../../metamodel/model';
import { DATASET_LIST, SURVEY_LIST } from '../../../../settings/test-data';

describe('[SearchMultiple][Datasets] Component: DatasetsByFamilyComponent', () => {
    let component: DatasetsByFamilyComponent;
    let fixture: ComponentFixture<DatasetsByFamilyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DatasetsByFamilyComponent]
        });
        fixture = TestBed.createComponent(DatasetsByFamilyComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getDatasetSortedByDisplay() should return dataset list sorted by display', () => {
        component.datasetList = DATASET_LIST.filter(d => d.id_dataset_family === 2);
        const sortedDatasetList: Dataset[] = component.getDatasetSortedByDisplay();
        expect(sortedDatasetList.length).toEqual(2);
        expect(sortedDatasetList[0].name).toBe('cat_2');
        expect(sortedDatasetList[1].name).toBe('cat_3');
    });

    it('#isSelected() should return true if the given dataset is selected', () => {
        component.selectedDatasets = ['toto'];
        expect(component.isSelected('toto')).toBeTruthy();
    });

    it('#isSelected() should return false if the given dataset is not selected', () => {
        component.selectedDatasets = [];
        expect(component.isSelected('toto')).toBeFalsy();
    });

    it('#toggleSelection() should remove the given dataset from selectedDatasets and raise updateSelectedDatasets event', () => {
        component.selectedDatasets = ['toto'];
        const expectedSelectedDatasets = [];
        component.updateSelectedDatasets.subscribe((event: string[]) => expect(event).toEqual(expectedSelectedDatasets));
        component.toggleSelection('toto');
    });

    it('#toggleSelection() should add the given dataset to selectedDatasets and raise updateSelectedDatasets event', () => {
        component.selectedDatasets = [];
        const expectedSelectedDatasets = ['toto'];
        component.updateSelectedDatasets.subscribe((event: string[]) => expect(event).toEqual(expectedSelectedDatasets));
        component.toggleSelection('toto');
    });

    it('#selectAll() should add all datasets to selectedDatasets and raise updateSelectedDatasets event', () => {
        component.datasetList = DATASET_LIST.filter(d => d.id_dataset_family === 2);
        component.selectedDatasets = [];
        const expectedSelectedDatasets = ['cat_3', 'cat_2'];
        component.updateSelectedDatasets.subscribe((event: string[]) => expect(event).toEqual(expectedSelectedDatasets));
        component.selectAll();
    });

    it('#unselectAll() should remove all datasets to selectedDatasets and raise updateSelectedDatasets event', () => {
        component.datasetList = DATASET_LIST.filter(d => d.id_dataset_family === 2);
        component.selectedDatasets = ['cat_3', 'cat_2'];
        const expectedSelectedDatasets = [];
        component.updateSelectedDatasets.subscribe((event: string[]) => expect(event).toEqual(expectedSelectedDatasets));
        component.unselectAll();
    });

    it('#getSurveyDescription() should return the description of the given survey', () => {
        component.surveyList = SURVEY_LIST;
        expect(component.getSurveyDescription('survey_1')).toEqual('Description of survey 1');
    });
});

