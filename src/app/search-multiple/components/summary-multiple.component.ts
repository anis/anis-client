/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { SearchMultipleQueryParams } from '../store/model';
import { Dataset, Family } from '../../metamodel/model';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { sortByDisplay } from '../../shared/utils';

@Component({
    selector: 'app-summary-multiple',
    templateUrl: 'summary-multiple.component.html',
    styleUrls: ['summary-multiple.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
/**
 * @class
 * @classdesc Search multiple summary component.
 */
export class SummaryMultipleComponent {
    @Input() currentStep: string;
    @Input() isValidConeSearch: boolean;
    @Input() coneSearch: ConeSearch;
    @Input() datasetSearchMetaIsLoading: boolean;
    @Input() datasetSearchMetaIsLoaded: boolean;
    @Input() datasetFamilyList: Family[];
    @Input() datasetList: Dataset[];
    @Input() selectedDatasets: string[];
    @Input() noSelectedDatasets: boolean;
    @Input() queryParams: SearchMultipleQueryParams;

    accordionFamilyIsOpen = true;

    /**
     * Returns dataset families sorted by display, that contains datasets with cone search enabled.
     *
     * @return Family[]
     */
    getDatasetFamilyList(): Family[] {
        const familiesId: number[] = [];
        this.datasetList.forEach(d => {
            if (!familiesId.includes(d.id_dataset_family)) {
                familiesId.push(d.id_dataset_family);
            }
        });
        return this.datasetFamilyList
            .filter(f => familiesId.includes(f.id))
            .sort(sortByDisplay);
    }

    /**
     * Returns dataset list for the given dataset family ID.
     *
     * @param  {number} familyId - The family ID.
     *
     * @return Dataset[]
     */
    getSelectedDatasetsByFamily(familyId: number): Dataset[] {
        return this.datasetList
            .filter(d => d.id_dataset_family === familyId)
            .filter(d => this.selectedDatasets.includes(d.name));
    }
}
