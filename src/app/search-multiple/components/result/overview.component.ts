/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';

import { DatasetCount, SearchMultipleQueryParams } from '../../store/model';
import { Dataset, Family } from '../../../metamodel/model';
import { ConeSearch } from '../../../shared/cone-search/store/model';
import { sortByDisplay } from '../../../shared/utils';

@Component({
    selector: 'app-overview',
    templateUrl: 'overview.component.html'
})
/**
 * @class
 * @classdesc Search multiple result overview component.
 */
export class OverviewComponent {
    /**
     * Emits event to get dataset result number when dataset metadata is loaded.
     *
     * @param  {boolean} datasetSearchMetaIsLoaded - Is dataset metadata loaded.
     */
    @Input()
    set datasetSearchMetaIsLoaded(datasetSearchMetaIsLoaded: boolean) {
        if (datasetSearchMetaIsLoaded) {
            this.getDatasetCount.emit();
        }
    }
    @Input() coneSearch: ConeSearch;
    @Input() datasetFamilyList: Family[];
    @Input() datasetList: Dataset[];
    @Input() selectedDatasets: string[];
    @Input() queryParams: SearchMultipleQueryParams;
    @Input() datasetsCountIsLoaded: boolean;
    @Input() datasetsCount: DatasetCount[];
    @Output() getDatasetCount: EventEmitter<null> = new EventEmitter();

    /**
     * Returns total amount of results for all datasets.
     *
     * @return number
     */
    getTotalObject(): number {
        let total: number = 0;
        this.datasetsCount
            .filter(c => c.count > 0)
            .forEach(c => total += c.count)
        return total;
    }

    /**
     * Returns total number of datasets with results.
     *
     * @return number
     */
    getTotalDatasets(): number {
        return this.datasetsCount.filter(c => c.count > 0).length;
    }

    /**
     * Returns dataset families sorted by display, that contains selected datasets.
     *
     * @return Family[]
     */
    getSortedDatasetFamilyList(): Family[] {
        let datasetFamiliesWithSelectedDataset: Family[] = [];
        this.selectedDatasets.forEach(dname => {
            const dataset: Dataset = this.datasetList.find(d => d.name === dname);
            const datasetFamily: Family = this.datasetFamilyList.find(f => f.id === dataset.id_dataset_family);
            if (!datasetFamiliesWithSelectedDataset.includes(datasetFamily)) {
                datasetFamiliesWithSelectedDataset.push(datasetFamily);
            }
        });
        return datasetFamiliesWithSelectedDataset.sort(sortByDisplay);
    }

    /**
     * Returns selected dataset list for the given dataset family ID.
     *
     * @param  {number} familyId - The family ID.
     *
     * @return Dataset[]
     */
    getSelectedDatasetsByFamily(familyId: number): Dataset[] {
        return this.datasetList
            .filter(d => d.id_dataset_family === familyId)
            .filter(d => this.selectedDatasets.includes(d.name));
    }

    /**
     * Returns the result number for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return number
     */
    getCountByDataset(dname: string): number {
        return this.datasetsCount.find(c => c.dname === dname).count;
    }
}
