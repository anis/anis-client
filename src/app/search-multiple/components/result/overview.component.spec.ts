import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';

import { OverviewComponent } from './overview.component';
import { DatasetCount, SearchMultipleQueryParams } from '../../store/model';
import { Dataset, Family } from '../../../metamodel/model';
import { ConeSearch } from '../../../shared/cone-search/store/model';
import { DATASET_FAMILY_LIST, DATASET_LIST } from '../../../../settings/test-data';

describe('[SearchMultiple][Result] Component: OverviewComponent', () => {
    @Component({
        selector: `app-host`,
        template: `
            <app-overview
                [datasetSearchMetaIsLoaded]='datasetSearchMetaIsLoaded'
                [coneSearch]='coneSearch'
                [datasetFamilyList]='datasetFamilyList'
                [datasetList]='datasetList'
                [selectedDatasets]='selectedDatasets'
                [queryParams]='queryParams'
                [datasetsCountIsLoaded]='datasetsCountIsLoaded'
                [datasetsCount]='datasetsCount'>
            </app-overview>`
    })
    class TestHostComponent {
        @ViewChild(OverviewComponent, { static: false })
        public testedComponent: OverviewComponent;
        public datasetSearchMetaIsLoaded: boolean = false;
        public coneSearch: ConeSearch;
        public datasetFamilyList: Family[] = DATASET_FAMILY_LIST;
        public datasetList: Dataset[] = DATASET_LIST;
        public selectedDatasets: string[];
        public queryParams: SearchMultipleQueryParams;
        public datasetsCountIsLoaded: boolean;
        public datasetsCount: DatasetCount[] = [{ dname: 'toto', count: 1 }, { dname: 'tutu', count: 1 }];
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: OverviewComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                OverviewComponent,
                TestHostComponent
            ]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#getTotalObject() should return the total number of object found', () => {
        expect(testedComponent.getTotalObject()).toEqual(2);
    });

    it('#getTotalDatasets() should return the number of datasets with results', () => {
        expect(testedComponent.getTotalDatasets()).toEqual(2);
    });

    it('#getSortedDatasetFamilyList() should return sorted dataset family list with selected datasets in it', () => {
        testedComponent.selectedDatasets = ['cat_1', 'cat_2', 'cat_3'];
        const sortedDatasetFamilyList: Family[] = testedComponent.getSortedDatasetFamilyList();
        expect(sortedDatasetFamilyList.length).toEqual(2);
        expect(sortedDatasetFamilyList[0].label).toBe('Default dataset family');
        expect(sortedDatasetFamilyList[1].label).toBe('Another dataset family');
    });

    it('#getSelectedDatasetsByFamily() should return selected dataset that belongs to the given dataset family', () => {
        testedComponent.selectedDatasets = ['cat_1'];
        const selectedDatasets: Dataset[] = testedComponent.getSelectedDatasetsByFamily(1);
        expect(selectedDatasets.length).toEqual(1);
    });

    it('#getCountByDataset() should return count for the given dataset', () => {
        expect(testedComponent.getCountByDataset('toto')).toEqual(1);
    });

    it('should emit getDatasetCount if datasetSearchMetaIsLoaded is true', () => {
        testedComponent.getDatasetCount.subscribe((event: null) => expect(event).toBeUndefined());
        testHostComponent.datasetSearchMetaIsLoaded = true;
        testHostFixture.detectChanges();
    });
});
