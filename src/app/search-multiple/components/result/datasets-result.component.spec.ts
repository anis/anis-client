import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasetsResultComponent } from './datasets-result.component';
import { Attribute, Dataset } from '../../../metamodel/model';
import { Pagination, PaginationOrder } from '../../../shared/datatable/model';
import { ATTRIBUTE_LIST, DATASET_FAMILY_LIST, DATASET_LIST } from '../../../../settings/test-data';

describe('[SearchMultiple][Result] Component: DatasetsResultComponent', () => {
    @Component({ selector: 'app-datatable', template: '' })
    class DatatableStubComponent {
        @Input() dataset: Dataset;
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Input() data: any[];
        @Input() dataLength: number;
        @Input() selectedData: (string | number)[];
    }

    let component: DatasetsResultComponent;
    let fixture: ComponentFixture<DatasetsResultComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetsResultComponent,
                DatatableStubComponent
            ]
        });
        fixture = TestBed.createComponent(DatasetsResultComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getOrderedDatasetWithResults() should return datasetList with results, ordered by survey and display', () => {
        component.datasetFamilyList = DATASET_FAMILY_LIST;
        component.datasetList = DATASET_LIST;
        component.datasetsCount = [{ dname: 'cat_1', count: 1 }, { dname: 'cat_3', count: 1 }];
        expect(component.getOrderedDatasetWithResults().length).toEqual(2);
        expect(component.getOrderedDatasetWithResults()[0].name).toBe('cat_1');
        expect(component.getOrderedDatasetWithResults()[1].name).toBe('cat_3');
    });

    it('#getCount() should return count of the given dataset', () => {
        component.datasetsCount = [{ dname: 'cat_1', count: 1 }, { dname: 'cat_3', count: 1 }];
        expect(component.getCount('cat_1')).toEqual(1);
    });

    it('#attributeListIsLoaded() should return if attributeList is not loaded for the given dataset', () => {
        component.datasetsWithAttributeList = [];
        expect(component.attributeListIsLoaded('cat_1')).toBeFalsy();
        component.datasetsWithAttributeList = ['cat_1'];
        component.allAttributeList = { 'cat_1': { datasetName: 'cat_1', isLoading: true, isLoaded: false, attributeList: [] }};
        expect(component.attributeListIsLoaded('cat_1')).toBeFalsy();
        component.allAttributeList = { 'cat_1': { datasetName: 'cat_1', isLoading: false, isLoaded: true, attributeList: [] }};
        expect(component.attributeListIsLoaded('cat_1')).toBeTruthy();
    });

    it('#getDatasetAttributeList() should return an empty attributeList if attributeList not loaded for the given dataset', () => {
        component.datasetsWithAttributeList = [];
        expect(component.getDatasetAttributeList('cat_1')).toEqual([]);
    });

    it('#getDatasetAttributeList() should return attributeList if attributeList loaded for the given dataset', () => {
        component.datasetsWithAttributeList = ['cat_1'];
        component.allAttributeList = { 'cat_1': { datasetName: 'cat_1', isLoading: false, isLoaded: true, attributeList: ATTRIBUTE_LIST }};
        expect(component.getDatasetAttributeList('cat_1')).toEqual(ATTRIBUTE_LIST);
    });

    it('#getOutputList() should return sorted outputList for the given dataset', () => {
        component.datasetsWithAttributeList = ['cat_1'];
        component.allAttributeList = { 'cat_1': { datasetName: 'cat_1', isLoading: false, isLoaded: true, attributeList: ATTRIBUTE_LIST }};
        expect(component.getOutputList('cat_1')).toEqual([2, 1]);
    });

    it('#getData() should return data for the given dataset if loaded', () => {
        component.datasetsWithData = [];
        expect(component.getData('cat_1')).toEqual([]);
        component.datasetsWithData = ['cat_1'];
        component.allData = { 'cat_1': { datasetName: 'cat_1', isLoading: true, isLoaded: false, data: [] }};
        expect(component.getData('cat_1')).toEqual([]);
        component.allData = { 'cat_1': { datasetName: 'cat_1', isLoading: false, isLoaded: true, data: [1, 2] }};
        expect(component.getData('cat_1')).toEqual([1, 2]);
    });

    it('#getSelectedData() should return selected data for the given dataset', () => {
        expect(component.getSelectedData('cat_1')).toEqual([]);
    });

    it('#addSelectedData() should emit updateSelectedData with updated selected data', () => {
        component.selectedData = [];
        let expectedSelectedData: { dname: string, data: (string | number)[] }[] = [{ dname: 'toto', data: [1] }];
        let subscribtion = component.updateSelectedData.subscribe((event: { dname: string, data: (string | number)[] }[]) => expect(event).toEqual(expectedSelectedData));
        component.addSelectedData('toto', 1);
        subscribtion.unsubscribe();
        component.selectedData = [{ dname: 'toto', data: [1] }];
        expectedSelectedData = [{ dname: 'toto', data: [1, 2] }];
        subscribtion = component.updateSelectedData.subscribe((event: { dname: string, data: (string | number)[] }[]) => expect(event).toEqual(expectedSelectedData));
        component.addSelectedData('toto', 2);
    });

    it('#deleteSelectedData() should emit updateSelectedData with updated selected data', () => {
        component.selectedData = [{ dname: 'toto', data: [1, 2] }];
        let expectedSelectedData: { dname: string, data: (string | number)[] }[] = [{ dname: 'toto', data: [1] }];
        let subscribtion = component.updateSelectedData.subscribe((event: { dname: string, data: (string | number)[] }[]) => expect(event).toEqual(expectedSelectedData));
        component.deleteSelectedData('toto', 2);
        subscribtion.unsubscribe();
        component.selectedData = [{ dname: 'toto', data: [1] }];
        expectedSelectedData = [];
        subscribtion = component.updateSelectedData.subscribe((event: { dname: string, data: (string | number)[] }[]) => expect(event).toEqual(expectedSelectedData));
        component.deleteSelectedData('toto', 1);
    });

    it('#loadData() should emit retrieveMeta if accordion opened and dataset not included in datasetsWithAttributeList', () => {
        component.datasetsWithAttributeList = [];
        component.retrieveMeta.subscribe((event: string) => expect(event).toEqual('toto'));
        component.loadData(true, 'toto');
    });

    it('#loadData() should emit retrieveData if accordion opened and dataset not included in datasetsWithData', () => {
        component.datasetsWithAttributeList = ['toto'];
        component.allAttributeList = { 'toto': { datasetName: 'toto', isLoading: false, isLoaded: true, attributeList: ATTRIBUTE_LIST }};
        component.datasetsWithData = [];
        const pagination: Pagination = { dname: 'toto', nbItems: 10, page: 1, sortedCol: 1, order: PaginationOrder.a };
        component.retrieveData.subscribe((event: Pagination) => expect(event).toEqual(pagination));
        component.loadData(true, 'toto');
    });
});

