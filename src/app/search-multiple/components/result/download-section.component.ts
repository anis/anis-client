/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Dataset } from '../../../metamodel/model';
import { getHost as host } from '../../../shared/utils';
import { ConeSearch } from '../../../shared/cone-search/store/model';

@Component({
    selector: 'app-download-section',
    templateUrl: 'download-section.component.html',
    styleUrls: ['download-section.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search multiple result download component.
 */
export class DownloadSectionComponent {
    @Input() dataset: Dataset;
    @Input() coneSearch: ConeSearch;
    @Input() outputList: number[];

    /**
     * Returns URL to download data in the given format.
     *
     * @param  {string} format - The format.
     *
     * @return string
     */
    getUrl(format: string): string {
        let query: string = host() + '/search/' + this.dataset.name + '?a=' + this.outputList.join(';');
        query += '&cs=' + this.coneSearch.ra + ':' + this.coneSearch.dec + ':' + this.coneSearch.radius;
        query += '&f=' + format
        return query;
    }
}