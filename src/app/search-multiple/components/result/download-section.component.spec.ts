import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadSectionComponent } from './download-section.component';
import { ConeSearch } from '../../../shared/cone-search/store/model';
import { DATASET, DATASET_LIST } from '../../../../settings/test-data';
import { environment } from '../../../../environments/environment';

describe('[SearchMultiple][Result] Component: DownloadSectionComponent', () => {
    let component: DownloadSectionComponent;
    let fixture: ComponentFixture<DownloadSectionComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DownloadSectionComponent]
        });
        fixture = TestBed.createComponent(DownloadSectionComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getUrl(format) should construct url with format parameter', () => {
        component.dataset = DATASET;
        component.outputList = [1, 2, 3];
        component.coneSearch = { ra: 4, dec: 5, radius: 6 } as ConeSearch;
        const url = component.getUrl('csv');
        expect(url).toBe(environment.apiUrl + '/search/cat_1?a=1;2;3&cs=4:5:6&f=csv');
    });
});

