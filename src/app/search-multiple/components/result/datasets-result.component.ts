/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { Dictionary } from '@ngrx/entity';

import { DataByDataset, DatasetCount } from '../../store/model';
import { Attribute, AttributesByDataset, Dataset, Family } from '../../../metamodel/model';
import { Pagination, PaginationOrder } from '../../../shared/datatable/model';
import { ConeSearch } from '../../../shared/cone-search/store/model';
import { sortByDisplay } from '../../../shared/utils';

@Component({
    selector: 'app-datasets-result',
    templateUrl: 'datasets-result.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Search multiple result datasets component.
 */
export class DatasetsResultComponent {
    @Input() datasetsCountIsLoaded: boolean;
    @Input() datasetFamilyList: Family[];
    @Input() datasetList: Dataset[];
    @Input() coneSearch: ConeSearch;
    @Input() selectedDatasets: string[];
    @Input() datasetsCount: DatasetCount[];
    @Input() datasetsWithAttributeList: (string | number) [];
    @Input() allAttributeList: Dictionary<AttributesByDataset>;
    @Input() datasetsWithData: (string | number) [];
    @Input() allData: Dictionary<DataByDataset>;
    @Input() selectedData: { dname: string, data:  (string | number)[] }[];
    @Output() retrieveMeta: EventEmitter<string> = new EventEmitter();
    @Output() retrieveData: EventEmitter<Pagination> = new EventEmitter();
    @Output() updateSelectedData: EventEmitter<{ dname: string, data: (string | number)[] }[]> = new EventEmitter();

    /**
     * Returns dataset list with result ordered by dataset family display and dataset display.
     *
     * @return Dataset[]
     */
    getOrderedDatasetWithResults(): Dataset[] {
        let datasets: Dataset[] = [];
        const sortedDatasetFamilyList: Family[] = [...this.datasetFamilyList].sort(sortByDisplay);
        sortedDatasetFamilyList.forEach(f => {
            this.datasetList.filter(d => d.id_dataset_family === f.id)
                .sort(sortByDisplay)
                .forEach(d => {
                    this.datasetsCount.forEach(c => {
                        if (c.dname === d.name && c.count > 0) {
                            datasets.push(d);
                        }
                    });
                });
        });
        return datasets;
    }

    /**
     * Returns results number for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return number
     */
    getCount(dname: string): number {
        return this.datasetsCount.find(c => c.dname === dname).count;
    }

    /**
     * Checks if attribute list is loaded for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return boolean
     */
    attributeListIsLoaded(dname: string): boolean {
        return this.datasetsWithAttributeList.includes(dname) && this.allAttributeList[dname].isLoaded;
    }

    /**
     * Returns attribute list for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return Attribute[]
     */
    getDatasetAttributeList(dname: string): Attribute[] {
        if (this.attributeListIsLoaded(dname)) {
            return this.allAttributeList[dname].attributeList;
        }
        return [];
    }

    /**
     * Returns output list for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return number[]
     */
    getOutputList(dname: string): number[] {
        return this.getDatasetAttributeList(dname)
            .filter(attribute => attribute.selected && attribute.id_output_category)
            .sort((a, b) => a.output_display - b.output_display)
            .map(attribute => attribute.id);
    }

    /**
     * Returns data for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return any[]
     */
    getData(dname: string): any[] {
        if (this.datasetsWithData.includes(dname) && this.allData[dname].isLoaded) {
            return this.allData[dname].data;
        }
        return [];
    }

    /**
     * Returns selected data for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return (string | number)[]
     */
    getSelectedData(dname: string): (string | number)[] {
        // if (this.selectedData.find(d => d.dname === dname) === undefined) {
            return [];
        // }
        // return this.selectedData.find(d => d.dname === dname).data;
    }

    /**
     * Adds data to selected data and emits updated data selection.
     *
     * @param  {string} dname - The dataset name.
     * @param  {(string | number)} data - The data ID.
     *
     * @fires EventEmitter<{ dname: string, data: (string | number)[] }[]>
     */
    addSelectedData(dname: string, data: string | number): void {
        const selectedDataByDataset = this.selectedData.find(d => d.dname === dname);
        let updatedSelection: { dname: string, data: (string | number)[] }[];
        if (selectedDataByDataset) {
            updatedSelection = [...this.selectedData.filter(d => d.dname !== dname), { dname: dname, data: [...selectedDataByDataset.data, data] }];
        } else {
            updatedSelection = [...this.selectedData, { dname: dname, data: [data] }];
        }
        this.updateSelectedData.emit(updatedSelection);
    }

    /**
     * Remove data to selected data and emits updated data selection.
     *
     * @param  {string} dname - The dataset name.
     * @param  {(string | number)} data - The data ID.
     *
     * @fires EventEmitter<{ dname: string, data: (string | number)[] }[]>
     */
    deleteSelectedData(dname: string, data: string | number): void {
        const selectedDataByDataset = this.selectedData.find(d => d.dname === dname);
        let updatedSelection: { dname: string, data: (string | number)[] }[];
        if (selectedDataByDataset.data.length === 1) {
            updatedSelection = [...this.selectedData.filter(d => d.dname !== dname)];
        } else {
            updatedSelection = [
                ...this.selectedData.filter(d => d.dname !== dname),
                { dname: dname, data: [...selectedDataByDataset.data.filter(d => d !== data)] }
                ];
        }
        this.updateSelectedData.emit(updatedSelection);
    }

    /**
     * When accordion opens, emits retrieve metadata for the
     * given dataset name if not already loaded and emits retrieve
     * data for the given dataset name otherwise.
     *
     * @param  {boolean} isOpen - If the accordion is open.
     * @param  {string} dname - The dataset name.
     */
    loadData(isOpen: boolean, dname: string) : void {
        if (isOpen && !this.datasetsWithAttributeList.includes(dname)) {
            this.retrieveMeta.emit(dname);
        } else if (isOpen && !this.datasetsWithData.includes(dname)) {
            const sortedCol: number = this.getDatasetAttributeList(dname).find(a => a.order_by).id;
            const pagination: Pagination = { dname: dname, nbItems: 10, page: 1, sortedCol: sortedCol, order: PaginationOrder.a };
            this.retrieveData.emit(pagination);
        }
    }

    /**
     * Emits execute process event.
     *
     * @param  {string} typeProcess - The process type.
     */
    executeProcess(typeProcess: string): void {
        // console.log('executeProcess: ' + typeProcess);
    }
}