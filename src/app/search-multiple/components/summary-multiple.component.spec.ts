import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { SummaryMultipleComponent } from './summary-multiple.component';
import { DATASET, DATASET_FAMILY_LIST, DATASET_LIST } from '../../../settings/test-data';

describe('[SearchMultiple] Component: SummaryMultipleComponent', () => {
    let component: SummaryMultipleComponent;
    let fixture: ComponentFixture<SummaryMultipleComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SummaryMultipleComponent],
            imports: [AccordionModule.forRoot(), BrowserAnimationsModule]
        });
        fixture = TestBed.createComponent(SummaryMultipleComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getDatasetFamilyList() should return list of dataset families that contains datasets with cone search enabled', () => {
        component.datasetFamilyList = DATASET_FAMILY_LIST;
        component.datasetList = [DATASET];
        expect(component.getDatasetFamilyList()[0].label).toBe('Default dataset family');
    });

    it('#getSelectedDatasetsByFamily() should return list of selected datasets for the given dataset family', () => {
        component.datasetList = DATASET_LIST;
        component.selectedDatasets = ['cat_1'];
        expect(component.getSelectedDatasetsByFamily(1).length).toEqual(1);
        expect(component.getSelectedDatasetsByFamily(1)[0].name).toEqual('cat_1');
    });
});

