/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromSearchMultiple from '../store/search-multiple.reducer';
import * as searchMultipleActions from '../store/search-multiple.action';
import * as searchMultipleSelector from '../store/search-multiple.selector';
import { SearchMultipleQueryParams } from '../store/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import { Dataset, Family, Survey } from '../../metamodel/model';
import * as metamodelSelector from '../../metamodel/selectors';
import * as coneSearchSelector from '../../shared/cone-search/store/cone-search.selector';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    searchMultiple: fromSearchMultiple.State;
    metamodel: fromMetamodel.State;
}

@Component({
    selector: 'app-datasets',
    templateUrl: 'datasets.component.html'
})
/**
 * @class
 * @classdesc Search multiple datasets container.
 *
 * @implements OnInit
 */
export class DatasetsComponent implements OnInit {
    public datasetSearchMetaIsLoading: Observable<boolean>;
    public datasetSearchMetaIsLoaded: Observable<boolean>;
    public currentStep: Observable<string>;
    public surveyList: Observable<Survey[]>;
    public datasetFamilyList: Observable<Family[]>;
    public datasetList: Observable<Dataset[]>;
    public isValidConeSearch: Observable<boolean>;
    public coneSearch: Observable<ConeSearch>;
    public selectedDatasets: Observable<string[]>;
    public noSelectedDatasets: Observable<boolean>;
    public queryParams: Observable<SearchMultipleQueryParams>;

    constructor(private store: Store<StoreState>, private scrollTopService: ScrollTopService) {
        this.datasetSearchMetaIsLoading = store.select(metamodelSelector.getDatasetSearchMetaIsLoading);
        this.datasetSearchMetaIsLoaded = store.select(metamodelSelector.getDatasetSearchMetaIsLoaded);
        this.currentStep = store.select(searchMultipleSelector.getCurrentStep);
        this.surveyList = store.select(metamodelSelector.getSurveyList);
        this.datasetFamilyList = store.select(metamodelSelector.getDatasetFamilyList);
        this.datasetList = store.select(metamodelSelector.getDatasetWithConeSearchList);
        this.isValidConeSearch = this.store.select(coneSearchSelector.getIsValidConeSearch);
        this.selectedDatasets = this.store.select(searchMultipleSelector.getSelectedDatasets);
        this.noSelectedDatasets = this.store.select(searchMultipleSelector.getNoSelectedDatasets);
        this.coneSearch = this.store.select(coneSearchSelector.getConeSearch);
        this.queryParams = this.store.select(searchMultipleSelector.getQueryParams);
    }

    ngOnInit() {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(new searchMultipleActions.InitSearchByUrlAction()));
        Promise.resolve(null).then(() => this.store.dispatch(new searchMultipleActions.ChangeStepAction('datasets')));
        this.store.dispatch(new datasetActions.LoadDatasetSearchMetaAction());
        this.scrollTopService.setScrollTop();
    }

    /**
     * Dispatches action to check datasets step.
     */
    checkStep(): void {
        this.store.dispatch(new searchMultipleActions.DatasetsCheckedAction());
    }

    /**
     * Dispatches action to update dataset list selection with the given updated selected dataset name list.
     *
     * @param  {string[]} selectedDatasets - The updated dataset name list.
     */
    updateSelectedDatasets(selectedDatasets: string[]): void {
        this.store.dispatch(new searchMultipleActions.UpdateSelectedDatasetsAction(selectedDatasets));
    }
}
