import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Dictionary } from '@ngrx/entity';

import { ResultMultipleComponent } from './result-multiple.component';
import * as fromSearchMultiple from '../store/search-multiple.reducer';
import * as searchMultipleActions from '../store/search-multiple.action';
import { DataByDataset, DatasetCount, SearchMultipleQueryParams } from '../store/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as attributeActions from '../../metamodel/action/attribute.action';
import { AttributesByDataset, Dataset, Family } from '../../metamodel/model';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';
import { Pagination, PaginationOrder } from '../../shared/datatable/model';
import { RouterLinkDirectiveStub } from '../../../settings/test-data/router-link-directive-stub';

describe('[SearchMultiple] Container: ResultMultipleComponent', () => {
    @Component({ selector: 'app-overview', template: '' })
    class OverviewStubComponent {
        @Input() datasetSearchMetaIsLoaded: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() datasetFamilyList: Family[];
        @Input() datasetList: Dataset[];
        @Input() selectedDatasets: string[];
        @Input() queryParams: SearchMultipleQueryParams;
        @Input() datasetsCountIsLoaded: boolean;
        @Input() datasetsCount: DatasetCount[];
    }

    @Component({ selector: 'app-datasets-result', template: '' })
    class DatasetsResultStubComponent {
        @Input() datasetsCountIsLoaded: boolean;
        @Input() datasetFamilyList: Family[];
        @Input() datasetList: Dataset[];
        @Input() selectedDatasets: string[];
        @Input() datasetsCount: DatasetCount[];
        @Input() datasetsWithAttributeList: (string | number) [];
        @Input() allAttributeList: Dictionary<AttributesByDataset>;
        @Input() datasetsWithData: (string | number) [];
        @Input() allData: Dictionary<DataByDataset>;
        @Input() selectedData: { dname: string, data:  (string | number)[] }[];
    }

    let scrollTopServiceStub: Partial<ScrollTopService> = {
        setScrollTop() {}
    };

    let component: ResultMultipleComponent;
    let fixture: ComponentFixture<ResultMultipleComponent>;
    let store: MockStore;
    const initialState = {
        searchMultiple: { ...fromSearchMultiple.initialState },
        metamodel: { ...fromMetamodel },
        coneSearch: { ...fromConeSearch }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                ResultMultipleComponent,
                OverviewStubComponent,
                DatasetsResultStubComponent,
                RouterLinkDirectiveStub
            ],
            providers: [
                provideMockStore({ initialState }),
                { provide: ScrollTopService, useValue: scrollTopServiceStub }
            ]
        });
        fixture = TestBed.createComponent(ResultMultipleComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        const loadDatasetSearchMetaAction = new datasetActions.LoadDatasetSearchMetaAction();
        const initSearchByUrlAction = new searchMultipleActions.InitSearchByUrlAction();
        const changeStepAction = new searchMultipleActions.ChangeStepAction('result');
        const spy = spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(3);
            expect(spy).toHaveBeenCalledWith(loadDatasetSearchMetaAction);
            expect(spy).toHaveBeenCalledWith(changeStepAction);
            expect(spy).toHaveBeenCalledWith(initSearchByUrlAction);
            done();
        });
    });

    it('#getDatasetsCount() should dispatch RetrieveDatasetsCountAction', () => {
        const retrieveDatasetsCountAction = new searchMultipleActions.RetrieveDatasetsCountAction();
        const spy = spyOn(store, 'dispatch');
        component.getDatasetsCount();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(retrieveDatasetsCountAction);
    });

    it('#retrieveMeta() should dispatch LoadAttributeListAction', () => {
        const loadAttributeListAction = new attributeActions.LoadAttributeListAction('toto');
        const spy = spyOn(store, 'dispatch');
        component.retrieveMeta('toto');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(loadAttributeListAction);
    });

    it('#retrieveData() should dispatch RetrieveDataAction', () => {
        const pagination: Pagination = { dname: 'toto', nbItems: 10, page: 1, sortedCol: 1, order: PaginationOrder.a };
        const retrieveDataAction = new searchMultipleActions.RetrieveDataAction(pagination);
        const spy = spyOn(store, 'dispatch');
        component.retrieveData(pagination);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(retrieveDataAction);
    });

    it('#updateSelectedData() should dispatch UpdateSelectedDataAction', () => {
        const data: { dname: string, data: (string | number)[] } = { dname: 'toto', data: [1] };
        const updateSelectedDataAction = new searchMultipleActions.UpdateSelectedDataAction([data]);
        const spy = spyOn(store, 'dispatch');
        component.updateSelectedData([data]);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(updateSelectedDataAction);
    });

    it('#ngOnDestroy() should dispatch DestroyResultsAction', () => {
        const destroyResultsAction = new searchMultipleActions.DestroyResultsAction();
        const spy = spyOn(store, 'dispatch');
        component.ngOnDestroy();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(destroyResultsAction);
    });
});
