import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { SearchMultipleComponent } from './search-multiple.component';
import * as fromSearchMultiple from '../store/search-multiple.reducer';
import { SearchMultipleQueryParams } from '../store/model';

describe('[SearchMultiple] Container: SearchMultipleComponent', () => {
    @Component({ selector: 'app-progress-bar-multiple', template: '' })
    class ProgressBarMultipleStubComponent {
        @Input() currentStep: string;
        @Input() positionStepChecked: boolean;
        @Input() datasetsStepChecked: boolean;
        @Input() resultStepChecked: boolean;
        @Input() isValidConeSearch: boolean;
        @Input() noSelectedDatasets: boolean;
        @Input() queryParams: SearchMultipleQueryParams;
        @Input() outputListEmpty: boolean;
    }

    @Component({ selector: 'router-outlet', template: '' })
    class RouterOutletStubComponent { }

    let component: SearchMultipleComponent;
    let fixture: ComponentFixture<SearchMultipleComponent>;
    let store: MockStore;
    const initialState = { searchMultiple: { ...fromSearchMultiple.initialState } };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                SearchMultipleComponent,
                ProgressBarMultipleStubComponent,
                RouterOutletStubComponent
            ],
            providers: [provideMockStore({ initialState })]
        });
        fixture = TestBed.createComponent(SearchMultipleComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});

