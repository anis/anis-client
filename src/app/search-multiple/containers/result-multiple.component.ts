/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { Dictionary } from '@ngrx/entity';
import { Observable } from 'rxjs';

import * as fromSearchMultiple from '../store/search-multiple.reducer';
import * as searchMultipleActions from '../store/search-multiple.action';
import * as searchMultipleSelector from '../store/search-multiple.selector';
import { DataByDataset, DatasetCount, SearchMultipleQueryParams } from '../store/model';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as attributeActions from '../../metamodel/action/attribute.action';
import * as metamodelSelector from '../../metamodel/selectors';
import { AttributesByDataset, Dataset, Family } from '../../metamodel/model';
import * as coneSearchSelector from '../../shared/cone-search/store/cone-search.selector';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { Pagination } from '../../shared/datatable/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    searchMultiple: fromSearchMultiple.State;
    metamodel: fromMetamodel.State;
}

@Component({
    selector: 'app-result-multiple',
    templateUrl: 'result-multiple.component.html'
})
/**
 * @class
 * @classdesc Search multiple result container.
 *
 * @implements OnInit
 * @implements OnDestroy
 */
export class ResultMultipleComponent implements OnInit, OnDestroy {
    public datasetSearchMetaIsLoading: Observable<boolean>;
    public datasetSearchMetaIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<Family[]>;
    public datasetList: Observable<Dataset[]>;
    public currentStep: Observable<string>;
    public coneSearch: Observable<ConeSearch>;
    public selectedDatasets: Observable<string[]>;
    public queryParams: Observable<SearchMultipleQueryParams>;
    public datasetsCountIsLoading: Observable<boolean>;
    public datasetsCountIsLoaded: Observable<boolean>;
    public datasetsCount: Observable<DatasetCount[]>;
    public datasetsWithAttributeList: Observable<string[] | number[]>;
    public allAttributeList: Observable<Dictionary<AttributesByDataset>>;
    public data: Observable<{ dname: string, data: any[]}[]>;
    public datasetsWithData: Observable<string[] | number[]>;
    public allData: Observable<Dictionary<DataByDataset>>;
    public selectedData: Observable<{ dname: string, data: (number | string)[] }[]>;

    constructor(private store: Store<StoreState>, private scrollTopService: ScrollTopService) {
        this.datasetSearchMetaIsLoading = store.select(metamodelSelector.getDatasetSearchMetaIsLoading);
        this.datasetSearchMetaIsLoaded = store.select(metamodelSelector.getDatasetSearchMetaIsLoaded);
        this.currentStep = store.select(searchMultipleSelector.getCurrentStep);
        this.datasetFamilyList = store.select(metamodelSelector.getDatasetFamilyList);
        this.datasetList = store.select(metamodelSelector.getDatasetList);
        this.coneSearch = this.store.select(coneSearchSelector.getConeSearch);
        this.selectedDatasets = this.store.select(searchMultipleSelector.getSelectedDatasets);
        this.queryParams = this.store.select(searchMultipleSelector.getQueryParams);
        this.datasetsCountIsLoading = this.store.select(searchMultipleSelector.getDatasetsCountIsLoading);
        this.datasetsCountIsLoaded = this.store.select(searchMultipleSelector.getDatasetsCountIsLoaded);
        this.datasetsCount = this.store.select(searchMultipleSelector.getDatasetsCount);
        this.datasetsWithAttributeList = this.store.select(metamodelSelector.getDatasetsWithAttributeList);
        this.allAttributeList = this.store.select(metamodelSelector.getAllAttributeList);
        this.datasetsWithData = this.store.select(searchMultipleSelector.getDatasetsWithData);
        this.allData = this.store.select(searchMultipleSelector.getAllData);
        this.selectedData = this.store.select(searchMultipleSelector.getSelectedData);
    }

    ngOnInit() {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(new searchMultipleActions.ChangeStepAction('result')));
        Promise.resolve(null).then(() => this.store.dispatch(new searchMultipleActions.InitSearchByUrlAction()));
        this.store.dispatch(new datasetActions.LoadDatasetSearchMetaAction());
        this.scrollTopService.setScrollTop();
    }

    /**
     * Dispatches action to retrieve dataset result number.
     */
    getDatasetsCount(): void {
        this.store.dispatch(new searchMultipleActions.RetrieveDatasetsCountAction());
    }

    /**
     * Dispatches action to retrieve metadata of a given dataset name.
     *
     * @param  {string} dname - The dataset name.
     */
    retrieveMeta(dname: string): void {
        this.store.dispatch(new attributeActions.LoadAttributeListAction(dname));
    }

    /**
     * Dispatches action to retrieve data with the given pagination parameters.
     *
     * @param  {Pagination} params - The pagination parameters.
     */
    retrieveData(params: Pagination): void {
        this.store.dispatch(new searchMultipleActions.RetrieveDataAction(params));
    }

    /**
     * Dispatches action to update data selection with the given updated selected data.
     *
     * @param  {{ dname: string, data: (string | number)[] }[]} data - The updated selected data.
     */
    updateSelectedData(data: { dname: string, data: (string | number)[] }[]): void {
        this.store.dispatch(new searchMultipleActions.UpdateSelectedDataAction(data));
    }

    // executeProcess(typeProcess: string): void {
    //     this.store.dispatch(new searchActions.ExecuteProcessAction(typeProcess));
    // }

    /**
     * Dispatches action to destroy search multiple results.
     */
    ngOnDestroy() {
        this.store.dispatch(new searchMultipleActions.DestroyResultsAction());
    }
}
