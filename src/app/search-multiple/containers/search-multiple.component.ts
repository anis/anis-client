/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as searchMultipleReducer from '../store/search-multiple.reducer';
import * as searchMultipleSelector from '../store/search-multiple.selector';
import { SearchMultipleQueryParams } from '../store/model';
import * as searchActions from '../../search/store/search.action';
import * as coneSearchSelector from '../../shared/cone-search/store/cone-search.selector';
import * as coneSearchActions from '../../shared/cone-search/store/cone-search.action';

@Component({
    selector: 'app-search-multiple',
    templateUrl: 'search-multiple.component.html'
})
/**
 * @class
 * @classdesc Search multiple container.
 */
export class SearchMultipleComponent {
    public currentStep: Observable<string>;
    public positionStepChecked: Observable<boolean>;
    public datasetsStepChecked: Observable<boolean>;
    public resultStepChecked: Observable<boolean>;
    public isValidConeSearch: Observable<boolean>;
    public noSelectedDatasets: Observable<boolean>;
    public queryParams: Observable<SearchMultipleQueryParams>;

    constructor(private store: Store<searchMultipleReducer.State>) {
        this.currentStep = store.select(searchMultipleSelector.getCurrentStep);
        this.positionStepChecked = store.select(searchMultipleSelector.getPositionStepChecked);
        this.datasetsStepChecked = store.select(searchMultipleSelector.getDatasetsStepChecked);
        this.resultStepChecked = store.select(searchMultipleSelector.getResultStepChecked);
        this.isValidConeSearch = store.select(coneSearchSelector.getIsValidConeSearch);
        this.noSelectedDatasets = store.select(searchMultipleSelector.getNoSelectedDatasets);
        this.queryParams = store.select(searchMultipleSelector.getQueryParams);
        this.store.dispatch(new searchActions.ResetSearchAction());
        this.store.dispatch(new coneSearchActions.DeleteConeSearchAction());
    }
}
