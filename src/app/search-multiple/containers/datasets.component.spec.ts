import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { DatasetsComponent } from './datasets.component';
import * as fromMetamodel from '../../metamodel/reducers';
import * as fromSearchMultiple from '../store/search-multiple.reducer';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as searchMultipleActions from '../store/search-multiple.action';
import { Dataset, Family } from '../../metamodel/model';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { SearchMultipleQueryParams } from '../store/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';
import { RouterLinkDirectiveStub } from '../../../settings/test-data/router-link-directive-stub';

describe('[SearchMultiple] Container: DatasetsComponent', () => {
    @Component({ selector: 'app-dataset-list', template: '' })
    class DatasetListStubComponent {
        @Input() datasetFamilyList: Family[];
        @Input() datasetList: Dataset[];
        @Input() selectedDatasets: string[];
        @Input() isConeSearchAdded: boolean;
    }

    @Component({ selector: 'app-summary-multiple', template: '' })
    class SummaryMultipleStubComponent {
        @Input() currentStep: string;
        @Input() isValidConeSearch: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() datasetSearchMetaIsLoading: boolean;
        @Input() datasetSearchMetaIsLoaded: boolean;
        @Input() datasetFamilyList: Family[];
        @Input() datasetList: Dataset[];
        @Input() selectedDatasets: string[];
        @Input() noSelectedDatasets: boolean;
        @Input() queryParams: SearchMultipleQueryParams;
    }

    let scrollTopServiceStub: Partial<ScrollTopService> = {
        setScrollTop() {}
    };

    let component: DatasetsComponent;
    let fixture: ComponentFixture<DatasetsComponent>;
    let store: MockStore;
    const initialState = {
        searchMultiple: { ...fromSearchMultiple.initialState },
        metamodel: { ...fromMetamodel },
        coneSearch: { ...fromConeSearch }
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetsComponent,
                DatasetListStubComponent,
                SummaryMultipleStubComponent,
                RouterLinkDirectiveStub
            ],
            providers: [
                provideMockStore({ initialState }),
                { provide: ScrollTopService, useValue: scrollTopServiceStub }
            ]
        });
        fixture = TestBed.createComponent(DatasetsComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        const loadDatasetSearchMetaAction = new datasetActions.LoadDatasetSearchMetaAction();
        const initSearchByUrlAction = new searchMultipleActions.InitSearchByUrlAction();
        const changeStepAction = new searchMultipleActions.ChangeStepAction('datasets');
        const spy = spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(3);
            expect(spy).toHaveBeenCalledWith(loadDatasetSearchMetaAction);
            expect(spy).toHaveBeenCalledWith(changeStepAction);
            expect(spy).toHaveBeenCalledWith(initSearchByUrlAction);
            done();
        });
    });

    it('#checkStep() should dispatch DatasetsCheckedAction', () => {
        const datasetsCheckedAction = new searchMultipleActions.DatasetsCheckedAction();
        const spy = spyOn(store, 'dispatch');
        component.checkStep();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetsCheckedAction);
    });

    it('#updateSelectedDatasets() should dispatch UpdateSelectedDatasetsAction', () => {
        const updateSelectedDatasetsAction = new searchMultipleActions.UpdateSelectedDatasetsAction(['toto']);
        const spy = spyOn(store, 'dispatch');
        component.updateSelectedDatasets(['toto']);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(updateSelectedDatasetsAction);
    });
});
