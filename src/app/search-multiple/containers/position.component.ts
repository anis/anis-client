/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromSearchMultiple from '../store/search-multiple.reducer';
import * as searchMultipleActions from '../store/search-multiple.action';
import * as searchMultipleSelector from '../store/search-multiple.selector';
import { SearchMultipleQueryParams } from '../store/model';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as fromMetamodel from '../../metamodel/reducers';
import * as metamodelSelector from '../../metamodel/selectors';
import { Dataset, Family } from '../../metamodel/model';
import * as coneSearchActions from '../../shared/cone-search/store/cone-search.action';
import * as coneSearchSelector from '../../shared/cone-search/store/cone-search.selector';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    searchMultiple: fromSearchMultiple.State;
    metamodel: fromMetamodel.State;
}

@Component({
    selector: 'app-position',
    templateUrl: 'position.component.html',
    styleUrls: ['position.component.css']
})
/**
 * @class
 * @classdesc Search multiple position container.
 *
 * @implements OnInit
 */
export class PositionComponent implements OnInit {
    public datasetSearchMetaIsLoading: Observable<boolean>;
    public datasetSearchMetaIsLoaded: Observable<boolean>;
    public currentStep: Observable<string>;
    public isValidConeSearch: Observable<boolean>;
    public coneSearch: Observable<ConeSearch>;
    public datasetFamilyList: Observable<Family[]>;
    public datasetList: Observable<Dataset[]>;
    public selectedDatasets: Observable<string[]>;
    public noSelectedDatasets: Observable<boolean>;
    public queryParams: Observable<SearchMultipleQueryParams>;

    constructor(private store: Store<StoreState>, private scrollTopService: ScrollTopService) {
        this.datasetSearchMetaIsLoading = store.select(metamodelSelector.getDatasetSearchMetaIsLoading);
        this.datasetSearchMetaIsLoaded = store.select(metamodelSelector.getDatasetSearchMetaIsLoaded);
        this.currentStep = store.select(searchMultipleSelector.getCurrentStep);
        this.isValidConeSearch = this.store.select(coneSearchSelector.getIsValidConeSearch);
        this.coneSearch = this.store.select(coneSearchSelector.getConeSearch);
        this.datasetFamilyList = store.select(metamodelSelector.getDatasetFamilyList);
        this.datasetList = store.select(metamodelSelector.getDatasetWithConeSearchList);
        this.selectedDatasets = store.select(searchMultipleSelector.getSelectedDatasets);
        this.noSelectedDatasets = store.select(searchMultipleSelector.getNoSelectedDatasets);
        this.queryParams = this.store.select(searchMultipleSelector.getQueryParams);
    }

    ngOnInit() {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(new searchMultipleActions.InitSearchByUrlAction()));
        Promise.resolve(null).then(() => this.store.dispatch(new searchMultipleActions.ChangeStepAction('position')));
        this.store.dispatch(new datasetActions.LoadDatasetSearchMetaAction());
        this.scrollTopService.setScrollTop();
    }

    /**
     * Dispatches action to check position step.
     */
    checkStep(): void {
        this.store.dispatch(new searchMultipleActions.PositionCheckedAction());
    }

    /**
     * Dispatches action to reset cone search.
     */
    resetConeSearch(): void {
        this.store.dispatch(new coneSearchActions.DeleteConeSearchAction());
    }
}
