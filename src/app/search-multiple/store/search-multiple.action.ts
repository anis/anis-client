/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import { DatasetCount } from './model';
import { Pagination } from '../../shared/datatable/model';


export const INIT_SEARCH_BY_URL = '[SearchMultiple] Init By Url';
export const INIT_SELECTED_DATASETS = '[SearchMultiple] Init Selected Datasets';
export const CHANGE_STEP = '[SearchMultiple] Change Step';
export const POSITION_CHECKED = '[SearchMultiple] Position Checked';
export const DATASETS_CHECKED = '[SearchMultiple] Datasets Checked';
export const RESULTS_CHECKED = '[SearchMultiple] Results Checked';
export const UPDATE_SELECTED_DATASETS = '[SearchMultiple] Update Selected Datasets';
export const RETRIEVE_DATASETS_COUNT = '[SearchMultiple] Retrieve Datasets Count';
export const RETRIEVE_DATASETS_COUNT_SUCCESS = '[SearchMultiple] Retrieve Datasets Count Success';
export const RETRIEVE_DATASETS_COUNT_FAIL = '[SearchMultiple] Retrieve Datasets Count Fail';
export const RETRIEVE_DATA = '[SearchMultiple] Retrieve Data';
export const RETRIEVE_DATA_SUCCESS = '[SearchMultiple] Retrieve Data Success';
export const RETRIEVE_DATA_FAIL = '[SearchMultiple] Retrieve Data Fail';
export const UPDATE_SELECTED_DATA = '[SearchMultiple] Update Selected Data';
export const DESTROY_RESULTS = '[SearchMultiple] Destroy Results';
export const RESET_SEARCH = '[SearchMultiple] Reset Search';

/**
 * @class
 * @classdesc InitSearchByUrlAction action.
 * @readonly
 */
export class InitSearchByUrlAction implements Action {
    readonly type = INIT_SEARCH_BY_URL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc InitSelectedDatasetsAction action.
 * @readonly
 */
export class InitSelectedDatasetsAction implements Action {
    readonly type = INIT_SELECTED_DATASETS;

    constructor(public payload: string[]) { }
}

/**
 * @class
 * @classdesc ChangeStepAction action.
 * @readonly
 */
export class ChangeStepAction implements Action {
    readonly type = CHANGE_STEP;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc PositionCheckedAction action.
 * @readonly
 */
export class PositionCheckedAction implements Action {
    readonly type = POSITION_CHECKED;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc DatasetsCheckedAction action.
 * @readonly
 */
export class DatasetsCheckedAction implements Action {
    readonly type = DATASETS_CHECKED;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc ResultsCheckedAction action.
 * @readonly
 */
export class ResultsCheckedAction implements Action {
    readonly type = RESULTS_CHECKED;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc UpdateSelectedDatasetsAction action.
 * @readonly
 */
export class UpdateSelectedDatasetsAction implements Action {
    readonly type = UPDATE_SELECTED_DATASETS;

    constructor(public payload: string[]) { }
}

/**
 * @class
 * @classdesc RetrieveDatasetsCountAction action.
 * @readonly
 */
export class RetrieveDatasetsCountAction implements Action {
    readonly type = RETRIEVE_DATASETS_COUNT;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc RetrieveDatasetsCountSuccessAction action.
 * @readonly
 */
export class RetrieveDatasetsCountSuccessAction implements Action {
    readonly type = RETRIEVE_DATASETS_COUNT_SUCCESS;

    constructor(public payload: DatasetCount[]) { }
}

/**
 * @class
 * @classdesc RetrieveDatasetsCountFailAction action.
 * @readonly
 */
export class RetrieveDatasetsCountFailAction implements Action {
    readonly type = RETRIEVE_DATASETS_COUNT_FAIL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc RetrieveDataAction action.
 * @readonly
 */
export class RetrieveDataAction implements Action {
    readonly type = RETRIEVE_DATA;

    constructor(public payload: Pagination) { }
}

/**
 * @class
 * @classdesc RetrieveDataSuccessAction action.
 * @readonly
 */
export class RetrieveDataSuccessAction implements Action {
    readonly type = RETRIEVE_DATA_SUCCESS;

    constructor(public payload: { datasetName: string, data: any[] }) { }
}

/**
 * @class
 * @classdesc RetrieveDataFailAction action.
 * @readonly
 */
export class RetrieveDataFailAction implements Action {
    readonly type = RETRIEVE_DATA_FAIL;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc UpdateSelectedDataAction action.
 * @readonly
 */
export class UpdateSelectedDataAction implements Action {
    readonly type = UPDATE_SELECTED_DATA;

    constructor(public payload: { dname: string, data: (string | number)[] }[]) { }
}

/**
 * @class
 * @classdesc DestroyResultsAction action.
 * @readonly
 */
export class DestroyResultsAction implements Action {
    readonly type = DESTROY_RESULTS;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc ResetSearchAction action.
 * @readonly
 */
export class ResetSearchAction implements Action {
    readonly type = RESET_SEARCH;

    constructor(public payload: {} = null) { }
}

export type Actions
    = InitSearchByUrlAction
    | InitSelectedDatasetsAction
    | ChangeStepAction
    | PositionCheckedAction
    | DatasetsCheckedAction
    | ResultsCheckedAction
    | UpdateSelectedDatasetsAction
    | RetrieveDatasetsCountAction
    | RetrieveDatasetsCountSuccessAction
    | RetrieveDatasetsCountFailAction
    | RetrieveDataAction
    | RetrieveDataSuccessAction
    | RetrieveDataFailAction
    | UpdateSelectedDataAction
    | DestroyResultsAction
    | ResetSearchAction;
