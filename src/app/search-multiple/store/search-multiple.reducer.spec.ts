import * as fromSearchMultiple from './search-multiple.reducer';
import * as searchMultipleActions from './search-multiple.action';
import { DataByDataset, DatasetCount } from './model';
import { Pagination, PaginationOrder } from '../../shared/datatable/model';

describe('[SearchMultiple] Reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromSearchMultiple;
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should init selectedDatasets', () => {
        const { initialState } = fromSearchMultiple;
        const action = new searchMultipleActions.InitSelectedDatasetsAction(['toto', 'tutu']);
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(2);
        expect(state.selectedDatasets.includes('toto')).toBeTruthy();
        expect(state.selectedDatasets.includes('tutu')).toBeTruthy();
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set currentStep to "position"', () => {
        const { initialState } = fromSearchMultiple;
        const action = new searchMultipleActions.ChangeStepAction('position');
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toEqual('position');
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set positionStepChecked to true', () => {
        const { initialState } = fromSearchMultiple;
        const action = new searchMultipleActions.PositionCheckedAction();
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeFalsy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeTruthy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set datasetsStepChecked to true', () => {
        const { initialState } = fromSearchMultiple;
        const action = new searchMultipleActions.DatasetsCheckedAction();
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeTruthy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set resultStepChecked to true', () => {
        const { initialState } = fromSearchMultiple;
        const action = new searchMultipleActions.ResultsCheckedAction();
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeTruthy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should change selectedDatasets', () => {
        const { initialState } = fromSearchMultiple;
        const action = new searchMultipleActions.UpdateSelectedDatasetsAction(['toto', 'tutu']);
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(2);
        expect(state.selectedDatasets.includes('toto')).toBeTruthy();
        expect(state.selectedDatasets.includes('tutu')).toBeTruthy();
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set datasetsCountIsLoading to true', () => {
        const { initialState } = fromSearchMultiple;
        const action = new searchMultipleActions.RetrieveDatasetsCountAction();
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeTruthy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set datasetsCountIsLoading to false, set datasetsCountIsLoaded to true and set datasetsCount', () => {
        const datasetsCount: DatasetCount[] = [{ dname: 'toto', count: 2}];
        const initialState = { ...fromSearchMultiple.initialState, datasetsCountIsLoading: true };
        const action = new searchMultipleActions.RetrieveDatasetsCountSuccessAction(datasetsCount);
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeTruthy();
        expect(state.datasetsCount).toEqual(datasetsCount);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set datasetsCountIsLoading to false', () => {
        const initialState = { ...fromSearchMultiple.initialState, datasetsCountIsLoading: true };
        const action = new searchMultipleActions.RetrieveDatasetsCountFailAction();
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set data', () => {
        const pagination: Pagination = { dname: 'toto', nbItems: 10, page: 1, sortedCol: 1, order: PaginationOrder.a };
        const expectedEntity: DataByDataset = { datasetName: 'toto', isLoading: true, isLoaded: false, data: [] };
        const { initialState } = fromSearchMultiple;
        const action = new searchMultipleActions.RetrieveDataAction(pagination);
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain('toto');
        expect(state.entities['toto']).toEqual(expectedEntity);
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should update data when success', () => {
        const expectedEntity: DataByDataset = { datasetName: 'toto', isLoading: false, isLoaded: true, data: [1, 2] };
        const initialState = {
            ...fromSearchMultiple.initialState,
            ids: ['toto'],
            entities: { 'toto': { datasetName: 'toto', isLoading: true, isLoaded: false, data: [] }}
        };
        const action = new searchMultipleActions.RetrieveDataSuccessAction({ datasetName: 'toto', data: [1, 2] });
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain('toto');
        expect(state.entities['toto']).toEqual(expectedEntity);
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should update data when fail', () => {
        const expectedEntity: DataByDataset = { datasetName: 'toto', isLoading: false, isLoaded: false, data: [] };
        const initialState = {
            ...fromSearchMultiple.initialState,
            ids: ['toto'],
            entities: { 'toto': { datasetName: 'toto', isLoading: true, isLoaded: false, data: [] }}
        };
        const action = new searchMultipleActions.RetrieveDataFailAction('toto');
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain('toto');
        expect(state.entities['toto']).toEqual(expectedEntity);
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should change selectedData', () => {
        const { initialState } = fromSearchMultiple;
        const action = new searchMultipleActions.UpdateSelectedDataAction([{ dname: 'toto', data:[1, 2] }]);
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(1);
        expect(state.selectedData).toContain({ dname: 'toto', data:[1, 2] });
        expect(state).not.toEqual(initialState);
    });

    it('should destroy results', () => {
        const initialState = {
            ...fromSearchMultiple.initialState,
            ids: ['toto'],
            entities: { 'toto': { datasetName: 'toto', isLoading: false, isLoaded: true, data: [1, 2] }},
            datasetsCountIsLoaded: true,
            datasetsCount: [{ dname: 'toto', count: 2}],
            selectedData: [{ dname: 'toto', data:[1, 2] }]
        };
        const action = new searchMultipleActions.DestroyResultsAction();
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should reset search multiple', () => {
        const initialState = {
            ...fromSearchMultiple.initialState,
            ids: ['toto'],
            entities: { 'toto': { datasetName: 'toto', isLoading: false, isLoaded: true, data: [1, 2] }},
            pristine: false,
            currentStep: 'datasets',
            positionStepChecked: true,
            datasetsStepChecked: true,
            resultStepChecked: true,
            selectedDatasets: ['toto'],
            datasetsCountIsLoading: true,
            datasetsCountIsLoaded: true,
            datasetsCount: [{ dname: 'toto', count: 2}],
            selectedData: [{ dname: 'toto', data:[1, 2] }]
        };
        const action = new searchMultipleActions.ResetSearchAction();
        const state = fromSearchMultiple.reducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({});
        expect(state.pristine).toBeTruthy();
        expect(state.currentStep).toBeNull();
        expect(state.positionStepChecked).toBeFalsy();
        expect(state.datasetsStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.selectedDatasets.length).toEqual(0);
        expect(state.datasetsCountIsLoading).toBeFalsy();
        expect(state.datasetsCountIsLoaded).toBeFalsy();
        expect(state.datasetsCount.length).toEqual(0);
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should get selectAll', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.selectAll(state).length).toEqual(0);
    });

    it('should get selectEntities', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.selectEntities(state)).toEqual({});
    });

    it('should get selectIds', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.selectIds(state).length).toEqual(0);
    });

    it('should get selectTotal', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.selectTotal(state)).toEqual(0);
    });

    it('should get currentStep', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.getCurrentStep(state)).toBeNull();
    });

    it('should get positionStepChecked', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.getPositionStepChecked(state)).toBeFalsy();
    });

    it('should get datasetsStepChecked', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.getDatasetsStepChecked(state)).toBeFalsy();
    });

    it('should get resultStepChecked', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.getResultStepChecked(state)).toBeFalsy();
    });

    it('should get selectedDatasets', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.getSelectedDatasets(state).length).toEqual(0);
    });

    it('should get datasetsCountIsLoading', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.getDatasetsCountIsLoading(state)).toBeFalsy();
    });

    it('should get datasetsCountIsLoaded', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.getDatasetsCountIsLoaded(state)).toBeFalsy();
    });

    it('should get datasetsCount', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.getDatasetsCount(state).length).toEqual(0);
    });

    it('should get selectedData', () => {
        const action = {} as searchMultipleActions.Actions;
        const state = fromSearchMultiple.reducer(undefined, action);

        expect(fromSearchMultiple.getSelectedData(state).length).toEqual(0);
    });
});
