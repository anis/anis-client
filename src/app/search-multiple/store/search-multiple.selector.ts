/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as searchMultiple from './search-multiple.reducer';
import { getConeSearch, getIsValidConeSearch } from '../../shared/cone-search/store/cone-search.selector';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { SearchMultipleQueryParams } from './model';


export const getSearchMultipleState = createFeatureSelector<searchMultiple.State>('searchMultiple');

export const getCurrentStep = createSelector(
    getSearchMultipleState,
    searchMultiple.getCurrentStep
);

export const getPositionStepChecked = createSelector(
    getSearchMultipleState,
    searchMultiple.getPositionStepChecked
);

export const getDatasetsStepChecked = createSelector(
    getSearchMultipleState,
    searchMultiple.getDatasetsStepChecked
);

export const getResultStepChecked = createSelector(
    getSearchMultipleState,
    searchMultiple.getResultStepChecked
);

export const getSelectedDatasets = createSelector(
    getSearchMultipleState,
    searchMultiple.getSelectedDatasets
);

export const getNoSelectedDatasets = createSelector(
    getSelectedDatasets,
    (
        selectedDatasets: string[]) => {
        return selectedDatasets.length === 0;

    }
);

export const getQueryParams = createSelector(
    getIsValidConeSearch,
    getConeSearch,
    getSelectedDatasets,
    (
        isValidConeSearch: boolean,
        coneSearch: ConeSearch,
        selectedDatasets: string[]) => {
        let queryParams: SearchMultipleQueryParams = { };
        if (isValidConeSearch) {
            queryParams = {
                ...queryParams,
                cs: coneSearch.ra + ':' + coneSearch.dec + ':' + coneSearch.radius
            };
        }
        if (selectedDatasets.length > 0) {
            queryParams = {
                ...queryParams,
                d: selectedDatasets.join(';')
            };
        }
        return queryParams;
    }
);

export const getDatasetsCountIsLoading = createSelector(
    getSearchMultipleState,
    searchMultiple.getDatasetsCountIsLoading
);

export const getDatasetsCountIsLoaded = createSelector(
    getSearchMultipleState,
    searchMultiple.getDatasetsCountIsLoaded
);

export const getDatasetsCount = createSelector(
    getSearchMultipleState,
    searchMultiple.getDatasetsCount
);

export const getDatasetsWithData = createSelector(
    getSearchMultipleState,
    searchMultiple.selectIds
);

export const getAllData = createSelector(
    getSearchMultipleState,
    searchMultiple.selectEntities
);

export const getSelectedData = createSelector(
    getSearchMultipleState,
    searchMultiple.getSelectedData
);
