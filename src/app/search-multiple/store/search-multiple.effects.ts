/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as fromRouter from '@ngrx/router-store';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as fromSearchMultiple from './search-multiple.reducer';
import * as searchMultipleActions from './search-multiple.action';
import { SearchMultipleService } from './search-multiple.service';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as attributeActions from '../../metamodel/action/attribute.action';
import { Dataset } from '../../metamodel/model';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import * as coneSearchActions from '../../shared/cone-search/store/cone-search.action';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { Pagination, PaginationOrder } from '../../shared/datatable/model';
import * as utils from '../../shared/utils';

@Injectable()
/**
 * @class
 * @classdesc Search multiple effects.
 */
export class SearchMultipleEffects {
    constructor(
        private actions$: Actions,
        private searchMultipleService: SearchMultipleService,
        private toastr: ToastrService,
        private store$: Store<{
            router: fromRouter.RouterReducerState<utils.RouterStateUrl>,
            searchMultiple: fromSearchMultiple.State,
            metamodel: fromMetamodel.State,
            coneSearch: fromConeSearch.State
        }>
    ) { }

    /**
     * Calls actions to fill store with data provided by the URL.
     */
    @Effect()
    initSearchByUrlAction$ = this.actions$.pipe(
        ofType(searchMultipleActions.INIT_SEARCH_BY_URL),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.searchMultiple.pristine) {
                const actions: Action[] = [];
                if (state.router.state.queryParams.cs) {
                    const params = state.router.state.queryParams.cs.split(':');
                    const coneSearch: ConeSearch = {
                        ra: parseFloat(params[0]),
                        dec: parseFloat(params[1]),
                        radius: parseInt(params[2], 10)
                    };
                    actions.push(new coneSearchActions.AddConeSearchFromUrlAction(coneSearch));
                    actions.push(new searchMultipleActions.PositionCheckedAction());
                }
                if (state.router.state.queryParams.d) {
                    actions.push(new searchMultipleActions.InitSelectedDatasetsAction(state.router.state.queryParams.d.split(';')));
                    actions.push(new searchMultipleActions.DatasetsCheckedAction());
                }
                return actions;
            } else {
                return of({ type: '[No Action] ' + searchMultipleActions.INIT_SEARCH_BY_URL });
            }
        })
    );

    /**
     * Fills selectedDatasets with all returned datasets if configured as this in Anis Admin.
     */
    @Effect()
    loadDatasetSearchMetaSuccessAction$ = this.actions$.pipe(
        ofType(datasetActions.LOAD_DATASET_SEARCH_META_SUCCESS),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const allDatasetsSelected = state.metamodel.instance.instance.config.search_multiple.all_datasets_selected;
            if (allDatasetsSelected && state.searchMultiple.selectedDatasets.length === 0) {
                const loadDatasetSearchMetaSuccessAction = action as datasetActions.LoadDatasetSearchMetaSuccessAction;
                const datasetWithConeSearchList: Dataset[] = loadDatasetSearchMetaSuccessAction.payload[1]
                    .filter(d => d.config.cone_search && d.config.cone_search.enabled === true);
                const selectedDatasets: string[] = datasetWithConeSearchList.map(d => d.name);
                return of(new searchMultipleActions.InitSelectedDatasetsAction(selectedDatasets));
            } else {
                return of({ type: '[No Action]' + searchMultipleActions.INIT_SELECTED_DATASETS });
            }
        })
    );

    /**
     * Calls multiple requests to get result count for each datasets.
     */
    @Effect()
    retrieveDatasetsCountAction$ = this.actions$.pipe(
        ofType(searchMultipleActions.RETRIEVE_DATASETS_COUNT),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetList: Dataset[] = state.metamodel.dataset.datasetList;
            let requests: Observable<any>[] = [];
            state.searchMultiple.selectedDatasets.forEach(dname => {
                const datasetConfig = datasetList.find(d => d.name === dname).config;
                if (datasetConfig !== null && 'cone_search' in datasetConfig && datasetConfig.cone_search.enabled) {
                    let query: string = dname + '?a=count';
                    query += '&cs=' + state.coneSearch.coneSearch.ra + ':' + state.coneSearch.coneSearch.dec + ':' + state.coneSearch.coneSearch.radius;
                    requests.push(this.searchMultipleService.getCount(dname, query));
                }
            });
            return forkJoin(requests);
        }),
        withLatestFrom(this.store$),
        switchMap(([data, state]) => {
            let counts: { dname: string, count: number }[] = [];
            data.forEach(d => {
                counts.push(d);
            });
            const datasetList: Dataset[] = state.metamodel.dataset.datasetList;
            state.searchMultiple.selectedDatasets.forEach(dname => {
                const datasetConfig = datasetList.find(d => d.name === dname).config;
                if (datasetConfig === null || !('cone_search' in datasetConfig) || !datasetConfig.cone_search.enabled) {
                    counts.push({ dname: dname, count: 0});
                }
            });
            return of(new searchMultipleActions.RetrieveDatasetsCountSuccessAction(counts));
        }),
        catchError(_ => {
            return of(new searchMultipleActions.RetrieveDatasetsCountFailAction());
        })
    );

    /**
     * Displays datasets count error notification.
     */
    @Effect({ dispatch: false })
    retrieveDatasetsCountFailAction$ = this.actions$.pipe(
        ofType(searchMultipleActions.RETRIEVE_DATASETS_COUNT_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'The data count of datasets loading failed'))
    );

    /**
     * Calls RetrieveDataAction when attributeList is Loaded.
     */
    @Effect()
    loadAttributeListSuccessAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_ATTRIBUTE_LIST_SUCCESS),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.router.state.url.includes('/search-multiple/')) {
                const loadAttributeListSuccessAction = action as attributeActions.LoadAttributeListSuccessAction;
                const dname: string = loadAttributeListSuccessAction.payload.datasetName;
                const sortedCol: number = loadAttributeListSuccessAction.payload.attributeList.find(a => a.search_flag === 'ID').id;
                const pagination: Pagination = { dname: dname, nbItems: 10, page: 1, sortedCol: sortedCol, order: PaginationOrder.a };
                return of(new searchMultipleActions.RetrieveDataAction(pagination));
            } else {
                return of({ type: '[No Action] ' + attributeActions.LOAD_ATTRIBUTE_LIST_SUCCESS });
            }
        })
    );

    /**
     * Calls RetrieveDataAction to get data for the given dataset and parameters.
     */
    @Effect()
    retrieveDataAction$ = this.actions$.pipe(
        ofType(searchMultipleActions.RETRIEVE_DATA),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const retrieveDataAction = action as searchMultipleActions.RetrieveDataAction;
            const dname: string = retrieveDataAction.payload.dname;
            const outputList: number[] = state.metamodel.attribute.entities[dname].attributeList
                .filter(attribute => attribute.selected && attribute.id_output_category)
                .sort((a, b) => a.output_display - b.output_display)
                .map(attribute => attribute.id);
            let query = dname;
            query +='?a=' + outputList.join(';');
            query += '&cs=' + state.coneSearch.coneSearch.ra + ':' + state.coneSearch.coneSearch.dec + ':' + state.coneSearch.coneSearch.radius;
            query += '&p=' + retrieveDataAction.payload.nbItems + ':' + retrieveDataAction.payload.page;
            query += '&o=' + retrieveDataAction.payload.sortedCol + ':' + retrieveDataAction.payload.order;
            return this.searchMultipleService.retrieveData(dname, query).pipe(
                map((data: { datasetName: string, data: any[] }) => new searchMultipleActions.RetrieveDataSuccessAction(data)),
                // TODO: Pass dname
                catchError(() => of(new searchMultipleActions.RetrieveDataFailAction('')))
            );
        })
    );

    /**
     * Displays retrieve data error notification.
     */
    @Effect({ dispatch: false })
    retrieveDataFailAction$ = this.actions$.pipe(
        ofType(searchMultipleActions.RETRIEVE_DATA_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'The search data loading failed'))
    );
}
