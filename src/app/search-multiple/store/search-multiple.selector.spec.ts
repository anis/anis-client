import * as searchMultipleSelector from './search-multiple.selector';
import * as fromSearchMultiple from './search-multiple.reducer';
import * as fromConeSearch from '../../shared/cone-search/store/cone-search.reducer';
import { ConeSearch } from '../../shared/cone-search/store/model';
import { SearchMultipleQueryParams } from './model';

describe('[SearchMultiple] Selector', () => {
    it('should get current step', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getCurrentStep(state)).toBeNull();
    });

    it('should get positionStepChecked', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getPositionStepChecked(state)).toBeFalsy();
    });

    it('should get datasetsStepChecked', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getDatasetsStepChecked(state)).toBeFalsy();
    });

    it('should get resultsStepChecked', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getResultStepChecked(state)).toBeFalsy();
    });

    it('should get selectedDatasets', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getSelectedDatasets(state).length).toEqual(0);
    });

    it('should get noSelectedDatasets', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getNoSelectedDatasets(state)).toBeTruthy();
    });

    it('should get queryParams without cone search and selected datasets', () => {
        const state = {
            searchMultiple: { ...fromSearchMultiple.initialState },
            coneSearch: { ...fromConeSearch.initialState }
        };

        expect(searchMultipleSelector.getQueryParams(state)).toEqual({});
    });

    it('should get queryParams with cone search', () => {
        const coneSearch: ConeSearch = { ra: 1, dec: 2, radius: 3 };
        const state = {
            searchMultiple: { ...fromSearchMultiple.initialState },
            coneSearch: {
                ...fromConeSearch.initialState,
                coneSearch
            }
        };
        const expectedQueryParams: SearchMultipleQueryParams = { cs: '1:2:3' };

        expect(searchMultipleSelector.getQueryParams(state)).toEqual(expectedQueryParams);
    });

    it('should get queryParams with selected datasets', () => {
        const selectedDatasets: string[] = ['toto', 'tutu'];
        const state = {
            searchMultiple: {
                ...fromSearchMultiple.initialState,
                selectedDatasets
            },
            coneSearch: { ...fromConeSearch.initialState }
        };
        const expectedQueryParams: SearchMultipleQueryParams = { d: 'toto;tutu' };

        expect(searchMultipleSelector.getQueryParams(state)).toEqual(expectedQueryParams);
    });

    it('should get datasetsCountIsLoading', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getDatasetsCountIsLoading(state)).toBeFalsy();
    });

    it('should get datasetsCountIsLoaded', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getDatasetsCountIsLoaded(state)).toBeFalsy();
    });

    it('should get datasetsCount', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getDatasetsCount(state).length).toEqual(0);
    });

    it('should get datasetsWithData', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getDatasetsWithData(state).length).toEqual(0);
    });

    it('should get allData', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getAllData(state)).toEqual({});
    });

    it('should get sSelectedData', () => {
        const state = { searchMultiple: { ...fromSearchMultiple.initialState }};
        expect(searchMultipleSelector.getSelectedData(state).length).toEqual(0);
    });
});