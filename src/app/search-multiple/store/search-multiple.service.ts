/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { DatasetCount } from './model';

@Injectable()
/**
 * @class
 * @classdesc Search multiple service.
 */
export class SearchMultipleService {
    API_PATH: string = environment.apiUrl;
    instanceName: string = environment.instanceName;

    constructor(private http: HttpClient) { }

    /**
     * Retrieves number of results for the given dataset and parameters.
     *
     * @param  {string} dname - The dataset name.
     * @param  {string} query - The query.
     *
     * @return Observable<DatasetCount>
     */
    getCount(dname: string, query: string): Observable<DatasetCount> {
        return this.http.get<{ nb: number }[]>(this.API_PATH + '/search/' + query).pipe(
            map(res => {
                return { dname: dname, count: res[0].nb };
            })
        );
    }

    /**
     * Retrieves data for the given dataset and parameters.
     *
     * @param  {string} dname - The dataset name.
     * @param  {string} query - The query.
     *
     * @return Observable<{ datasetName: string, data: any[] }>
     */
    retrieveData(dname: string, query: string): Observable<{ datasetName: string, data: any[] }> {
        return this.http.get<any[]>(this.API_PATH + '/search/' + query).pipe(
            map(res => {
                return { datasetName: dname, data: res };
            })
        );
    }
}
