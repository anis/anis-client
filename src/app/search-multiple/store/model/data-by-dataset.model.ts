/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for search multiple data by dataset.
 *
 * @interface DataByDataset
 */
export interface DataByDataset {
    datasetName: string;
    isLoading: boolean;
    isLoaded: boolean;
    data: any[];
}