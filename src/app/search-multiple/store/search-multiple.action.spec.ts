import * as searchMultipleActions from '../store/search-multiple.action';
import { DatasetCount } from './model';
import { Pagination, PaginationOrder } from '../../shared/datatable/model';

describe('[SearchMultiple] Action', () => {
    it('should create InitSearchByUrlAction', () => {
        const action = new searchMultipleActions.InitSearchByUrlAction();
        expect(action.type).toEqual(searchMultipleActions.INIT_SEARCH_BY_URL);
    });

    it('should create InitSelectedDatasetsAction', () => {
        const action = new searchMultipleActions.InitSelectedDatasetsAction(['toto']);
        expect(action.type).toEqual(searchMultipleActions.INIT_SELECTED_DATASETS);
        expect(action.payload).toEqual(['toto']);
    });

    it('should create ChangeStepAction', () => {
        const action = new searchMultipleActions.ChangeStepAction('toto');
        expect(action.type).toEqual(searchMultipleActions.CHANGE_STEP);
        expect(action.payload).toEqual('toto');
    });

    it('should create PositionCheckedAction', () => {
        const action = new searchMultipleActions.PositionCheckedAction();
        expect(action.type).toEqual(searchMultipleActions.POSITION_CHECKED);
    });

    it('should create DatasetsCheckedAction', () => {
        const action = new searchMultipleActions.DatasetsCheckedAction();
        expect(action.type).toEqual(searchMultipleActions.DATASETS_CHECKED);
    });

    it('should create ResultsCheckedAction', () => {
        const action = new searchMultipleActions.ResultsCheckedAction();
        expect(action.type).toEqual(searchMultipleActions.RESULTS_CHECKED);
    });

    it('should create UpdateSelectedDatasetsAction', () => {
        const action = new searchMultipleActions.UpdateSelectedDatasetsAction(['toto']);
        expect(action.type).toEqual(searchMultipleActions.UPDATE_SELECTED_DATASETS);
        expect(action.payload).toEqual(['toto']);
    });

    it('should create RetrieveDatasetsCountAction', () => {
        const action = new searchMultipleActions.RetrieveDatasetsCountAction();
        expect(action.type).toEqual(searchMultipleActions.RETRIEVE_DATASETS_COUNT);
    });

    it('should create RetrieveDatasetsCountSuccessAction', () => {
        const datasetCount: DatasetCount = { dname: 'toto', count: 1 };
        const action = new searchMultipleActions.RetrieveDatasetsCountSuccessAction([datasetCount]);
        expect(action.type).toEqual(searchMultipleActions.RETRIEVE_DATASETS_COUNT_SUCCESS);
        expect(action.payload).toEqual([datasetCount]);
    });

    it('should create RetrieveDatasetsCountFailAction', () => {
        const action = new searchMultipleActions.RetrieveDatasetsCountFailAction();
        expect(action.type).toEqual(searchMultipleActions.RETRIEVE_DATASETS_COUNT_FAIL);
    });

    it('should create RetrieveDataAction', () => {
        const pagination: Pagination = { dname: 'toto', nbItems: 10, page: 1, sortedCol: 1, order: PaginationOrder.a };
        const action = new searchMultipleActions.RetrieveDataAction(pagination);
        expect(action.type).toEqual(searchMultipleActions.RETRIEVE_DATA);
        expect(action.payload).toEqual(pagination);
    });

    it('should create RetrieveDataSuccessAction', () => {
        const action = new searchMultipleActions.RetrieveDataSuccessAction({ datasetName: 'toto', data: [1] });
        expect(action.type).toEqual(searchMultipleActions.RETRIEVE_DATA_SUCCESS);
        expect(action.payload).toEqual({ datasetName: 'toto', data: [1] });
    });

    it('should create RetrieveDataFailAction', () => {
        const action = new searchMultipleActions.RetrieveDataFailAction('toto');
        expect(action.type).toEqual(searchMultipleActions.RETRIEVE_DATA_FAIL);
        expect(action.payload).toEqual('toto');
    });

    it('should create UpdateSelectedDataAction', () => {
        const action = new searchMultipleActions.UpdateSelectedDataAction([{ dname: 'toto', data: [1] }]);
        expect(action.type).toEqual(searchMultipleActions.UPDATE_SELECTED_DATA);
        expect(action.payload).toEqual([{ dname: 'toto', data: [1] }]);
    });

    it('should create DestroyResultsAction', () => {
        const action = new searchMultipleActions.DestroyResultsAction();
        expect(action.type).toEqual(searchMultipleActions.DESTROY_RESULTS);
    });

    it('should create ResetSearchAction', () => {
        const action = new searchMultipleActions.ResetSearchAction();
        expect(action.type).toEqual(searchMultipleActions.RESET_SEARCH);
    });
});
