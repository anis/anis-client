/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchMultipleComponent } from './containers/search-multiple.component';
import { PositionComponent } from './containers/position.component';
import { DatasetsComponent } from './containers/datasets.component';
import { ResultMultipleComponent } from './containers/result-multiple.component';

const routes: Routes = [
    {
        path: '', component: SearchMultipleComponent, children: [
            { path: '', redirectTo: 'position', pathMatch: 'full' },
            { path: 'position', component: PositionComponent },
            { path: 'datasets', component: DatasetsComponent },
            { path: 'result', component: ResultMultipleComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
/**
 * @class
 * @classdesc Search multiple routing module.
 */
export class SearchMultipleRoutingModule { }

export const routedComponents = [
    SearchMultipleComponent,
    PositionComponent,
    DatasetsComponent,
    ResultMultipleComponent
];
