/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { SampService } from './samp.service';
import * as sampActions from './samp.action';

@Injectable()
/**
 * @class
 * @classdesc Search effects.
 */
export class SampEffects {
    constructor(
        private actions$: Actions,
        private sampService: SampService,
        private toastr: ToastrService
    ) { }

    register$ = createEffect(() =>
        this.actions$.pipe(
            ofType(sampActions.REGISTER),
            switchMap(_ => this.sampService.register().pipe(
                map(_ => new sampActions.RegisterSuccessAction()),
                catchError(_ => of(new sampActions.RegisterFailAction()))
            ))
        )
    );

    registerSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(sampActions.REGISTER_SUCCESS),
            tap(_ => this.toastr.success('You are now connected to a SAMP-hub', 'SAMP-hub register success'))
        ),
        { dispatch: false }
    );

    registerFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(sampActions.REGISTER_FAIL),
            tap(_ => this.toastr.error('Connection to a SAMP-hub has failed', 'SAMP-hub register fail'))
        ),
        { dispatch: false }
    );

    unregister$ = createEffect(() =>
        this.actions$.pipe(
            ofType(sampActions.UNREGISTER),
            tap(_ => {
                this.sampService.unregister();
            })
        ),
        { dispatch: false }
    );

    broadcastVotable$ = createEffect(() =>
        this.actions$.pipe(
            ofType(sampActions.BROADCAST_VOTABLE),
            tap((action: sampActions.BroadcastVotableAction) => {
                this.sampService.broadcast('table.load.votable', action.payload)
            })
        ),
        { dispatch: false }
    );
}
