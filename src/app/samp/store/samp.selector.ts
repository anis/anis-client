/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as samp from './samp.reducer';

export const getSampState = createFeatureSelector<samp.State>('samp');

export const getRegistered = createSelector(
    getSampState,
    samp.getRegistered
);
