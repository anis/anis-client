/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from './samp.action';

/**
 * Interface for samp state.
 *
 * @interface State
 */
export interface State {
    registered: boolean;
}

export const initialState: State = {
    registered: false
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.REGISTER_SUCCESS:
            return {
                ...state,
                registered: true
            };

        case actions.UNREGISTER:
            return {
                ...state,
                registered: false
            };

        default:
            return state;
    }
}

export const getRegistered = (state: State) => state.registered;
