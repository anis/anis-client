/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

export const REGISTER = '[Samp] Register';
export const REGISTER_SUCCESS = '[Samp] Register Success';
export const REGISTER_FAIL = '[Samp] Register Fail';
export const UNREGISTER = '[Samp] unregister';
export const BROADCAST_VOTABLE = '[Samp] Broadcast Votable';

/**
 * @class
 * @classdesc Register action.
 * @readonly
 */
export class RegisterAction implements Action {
    readonly type = REGISTER;

    constructor(public payload: {} = null) { }
}

export class RegisterSuccessAction implements Action {
    readonly type = REGISTER_SUCCESS;

    constructor(public payload: {} = null) { }
}

export class RegisterFailAction implements Action {
    readonly type = REGISTER_FAIL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc Unregister action.
 * @readonly
 */
 export class UnregisterAction implements Action {
    readonly type = UNREGISTER;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc Broadcast votable action.
 * @readonly
 */
 export class BroadcastVotableAction implements Action {
    readonly type = BROADCAST_VOTABLE;

    constructor(public payload: string) { }
}

export type Actions
    = RegisterAction
    | RegisterSuccessAction
    | RegisterFailAction
    | UnregisterAction
    | BroadcastVotableAction;
