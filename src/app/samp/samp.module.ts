/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
 
import { SampEffects } from './store/samp.effects';
import { SampService } from './store/samp.service';
import { reducer } from './store/samp.reducer';
 
@NgModule({
    imports: [
        StoreModule.forFeature('samp', reducer),
        EffectsModule.forFeature([ SampEffects ])
    ],
    providers: [
        SampService
    ]
})
/**
 * @class
 * @classdesc Samp module.
 */
export class SampModule { }
