/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '../shared/shared.module';
import { DocumentationRoutingModule, routedComponents } from './documentation-routing.module';
import { reducer } from './store/documentation.reducer';
import { DocumentationEffects } from './store/documentation.effects';

@NgModule({
    imports: [
        SharedModule,
        DocumentationRoutingModule,
        StoreModule.forFeature('documentation', reducer),
        EffectsModule.forFeature([ DocumentationEffects ])
    ],
    declarations: [
        routedComponents
    ]
})
/**
 * @class
 * @classdesc Documentation module.
 */
export class DocumentationModule { }
