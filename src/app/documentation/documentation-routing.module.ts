/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentationComponent } from './containers/documentation.component';

const routes: Routes = [{ path: '', component: DocumentationComponent }];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
/**
 * @class
 * @classdesc Documentation routing module.
 */
export class DocumentationRoutingModule { }

export const routedComponents = [
    DocumentationComponent
];
