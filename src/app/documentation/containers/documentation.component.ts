/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Dictionary } from '@ngrx/entity';
import { Observable } from 'rxjs';

import * as fromDocumentation from '../store/documentation.reducer';
import * as documentationActions from '../store/documentation.action';
import * as documentationSelector from '../store/documentation.selector';
import * as metamodelSelector from '../../metamodel/selectors';
import { Dataset, Attribute, AttributesByDataset } from '../../metamodel/model';
import { getHost as host } from '../../shared/utils';
import { ScrollTopService } from '../../shared/service/sroll-top.service';

@Component({
    selector: 'app-documentation',
    templateUrl: 'documentation.component.html',
    styleUrls: ['documentation.component.css']
})
/**
 * @class
 * @classdesc Documentation container.
 *
 * @implements OnInit
 */
export class DocumentationComponent implements OnInit {
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetList: Observable<Dataset[]>;
    public attributeListsIsLoading: Observable<boolean>;
    public attributeListsIsLoaded: Observable<boolean>;
    public attributeList: Observable<Dictionary<AttributesByDataset>>;

    constructor(private store: Store<{ documentation: fromDocumentation.State }>, private scrollTopService: ScrollTopService) {
        this.datasetListIsLoading = store.select(metamodelSelector.getDatasetSearchMetaIsLoading);
        this.datasetListIsLoaded = store.select(metamodelSelector.getDatasetSearchMetaIsLoaded);
        this.datasetList = store.select(metamodelSelector.getDatasetList);
        this.attributeListsIsLoading = store.select(documentationSelector.getAttributeListsIsLoading);
        this.attributeListsIsLoaded = store.select(documentationSelector.getAttributeListsIsLoaded);
        this.attributeList = store.select(metamodelSelector.getAllAttributeList);
    }

    ngOnInit() {
        this.store.dispatch(new documentationActions.RetrieveAttributeListsAction());
        this.scrollTopService.setScrollTop();
    }

    /**
     * Returns host URL.
     *
     * @return string
     */
    getHost(): string {
        return host();
    }

    /**
     * Returns attribute list sorted by ID, for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     * @param  {Dictionary<AttributesByDataset>} attributesLists - Attribute list by dataset dictionary.
     *
     * @return Attribute[]
     */
    getAttributeList(dname: string, attributesLists: Dictionary<AttributesByDataset>): Attribute[] {
        return [...attributesLists[dname].attributeList].sort((a, b) => a.id - b.id);
    }
}
