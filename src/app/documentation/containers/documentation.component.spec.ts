import { ComponentFixture, TestBed } from '@angular/core/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { Dictionary } from '@ngrx/entity';

import { DocumentationComponent } from './documentation.component';
import * as fromDocumentation from '../store/documentation.reducer';
import * as documentationActions from '../store/documentation.action';
import { AttributesByDataset } from '../../metamodel/model';
import { ScrollTopService } from '../../shared/service/sroll-top.service';
import { environment } from '../../../environments/environment';
import { ATTRIBUTE_LIST } from '../../../settings/test-data';

describe('[Documentation] Component: DocumentationComponent', () => {
    let scrollTopServiceStub: Partial<ScrollTopService> = {
        setScrollTop() {}
    };

    let component: DocumentationComponent;
    let fixture: ComponentFixture<DocumentationComponent>;
    let store: MockStore;
    const initialState = { documentation: { ...fromDocumentation.initialState }};;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ DocumentationComponent ],
            providers: [
                provideMockStore({ initialState }),
                { provide: ScrollTopService, useValue: scrollTopServiceStub }
            ]
        });
        fixture = TestBed.createComponent(DocumentationComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', () => {
        const action = new documentationActions.RetrieveAttributeListsAction();
        const spy = spyOn(store, 'dispatch');
        component.ngOnInit()

        expect(spy).toHaveBeenCalledWith(action);
    });

    it('#getHost() should return protocol and host', () => {
        expect(component.getHost()).toEqual(environment.apiUrl);
    });

    it('#getAttributeList() should return attributes for selected dataset, sorted by id', () => {
        const attributeLists: Dictionary<AttributesByDataset> = {
            'toto': { datasetName: 'toto', isLoading: false, isLoaded: true, attributeList: ATTRIBUTE_LIST }
        };
        const attributeList = component.getAttributeList('toto', attributeLists);
        expect(attributeList.length).toEqual(2);
        expect(attributeList[0].id).toEqual(1);
        expect(attributeList[1].id).toEqual(2);
    });
});
