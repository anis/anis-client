/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as documentation from './documentation.reducer';

export const getDocumentationState = createFeatureSelector<documentation.State>('documentation');

export const getAttributeListsIsLoading = createSelector(
    getDocumentationState,
    documentation.getAttributeListsIsLoading
);

export const getAttributeListsIsLoaded = createSelector(
    getDocumentationState,
    documentation.getAttributeListsIsLoaded
);
