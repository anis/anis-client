import * as documentationSelector from './documentation.selector';
import * as fromDocumentation from './documentation.reducer';

describe('[Documentation] Selector', () => {
    it('should get attributeListsIsLoading', () => {
        const state = { documentation: { ...fromDocumentation.initialState }};
        expect(documentationSelector.getAttributeListsIsLoading(state)).toBeFalsy();
    })

    it('should get attributeListsIsLoaded', () => {
        const state = { documentation: { ...fromDocumentation.initialState }};
        expect(documentationSelector.getAttributeListsIsLoaded(state)).toBeFalsy();
    });
});
