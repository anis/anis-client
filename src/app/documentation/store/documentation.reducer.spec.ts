import * as fromDocumentation from './documentation.reducer';
import * as documentationActions from './documentation.action';

describe('[Documentation] Reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromDocumentation;
        const action = {} as documentationActions.Actions;
        const state = fromDocumentation.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should set attributeListsIsLoading to true', () => {
        const { initialState } = fromDocumentation;
        const action = new documentationActions.AttributeListsIsLoadingAction(true);
        const state = fromDocumentation.reducer(initialState, action);

        expect(state.attributeListsIsLoading).toBeTruthy();
        expect(state.attributeListsIsLoaded).toBeFalsy();
        expect(state).not.toEqual(initialState);
    });

    it('should set attributeListsIsLoaded to true', () => {
        const { initialState } = fromDocumentation;
        const action = new documentationActions.AttributeListsIsLoadedAction(true);
        const state = fromDocumentation.reducer(initialState, action);

        expect(state.attributeListsIsLoading).toBeFalsy();
        expect(state.attributeListsIsLoaded).toBeTruthy();
        expect(state).not.toEqual(initialState);
    });

    it('should get attributeListsIsLoading', () => {
        const action = {} as documentationActions.Actions;
        const state = fromDocumentation.reducer(undefined, action);

        expect(fromDocumentation.getAttributeListsIsLoading(state)).toBeFalsy();
    });

    it('should get attributeListsIsLoaded', () => {
        const action = {} as documentationActions.Actions;
        const state = fromDocumentation.reducer(undefined, action);

        expect(fromDocumentation.getAttributeListsIsLoaded(state)).toBeFalsy();
    });
});
