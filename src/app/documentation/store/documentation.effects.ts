/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as fromRouter from '@ngrx/router-store';
import { of } from 'rxjs';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';

import * as documentationActions from './documentation.action';
import * as fromMetamodel from '../../metamodel/reducers';
import * as datasetActions from '../../metamodel/action/dataset.action';
import * as attributeActions from '../../metamodel/action/attribute.action';
import * as utils from '../../shared/utils';

@Injectable()
/**
 * @class
 * @classdesc Documentation effects.
 */
export class DocumentationEffects {
    constructor(
        private actions$: Actions,
        private store$: Store<{
            router: fromRouter.RouterReducerState<utils.RouterStateUrl>,
            metamodel: fromMetamodel.State
        }>
    ) { }

    /**
     * Retrieves attribute lists for datasets instance.
     */
    @Effect()
    retrieveAttributeListsAction$ = this.actions$.pipe(
        ofType(documentationActions.RETRIEVE_ATTRIBUTE_LISTS),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.metamodel.dataset.datasetSearchMetaIsLoaded) {
                const dnames: string[] = state.metamodel.dataset.datasetList.map(d => d.name);
                const actions: Action[] = [
                    new attributeActions.LoadMultipleAttributeListsAction(dnames),
                    new documentationActions.AttributeListsIsLoadingAction(true)
                ];
                return actions;
            } else {
                return of(new datasetActions.LoadDatasetSearchMetaAction());
            }
        })
    );

    /**
     * Retrieves attribute lists for datasets instance.
     */
    @Effect()
    loadDatasetSearchMetaSuccessAction$ = this.actions$.pipe(
        ofType(datasetActions.LOAD_DATASET_SEARCH_META_SUCCESS),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.router.state.url.includes('/documentation')) {
                const dnames: string[] = state.metamodel.dataset.datasetList.map(d => d.name);
                const actions: Action[] = [
                    new attributeActions.LoadMultipleAttributeListsAction(dnames),
                    new documentationActions.AttributeListsIsLoadingAction(true)
                ];
                return actions;
            } else {
                return of({ type: '[No Action][Documentation]' + attributeActions.LOAD_ATTRIBUTE_LIST_SUCCESS });
            }
        })
    );

    /**
     * Calls actions to store that attribute lists loading is done.
     */
    @Effect()
    loadAttributeListsSuccessAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_MULTIPLE_ATTRIBUTE_LISTS_SUCCESS),
        switchMap(_ => {
            const actions: Action[] = [
                new documentationActions.AttributeListsIsLoadingAction(false),
                new documentationActions.AttributeListsIsLoadedAction(true)
            ];
            return actions;
        })
    );

    /**
     * Displays retrieve attribute lists error notification.
     */
    @Effect()
    loadAttributeListsFailAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_MULTIPLE_ATTRIBUTE_LISTS_FAIL),
        map(_ => {
            return new documentationActions.AttributeListsIsLoadingAction(false);
        })
    );
}
