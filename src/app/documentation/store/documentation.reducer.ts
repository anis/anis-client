/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from './documentation.action';

/**
 * Interface for documentation state.
 *
 * @interface State
 */
export interface State {
    attributeListsIsLoading: boolean;
    attributeListsIsLoaded: boolean;
}

export const initialState: State = {
    attributeListsIsLoading: false,
    attributeListsIsLoaded: false
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.ATTRIBUTE_LISTS_IS_LOADING:
            return {
                ...state,
                attributeListsIsLoading: action.payload
            };

        
        case actions.ATTRIBUTE_LISTS_IS_LOADED:
            return {
                ...state,
                attributeListsIsLoaded: action.payload
            };

        default:
            return state;
    }
}

export const getAttributeListsIsLoading = (state: State) => state.attributeListsIsLoading;
export const getAttributeListsIsLoaded = (state: State) => state.attributeListsIsLoaded;
