/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';


export const RETRIEVE_ATTRIBUTE_LISTS = '[Documentation] Retrieve Attribute Lists';
export const ATTRIBUTE_LISTS_IS_LOADING = '[Documentation] Attribute Lists Is Loading';
export const ATTRIBUTE_LISTS_IS_LOADED = '[Documentation] Attribute Lists Is Loaded';

/**
 * @class
 * @classdesc RetrieveAttributeListsAction action.
 * @readonly
 */
export class RetrieveAttributeListsAction implements Action {
    readonly type = RETRIEVE_ATTRIBUTE_LISTS;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc AttributeListsIsLoadingAction action.
 * @readonly
 */
export class AttributeListsIsLoadingAction implements Action {
    readonly type = ATTRIBUTE_LISTS_IS_LOADING;

    constructor(public payload: boolean) { }
}

/**
 * @class
 * @classdesc AttributeListsIsLoadedAction action.
 * @readonly
 */
export class AttributeListsIsLoadedAction implements Action {
    readonly type = ATTRIBUTE_LISTS_IS_LOADED;

    constructor(public payload: boolean) { }
}

export type Actions
    = RetrieveAttributeListsAction
    | AttributeListsIsLoadingAction
    | AttributeListsIsLoadedAction;
