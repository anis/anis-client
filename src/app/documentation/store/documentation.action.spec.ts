import * as documentationActions from './documentation.action';

describe('[Documentation] Action', () => {
    it('should create AttributeListsIsLoadingAction action', () => {
        const action = new documentationActions.RetrieveAttributeListsAction();
        expect(action.type).toEqual(documentationActions.RETRIEVE_ATTRIBUTE_LISTS);
    });

    it('should create AttributeListsIsLoadingAction action', () => {
        const action = new documentationActions.AttributeListsIsLoadingAction(true);
        expect(action.type).toEqual(documentationActions.ATTRIBUTE_LISTS_IS_LOADING);
        expect(action.payload).toBeTruthy();
    });

    it('should create AttributeListsIsLoadedAction action', () => {
        const action = new documentationActions.AttributeListsIsLoadedAction(true);
        expect(action.type).toEqual(documentationActions.ATTRIBUTE_LISTS_IS_LOADED);
        expect(action.payload).toBeTruthy();
    });
});
