/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { Family, Category } from '../model';
import * as outputActions from '../action/output.action';
import { OutputService } from '../services/output.service';

@Injectable()
/**
 * @class
 * @classdesc Output effects.
 */
export class OutputEffects {
    constructor(
        private actions$: Actions,
        private outputService: OutputService,
        private toastr: ToastrService
    ) { }

    /**
     * Calls action to retrieve output metadata.
     */
    @Effect()
    loadOutputSearchMetaAction$ = this.actions$.pipe(
        ofType(outputActions.LOAD_OUTPUT_SEARCH_META),
        switchMap((action: outputActions.LoadOutputSearchMetaAction) => {
            return of(new outputActions.LoadOutputSearchMetaWipAction(action.payload));
        })
    );

    /**
     * Retrieves output metadata.
     */
    @Effect()
    loadOutputSearchMetaWipAction$ = this.actions$.pipe(
        ofType(outputActions.LOAD_OUTPUT_SEARCH_META_WIP),
        switchMap((action: outputActions.LoadOutputSearchMetaWipAction) => {
            return this.outputService.retrieveOutputSearchMeta(action.payload).pipe(
                map((outputSearchMeta: [Family[], Category[]]) =>
                    new outputActions.LoadOutputSearchMetaSuccessAction(outputSearchMeta)),
                catchError(() => of(new outputActions.LoadOutputSearchMetaFailAction()))
            );
        })
    );

    /**
     * Displays retrieve output metadata error notification.
     */
    @Effect({ dispatch: false })
    loadOutputSearchMetaFailedAction$ = this.actions$.pipe(
        ofType(outputActions.LOAD_OUTPUT_SEARCH_META_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Output search info loading failed'))
    );
}
