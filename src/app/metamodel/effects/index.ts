import { InstanceEffects } from './instance.effects';
import { DatasetEffects } from './dataset.effects';
import { AttributeEffects } from './attribute.effects';
import { CriteriaEffects } from './criteria.effects';
import { OutputEffects } from './output.effects';

export const effects = [
    InstanceEffects,
    DatasetEffects,
    AttributeEffects,
    CriteriaEffects,
    OutputEffects
];
