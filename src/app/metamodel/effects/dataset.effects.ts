/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { withLatestFrom, switchMap, map, catchError, tap } from 'rxjs/operators';

import { Survey, Dataset, Family } from '../model';
import * as datasetActions from '../action/dataset.action';
import * as fromMetamodel from '../reducers';
import { DatasetService } from '../services/dataset.service';

@Injectable()
/**
 * @class
 * @classdesc Dataset effects.
 */
export class DatasetEffects {
    constructor(
        private actions$: Actions,
        private datasetService: DatasetService,
        private toastr: ToastrService,
        private store$: Store<{ metamodel: fromMetamodel.State }>
    ) { }

    /**
     * Calls action to retrieve datasets metadata.
     */
    @Effect()
    loadDatasetSearchMetaAction$ = this.actions$.pipe(
        ofType(datasetActions.LOAD_DATASET_SEARCH_META),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.metamodel.dataset.datasetSearchMetaIsLoaded) {
                return of({ type: '[No Action] [Dataset] Dataset search meta is already loaded' });
            } else {
                return of(new datasetActions.LoadDatasetSearchMetaWipAction());
            }
        })
    );

    /**
     * Retrieves datasets metadata.
     */
    @Effect()
    loadDatasetSearchMetaWipAction$ = this.actions$.pipe(
        ofType(datasetActions.LOAD_DATASET_SEARCH_META_WIP),
        switchMap(_ =>
            this.datasetService.retrieveDatasetSearchMeta().pipe(
                map((datasetSearchMeta: [Survey[], Dataset[], Family[]]) =>
                    new datasetActions.LoadDatasetSearchMetaSuccessAction(datasetSearchMeta)),
                catchError(() => of(new datasetActions.LoadDatasetSearchMetaFailAction()))
            )
        )
    );

    /**
     * Displays retrieve datasets metadata error notification.
     */
    @Effect({ dispatch: false })
    loadDatasetSearchMetaFailedAction$ = this.actions$.pipe(
        ofType(datasetActions.LOAD_DATASET_SEARCH_META_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Dataset search info loading failed'))
    );
}
