/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, tap, catchError } from 'rxjs/operators';

import { Family } from '../model';
import * as criteriaActions from '../action/criteria.action';
import { CriteriaService } from '../services/criteria.service';

@Injectable()
/**
 * @class
 * @classdesc Criteria effects.
 */
export class CriteriaEffects {
    constructor(
        private actions$: Actions,
        private criteriaService: CriteriaService,
        private toastr: ToastrService
    ) { }

    /**
     * Calls action to retrieve criteria metadata.
     */
    @Effect()
    loadCriteriaSearchMetaAction$ = this.actions$.pipe(
        ofType(criteriaActions.LOAD_CRITERIA_SEARCH_META),
        switchMap((action: criteriaActions.LoadCriteriaSearchMetaAction) => {
            return of(new criteriaActions.LoadCriteriaSearchMetaWipAction(action.payload));
        })
    );

    /**
     * Retrieves criteria metadata.
     */
    @Effect()
    loadCriteriaSearchMetaWipAction$ = this.actions$.pipe(
        ofType(criteriaActions.LOAD_CRITERIA_SEARCH_META_WIP),
        switchMap((action: criteriaActions.LoadCriteriaSearchMetaWipAction) => {
            return this.criteriaService.retrieveCriteriaSearchMeta(action.payload).pipe(
                map((datasetSearchMeta: Family[]) =>
                    new criteriaActions.LoadCriteriaSearchMetaSuccessAction(datasetSearchMeta)),
                catchError(() => of(new criteriaActions.LoadCriteriaSearchMetaFailAction()))
            );
        })
    );

    /**
     * Displays retrieve criteria metadata error notification.
     */
    @Effect({ dispatch: false })
    loadCriteriaSearchMetaFailedAction$ = this.actions$.pipe(
        ofType(criteriaActions.LOAD_CRITERIA_SEARCH_META_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Criteria search info loading failed'))
    );
}
