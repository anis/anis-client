/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, catchError, tap } from 'rxjs/operators';

import { Instance } from '../model';
import * as instanceActions from '../action/instance.action';
import { InstanceService } from '../services/instance.service';

@Injectable()
/**
 * @class
 * @classdesc Instance effects.
 */
export class InstanceEffects {
    constructor(
        private actions$: Actions,
        private instanceService: InstanceService,
        private toastr: ToastrService
    ) { }

    /**
     * Retrieves instance metadata.
     */
    @Effect()
    loadInstanceMetaAction$ = this.actions$.pipe(
        ofType(instanceActions.LOAD_INSTANCE_META),
        switchMap(_ =>
            this.instanceService.retrieveInstanceMeta().pipe(
                map((instanceMeta: Instance) =>
                    new instanceActions.LoadInstanceMetaSuccessAction(instanceMeta)),
                catchError(() => of(new instanceActions.LoadInstanceMetaFailAction()))
            )
        )
    );

    /**
     * Displays retrieve instance metadata error notification.
     */
    @Effect({ dispatch: false })
    loadInstanceMetaFailedAction$ = this.actions$.pipe(
        ofType(instanceActions.LOAD_INSTANCE_META_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Instance meta loading failed'))
    );
}
