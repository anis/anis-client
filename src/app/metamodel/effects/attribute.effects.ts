/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { forkJoin, Observable, of } from 'rxjs';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as attributeActions from '../action/attribute.action';
import { AttributeService } from '../services/attribute.service';
import { Attribute, AttributesByDataset } from '../model';

@Injectable()
/**
 * @class
 * @classdesc Attribute effects.
 */
export class AttributeEffects {
    constructor(
        private actions$: Actions,
        private attributeService: AttributeService,
        private toastr: ToastrService
    ) { }

    /**
     * Retrieves attribute list for a given dataset name.
     */
    @Effect()
    loadAttributeListAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_ATTRIBUTE_LIST),
        switchMap((action: attributeActions.LoadAttributeListAction) =>
            this.attributeService.retrieveAttributeList(action.payload).pipe(
                map((attributeList: { datasetName: string, attributeList: Attribute[] }) =>
                    new attributeActions.LoadAttributeListSuccessAction(attributeList)),
                catchError(err => {
                    return of(new attributeActions.LoadAttributeListFailAction(err.datasetName));
                })
            )
        )
    );

    /**
     * Displays retrieve attribute list error notification.
     */
    @Effect({ dispatch: false })
    loadAttributeListFailAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_ATTRIBUTE_LIST_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'Attribute list loading failed'))
    );

    /**
     * Retrieves attribute lists for a given datasets name.
     */
    @Effect()
    loadMultipleAttributeListsAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_MULTIPLE_ATTRIBUTE_LISTS),
        switchMap((action: attributeActions.LoadMultipleAttributeListsAction) => {
            let requests: Observable<any>[] = [];
            action.payload.forEach(dname => {
                requests.push(this.attributeService.retrieveAttributeList(dname));
            });
            return forkJoin(requests);
        }),
        switchMap((data: { datasetName: string, attributeList: Attribute[] }[]) => {
            let attributesByDataset: AttributesByDataset[] = [];
            data.forEach(d => {
                const attributeList: AttributesByDataset = {
                    datasetName: d.datasetName,
                    isLoading: false,
                    isLoaded: true,
                    attributeList: d.attributeList
                };
                attributesByDataset.push(attributeList);
            });
            return of(new attributeActions.LoadMultipleAttributeListsSuccessAction(attributesByDataset));
        }),
        catchError(_ => {
            return of(new attributeActions.LoadMultipleAttributeListsFailAction());
        })
    );

    /**
     * Displays retrieve attribute lists error notification.
     */
    @Effect({ dispatch: false })
    loadAttributeListsFailedAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_MULTIPLE_ATTRIBUTE_LISTS_FAIL),
        tap(_ => {
            this.toastr.error('Loading Failed!', 'Attribute lists loading failed');
        })
    );
}
