/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

 import { Image } from './image.model';

/**
 * Interface for dataset.
 *
 * @interface Dataset
 */
export interface Dataset {
    name: string;
    table_ref: string;
    label: string;
    description: string;
    display: number;
    count: number;
    vo: boolean;
    survey_name: string;
    id_dataset_family: number;
    public: boolean;
    config: {
        images: Image[],
        cone_search: {
            enabled: boolean;
            opened: boolean;
            column_ra: number;
            column_dec: number;
            plot_enabled: boolean;
            sdss_enabled: boolean;
            sdss_display: number;
            background: {id: number, enabled: boolean, display: number}[];
        },
        download: {
            enabled: boolean;
            opened: boolean;
            csv: boolean;
            ascii: boolean;
            vo: boolean;
            archive: boolean;
        },
        summary: {
            enabled: boolean;
            opened: boolean;
        },
        server_link: {
            enabled: boolean;
            opened: boolean;
        },
        samp: {
            enabled: boolean;
            opened: boolean;
        },
        datatable: {
            enabled: boolean;
            opened: boolean;
            selectable_row: boolean;
        }
    };
}
