/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for survey.
 *
 * @interface Survey
 */
export interface Survey {
    name: string;
    label: string;
    description: string;
    link: string;
    manager: string;
    id_database: number;
}
