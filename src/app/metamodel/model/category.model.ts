/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for category.
 *
 * @interface Category
 */
export interface Category {
    id: number;
    label: string;
    display: number;
    id_output_family: number;
}
