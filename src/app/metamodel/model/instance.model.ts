/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for instance.
 *
 * @interface Instance
 */
export interface Instance {
    name: string;
    label: string;
    client_url: string;
    nb_dataset_families: number;
    nb_datasets: number;
    config: {
        authentication: {
            allowed: boolean;
        };
        search: {
            allowed: boolean;
        };
        search_multiple: {
            allowed: boolean;
            all_datasets_selected: boolean;
        };
        documentation: {
            allowed: boolean;
        };
    };
}
