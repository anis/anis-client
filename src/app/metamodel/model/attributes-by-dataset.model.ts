/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Attribute } from './attribute.model';

/**
 * Interface for attributes by dataset.
 *
 * @interface AttributesByDataset
 */
export interface AttributesByDataset {
    datasetName: string;
    isLoading: boolean;
    isLoaded: boolean;
    attributeList: Attribute[];
}
