export interface Image {
    id: number;
    name: string;
    size: number;
    ra_min: number;
    ra_max: number;
    dec_min: number;
    dec_max: number;
    stretch: string;
    pmin: number;
    pmax: number;
}
