/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from '../action/instance.action';
import { Instance } from '../model';

/**
 * Interface for instance state.
 *
 * @interface State
 */
export interface State {
    instance: Instance;
}

export const initialState: State = {
    instance: null
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_INSTANCE_META_SUCCESS:
            return {
                ...state,
                instance: action.payload
            };

        default:
            return state;
    }
}

export const getInstance = (state: State) => state.instance;
