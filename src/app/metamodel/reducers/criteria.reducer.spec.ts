import * as fromCriteria from './criteria.reducer';
import * as criteriaActions from '../action/criteria.action';
import { Dataset, Family } from '../model';
import { DATASET_LIST, CRITERIA_FAMILY_LIST } from '../../../settings/test-data';

describe('[Metamodel] Criteria reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromCriteria;
        const action = {} as criteriaActions.Actions;
        const state = fromCriteria.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should set criteriaSearchMetaIsLoading to true', () => {
        const dataset: Dataset = DATASET_LIST[0];
        const { initialState } = fromCriteria;
        const action = new criteriaActions.LoadCriteriaSearchMetaWipAction(dataset.name);
        const state = fromCriteria.reducer(initialState, action);

        expect(state.criteriaSearchMetaIsLoading).toBeTruthy();
        expect(state.criteriaSearchMetaIsLoaded).toBeFalsy();
        expect(state.criteriaFamilyList.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set criteriaFamilyList, criteriaSearchMetaIsLoaded to true and criteriaSearchMetaIsLoading to false', () => {
        const criteriaFamilyList: Family[] = CRITERIA_FAMILY_LIST;
        const initialState = { ...fromCriteria.initialState, criteriaSearchMetaIsLoading: true };
        const action = new criteriaActions.LoadCriteriaSearchMetaSuccessAction(criteriaFamilyList);
        const state = fromCriteria.reducer(initialState, action);

        expect(state.criteriaSearchMetaIsLoading).toBeFalsy();
        expect(state.criteriaSearchMetaIsLoaded).toBeTruthy();
        expect(state.criteriaFamilyList.length).toEqual(3);
        expect(state).not.toEqual(initialState);
    });

    it('should set criteriaSearchMetaIsLoading to false', () => {
        const initialState = { ...fromCriteria.initialState, criteriaSearchMetaIsLoading: true };
        const action = new criteriaActions.LoadCriteriaSearchMetaFailAction();
        const state = fromCriteria.reducer(initialState, action);

        expect(state.criteriaSearchMetaIsLoading).toBeFalsy();
        expect(state.criteriaSearchMetaIsLoaded).toBeFalsy();
        expect(state.criteriaFamilyList.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });
   
    it('should get criteriaSearchMetaIsLoading', () => {
        const action = {} as criteriaActions.Actions;
        const state = fromCriteria.reducer(undefined, action);

        expect(fromCriteria.getCriteriaSearchMetaIsLoading(state)).toBeFalsy();
    });
   
    it('should get criteriaSearchMetaIsLoaded', () => {
        const action = {} as criteriaActions.Actions;
        const state = fromCriteria.reducer(undefined, action);

        expect(fromCriteria.getCriteriaSearchMetaIsLoaded(state)).toBeFalsy();
    });
   
    it('should get criteriaFamilyList', () => {
        const action = {} as criteriaActions.Actions;
        const state = fromCriteria.reducer(undefined, action);

        expect(fromCriteria.getCriteriaFamilyList(state).length).toEqual(0);
    });
});

