import * as fromDataset from './dataset.reducer';
import * as datasetActions from '../action/dataset.action';
import { Survey, Dataset, Family } from '../model';
import { SURVEY_LIST, DATASET_LIST, DATASET_FAMILY_LIST } from '../../../settings/test-data';

describe('[Metamodel] Dataset reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromDataset;
        const action = {} as datasetActions.Actions;
        const state = fromDataset.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should set datasetSearchMetaIsLoading to true', () => {
        const { initialState } = fromDataset;
        const action = new datasetActions.LoadDatasetSearchMetaWipAction();
        const state = fromDataset.reducer(initialState, action);

        expect(state.datasetSearchMetaIsLoading).toBeTruthy();
        expect(state.datasetSearchMetaIsLoaded).toBeFalsy();
        expect(state.surveyList.length).toEqual(0);
        expect(state.datasetList.length).toEqual(0);
        expect(state.datasetFamilyList.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });
    
    it('should set surveyList, datasetList and datasetFamilyList, set datasetSearchMetaIsLoading to false and set datasetSearchMetaIsLoaded to true', () => {
        const surveyList: Survey[] = SURVEY_LIST;
        const datasetList: Dataset[] = DATASET_LIST;
        const datasetFamilyList: Family[] = DATASET_FAMILY_LIST;
        const initialState = { ...fromDataset.initialState, datasetSearchMetaIsLoading: true };
        const action = new datasetActions.LoadDatasetSearchMetaSuccessAction([surveyList, datasetList, datasetFamilyList]);
        const state = fromDataset.reducer(initialState, action);

        expect(state.datasetSearchMetaIsLoading).toBeFalsy();
        expect(state.datasetSearchMetaIsLoaded).toBeTruthy();
        expect(state.surveyList.length).toEqual(2);
        expect(state.datasetList.length).toEqual(3);
        expect(state.datasetFamilyList.length).toEqual(3);
        expect(state).not.toEqual(initialState);
    });
    
    it('should set datasetSearchMetaIsLoading to false', () => {
        const initialState = { ...fromDataset.initialState, datasetSearchMetaIsLoading: true };
        const action = new datasetActions.LoadDatasetSearchMetaFailAction();
        const state = fromDataset.reducer(initialState, action);

        expect(state.datasetSearchMetaIsLoading).toBeFalsy();
        expect(state.datasetSearchMetaIsLoaded).toBeFalsy();
        expect(state.surveyList.length).toEqual(0);
        expect(state.datasetList.length).toEqual(0);
        expect(state.datasetFamilyList.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should get datasetSearchMetaIsLoading', () => {
        const action = {} as datasetActions.Actions;
        const state = fromDataset.reducer(undefined, action);

        expect(fromDataset.getDatasetSearchMetaIsLoading(state)).toBeFalsy();
    });

    it('should get datasetSearchMetaIsLoaded', () => {
        const action = {} as datasetActions.Actions;
        const state = fromDataset.reducer(undefined, action);

        expect(fromDataset.getDatasetSearchMetaIsLoaded(state)).toBeFalsy();
    });

    it('should get surveyList', () => {
        const action = {} as datasetActions.Actions;
        const state = fromDataset.reducer(undefined, action);

        expect(fromDataset.getSurveyList(state).length).toEqual(0);
    });

    it('should get datasetList', () => {
        const action = {} as datasetActions.Actions;
        const state = fromDataset.reducer(undefined, action);

        expect(fromDataset.getDatasetList(state).length).toEqual(0);
    });
   
    it('should get datasetFamilyList', () => {
        const action = {} as datasetActions.Actions;
        const state = fromDataset.reducer(undefined, action);

        expect(fromDataset.getDatasetFamilyList(state).length).toEqual(0);
    });
});
