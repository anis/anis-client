import * as fromOutput from './output.reducer';
import * as outputActions from '../action/output.action';
import { Dataset, Family, Category } from '../model';
import { DATASET_LIST, OUTPUT_FAMILY_LIST, CATEGORY_LIST } from '../../../settings/test-data';

describe('[Metamodel] Output reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromOutput;
        const action = {} as outputActions.Actions;
        const state = fromOutput.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should set outputSearchMetaIsLoading to true', () => {
        const dataset: Dataset = DATASET_LIST[0];
        const { initialState } = fromOutput;
        const action = new outputActions.LoadOutputSearchMetaWipAction(dataset.name);
        const state = fromOutput.reducer(initialState, action);

        expect(state.outputSearchMetaIsLoading).toBeTruthy();
        expect(state.outputSearchMetaIsLoaded).toBeFalsy();
        expect(state.outputFamilyList.length).toEqual(0);
        expect(state.categoryList.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set outputFamilyList, categoryList, outputSearchMetaIsLoaded to true and outputSearchMetaIsLoading to false', () => {
        const outputFamilyList: Family[] = OUTPUT_FAMILY_LIST;
        const outputCategoryList: Category[] = CATEGORY_LIST;
        const initialState = { ...fromOutput.initialState, outputSearchMetaIsLoading: true };
        const action = new outputActions.LoadOutputSearchMetaSuccessAction([outputFamilyList, outputCategoryList]);
        const state = fromOutput.reducer(initialState, action);

        expect(state.outputSearchMetaIsLoading).toBeFalsy();
        expect(state.outputSearchMetaIsLoaded).toBeTruthy();
        expect(state.outputFamilyList.length).toEqual(2);
        expect(state.categoryList.length).toEqual(3);
        expect(state).not.toEqual(initialState);
    });

    it('should set outputSearchMetaIsLoading to false', () => {
        const initialState = { ...fromOutput.initialState, outputSearchMetaIsLoading: true };
        const action = new outputActions.LoadOutputSearchMetaFailAction();
        const state = fromOutput.reducer(initialState, action);

        expect(state.outputSearchMetaIsLoading).toBeFalsy();
        expect(state.outputSearchMetaIsLoaded).toBeFalsy();
        expect(state.outputFamilyList.length).toEqual(0);
        expect(state.categoryList.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });
   
    it('should get outputSearchMetaIsLoading', () => {
        const action = {} as outputActions.Actions;
        const state = fromOutput.reducer(undefined, action);

        expect(fromOutput.getOutputSearchMetaIsLoading(state)).toBeFalsy();
    });
   
    it('should get outputSearchMetaIsLoaded', () => {
        const action = {} as outputActions.Actions;
        const state = fromOutput.reducer(undefined, action);

        expect(fromOutput.getOutputSearchMetaIsLoaded(state)).toBeFalsy();
    });
   
    it('should get outputFamilyList', () => {
        const action = {} as outputActions.Actions;
        const state = fromOutput.reducer(undefined, action);

        expect(fromOutput.getOutputFamilyList(state).length).toEqual(0);
    });
   
    it('should get categoryList', () => {
        const action = {} as outputActions.Actions;
        const state = fromOutput.reducer(undefined, action);

        expect(fromOutput.getCategoryList(state).length).toEqual(0);
    });
});
