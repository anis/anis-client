/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createEntityAdapter, EntityAdapter, EntityState, Update } from '@ngrx/entity';

import * as actions from '../action/attribute.action';
import { AttributesByDataset } from '../model';

/**
 * Interface for attribute state.
 *
 * @interface State
 * @extends EntityState<AttributesByDataset>
 */
export interface State extends EntityState<AttributesByDataset> { }

/**
 * Returns datasetName from AttributesByDataset object.
 *
 * @param  {AttributesByDataset} a - The attributes by dataset.
 *
 * @return string
 */
export function selectAttributesByDatasetId(a: AttributesByDataset): string {
    return a.datasetName;
}

export const adapter : EntityAdapter<AttributesByDataset> =
    createEntityAdapter<AttributesByDataset>({
        selectId: selectAttributesByDatasetId
    });

export const initialState: State = adapter.getInitialState();

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_ATTRIBUTE_LIST:
            return adapter.addOne({
                datasetName: action.payload,
                isLoading: true,
                isLoaded: false,
                attributeList: []
            }, state);

        case actions.LOAD_ATTRIBUTE_LIST_SUCCESS:
            return adapter.updateOne({
                id: action.payload.datasetName,
                changes: {
                    isLoading: false,
                    isLoaded: true,
                    attributeList: action.payload.attributeList
                }
            } as Update<AttributesByDataset>, state);

        case actions.LOAD_ATTRIBUTE_LIST_FAIL:
            return adapter.updateOne({
                id: action.payload,
                changes: {
                    isLoading: false
                }
            } as Update<AttributesByDataset>, state);

        case actions.LOAD_MULTIPLE_ATTRIBUTE_LISTS_SUCCESS:
            return adapter.upsertMany(action.payload, state);

        default:
            return state;
    }
}

export const {
    selectAll,
    selectEntities,
    selectIds,
    selectTotal
} = adapter.getSelectors();
