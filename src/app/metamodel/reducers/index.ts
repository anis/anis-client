import { combineReducers, createFeatureSelector } from '@ngrx/store';

import * as instance from './instance.reducer';
import * as dataset from './dataset.reducer';
import * as attribute from './attribute.reducer';
import * as criteria from './criteria.reducer';
import * as output from './output.reducer';

export interface State {
    instance: instance.State;
    dataset: dataset.State;
    attribute: attribute.State;
    criteria: criteria.State;
    output: output.State;
}

const reducers = {
    instance: instance.reducer,
    dataset: dataset.reducer,
    attribute: attribute.reducer,
    criteria: criteria.reducer,
    output: output.reducer
};

const productionReducer = combineReducers(reducers);

export function reducer(state: any, action: any) {
    return productionReducer(state, action);
}

export const getMetamodelState = createFeatureSelector<State>('metamodel');
