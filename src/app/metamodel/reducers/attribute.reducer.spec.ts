import * as fromAttribute from './attribute.reducer';
import * as attributeActions from '../action/attribute.action';
import { Attribute, AttributesByDataset } from '../model';
import { ATTRIBUTE_LIST } from '../../../settings/test-data';

describe('[Metamodel] Attribute reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromAttribute;
        const action = {} as attributeActions.Actions;
        const state = fromAttribute.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should add new entity with isLoading to true for the given dataset', () => {
        const { initialState } = fromAttribute;
        const action = new attributeActions.LoadAttributeListAction('toto');
        const state = fromAttribute.reducer(initialState, action);

        expect(state.ids).toContain('toto');
        expect(state.entities).toEqual({ 'toto': { datasetName: 'toto', isLoading: true, isLoaded: false, attributeList: [] }});
        expect(state).not.toEqual(initialState);
    });

    it('should set isLoading to false, isLoaded to true and set attributeList for the given entity', () => {
        const attributeList: { datasetName: string, attributeList: Attribute[] } = { datasetName: 'toto', attributeList: ATTRIBUTE_LIST };
        const initialState = {
            ...fromAttribute,
            ids: ['toto'],
            entities: { 'toto': { datasetName: 'toto', isLoading: true, isLoaded: false, attributeList: [] }}
        };
        const action = new attributeActions.LoadAttributeListSuccessAction(attributeList);
        const state = fromAttribute.reducer(initialState, action);

        expect(state.entities).toEqual({ 'toto': { datasetName: 'toto', isLoading: false, isLoaded: true, attributeList: ATTRIBUTE_LIST }});
        expect(state).not.toEqual(initialState);
    });

    it('should set isLoading to false for the given entity', () => {
        const initialState = {
            ...fromAttribute,
            ids: ['toto'],
            entities: { 'toto': { datasetName: 'toto', isLoading: true, isLoaded: false, attributeList: [] }}
        };
        const action = new attributeActions.LoadAttributeListFailAction('toto');
        const state = fromAttribute.reducer(initialState, action);

        expect(state.entities).toEqual({ 'toto': { datasetName: 'toto', isLoading: false, isLoaded: false, attributeList: [] }});
        expect(state).not.toEqual(initialState);
    });

    it('should update or insert the given entities', () => {
        const toto: AttributesByDataset = { datasetName: 'toto', isLoading: false, isLoaded: false, attributeList: ATTRIBUTE_LIST };
        const tutu: AttributesByDataset = { datasetName: 'tutu', isLoading: false, isLoaded: false, attributeList: ATTRIBUTE_LIST };
        const { initialState } = fromAttribute;
        const action = new attributeActions.LoadMultipleAttributeListsSuccessAction([toto, tutu]);
        const state = fromAttribute.reducer(initialState, action);

        expect(state.ids).toEqual(['toto', 'tutu']);
        expect(state.entities).toEqual({ 'toto': toto, 'tutu': tutu});
        expect(state).not.toEqual(initialState);
    });
});
