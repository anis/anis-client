/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from '../action/criteria.action';
import { Family } from '../model';

/**
 * Interface for criteria state.
 *
 * @interface State
 */
export interface State {
    criteriaSearchMetaIsLoading: boolean;
    criteriaSearchMetaIsLoaded: boolean;
    criteriaFamilyList: Family[];
}

export const initialState: State = {
    criteriaSearchMetaIsLoading: false,
    criteriaSearchMetaIsLoaded: false,
    criteriaFamilyList: []
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_CRITERIA_SEARCH_META_WIP:
            return {
                ...state,
                criteriaSearchMetaIsLoading: true
            };

        case actions.LOAD_CRITERIA_SEARCH_META_SUCCESS:
            const criteriaFamilyList: Family[] = action.payload;

            return {
                ...state,
                criteriaFamilyList,
                criteriaSearchMetaIsLoading: false,
                criteriaSearchMetaIsLoaded: true
            };

        case actions.LOAD_CRITERIA_SEARCH_META_FAIL:
            return {
                ...state,
                criteriaSearchMetaIsLoading: false
            };

        default:
            return state;
    }
}

export const getCriteriaSearchMetaIsLoading = (state: State) => state.criteriaSearchMetaIsLoading;
export const getCriteriaSearchMetaIsLoaded = (state: State) => state.criteriaSearchMetaIsLoaded;
export const getCriteriaFamilyList = (state: State) => state.criteriaFamilyList;
