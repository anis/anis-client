/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from '../action/dataset.action';
import { Survey, Dataset, Family } from '../model';

/**
 * Interface for dataset state.
 *
 * @interface State
 */
export interface State {
    datasetSearchMetaIsLoading: boolean;
    datasetSearchMetaIsLoaded: boolean;
    surveyList: Survey[];
    datasetList: Dataset[];
    datasetFamilyList: Family[];
}

export const initialState: State = {
    datasetSearchMetaIsLoading: false,
    datasetSearchMetaIsLoaded: false,
    surveyList: [],
    datasetList: [],
    datasetFamilyList: []
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_DATASET_SEARCH_META_WIP:
            return {
                ...state,
                datasetSearchMetaIsLoading: true
            };

        case actions.LOAD_DATASET_SEARCH_META_SUCCESS:
            const datasetSearchMeta: [Survey[], Dataset[], Family[]] = action.payload;

            return {
                ...state,
                surveyList: datasetSearchMeta[0],
                datasetList: datasetSearchMeta[1],
                datasetFamilyList: datasetSearchMeta[2],
                datasetSearchMetaIsLoading: false,
                datasetSearchMetaIsLoaded: true
            };

        case actions.LOAD_DATASET_SEARCH_META_FAIL:
            return {
                ...state,
                datasetSearchMetaIsLoading: false
            };

        default:
            return state;
    }
}

export const getDatasetSearchMetaIsLoading = (state: State) => state.datasetSearchMetaIsLoading;
export const getDatasetSearchMetaIsLoaded = (state: State) => state.datasetSearchMetaIsLoaded;
export const getSurveyList = (state: State) => state.surveyList;
export const getDatasetList = (state: State) => state.datasetList;
export const getDatasetFamilyList = (state: State) => state.datasetFamilyList;
