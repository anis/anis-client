/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from '../action/output.action';
import { Family, Category } from '../model';

/**
 * Interface for output state.
 *
 * @interface State
 */
export interface State {
    outputSearchMetaIsLoading: boolean;
    outputSearchMetaIsLoaded: boolean;
    outputFamilyList: Family[];
    categoryList: Category[];
}

export const initialState: State = {
    outputSearchMetaIsLoading: false,
    outputSearchMetaIsLoaded: false,
    outputFamilyList: [],
    categoryList: []
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.LOAD_OUTPUT_SEARCH_META_WIP:
            return {
                ...state,
                outputSearchMetaIsLoading: true
            };

        case actions.LOAD_OUTPUT_SEARCH_META_SUCCESS:
            const outputSearchMeta: [Family[], Category[]] = action.payload;

            return {
                ...state,
                outputFamilyList: outputSearchMeta[0],
                categoryList: outputSearchMeta[1],
                outputSearchMetaIsLoading: false,
                outputSearchMetaIsLoaded: true
            };

        case actions.LOAD_OUTPUT_SEARCH_META_FAIL:
            return {
                ...state,
                outputSearchMetaIsLoading: false
            };

        default:
            return state;
    }
}

export const getOutputSearchMetaIsLoading = (state: State) => state.outputSearchMetaIsLoading;
export const getOutputSearchMetaIsLoaded = (state: State) => state.outputSearchMetaIsLoaded;
export const getOutputFamilyList = (state: State) => state.outputFamilyList;
export const getCategoryList = (state: State) => state.categoryList;
