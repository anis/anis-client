import * as fromInstance from './instance.reducer';
import * as instanceActions from '../action/instance.action';
import { Instance } from '../model';
import { INSTANCE } from '../../../settings/test-data/instance';

describe('[Metamodel] Instance reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromInstance;
        const action = {} as instanceActions.Actions;
        const state = fromInstance.reducer(undefined, action);

        expect(state).toBe(initialState);
    });
    
    it('should set instance', () => {
        const instance: Instance = INSTANCE;
        const { initialState } = fromInstance;
        const action = new instanceActions.LoadInstanceMetaSuccessAction(instance);
        const state = fromInstance.reducer(initialState, action);

        expect(state.instance).toEqual(instance);
        expect(state).not.toEqual(initialState);
    });

    it('should get instance', () => {
        const action = {} as instanceActions.Actions;
        const state = fromInstance.reducer(undefined, action);

        expect(fromInstance.getInstance(state)).toBeNull();
    });
});
