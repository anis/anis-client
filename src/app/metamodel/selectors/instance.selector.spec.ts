import * as instanceSelector from './instance.selector';
import * as fromInstance from '../reducers/instance.reducer';

describe('[Metamodel] Instance selector', () => {
    it('should get instance', () => {
        const state = { metamodel: { instance: { ...fromInstance.initialState }}};
        expect(instanceSelector.getInstance(state)).toBeNull();
    });
});