import * as attributeSelector from './attribute.selector';
import * as fromAttribute from '../reducers/attribute.reducer';
import { ATTRIBUTE_LIST } from '../../../settings/test-data';

describe('[Metamodel] Attribute selector', () => {
    it('should get all attributeList by dataset', () => {
        const state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.getAllAttributeList(state)).toEqual({});
    });

    it('should get datasets with attributeList', () => {
        const state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.getDatasetsWithAttributeList(state)).toEqual([]);
    });

    it('should get attributeListIsLoading for the given dataset', () => {
        let state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.getAttributeListIsLoading(state, { dname: 'toto' })).toBeFalsy();
        state = { metamodel: { attribute: {
            ...fromAttribute.initialState,
            ids: ['toto'],
            entities: { 'toto': { datasetName: 'toto', isLoading: true, isLoaded: false, attributeList: [] }}
        }}};
        expect(attributeSelector.getAttributeListIsLoading(state, { dname: 'toto' })).toBeTruthy();
    });

    it('should get attributeListIsLoaded for the given dataset', () => {
        let state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.getAttributeListIsLoaded(state, { dname: 'toto' })).toBeFalsy();
        state = { metamodel: { attribute: {
            ...fromAttribute.initialState,
            ids: ['toto'],
            entities: { 'toto': { datasetName: 'toto', isLoading: false, isLoaded: true, attributeList: [] }}
        }}};
        expect(attributeSelector.getAttributeListIsLoaded(state, { dname: 'toto' })).toBeTruthy();
    });

    it('should get attributeList for the given dataset', () => {
        let state = { metamodel: { attribute: { ...fromAttribute.initialState }}};
        expect(attributeSelector.getAttributeList(state, { dname: 'toto' })).toEqual([]);
        state = { metamodel: { attribute: {
            ...fromAttribute.initialState,
            ids: ['toto'],
            entities: { 'toto': { datasetName: 'toto', isLoading: false, isLoaded: false, attributeList: ATTRIBUTE_LIST }}
        }}};
        expect(attributeSelector.getAttributeList(state, { dname: 'toto' })).toEqual(ATTRIBUTE_LIST);
    });
});