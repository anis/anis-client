/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as metamodel from '../reducers';
import * as dataset from '../reducers/dataset.reducer';
import { Dataset } from '../model';

export const getDatasetState = createSelector(
    metamodel.getMetamodelState,
    (state: metamodel.State) => state.dataset
);

export const getDatasetSearchMetaIsLoading = createSelector(
    getDatasetState,
    dataset.getDatasetSearchMetaIsLoading
);

export const getDatasetSearchMetaIsLoaded = createSelector(
    getDatasetState,
    dataset.getDatasetSearchMetaIsLoaded
);

export const getSurveyList = createSelector(
    getDatasetState,
    dataset.getSurveyList
);

export const getDatasetList = createSelector(
    getDatasetState,
    dataset.getDatasetList
);

export const getDatasetWithConeSearchList = createSelector(
    getDatasetList,
    (datasetList: Dataset[]) => {
        return datasetList.filter(d => d.config.cone_search && d.config.cone_search.enabled === true);
    }
);

export const getDatasetFamilyList = createSelector(
    getDatasetState,
    dataset.getDatasetFamilyList
);
