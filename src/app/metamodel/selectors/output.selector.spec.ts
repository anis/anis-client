import * as outputSelector from './output.selector';
import * as fromOutput from '../reducers/output.reducer';

describe('[Metamodel] Output selector', () => {
    it('should get outputSearchMetaIsLoading', () => {
        const state = { metamodel: { output: { ...fromOutput.initialState }}};
        expect(outputSelector.getOutputSearchMetaIsLoading(state)).toBeFalsy();
    });
   
    it('should get outputSearchMetaIsLoaded', () => {
        const state = { metamodel: { output: { ...fromOutput.initialState }}};
        expect(outputSelector.getOutputSearchMetaIsLoaded(state)).toBeFalsy();
    });
   
    it('should get outputFamilyList', () => {
        const state = { metamodel: { output: { ...fromOutput.initialState }}};
        expect(outputSelector.getOutputFamilyList(state).length).toEqual(0);
    });
   
    it('should get categoryList', () => {
        const state = { metamodel: { output: { ...fromOutput.initialState }}};
        expect(outputSelector.getCategoryList(state).length).toEqual(0);
    });
});