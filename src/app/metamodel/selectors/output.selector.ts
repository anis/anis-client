/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as metamodel from '../reducers';
import * as output from '../reducers/output.reducer';

export const getOutputState = createSelector(
    metamodel.getMetamodelState,
    (state: metamodel.State) => state.output
);

export const getOutputSearchMetaIsLoading = createSelector(
    getOutputState,
    output.getOutputSearchMetaIsLoading
);

export const getOutputSearchMetaIsLoaded = createSelector(
    getOutputState,
    output.getOutputSearchMetaIsLoaded
);

export const getOutputFamilyList = createSelector(
    getOutputState,
    output.getOutputFamilyList
);

export const getCategoryList = createSelector(
    getOutputState,
    output.getCategoryList
);
