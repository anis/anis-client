/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as metamodel from '../reducers';
import * as instance from '../reducers/instance.reducer';

export const getInstanceState = createSelector(
    metamodel.getMetamodelState,
    (state: metamodel.State) => state.instance
);

export const getInstance = createSelector(
    getInstanceState,
    instance.getInstance
);
