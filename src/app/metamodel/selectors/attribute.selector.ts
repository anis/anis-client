/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as metamodel from '../reducers';
import * as att from '../reducers/attribute.reducer';
import { AttributesByDataset } from '../model';

export const getAttState = createSelector(
    metamodel.getMetamodelState,
    (state: metamodel.State) => state.attribute
);

export const getAllAttributeList = createSelector(
    getAttState,
    att.selectEntities
);

export const getDatasetsWithAttributeList = createSelector(
    getAttState,
    att.selectIds
);

export const getAttributeListIsLoading = createSelector(
    getDatasetsWithAttributeList,
    getAllAttributeList,
    (ids: string[], entities: {[key:string]: AttributesByDataset}, props: {dname: string}) => {
        if (ids.includes(props.dname)) {
            return entities[props.dname].isLoading;
        }
        return false;
    }
);

export const getAttributeListIsLoaded = createSelector(
    getDatasetsWithAttributeList,
    getAllAttributeList,
    (ids: string[], entities: {[key:string]: AttributesByDataset}, props: {dname: string}) => {
        if (ids.includes(props.dname)) {
            return entities[props.dname].isLoaded;
        }
        return false;
    }
);

export const getAttributeList = createSelector(
    getDatasetsWithAttributeList,
    getAllAttributeList,
    (ids: string[], entities: {[key:string]: AttributesByDataset}, props: {dname: string}) => {
        if (ids.includes(props.dname)) {
            return entities[props.dname].attributeList;
        }
        return [];
    }
);
