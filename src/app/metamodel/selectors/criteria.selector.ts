/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as metamodel from '../reducers';
import * as criteria from '../reducers/criteria.reducer';

export const getCriteriaState = createSelector(
    metamodel.getMetamodelState,
    (state: metamodel.State) => state.criteria
);

export const getCriteriaSearchMetaIsLoading = createSelector(
    getCriteriaState,
    criteria.getCriteriaSearchMetaIsLoading
);

export const getCriteriaSearchMetaIsLoaded = createSelector(
    getCriteriaState,
    criteria.getCriteriaSearchMetaIsLoaded
);

export const getCriteriaFamilyList = createSelector(
    getCriteriaState,
    criteria.getCriteriaFamilyList
);
