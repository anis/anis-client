import * as datasetSelector from './dataset.selector';
import * as fromDataset from '../reducers/dataset.reducer';
import { Dataset } from '../model';
import { DATASET_LIST } from '../../../settings/test-data';

describe('[Metamodel] Dataset selector', () => {
    it('should get datasetSearchMetaIsLoading', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.getDatasetSearchMetaIsLoading(state)).toBeFalsy();
    });

    it('should get datasetSearchMetaIsLoaded', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.getDatasetSearchMetaIsLoaded(state)).toBeFalsy();
    });

    it('should get surveyList', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.getSurveyList(state).length).toEqual(0);
    });

    it('should get datasetList', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.getDatasetList(state).length).toEqual(0);
    });

    it('should get datasetList with cone search enabled', () => {
        const state = {
            metamodel: {
                dataset: {
                    ...fromDataset.initialState,
                    datasetList: DATASET_LIST
                }
            }
        };
        const datasetWithConeSearchList: Dataset[] = datasetSelector.getDatasetWithConeSearchList(state);
        expect(datasetWithConeSearchList.length).toEqual(1);
        expect(datasetWithConeSearchList[0].name).toEqual('cat_1');
    });
   
    it('should get datasetFamilyList', () => {
        const state = { metamodel: { dataset: { ...fromDataset.initialState }}};
        expect(datasetSelector.getDatasetFamilyList(state).length).toEqual(0);
    });
});