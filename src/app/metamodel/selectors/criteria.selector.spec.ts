import * as criteriaSelector from './criteria.selector';
import * as fromCriteria from '../reducers/criteria.reducer';

describe('[Metamodel] Criteria selector', () => {
    it('should get criteriaSearchMetaIsLoading', () => {
        const state = { metamodel: { criteria: { ...fromCriteria.initialState }}};
        expect(criteriaSelector.getCriteriaSearchMetaIsLoading(state)).toBeFalsy();
    });
   
    it('should get criteriaSearchMetaIsLoaded', () => {
        const state = { metamodel: { criteria: { ...fromCriteria.initialState }}};
        expect(criteriaSelector.getCriteriaSearchMetaIsLoaded(state)).toBeFalsy();
    });
   
    it('should get criteriaFamilyList', () => {
        const state = { metamodel: { criteria: { ...fromCriteria.initialState }}};
        expect(criteriaSelector.getCriteriaFamilyList(state).length).toEqual(0);
    });
});