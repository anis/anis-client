/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import { Dataset, Survey, Family } from '../model';

export const LOAD_DATASET_SEARCH_META = '[Dataset] Load Dataset Search Meta';
export const LOAD_DATASET_SEARCH_META_WIP = '[Dataset] Load Dataset Search Meta WIP';
export const LOAD_DATASET_SEARCH_META_SUCCESS = '[Dataset] Load Dataset Search Meta Success';
export const LOAD_DATASET_SEARCH_META_FAIL = '[Dataset] Load Dataset Search Meta Fail';

/**
 * @class
 * @classdesc LoadDatasetSearchMetaAction action.
 * @readonly
 */
export class LoadDatasetSearchMetaAction implements Action {
    readonly type = LOAD_DATASET_SEARCH_META;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc LoadDatasetSearchMetaWipAction action.
 * @readonly
 */
export class LoadDatasetSearchMetaWipAction implements Action {
    readonly type = LOAD_DATASET_SEARCH_META_WIP;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc LoadDatasetSearchMetaSuccessAction action.
 * @readonly
 */
export class LoadDatasetSearchMetaSuccessAction implements Action {
    readonly type = LOAD_DATASET_SEARCH_META_SUCCESS;

    constructor(public payload: [Survey[], Dataset[], Family[]]) { }
}

/**
 * @class
 * @classdesc LoadDatasetSearchMetaFailAction action.
 * @readonly
 */
export class LoadDatasetSearchMetaFailAction implements Action {
    readonly type = LOAD_DATASET_SEARCH_META_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadDatasetSearchMetaAction
    | LoadDatasetSearchMetaWipAction
    | LoadDatasetSearchMetaSuccessAction
    | LoadDatasetSearchMetaFailAction;
