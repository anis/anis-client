import * as criteriaActions from './criteria.action';
import { CRITERIA_FAMILY_LIST } from '../../../settings/test-data';
import { Family } from '../model';

describe('[Metamodel] Criteria action', () => {
    it('should create LoadCriteriaSearchMetaAction', () => {
        const action = new criteriaActions.LoadCriteriaSearchMetaAction('toto');
        expect(action.type).toEqual(criteriaActions.LOAD_CRITERIA_SEARCH_META);
        expect(action.payload).toEqual('toto');
    });
    
    it('should create LoadCriteriaSearchMetaWipAction', () => {
        const action = new criteriaActions.LoadCriteriaSearchMetaWipAction('toto');
        expect(action.type).toEqual(criteriaActions.LOAD_CRITERIA_SEARCH_META_WIP);
        expect(action.payload).toEqual('toto');
    });
    
    it('should create LoadCriteriaSearchMetaSuccessAction', () => {
        const criteriaFamilyList: Family[] = CRITERIA_FAMILY_LIST;
        const action = new criteriaActions.LoadCriteriaSearchMetaSuccessAction(criteriaFamilyList);
        expect(action.type).toEqual(criteriaActions.LOAD_CRITERIA_SEARCH_META_SUCCESS);
        expect(action.payload).toEqual(criteriaFamilyList);
    });
    
    it('should create LoadCriteriaSearchMetaFailAction', () => {
        const action = new criteriaActions.LoadCriteriaSearchMetaFailAction();
        expect(action.type).toEqual(criteriaActions.LOAD_CRITERIA_SEARCH_META_FAIL);
    });
});
