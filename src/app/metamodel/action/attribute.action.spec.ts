import * as attributeActions from './attribute.action';
import { Attribute, AttributesByDataset } from '../model';
import { ATTRIBUTE_LIST } from '../../../settings/test-data';

describe('[Metamodel] Attribute action', () => {
    it('should create LoadAttributeListAction', () => {
        const action = new attributeActions.LoadAttributeListAction('toto');
        expect(action.type).toEqual(attributeActions.LOAD_ATTRIBUTE_LIST);
        expect(action.payload).toEqual('toto');
    });

    it('should create LoadAttributeListSuccessAction', () => {
        const attributeList: { datasetName: string, attributeList: Attribute[] } = {
            datasetName: 'toto',
            attributeList: ATTRIBUTE_LIST
        };
        const action = new attributeActions.LoadAttributeListSuccessAction(attributeList);
        expect(action.type).toEqual(attributeActions.LOAD_ATTRIBUTE_LIST_SUCCESS);
        expect(action.payload).toEqual(attributeList);
    });

    it('should create LoadAttributeListFailAction', () => {
        const action = new attributeActions.LoadAttributeListFailAction('toto');
        expect(action.type).toEqual(attributeActions.LOAD_ATTRIBUTE_LIST_FAIL);
        expect(action.payload).toEqual('toto');
    });

    it('should create LoadMultipleAttributeListsAction', () => {
        const action = new attributeActions.LoadMultipleAttributeListsAction(['toto']);
        expect(action.type).toEqual(attributeActions.LOAD_MULTIPLE_ATTRIBUTE_LISTS);
        expect(action.payload).toEqual(['toto']);
    });

    it('should create LoadAttributeListSuccessAction', () => {
        const attributeLists: AttributesByDataset[] = [
            { datasetName: 'toto', isLoading: true, isLoaded: false, attributeList: ATTRIBUTE_LIST },
            { datasetName: 'tutu', isLoading: true, isLoaded: false, attributeList: ATTRIBUTE_LIST }
        ];
        const action = new attributeActions.LoadMultipleAttributeListsSuccessAction(attributeLists);
        expect(action.type).toEqual(attributeActions.LOAD_MULTIPLE_ATTRIBUTE_LISTS_SUCCESS);
        expect(action.payload).toEqual(attributeLists);
    });

    it('should create LoadMultipleAttributeListsFailAction', () => {
        const action = new attributeActions.LoadMultipleAttributeListsFailAction();
        expect(action.type).toEqual(attributeActions.LOAD_MULTIPLE_ATTRIBUTE_LISTS_FAIL);
    });
});
