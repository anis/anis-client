/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import { Attribute, AttributesByDataset } from '../model';


export const LOAD_ATTRIBUTE_LIST = '[Attribute] Load Attribute List';
export const LOAD_ATTRIBUTE_LIST_SUCCESS = '[Attribute] Load Attribute List Success';
export const LOAD_ATTRIBUTE_LIST_FAIL = '[Attribute] Load Attribute List Fail';
export const LOAD_MULTIPLE_ATTRIBUTE_LISTS = '[Attribute] Load Multiple Attribute Lists';
export const LOAD_MULTIPLE_ATTRIBUTE_LISTS_SUCCESS = '[Attribute] Load Multiple Attribute Lists Success';
export const LOAD_MULTIPLE_ATTRIBUTE_LISTS_FAIL = '[Attribute] Load Multiple Attribute Lists Fail';

/**
 * @class
 * @classdesc LoadAttributeListAction action.
 * @readonly
 */
export class LoadAttributeListAction implements Action {
    readonly type = LOAD_ATTRIBUTE_LIST;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc LoadAttributeListSuccessAction action.
 * @readonly
 */
export class LoadAttributeListSuccessAction implements Action {
    readonly type = LOAD_ATTRIBUTE_LIST_SUCCESS;

    constructor(public payload: { datasetName: string, attributeList: Attribute[] }) { }
}

/**
 * @class
 * @classdesc LoadAttributeListFailAction action.
 * @readonly
 */
export class LoadAttributeListFailAction implements Action {
    readonly type = LOAD_ATTRIBUTE_LIST_FAIL;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc LoadMultipleAttributeListsAction action.
 * @readonly
 */
export class LoadMultipleAttributeListsAction implements Action {
    readonly type = LOAD_MULTIPLE_ATTRIBUTE_LISTS;

    constructor(public payload: string[]) { }
}

/**
 * @class
 * @classdesc LoadMultipleAttributeListsSuccessAction action.
 * @readonly
 */
export class LoadMultipleAttributeListsSuccessAction implements Action {
    readonly type = LOAD_MULTIPLE_ATTRIBUTE_LISTS_SUCCESS;

    constructor(public payload: AttributesByDataset[]) { }
}

/**
 * @class
 * @classdesc LoadMultipleAttributeListsFailAction action.
 * @readonly
 */
export class LoadMultipleAttributeListsFailAction implements Action {
    readonly type = LOAD_MULTIPLE_ATTRIBUTE_LISTS_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadAttributeListAction
    | LoadAttributeListSuccessAction
    | LoadAttributeListFailAction
    | LoadMultipleAttributeListsAction
    | LoadMultipleAttributeListsSuccessAction
    | LoadMultipleAttributeListsFailAction;
