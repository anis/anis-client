import * as instanceActions from './instance.action';
import { INSTANCE } from '../../../settings/test-data/instance';

describe('[Metamodel] Instance action', () => {
    it('should create LoadInstanceMetaAction', () => {
        const action = new instanceActions.LoadInstanceMetaAction();
        expect(action.type).toEqual(instanceActions.LOAD_INSTANCE_META);
    });
    
    it('should create LoadDatasetSearchMetaSuccessAction', () => {
        const action = new instanceActions.LoadInstanceMetaSuccessAction(INSTANCE);
        expect(action.type).toEqual(instanceActions.LOAD_INSTANCE_META_SUCCESS);
        expect(action.payload).toEqual(INSTANCE);
    });
    
    it('should create LoadDatasetSearchMetaFailAction', () => {
        const action = new instanceActions.LoadInstanceMetaFailAction();
        expect(action.type).toEqual(instanceActions.LOAD_INSTANCE_META_FAIL);
    });
});
