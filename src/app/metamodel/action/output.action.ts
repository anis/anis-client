/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import { Family, Category } from '../model';


export const LOAD_OUTPUT_SEARCH_META = '[Output] Load Output Search Meta';
export const LOAD_OUTPUT_SEARCH_META_WIP = '[Output] Load Output Search Meta WIP';
export const LOAD_OUTPUT_SEARCH_META_SUCCESS = '[Output] Load Output Search Meta Success';
export const LOAD_OUTPUT_SEARCH_META_FAIL = '[Output] Load Output Search Meta Fail';

/**
 * @class
 * @classdesc LoadOutputSearchMetaAction action.
 * @readonly
 */
export class LoadOutputSearchMetaAction implements Action {
    readonly type = LOAD_OUTPUT_SEARCH_META;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc LoadOutputSearchMetaWipAction action.
 * @readonly
 */
export class LoadOutputSearchMetaWipAction implements Action {
    readonly type = LOAD_OUTPUT_SEARCH_META_WIP;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc LoadOutputSearchMetaSuccessAction action.
 * @readonly
 */
export class LoadOutputSearchMetaSuccessAction implements Action {
    readonly type = LOAD_OUTPUT_SEARCH_META_SUCCESS;

    constructor(public payload: [Family[], Category[]]) { }
}

/**
 * @class
 * @classdesc LoadOutputSearchMetaFailAction action.
 * @readonly
 */
export class LoadOutputSearchMetaFailAction implements Action {
    readonly type = LOAD_OUTPUT_SEARCH_META_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadOutputSearchMetaAction
    | LoadOutputSearchMetaWipAction
    | LoadOutputSearchMetaSuccessAction
    | LoadOutputSearchMetaFailAction;
