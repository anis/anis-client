import * as datasetActions from './dataset.action';
import { SURVEY_LIST, DATASET_LIST, DATASET_FAMILY_LIST } from '../../../settings/test-data';
import { Survey, Dataset, Family } from '../model';

describe('[Metamodel] Dataset action', () => {
    it('should create LoadDatasetSearchMetaAction', () => {
        const action = new datasetActions.LoadDatasetSearchMetaAction();
        expect(action.type).toEqual(datasetActions.LOAD_DATASET_SEARCH_META);
    });
    
    it('should create LoadDatasetSearchMetaWipAction', () => {
        const action = new datasetActions.LoadDatasetSearchMetaWipAction();
        expect(action.type).toEqual(datasetActions.LOAD_DATASET_SEARCH_META_WIP);
    });
    
    it('should create LoadDatasetSearchMetaSuccessAction', () => {
        const payload: [Survey[], Dataset[], Family[]] = [SURVEY_LIST, DATASET_LIST, DATASET_FAMILY_LIST];
        const action = new datasetActions.LoadDatasetSearchMetaSuccessAction(payload);
        expect(action.type).toEqual(datasetActions.LOAD_DATASET_SEARCH_META_SUCCESS);
        expect(action.payload).toEqual(payload);
    });
    
    it('should create LoadDatasetSearchMetaFailAction', () => {
        const action = new datasetActions.LoadDatasetSearchMetaFailAction();
        expect(action.type).toEqual(datasetActions.LOAD_DATASET_SEARCH_META_FAIL);
    });
});
