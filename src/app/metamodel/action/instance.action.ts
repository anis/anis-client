/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import { Instance } from '../model';


export const LOAD_INSTANCE_META = '[Instance] Load Instance Meta';
export const LOAD_INSTANCE_META_SUCCESS = '[Instance] Load Instance Meta Success';
export const LOAD_INSTANCE_META_FAIL = '[Instance] Load Instance Meta Fail';

/**
 * @class
 * @classdesc LoadInstanceMetaAction action.
 * @readonly
 */
export class LoadInstanceMetaAction implements Action {
    readonly type = LOAD_INSTANCE_META;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc LoadInstanceMetaSuccessAction action.
 * @readonly
 */
export class LoadInstanceMetaSuccessAction implements Action {
    readonly type = LOAD_INSTANCE_META_SUCCESS;

    constructor(public payload: Instance) { }
}

/**
 * @class
 * @classdesc LoadInstanceMetaFailAction action.
 * @readonly
 */
export class LoadInstanceMetaFailAction implements Action {
    readonly type = LOAD_INSTANCE_META_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadInstanceMetaAction
    | LoadInstanceMetaSuccessAction
    | LoadInstanceMetaFailAction;
