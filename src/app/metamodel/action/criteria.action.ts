/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import { Family } from '../model';


export const LOAD_CRITERIA_SEARCH_META = '[Criteria] Load Criteria Search Meta';
export const LOAD_CRITERIA_SEARCH_META_WIP = '[Criteria] Load Criteria Search Meta WIP';
export const LOAD_CRITERIA_SEARCH_META_SUCCESS = '[Criteria] Load Criteria Search Meta Success';
export const LOAD_CRITERIA_SEARCH_META_FAIL = '[Criteria] Load Criteria Search Meta Fail';

/**
 * @class
 * @classdesc LoadCriteriaSearchMetaAction action.
 * @readonly
 */
export class LoadCriteriaSearchMetaAction implements Action {
    readonly type = LOAD_CRITERIA_SEARCH_META;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc LoadCriteriaSearchMetaWipAction action.
 * @readonly
 */
export class LoadCriteriaSearchMetaWipAction implements Action {
    readonly type = LOAD_CRITERIA_SEARCH_META_WIP;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc LoadCriteriaSearchMetaSuccessAction action.
 * @readonly
 */
export class LoadCriteriaSearchMetaSuccessAction implements Action {
    readonly type = LOAD_CRITERIA_SEARCH_META_SUCCESS;

    constructor(public payload: Family[]) { }
}

/**
 * @class
 * @classdesc LoadCriteriaSearchMetaFailAction action.
 * @readonly
 */
export class LoadCriteriaSearchMetaFailAction implements Action {
    readonly type = LOAD_CRITERIA_SEARCH_META_FAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoadCriteriaSearchMetaAction
    | LoadCriteriaSearchMetaWipAction
    | LoadCriteriaSearchMetaSuccessAction
    | LoadCriteriaSearchMetaFailAction;
