import * as outputActions from './output.action';
import { OUTPUT_FAMILY_LIST, CATEGORY_LIST } from '../../../settings/test-data';
import { Family, Category } from '../model';

describe('[Metamodel] Output action', () => {
    it('should create LoadOutputSearchMetaAction', () => {
        const action = new outputActions.LoadOutputSearchMetaAction('toto');
        expect(action.type).toEqual(outputActions.LOAD_OUTPUT_SEARCH_META);
        expect(action.payload).toEqual('toto');
    });
    
    it('should create LoadOutputSearchMetaWipAction', () => {
        const action = new outputActions.LoadOutputSearchMetaWipAction('toto');
        expect(action.type).toEqual(outputActions.LOAD_OUTPUT_SEARCH_META_WIP);
        expect(action.payload).toEqual('toto');
    });
    
    it('should create LoadOutputSearchMetaSuccessAction', () => {
        const payload: [Family[], Category[]] = [OUTPUT_FAMILY_LIST, CATEGORY_LIST];
        const action = new outputActions.LoadOutputSearchMetaSuccessAction(payload);
        expect(action.type).toEqual(outputActions.LOAD_OUTPUT_SEARCH_META_SUCCESS);
        expect(action.payload).toEqual(payload);
    });
    
    it('should create LoadOutputSearchMetaFailAction', () => {
        const action = new outputActions.LoadOutputSearchMetaFailAction();
        expect(action.type).toEqual(outputActions.LOAD_OUTPUT_SEARCH_META_FAIL);
    });
});
