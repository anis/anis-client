/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Instance } from '../model';
import { environment } from '../../../environments/environment';

@Injectable()
/**
 * @class
 * @classdesc Instance service.
 */
export class InstanceService {
    private API_PATH: string = environment.apiUrl;
    private instanceName: string = environment.instanceName;

    constructor(private http: HttpClient) { }

    /**
     * Retrieves instance metadata.
     *
     * @return Observable<Instance>
     */
    retrieveInstanceMeta(): Observable<Instance> {
        return this.http.get<Instance>(this.API_PATH + '/instance/' + this.instanceName);
    }
}
