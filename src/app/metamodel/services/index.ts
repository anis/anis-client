import { InstanceService } from './instance.service';
import { DatasetService } from './dataset.service';
import { AttributeService } from './attribute.service';
import { CriteriaService } from './criteria.service';
import { OutputService } from './output.service';

export const services = [
    InstanceService,
    DatasetService,
    AttributeService,
    CriteriaService,
    OutputService
];
