/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { sortByDisplay } from '../../shared/utils';
import { Family, Category } from '../model';
import { environment } from '../../../environments/environment';

@Injectable()
/**
 * @class
 * @classdesc Output service.
 */
export class OutputService {
    private API_PATH: string = environment.apiUrl;

    constructor(private http: HttpClient) { }

    /**
     * Retrieves output metadata for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return Observable<[Family[], Category[]]>
     */
    retrieveOutputSearchMeta(dname: string): Observable<[Family[], Category[]]> {
        const outputFamilyList = this.retrieveOutputFamilyList(dname);
        const categoryList = this.retrieveCategoryList(dname);

        return forkJoin([outputFamilyList, categoryList]);
    }

    /**
     * Retrieves output families for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return Observable<Family[]>
     */
    retrieveOutputFamilyList(dname: string): Observable<Family[]> {
        return this.http.get<Family[]>(this.API_PATH + '/dataset/' + dname + '/output-family').pipe(
            map(outputFamilyList => [...outputFamilyList].sort(sortByDisplay))
        );
    }

    /**
     * Retrieves output categories for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return Observable<Category[]>
     */
    retrieveCategoryList(dname: string): Observable<Category[]> {
        return this.http.get<Category[]>(this.API_PATH + '/dataset/' + dname + '/output-category').pipe(
            map(outputCategoryList => [...outputCategoryList].sort(sortByDisplay))
        );
    }
}
