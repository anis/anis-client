/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Attribute } from '../model';
import { environment } from '../../../environments/environment';

@Injectable()
/**
 * @class
 * @classdesc Attribute service.
 */
export class AttributeService {
    private API_PATH: string = environment.apiUrl;

    constructor(private http: HttpClient) { }

    /**
     * Retrieves attributes metadata for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return Observable<{ datasetName: string, attributeList: Attribute[] }>
     */
    retrieveAttributeList(dname: string): Observable<{ datasetName: string, attributeList: Attribute[] }> {
        return this.http.get<Attribute[]>(this.API_PATH + '/dataset/' + dname + '/attribute').pipe(
            map(res => {
                return { datasetName: dname, attributeList: res };
            }),
            catchError(error => {
                error.datasetName = dname;
                return throwError(error);
            })
        );
    }
}
