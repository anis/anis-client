/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { sortByDisplay } from '../../shared/utils';
import { Survey, Dataset, Family } from '../model';
import { environment } from '../../../environments/environment';

@Injectable()
/**
 * @class
 * @classdesc Dataset service.
 */
export class DatasetService {
    private API_PATH: string = environment.apiUrl;
    private instanceName: string = environment.instanceName;

    constructor(private http: HttpClient) { }

    /**
     * Retrieves surveys, datasets and dataset families metadata.
     *
     * @return Observable<[Survey[], Dataset[], Family[]]>
     */
    retrieveDatasetSearchMeta(): Observable<[Survey[], Dataset[], Family[]]> {
        const surveyList = this.retrieveSurveyList();
        const datasetList = this.retrieveDatasetList();
        const datasetFamilyList = this.retrieveDatasetFamilyList();

        return forkJoin([surveyList, datasetList, datasetFamilyList]);
    }

    /**
     * Retrieves surveys metadata.
     *
     * @return Observable<Survey[]>
     */
     retrieveSurveyList(): Observable<Survey[]> {
        return this.http.get<Survey[]>(this.API_PATH + '/survey');
    }

    /**
     * Retrieves datasets metadata.
     *
     * @return Observable<Dataset[]>
     */
    retrieveDatasetList(): Observable<Dataset[]> {
        return this.http.get<Dataset[]>(this.API_PATH + '/instance/' + this.instanceName + '/dataset');
    }

    /**
     * Retrieves dataset families metadata.
     *
     * @return Observable<Family[]>
     */
    retrieveDatasetFamilyList(): Observable<Family[]> {
        return this.http.get<Family[]>(this.API_PATH + '/instance/' + this.instanceName + '/dataset-family').pipe(
            map(datasetFamilyList => [...datasetFamilyList].sort(sortByDisplay))
        );
    }
}
