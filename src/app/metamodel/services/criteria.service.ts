/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Family } from '../model';
import { sortByDisplay } from '../../shared/utils';
import { environment } from '../../../environments/environment';

@Injectable()
/**
 * @class
 * @classdesc Criteria service.
 */
export class CriteriaService {
    private API_PATH: string = environment.apiUrl;

    constructor(private http: HttpClient) { }

    /**
     * Retrieves criteria metadata for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return Observable<Family[]>
     */
    retrieveCriteriaSearchMeta(dname: string): Observable<Family[]> {
        return this.retrieveCriteriaFamilyList(dname);
    }

    /**
     * Retrieves criteria families metadata for the given dataset name.
     *
     * @param  {string} dname - The dataset name.
     *
     * @return Observable<Family[]>
     */
    retrieveCriteriaFamilyList(dname: string): Observable<Family[]> {
        return this.http.get<Family[]>(this.API_PATH + '/dataset/' + dname + '/criteria-family').pipe(
            map(criteriaFamilyList => [...criteriaFamilyList].sort(sortByDisplay))
        );
    }
}
