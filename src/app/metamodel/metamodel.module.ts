/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { effects } from './effects';
import { services } from './services';
import { reducer } from './reducers';

@NgModule({
    imports: [
        StoreModule.forFeature('metamodel', reducer),
        EffectsModule.forFeature(effects)
    ],
    providers: [
        services
    ]
})
/**
 * @class
 * @classdesc Metamodel module.
 */
export class MetamodelModule { }
