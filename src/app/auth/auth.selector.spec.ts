import * as authSelector from './auth.selector';
import * as fromAuth from './auth.reducer';

describe('[Auth] Selector', () => {
    it('should get isAuthenticated', () => {
        const state = { auth: { ...fromAuth.initialState }};
        expect(authSelector.isAuthenticated(state)).toBeFalsy();
    });

    it('should get userProfile', () => {
        const state = { auth: { ...fromAuth.initialState }};
        expect(authSelector.getUserProfile(state)).toBeNull();
    });

    it('should get userRoles', () => {
        const state = { auth: { ...fromAuth.initialState }};
        expect(authSelector.getUserRoles(state).length).toEqual(0);
    });
});
