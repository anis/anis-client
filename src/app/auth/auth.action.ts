/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import { UserProfile } from './user-profile.model';

export const LOGIN = '[Auth] Login';
export const LOGOUT = '[Auth] Logout';
export const AUTH_SUCCESS = '[Auth] Auth Success';
export const LOAD_USER_PROFILE_SUCCESS = '[Auth] Load User Profile Success';
export const LOAD_USER_ROLES_SUCCESS = '[Auth] Load User Roles Success';
export const OPEN_EDIT_PROFILE = '[Auth] Open Edit Profile';

/**
 * @class
 * @classdesc LoginAction action.
 * @readonly
 */
export class LoginAction implements Action {
    readonly type = LOGIN;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc LogoutAction action.
 * @readonly
 */
export class LogoutAction implements Action {
    readonly type = LOGOUT;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc AuthSuccessAction action.
 * @readonly
 */
export class AuthSuccessAction implements Action {
    readonly type = AUTH_SUCCESS;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc LoadUserProfileSuccessAction action.
 * @readonly
 */
export class LoadUserProfileSuccessAction implements Action {
    readonly type = LOAD_USER_PROFILE_SUCCESS;

    constructor(public payload: UserProfile) { }
}

/**
 * @class
 * @classdesc LoadUserRolesSuccessAction action.
 * @readonly
 */
export class LoadUserRolesSuccessAction implements Action {
    readonly type = LOAD_USER_ROLES_SUCCESS;

    constructor(public payload: string[]) { }
}

/**
 * @class
 * @classdesc OpenEditProfileAction action.
 * @readonly
 */
export class OpenEditProfileAction implements Action {
    readonly type = OPEN_EDIT_PROFILE;

    constructor(public payload: {} = null) { }
}

export type Actions
    = LoginAction
    | LogoutAction
    | AuthSuccessAction
    | LoadUserProfileSuccessAction
    | LoadUserRolesSuccessAction
    | OpenEditProfileAction;
