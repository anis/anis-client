/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from './auth.action';
import { UserProfile } from './user-profile.model';

/**
 * Interface for authentication state.
 *
 * @interface State
 */
export interface State {
    isAuthenticated: boolean;
    userProfile: UserProfile;
    userRoles: string[];
}

export const initialState: State = {
    isAuthenticated: false,
    userProfile: null,
    userRoles: []
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.AUTH_SUCCESS:
            return {
                ...state,
                isAuthenticated: true
            };

        case actions.LOAD_USER_PROFILE_SUCCESS:
            return {
                ...state,
                userProfile: action.payload
            };

        case actions.LOAD_USER_ROLES_SUCCESS:
            return {
                ...state,
                userRoles: action.payload
            };

        default:
            return state;
    }
}

export const isAuthenticated = (state: State) => state.isAuthenticated;
export const getUserProfile = (state: State) => state.userProfile;
export const getUserRoles = (state: State) => state.userRoles;
