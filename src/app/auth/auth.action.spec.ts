import * as authActions from './auth.action';
import { UserProfile } from './user-profile.model';

describe('[Auth] Action', () => {
    it('should create LoginAction', () => {
        const action = new authActions.LoginAction();
        expect(action.type).toEqual(authActions.LOGIN);
    });

    it('should create LogoutAction', () => {
        const action = new authActions.LogoutAction();
        expect(action.type).toEqual(authActions.LOGOUT);
    });

    it('should create AuthSuccessAction', () => {
        const action = new authActions.AuthSuccessAction();
        expect(action.type).toEqual(authActions.AUTH_SUCCESS);
    });

    it('should create LoadUserProfileSuccessAction', () => {
        const profile: UserProfile = { id: 'id', username: 'toto' };
        const action = new authActions.LoadUserProfileSuccessAction(profile);
        expect(action.type).toEqual(authActions.LOAD_USER_PROFILE_SUCCESS);
        expect(action.payload).toEqual(profile);
    });

    it('should create LoadUserRolesSuccessAction', () => {
        const action = new authActions.LoadUserRolesSuccessAction(['toto']);
        expect(action.type).toEqual(authActions.LOAD_USER_ROLES_SUCCESS);
        expect(action.payload).toEqual(['toto']);
    });

    it('should create OpenEditProfileAction', () => {
        const action = new authActions.OpenEditProfileAction();
        expect(action.type).toEqual(authActions.OPEN_EDIT_PROFILE);
    });
});
