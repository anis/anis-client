/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { from } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { KeycloakService } from 'keycloak-angular';

import * as authActions from './auth.action';
import { environment } from '../../environments/environment';

@Injectable()
/**
 * @class
 * @classdesc Authentication effects.
 */

export class AuthEffects {
    constructor(
        private actions$: Actions,
        private keycloak: KeycloakService
    ) { }

    /**
     * Executes log in.
     */
    @Effect({ dispatch: false })
    loginAction$ = this.actions$.pipe(
        ofType(authActions.LOGIN),
        tap(_ => this.keycloak.login())
    );

    /**
     * Executes log out.
     */
    @Effect({ dispatch: false })
    logoutAction$ = this.actions$.pipe(
        ofType(authActions.LOGOUT),
        tap(_ => this.keycloak.logout())
    );

    /**
     * Saves user profile and gets user roles.
     */
    @Effect()
    authSuccessAction$ = this.actions$.pipe(
        ofType(authActions.AUTH_SUCCESS),
        switchMap(_ =>
            from(this.keycloak.loadUserProfile()).pipe(
                switchMap(userProfile => [
                    new authActions.LoadUserProfileSuccessAction(userProfile),
                    new authActions.LoadUserRolesSuccessAction(this.keycloak.getUserRoles())
                ])
            )
        )
    );

    /**
     * Opens edit profile page.
     */
    @Effect({ dispatch: false })
    OpenEditProfileAction$ = this.actions$.pipe(
        ofType(authActions.OPEN_EDIT_PROFILE),
        tap(_ => window.open(environment.ssoAuthUrl + '/realms/' + environment.ssoRealm + '/account', '_blank'))
    );
}
