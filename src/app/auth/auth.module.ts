/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { KeycloakAngularModule } from 'keycloak-angular';

import { reducer } from './auth.reducer';
import { AuthEffects } from './auth.effects';
import { initializeKeycloakAnis } from './init.keycloak';

@NgModule({
    imports: [
        KeycloakAngularModule,
        StoreModule.forFeature('auth', reducer),
        EffectsModule.forFeature([ AuthEffects ])
    ],
    providers: [
        initializeKeycloakAnis
    ]
})
/**
 * @class
 * @classdesc Authentication module.
 */
export class AuthModule { }
