import * as fromAuth from './auth.reducer';
import * as authActions from './auth.action';
import { UserProfile } from './user-profile.model';

describe('[Auth] Reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromAuth;
        const action = {} as authActions.Actions;
        const state = fromAuth.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should set isAuthenticated to true', () => {
        const { initialState } = fromAuth;
        const action = new authActions.AuthSuccessAction();
        const state = fromAuth.reducer(initialState, action);

        expect(state.isAuthenticated).toBeTruthy();
        expect(state.userProfile).toBeNull();
        expect(state.userRoles.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set userProfile', () => {
        const profile: UserProfile = { id: 'id', username: 'toto' };
        const { initialState } = fromAuth;
        const action = new authActions.LoadUserProfileSuccessAction(profile);
        const state = fromAuth.reducer(initialState, action);

        expect(state.isAuthenticated).toBeFalsy();
        expect(state.userProfile).toEqual(profile);
        expect(state.userRoles.length).toEqual(0);
        expect(state).not.toEqual(initialState);
    });

    it('should set userRoles', () => {
        const { initialState } = fromAuth;
        const action = new authActions.LoadUserRolesSuccessAction(['toto']);
        const state = fromAuth.reducer(initialState, action);

        expect(state.isAuthenticated).toBeFalsy();
        expect(state.userProfile).toBeNull();
        expect(state.userRoles.length).toEqual(1);
        expect(state.userRoles[0]).toEqual('toto');
        expect(state).not.toEqual(initialState);
    });

    it('should get isAuthenticated', () => {
        const action = {} as authActions.Actions;
        const state = fromAuth.reducer(undefined, action);

        expect(fromAuth.isAuthenticated(state)).toBeFalsy();
    });

    it('should get userProfile', () => {
        const action = {} as authActions.Actions;
        const state = fromAuth.reducer(undefined, action);

        expect(fromAuth.getUserProfile(state)).toBeNull();
    });

    it('should get userRoles', () => {
        const action = {} as authActions.Actions;
        const state = fromAuth.reducer(undefined, action);

        expect(fromAuth.getUserRoles(state).length).toEqual(0);
    });
});
