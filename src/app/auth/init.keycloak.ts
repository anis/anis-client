/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { APP_INITIALIZER } from '@angular/core';

import { Store } from '@ngrx/store';
import { from } from 'rxjs';
import { KeycloakService, KeycloakEventType } from 'keycloak-angular';

import * as keycloakActions from './auth.action';
import * as fromKeycloak from './auth.reducer';
import { environment } from '../../environments/environment';

/**
 * Inits Keycloak.
 *
 * @param  {KeycloakService} keycloak - The Keycloak service.
 * @param  {Store<{ keycloak: fromKeycloak.State }>} store - The Keycloak store.
 *
 * @return any
 */

function initializeKeycloak(keycloak: KeycloakService, store: Store<{ keycloak: fromKeycloak.State }>): any {
    return async () => {
        from(keycloak.keycloakEvents$).subscribe(event => {
            if (event.type === KeycloakEventType.OnAuthSuccess) {
                store.dispatch(new keycloakActions.AuthSuccessAction());
            }
        })

        let silentCheckSsoRedirectUri = window.location.origin;
        if (environment.baseHref != '/') {
            silentCheckSsoRedirectUri += environment.baseHref;
        }
        silentCheckSsoRedirectUri += '/assets/silent-check-sso.html';

        return keycloak.init({
            config: {
                url: environment.ssoAuthUrl,
                realm: environment.ssoRealm,
                clientId: environment.ssoClientId,
            },
            initOptions: {
                onLoad: 'check-sso',
                silentCheckSsoRedirectUri
            },
            loadUserProfileAtStartUp: true
        });
    }
}

export const initializeKeycloakAnis = {
    provide: APP_INITIALIZER,
    useFactory: initializeKeycloak,
    multi: true,
    deps: [ KeycloakService, Store ]
};
