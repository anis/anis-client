import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { DatatableComponent } from './datatable.component';
import { ATTRIBUTE_LIST, DATASET } from '../../../settings/test-data';
import { Pagination, PaginationOrder } from './model';

describe('[Search][Result] Component: DatatableComponent', () => {
    @Component({ selector: 'app-img', template: '' })
    class ImgStubComponent {
        @Input() src: string;
    }

    @Component({ selector: 'app-thumbnail', template: '' })
    class ThumbnailStubComponent {
        @Input() src: string;
        @Input() attributeName: string;
    }

    @Component({ selector: 'app-link', template: '' })
    class LinkStubComponent {
        @Input() href: string;
    }

    @Component({ selector: 'app-btn', template: '' })
    class BtnStubComponent {
        @Input() href: string;
    }

    @Component({ selector: 'app-detail', template: '' })
    class DetailStubComponent {
        @Input() style: string;
        @Input() datasetName: string;
        @Input() data: string | number;
    }

    @Component({ selector: 'app-download', template: '' })
    class DownloadStubComponent {
        @Input() href: string;
    }

    @Component({ selector: 'app-json-renderer', template: '' })
    class JsonStubComponent {
        @Input() attributeName: string;
        @Input() json: string;
    }

    @Component({ selector: 'pagination', template: '' })
    class PaginationStubComponent {
        @Input() totalItems: number;
        @Input() boundaryLinks: boolean;
        @Input() rotate: boolean;
        @Input() maxSize: number;
    }

    let component: DatatableComponent;
    let fixture: ComponentFixture<DatatableComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatatableComponent,
                ImgStubComponent,
                ThumbnailStubComponent,
                LinkStubComponent,
                BtnStubComponent,
                DetailStubComponent,
                DownloadStubComponent,
                JsonStubComponent,
                PaginationStubComponent
            ],
            imports: [AccordionModule.forRoot(), BrowserAnimationsModule]
        });
        fixture = TestBed.createComponent(DatatableComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#requiredParams() should return if required params are loaded', () => {
        component.attributeList = ATTRIBUTE_LIST;
        component.outputList = [1];
        component.dataLength = 1;
        expect(component.requiredParams()).toBeTruthy();
        component.dataLength = undefined;
        expect(component.requiredParams()).toBeFalsy();
        component.dataLength = 1;
        component.outputList = [];
        expect(component.requiredParams()).toBeFalsy();
        component.outputList = [1];
        component.attributeList = []
        expect(component.requiredParams()).toBeFalsy();
    });

    it('#noSelectedData() should return true if no selectedData', () => {
        component.selectedData = [];
        expect(component.noSelectedData()).toBeTruthy();
    });

    it('#noSelectedData() should return false if there are selectedData', () => {
        component.selectedData = [123456];
        expect(component.noSelectedData()).toBeFalsy();
    });

    it('#getOutputList() should return filtered output list', () => {
        component.outputList = [2]
        component.attributeList = ATTRIBUTE_LIST;
        expect(component.getOutputList().length).toBe(1);
    });

    it('#toggleSelection(datum) should return add datum to selectedData', () => {
        const datum = { label_one: 123456 };
        component.attributeList = ATTRIBUTE_LIST;
        component.selectedData = [];
        component.addSelectedData.subscribe((event: any) => expect(event).toBe(123456));
        component.toggleSelection(datum);
    });

    it('#toggleSelection(datum) should return remove datum to selectedData', () => {
        const datum = { label_one: 123456 };
        component.selectedData = [123456];
        component.attributeList = ATTRIBUTE_LIST;
        component.deleteSelectedData.subscribe((event: any) => expect(event).toBe(123456));
        component.toggleSelection(datum);
    });

    it('#isSelected(datum) should return true datum is selected', () => {
        const datum = { label_one: 123456 };
        component.attributeList = ATTRIBUTE_LIST;
        component.selectedData = [123456];
        expect(component.isSelected(datum)).toBeTruthy();
    });

    it('#isSelected(datum) should return false datum is not selected', () => {
        const datum = { label_one: 123456 };
        component.attributeList = ATTRIBUTE_LIST;
        component.selectedData = [];
        expect(component.isSelected(datum)).toBeFalsy();
    });

    it('#changePage() should change page value and raise getData event', () => {
        component.dataset = DATASET;
        component.sortedCol = 1;
        component.sortedOrder = PaginationOrder.a;
        const expectedPagination: Pagination = {
            dname: 'cat_1',
            page: 2,
            nbItems: 10,
            sortedCol: 1,
            order: PaginationOrder.a
        }
        component.getData.subscribe((event: Pagination) => expect(event).toEqual(expectedPagination));
        component.changePage(2);
    });

    it('#changeNbItems() should change nbItems value and raise getData event', () => {
        component.dataset = DATASET;
        component.sortedCol = 1;
        component.sortedOrder = PaginationOrder.a;
        const expectedPagination: Pagination = {
            dname: 'cat_1',
            page: 1,
            nbItems: 20,
            sortedCol: 1,
            order: PaginationOrder.a
        }
        component.getData.subscribe((event: Pagination) => expect(event).toEqual(expectedPagination));
        component.changeNbItems(20);
    });

    it('#sort() should raise getData event with correct parameters', () => {
        component.dataset = DATASET;
        component.sortedOrder = PaginationOrder.a;
        let expectedPagination: Pagination = {
            dname: 'cat_1',
            page: 1,
            nbItems: 10,
            sortedCol: 1,
            order: PaginationOrder.a
        }
        let subscribtion = component.getData.subscribe((event: Pagination) => expect(event).toEqual(expectedPagination));
        component.sort(1);
        subscribtion.unsubscribe();
        component.sortedCol = 1;
        component.sortedOrder = PaginationOrder.a;
        expectedPagination = {
            dname: 'cat_1',
            page: 1,
            nbItems: 10,
            sortedCol: 1,
            order: PaginationOrder.d
        }
        subscribtion = component.getData.subscribe((event: Pagination) => expect(event).toEqual(expectedPagination));
        component.sort(1);
        subscribtion.unsubscribe();
        component.sortedCol = 1;
        component.sortedOrder = PaginationOrder.d;
        expectedPagination = {
            dname: 'cat_1',
            page: 1,
            nbItems: 10,
            sortedCol: 1,
            order: PaginationOrder.a
        }
        subscribtion = component.getData.subscribe((event: Pagination) => expect(event).toEqual(expectedPagination));
        component.sort(1);
    });

    it('#ngOnInit() should init sortedCol value', () => {
        component.attributeList = ATTRIBUTE_LIST;
        component.ngOnInit();
        expect(component.sortedCol).toEqual(1);
    });
});

