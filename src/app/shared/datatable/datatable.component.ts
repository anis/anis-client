/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Attribute, Dataset } from '../../metamodel/model';
import { Pagination, PaginationOrder } from './model';


@Component({
    selector: 'app-datatable',
    templateUrl: 'datatable.component.html',
    styleUrls: ['datatable.component.css'],
})
/**
 * @class
 * @classdesc Datatable component.
 *
 * @implements OnInit
 */
export class DatatableComponent implements OnInit {
    @Input() dataset: Dataset;
    @Input() attributeList: Attribute[];
    @Input() outputList: number[];
    @Input() data: any[];
    @Input() dataLength: number;
    @Input() selectedData: any[] = [];
    @Input() processWip: boolean = false;
    @Input() processDone: boolean = false;
    @Input() processId: string = null;
    @Output() getData: EventEmitter<Pagination> = new EventEmitter();
    @Output() addSelectedData: EventEmitter<number | string> = new EventEmitter();
    @Output() deleteSelectedData: EventEmitter<number | string> = new EventEmitter();
    @Output() executeProcess: EventEmitter<string> = new EventEmitter();
    nbItems = 10;
    page = 1;
    sortedCol: number = null;
    sortedOrder: PaginationOrder = PaginationOrder.a;
    
    ngOnInit() {
        this.sortedCol = this.attributeList.find(a => a.order_by).id;
    }

    /**
     * Checks if required parameters to display datatable are passed to the component.
     *
     * @return boolean
     */
    requiredParams(): boolean {
        if (this.attributeList.length === 0 || this.outputList.length === 0 || !this.dataLength) {
            return false;
        }
        return true;
    }

    /**
     * Checks if there is no data selected.
     *
     * @return boolean
     */
    noSelectedData(): boolean {
        return this.selectedData.length < 1;
    }

    /**
     * Returns output list from attribute list.
     *
     * @return Attribute[]
     */
    getOutputList(): Attribute[] {
        return this.attributeList
            .filter(a => this.outputList.includes(a.id))
            .sort((a, b) => a.output_display - b.output_display);
    }

    /**
     * Emits events to select or unselect data.
     *
     * @param  {any} datum - The data to select or unselect.
     *
     * @fires EventEmitter<number | string>
     */
    toggleSelection(datum: any): void {
        const attribute = this.attributeList.find(a => a.search_flag === 'ID');
        const index = this.selectedData.indexOf(datum[attribute.label]);
        if (index > -1) {
            this.deleteSelectedData.emit(datum[attribute.label]);
        } else {
            this.addSelectedData.emit(datum[attribute.label]);
        }
    }

    /**
     * Checks if data is selected.
     *
     * @param  {any} datum - The data.
     *
     * @return boolean
     */
    isSelected(datum: any): boolean {
        const attribute = this.attributeList.find(a => a.search_flag === 'ID');

        if (this.selectedData.indexOf(datum[attribute.label]) > -1) {
            return true;
        }
        return false;
    }

    /**
     * Emits event to change datatable page.
     *
     * @param  {number} nb - The page number to access.
     *
     * @fires EventEmitter<Pagination>
     */
    changePage(nb: number): void {
        this.page = nb;
        const pagination: Pagination = {
            dname: this.dataset.name,
            page: this.page,
            nbItems: this.nbItems,
            sortedCol: this.sortedCol,
            order: this.sortedOrder
        };
        this.getData.emit(pagination);
    }

    /**
     * Emits event to change datatable displayed items.
     *
     * @param  {number} nb - The number of items to display.
     *
     * @fires EventEmitter<Pagination>
     */
    changeNbItems(nb: number): void {
        this.nbItems = nb;
        this.changePage(1);
    }

    /**
     * Emits event to change the sorted order and the sorted column of the datatable.
     *
     * @param  {number} id - The id of the column to sort.
     *
     * @fires EventEmitter<Pagination>
     */
    sort(id: number): void {
        if (id === this.sortedCol) {
            this.sortedOrder = this.sortedOrder === PaginationOrder.a ? PaginationOrder.d : PaginationOrder.a;
        } else {
            this.sortedCol = id;
            this.sortedOrder = PaginationOrder.a;
        }
        this.changePage(1);
    }
}
