/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { RendererConfig } from "../../../metamodel/model";

/**
 * Interface for detail renderer configuration.
 *
 * @interface DetailConfig
 * @extends RendererConfig
 */
interface DetailConfig extends RendererConfig {
    display: string;
    blank: boolean;
}

@Component({
    selector: 'app-detail',
    templateUrl: 'detail.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Detail renderer component.
 */
export class DetailComponent {
    @Input() value: string | number;
    @Input() datasetName: string;
    @Input() config: DetailConfig;
}
