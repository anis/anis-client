import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsModalService } from 'ngx-bootstrap/modal';

import { JsonComponent } from './json.component';

describe('[Search][Result][Renderer] Component: JsonComponent', () => {
    let component: JsonComponent;
    let fixture: ComponentFixture<JsonComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [JsonComponent],
            imports: [NgxJsonViewerModule, ModalModule.forRoot()],
            providers: [BsModalService]
        });
        fixture = TestBed.createComponent(JsonComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
