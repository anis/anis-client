/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { RendererConfig } from '../../../metamodel/model';
import { getHost } from "../../utils";

/**
 * Interface for download renderer configuration.
 *
 * @interface DownloadConfig
 * @extends RendererConfig
 */
interface DownloadConfig extends RendererConfig {
    display: string;
    text: string;
    icon: string;
}

@Component({
    selector: 'app-download',
    templateUrl: 'download.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Download renderer component.
 */
export class DownloadComponent {
    @Input() value: string;
    @Input() datasetName: string;
    @Input() config: DownloadConfig;

    /**
     * Returns link href.
     *
     * @return string
     */
    getHref(): string {
        return getHost() + '/download-file/' + this.datasetName + '/' + this.value;
    }

    /**
     * Returns config text.
     *
     * @return string
     */
     getText(): string {
        return this.config.text.replace('$value', this.value.toString());
    }
}
