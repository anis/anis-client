import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadComponent } from './download.component';
import { environment } from '../../../../environments/environment';

describe('[Search][Result][Renderer] Component: DownloadComponent', () => {
    let component: DownloadComponent;
    let fixture: ComponentFixture<DownloadComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DownloadComponent]
        });
        fixture = TestBed.createComponent(DownloadComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getHref() should return file url', () => {
        component.datasetName = 'dname';
        component.value = 'val';
        expect(component.getHref()).toBe(environment.apiUrl + '/download-file/dname/val');
    });
});
