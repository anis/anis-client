import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BsModalService } from 'ngx-bootstrap/modal';

import { ImageComponent } from './image.component';
import { environment } from '../../../../environments/environment';

class MockBsModalService {
}

describe('[Search][Result][Renderer] Component: ImageComponent', () => {
    let component: ImageComponent;
    let fixture: ComponentFixture<ImageComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ImageComponent],
            providers: [
                { provide: BsModalService, useClass: MockBsModalService }
            ]
        });
        fixture = TestBed.createComponent(ImageComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getValue() should return image url', () => {
        component.datasetName = 'anis_observation';
        component.value = '20141002/M_81-S001-R001-C001-SDSS_g.fits';
        component.config = {
            type: 'fits',
            display: 'modal',
            height: null,
            width: null
        };
        expect(component.getValue()).toEqual(
            environment.servicesUrl + '/fits-to-png/anis_observation?filename=20141002/M_81-S001-R001-C001-SDSS_g.fits&stretch=linear&pmin=0.25&pmax=99.75&axes=true'
        );
    });
});
