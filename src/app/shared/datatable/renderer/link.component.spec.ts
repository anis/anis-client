import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkComponent } from './link.component';

describe('[Search][Result][Renderer] Component: LinkComponent', () => {
    let component: LinkComponent;
    let fixture: ComponentFixture<LinkComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LinkComponent]
        });
        fixture = TestBed.createComponent(LinkComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getValue() should return link url', () => {
        component.config = {
            href: 'url',
            display: 'display',
            text: 'text',
            icon: 'icon',
            blank: true
        };
        component.value = 'val';
        expect(component.getValue()).toEqual('url');
    });

    it('#getText() should return link text', () => {
        component.config = {
            href: 'url',
            display: 'display',
            text: 'text',
            icon: 'icon',
            blank: true
        };
        component.value = 'val';
        expect(component.getText()).toEqual('text');
    });
});
