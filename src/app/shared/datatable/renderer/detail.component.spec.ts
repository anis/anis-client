import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { DetailComponent } from './detail.component';

describe('[Search][Result][Renderer] Component: DetailComponent', () => {
    let component: DetailComponent;
    let fixture: ComponentFixture<DetailComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DetailComponent],
            imports: [RouterTestingModule]
        });
        fixture = TestBed.createComponent(DetailComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
