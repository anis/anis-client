/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { RendererConfig } from '../../../metamodel/model';

/**
 * Interface for JSON renderer configuration.
 *
 * @interface JsonConfig
 * @extends RendererConfig
 */
interface JsonConfig extends RendererConfig {
}

@Component({
    selector: 'app-json',
    templateUrl: 'json.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc JSON renderer component.
 */
export class JsonComponent {
    @Input() value: string | number;
    @Input() attributeLabel: string;
    @Input() config: JsonConfig;

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    /**
     * Opens modal.
     *
     * @param  {TemplateRef<any>} template - The modal template to open.
     */
    openModal(template: TemplateRef<any>): void {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'modal-fit-content' })
        );
    }
}
