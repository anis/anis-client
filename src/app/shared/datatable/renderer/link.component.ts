/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { RendererConfig } from '../../../metamodel/model';

/**
 * Interface for link renderer configuration.
 *
 * @interface LinkConfig
 * @extends RendererConfig
 */
interface LinkConfig extends RendererConfig {
    href: string;
    display: string;
    text: string;
    icon: string;
    blank: boolean;
}

@Component({
    selector: 'app-link',
    templateUrl: 'link.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Link renderer component.
 */
export class LinkComponent {
    @Input() value: string | number;
    @Input() datasetName: string;
    @Input() config: LinkConfig;

    /**
     * Returns config href.
     *
     * @return string
     */
    getValue(): string {
        return this.config.href.replace('$value', this.value.toString());
    }

    /**
     * Returns config text.
     *
     * @return string
     */
    getText(): string {
        return this.config.text.replace('$value', this.value.toString());
    }
}
