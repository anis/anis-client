/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { RendererConfig } from '../../../metamodel/model';
import { environment } from '../../../../environments/environment';

/**
 * Interface for image renderer configuration.
 *
 * @interface ImageConfig
 * @extends RendererConfig
 */
interface ImageConfig extends RendererConfig {
    type: string;
    display: string;
    width: number;
    height: number;
}

@Component({
    selector: 'app-image',
    templateUrl: 'image.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Image renderer component.
 */
export class ImageComponent {
    @Input() value: string | number;
    @Input() datasetName: string;
    @Input() config: ImageConfig;
    private SERVICES_PATH: string = environment.servicesUrl;

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    /**
     * Returns source image.
     *
     * @return string
     */
    getValue(): string {
        if (this.config.type === 'fits') {
            return `${this.SERVICES_PATH}/fits-to-png/${this.datasetName}?filename=${this.value}`
                + `&stretch=linear&pmin=0.25&pmax=99.75&axes=true`;
        } else {
            return this.value as string;
        }
    }
}
