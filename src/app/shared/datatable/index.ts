import { DatatableComponent } from './datatable.component';
import { DetailComponent } from './renderer/detail.component';
import { ImageComponent } from './renderer/image.component';
import { JsonComponent } from './renderer/json.component';
import { LinkComponent } from './renderer/link.component';
import { DownloadComponent } from './renderer/download.component';

export const datatableComponents = [
    DatatableComponent,
    DetailComponent,
    ImageComponent,
    JsonComponent,
    LinkComponent,
    DownloadComponent
];
