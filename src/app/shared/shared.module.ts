/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { ToastrModule } from 'ngx-toastr';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxJsonViewerModule } from 'ngx-json-viewer';

import { ScrollTopService } from './service/sroll-top.service';
import { PrettyOperatorPipe } from './pretty-operator.pipe';
import { NgSelectModule } from '@ng-select/ng-select';
import { InlineSVGModule } from 'ng-inline-svg';
import { coneSearchComponents } from './cone-search';
import { reducer } from './cone-search/store/cone-search.reducer';
import { ConeSearchEffects } from './cone-search/store/cone-search.effects';
import { ConeSearchService } from './cone-search/store/cone-search.service';
import { datatableComponents } from './datatable';
import { SampModule } from '../samp/samp.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        AccordionModule.forRoot(),
        CollapseModule.forRoot(),
        PopoverModule.forRoot(),
        PaginationModule.forRoot(),
        TooltipModule.forRoot(),
        BsDatepickerModule.forRoot(),
        NgSelectModule,
        NgxJsonViewerModule,
        InlineSVGModule.forRoot(),
        RouterModule,
        StoreModule.forFeature('coneSearch', reducer),
        EffectsModule.forFeature([ConeSearchEffects]),
        SampModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule,
        ModalModule,
        TabsModule,
        AccordionModule,
        CollapseModule,
        PopoverModule,
        PaginationModule,
        TooltipModule,
        BsDatepickerModule,
        NgSelectModule,
        NgxJsonViewerModule,
        InlineSVGModule,
        PrettyOperatorPipe,
        coneSearchComponents,
        datatableComponents,
        SampModule
    ],
    providers: [
        BsModalService,
        ScrollTopService,
        ConeSearchService
    ],
    declarations: [
        PrettyOperatorPipe,
        coneSearchComponents,
        datatableComponents
    ]
})
export class SharedModule { }
