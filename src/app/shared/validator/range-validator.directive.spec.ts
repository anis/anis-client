import { FormControl } from '@angular/forms';

import { rangeValidator } from './range-validator.directive';

describe('[Shared] rangeValidator', () => {
    it('should return valid', () => {
        let field = new FormControl('', rangeValidator(0, 10));
        field.setValue(7);
        expect(field.errors).toBeNull();
    });
    
    it('should return range error', () => {
        let field = new FormControl('', rangeValidator(0, 10));
        field.setValue(17);
        expect(field.errors.range).toBeTruthy();
        field.setValue(-2);
        expect(field.errors.range).toBeTruthy();
    });
    
    it('should return range error with form label', () => {
        let field = new FormControl('', rangeValidator(0, 10, 'toto'));
        field.setValue(17);
        expect(field.errors.range.value).toEqual('toto must be between 0 and 10');
    });
});
