import { FormControl } from '@angular/forms';

import { nanValidator } from './nan-validator.directive';

describe('[Shared] nanValidator', () => {
    it('should return valid', () => {
        let field = new FormControl('');
        expect(nanValidator(field)).toBeNull();
        field.setValue(17);
        expect(nanValidator(field)).toBeNull();
    });
    
    it('should return nan error', () => {
        let field = new FormControl('');
        field.setValue('toto');
        expect(nanValidator(field).nan.value).toBe('toto is not a number');
    });
});
