import { printCriterion, getCriterionStr, getHost } from './utils';
import {
    FieldCriterion,
    JsonCriterion,
    SelectMultipleCriterion,
    BetweenCriterion,
    Criterion,
    ListCriterion
} from '../search/store/model';
import { environment } from '../../environments/environment';
import { environment as prod } from '../../environments/environment.prod';

describe('[Shared] Utils', () => {

    it('#getHost should return protocol and host', () => {
        const apiUrl = environment.apiUrl;
        expect(getHost()).toBe(environment.apiUrl);
        environment.apiUrl = prod.apiUrl;
        expect(getHost()).toBe('http://localhost:9876/server');
        environment.apiUrl = apiUrl;
    });

    it('#printCriterion should print criterion correctly', () => {
        const fieldCriterion = { id: 1, type: 'field', operator: 'eq', value: 'value' } as FieldCriterion;
        let expectedPrintedCriterion = '= value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        const jsonCriterion = { id: 1, type: 'json', path: 'path', operator: 'eq', value: 'value' } as JsonCriterion;
        expectedPrintedCriterion = 'path eq value';
        expect(printCriterion(jsonCriterion)).toEqual(expectedPrintedCriterion);
        const selectMultipleCriterion = { id: 1, type: 'multiple', options: [{label: 'un', value: '1', display: 1}, {label: 'deux', value: '2', display: 2}] } as SelectMultipleCriterion;
        expectedPrintedCriterion = '[un,deux]';
        expect(printCriterion(selectMultipleCriterion)).toEqual(expectedPrintedCriterion);
        let betweenCriterion = { id: 1, type: 'between', min: 'un', max: null } as BetweenCriterion;
        expectedPrintedCriterion = '>= un';
        expect(printCriterion(betweenCriterion)).toEqual(expectedPrintedCriterion);
        betweenCriterion = { id: 1, type: 'between', min: null, max: 'deux' } as BetweenCriterion;
        expectedPrintedCriterion = '<= deux';
        expect(printCriterion(betweenCriterion)).toEqual(expectedPrintedCriterion);
        betweenCriterion = { id: 1, type: 'between', min: 'un', max: 'deux' } as BetweenCriterion;
        expectedPrintedCriterion = '∈ [un;deux]';
        expect(printCriterion(betweenCriterion)).toEqual(expectedPrintedCriterion);
        const listCriterion = { id: 1, type: 'list', values: ['un', 'deux'] } as ListCriterion;
        expectedPrintedCriterion = '= [un,deux]';
        expect(printCriterion(listCriterion)).toEqual(expectedPrintedCriterion);
        const notCriterion = {id: 1, type: null} as Criterion;
        expectedPrintedCriterion = 'Criterion type not valid!';
        expect(printCriterion(notCriterion)).toEqual(expectedPrintedCriterion);
    });

    it('#getCriterionStr() should print criterion url string correctly', () => {
        const fieldCriterion = { id: 1, type: 'field', operator: 'eq', value: 'value' } as FieldCriterion;
        let expectedPrintedCriterion = '1::eq::value';
        expect(getCriterionStr(fieldCriterion)).toEqual(expectedPrintedCriterion);
        const jsonCriterion = { id: 1, type: 'json', path: 'path', operator: 'eq', value: 'value' } as JsonCriterion;
        expectedPrintedCriterion = '1::js::path|eq|value';
        expect(getCriterionStr(jsonCriterion)).toEqual(expectedPrintedCriterion);
        const selectMultipleCriterion = { id: 1, type: 'multiple', options: [{label: 'un', value: '1', display: 1}, {label: 'deux', value: '2', display: 2}] } as SelectMultipleCriterion;
        expectedPrintedCriterion = '1::in::1|2';
        expect(getCriterionStr(selectMultipleCriterion)).toEqual(expectedPrintedCriterion);
        const listCriterion = { id: 1, type: 'list', values: ['un', 'deux'] } as ListCriterion;
        expectedPrintedCriterion = '1::in::un|deux';
        expect(getCriterionStr(listCriterion)).toEqual(expectedPrintedCriterion);
        let betweenCriterion = { id: 1, type: 'between', min: 'un', max: null } as BetweenCriterion;
        expectedPrintedCriterion = '1::gte::un';
        expect(getCriterionStr(betweenCriterion)).toEqual(expectedPrintedCriterion);
        betweenCriterion = { id: 1, type: 'between', min: null, max: 'deux' } as BetweenCriterion;
        expectedPrintedCriterion = '1::lte::deux';
        expect(getCriterionStr(betweenCriterion)).toEqual(expectedPrintedCriterion);
        betweenCriterion = { id: 1, type: 'between', min: 'un', max: 'deux' } as BetweenCriterion;
        expectedPrintedCriterion = '1::bw::un|deux';
        expect(getCriterionStr(betweenCriterion)).toEqual(expectedPrintedCriterion);
    });

    it('#getPrettyOperator() should prettify operator correctly', () => {
        let fieldCriterion = { id: 1, type: 'field', operator: 'eq', value: 'value' } as FieldCriterion;
        let expectedPrintedCriterion = '= value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'neq', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = '≠ value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'gt', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = '> value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'gte', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = '>= value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'lt', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = '< value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'lte', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = '<= value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'lk', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = 'like value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'nlk', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = 'not like value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'in', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = 'in value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'nin', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = 'not in value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
        fieldCriterion = { id: 1, type: 'field', operator: 'toto', value: 'value' } as FieldCriterion;
        expectedPrintedCriterion = 'toto value';
        expect(printCriterion(fieldCriterion)).toEqual(expectedPrintedCriterion);
    });
});
