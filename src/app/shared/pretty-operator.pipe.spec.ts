import { PrettyOperatorPipe } from './pretty-operator.pipe';

describe('PrettyOperatorPipe', () => {
    let pipe = new PrettyOperatorPipe();

    it('transforms "eq" to "="', () => {
        expect(pipe.transform('eq')).toBe('=');
    });

    it('transforms "neq" to "≠"', () => {
        expect(pipe.transform('neq')).toBe('≠');
    });

    it('transforms "gt" to ">"', () => {
        expect(pipe.transform('gt')).toBe('>');
    });

    it('transforms "gte" to ">="', () => {
        expect(pipe.transform('gte')).toBe('>=');
    });

    it('transforms "lt" to "<"', () => {
        expect(pipe.transform('lt')).toBe('<');
    });

    it('transforms "lte" to "<="', () => {
        expect(pipe.transform('lte')).toBe('<=');
    });

    it('transforms "lk" to "like"', () => {
        expect(pipe.transform('lk')).toBe('like');
    });

    it('transforms "nlk" to "not like"', () => {
        expect(pipe.transform('nlk')).toBe('not like');
    });

    it('transforms "in" to "in"', () => {
        expect(pipe.transform('in')).toBe('in');
    });

    it('transforms "nin" to "not in"', () => {
        expect(pipe.transform('nin')).toBe('not in');
    });

    it('not transforms unknown operator', () => {
        expect(pipe.transform('toto')).toBe('toto');
    });
});
