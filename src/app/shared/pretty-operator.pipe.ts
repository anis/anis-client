/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Pipe, PipeTransform } from '@angular/core';

/**
 * @class
 * @classdesc Translate Anis string operator to a pretty form label operator.
 *
 * @example
 * // formats eq to =
 * {{ eq | prettyOperator }}
 */
@Pipe({ name: 'prettyOperator' })
export class PrettyOperatorPipe implements PipeTransform {
    transform(operator: string): string {
        return this.getPrettyOperator(operator);
    }

    getPrettyOperator(operator: string): string {
        switch (operator) {
            case 'eq':
                return '=';
            case 'neq':
                return '≠';
            case 'gt':
                return '>';
            case 'gte':
                return '>=';
            case 'lt':
                return '<';
            case 'lte':
                return '<=';
            case 'lk':
                return 'like';
            case 'nlk':
                return 'not like';
            case 'in':
                return 'in';
            case 'nin':
                return 'not in';
            default:
                return operator;
        }
    }
}
