/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { RouterStateSerializer } from '@ngrx/router-store';
import { RouterStateSnapshot, Params } from '@angular/router';
import {
    Criterion,
    BetweenCriterion,
    FieldCriterion,
    JsonCriterion,
    SelectMultipleCriterion,
    ListCriterion
} from '../search/store/model';
import { environment } from '../../environments/environment';

export interface RouterStateUrl {
    url: string;
    params: Params;
    queryParams: Params;
}

/**
 * @class
 * @classdesc Returns a simpler form of RouterStateSnapshot class.
 *
 * @see https://ngrx.io/guide/router-store/configuration#custom-router-state-serializer
 */
export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
    serialize(routerState: RouterStateSnapshot): RouterStateUrl {
        let route = routerState.root;

        while (route.firstChild) {
            route = route.firstChild;
        }

        const {
            url,
            root: { queryParams }
        } = routerState;
        const { params } = route;

        // Only return an object including the URL, params and query params
        // instead of the entire snapshot
        return { url, params, queryParams };
    }
}

/**
 * Sorts objects by the display property.
 *
 * @param  {number} a - The first object to sort.
 * @param  {number} b - The second object to sort.
 *
 * @example
 * [objects].sortByDisplay()
 */
export const sortByDisplay = (a, b) => a.display - b.display;

/**
 * Returns strict url address.
 *
 * @return string
 *
 * @example
 * const url: string = getHost() + '/following-url/';
 */
export const getHost = (): string => {
    if (!environment.apiUrl.startsWith('http')) {
        const url = window.location;
        return url.protocol + '//' + url.host + environment.apiUrl;
    }
    return environment.apiUrl;
}

/**
 * Returns pretty criterion string.
 *
 * @param  {Criterion} criterion - The criterion to pretty print.
 *
 * @return string
 *
 * @example
 * {{ printCriterion(criterion) }}
 */
export const printCriterion = (criterion: Criterion): string => {
    switch (criterion.type) {
        case 'between':
            const bw = criterion as BetweenCriterion;
            if (bw.min === null) {
                return '<= ' + bw.max;
            } else if (bw.max === null) {
                return '>= ' + bw.min;
            } else {
                return '∈ [' + bw.min + ';' + bw.max + ']';
            }
        case 'field':
            const fd = criterion as FieldCriterion;
            return getPrettyOperator(fd.operator) + ' ' + fd.value.split('|').join(', ');
        case 'list':
            const ls = criterion as ListCriterion;
            return '= [' + ls.values.join(',') + ']';
        case 'json' :
            const json = criterion as JsonCriterion;
            return json.path + ' ' + json.operator + ' ' + json.value;
        case 'multiple':
            const multiple = criterion as SelectMultipleCriterion;
            return '[' + multiple.options.map(option => option.label).join(',') + ']';
        default:
            return 'Criterion type not valid!';
    }
}

/**
 * Returns criterion notation for Anis Server.
 *
 * @param  {Criterion} criterion - The criterion to pretty print.
 *
 * @return string
 *
 * @example
 * getCriterionStr(criterion)
 */
export const getCriterionStr = (criterion: Criterion): string => {
    let str: string = criterion.id.toString();
    if (criterion.type === 'between') {
        const bw = criterion as BetweenCriterion;
        if (bw.min === null) {
            str += '::lte::' + bw.max;
        } else if (bw.max === null) {
            str += '::gte::' + bw.min;
        } else {
            str += '::bw::' + bw.min + '|' + bw.max;
        }
    }
    if (criterion.type === 'field') {
        const fd = criterion as FieldCriterion;
        str += '::' + fd.operator + '::' + fd.value;
    }
    if (criterion.type === 'list') {
        const ls = criterion as ListCriterion;
        str += '::in::' + ls.values.join('|');
    }
    if (criterion.type === 'json') {
        const json = criterion as JsonCriterion;
        str += '::js::' + json.path + '|' + json.operator + '|' + json.value;
    }
    if (criterion.type === 'multiple') {
        const multiple = criterion as SelectMultipleCriterion;
        str += '::in::' + multiple.options.map(option => option.value).join('|');
    }
    return str;
}

/**
 * Returns an Anis Server string operator to a pretty form label operator.
 *
 * @param  {string} operator - The operator to prettify.
 *
 * @return string
 *
 * @example
 * // returns =
 * getPrettyOperator('eq')
 */
const getPrettyOperator = (operator: string): string => {
    switch (operator) {
        case 'eq':
            return '=';
        case 'neq':
            return '≠';
        case 'gt':
            return '>';
        case 'gte':
            return '>=';
        case 'lt':
            return '<';
        case 'lte':
            return '<=';
        case 'lk':
            return 'like';
        case 'nlk':
            return 'not like';
        case 'in':
            return 'in';
        case 'nin':
            return 'not in';
        default:
            return operator;
    }
}
