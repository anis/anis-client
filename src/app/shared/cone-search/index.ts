import { ConeSearchComponent } from "./containers/cone-search.component";
import { ConeSearchPlotComponent } from './components/cone-search-plot.component';
import { ResolverComponent } from './components/resolver.component';
import { RaComponent } from "./components/ra.component";
import { DecComponent } from "./components/dec.component";
import { RadiusComponent } from './components/radius.component';

export const coneSearchComponents = [
    ConeSearchComponent,
    ConeSearchPlotComponent,
    ResolverComponent,
    RaComponent,
    DecComponent,
    RadiusComponent
];
