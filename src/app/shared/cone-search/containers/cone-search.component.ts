/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { ConeSearch, Resolver } from '../store/model';
import { Observable } from 'rxjs';
import * as coneSearchSelector from '../store/cone-search.selector';
import * as fromConeSearch from '../store/cone-search.reducer';
import * as coneSearchActions from '../store/cone-search.action';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    coneSearch: fromConeSearch.State;
}

@Component({
    selector: 'app-cone-search',
    templateUrl: 'cone-search.component.html'
})
/**
 * @class
 * @classdesc Cone search container.
 */
export class ConeSearchComponent {
    @Input() disabled: boolean = false;

    resolverWip: Observable<boolean>;
    resolver: Observable<Resolver>;
    coneSearch: Observable<ConeSearch>;
    unit = 'degree';

    constructor(private store: Store<StoreState>) {
        this.resolverWip = store.select(coneSearchSelector.getResolverWip);
        this.resolver = store.select(coneSearchSelector.getResolver);
        this.coneSearch = store.select(coneSearchSelector.getConeSearch);
    }

    /**
     * Dispatches action to retrieve coordinates of an object.
     *
     * @param  {string} name - The object name.
     */
    retrieveCoordinates(name: string): void {
        this.store.dispatch(new coneSearchActions.RetrieveCoordinatesAction(name));
    }

    /**
     * Dispatches action to update cone search.
     *
     * @param  {ConeSearch} coneSearch - The cone search coordinates.
     */
    updateConeSearch(coneSearch: ConeSearch): void {
        this.store.dispatch(new coneSearchActions.AddConeSearchAction({
            ra: +coneSearch.ra,
            dec: +coneSearch.dec,
            radius: +coneSearch.radius
        }));
    }

    /**
     * Dispatches action to delete the object name from resolver.
     */
    deleteResolver(): void {
        this.store.dispatch(new coneSearchActions.DeleteResolverAction());
    }
}
