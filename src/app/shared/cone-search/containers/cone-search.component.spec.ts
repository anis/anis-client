import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, ViewChild, Input } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { ConeSearchComponent } from './cone-search.component';
import { ConeSearch, Resolver } from '../store/model';
import * as fromConeSearch from '../store/cone-search.reducer';
import * as coneSearchActions from '../store/cone-search.action';

describe('[Shared][ConeSearch] Component: ConeSearchComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-cone-search [disabled]='disabled'></app-cone-search>`
    })
    class TestHostComponent {
        @ViewChild(ConeSearchComponent, { static: false })
        public testedComponent: ConeSearchComponent;
        public disabled: boolean = false;
    }

    @Component({ selector: 'app-resolver', template: '' })
    class ResolverStubComponent {
        @Input() disabled: boolean;
        @Input() resolverWip: boolean;
        @Input() resolver: Resolver;
    }

    @Component({ selector: 'app-ra', template: '' })
    class RaStubComponent {
        @Input() disabled: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() resolver: Resolver;
        @Input() unit: string;
    }

    @Component({ selector: 'app-dec', template: '' })
    class DecStubComponent {
        @Input() disabled: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() resolver: Resolver;
        @Input() unit: string;
    }

    @Component({ selector: 'app-radius', template: '' })
    class RadiusStubComponent {
        @Input() coneSearch: ConeSearch;
        @Input() disabled: boolean;
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: ConeSearchComponent;
    let store: MockStore;
    const initialState = {
        coneSearch: { ...fromConeSearch.initialState },
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConeSearchComponent,
                TestHostComponent,
                ResolverStubComponent,
                RaStubComponent,
                DecStubComponent,
                RadiusStubComponent
            ],
            imports: [ ReactiveFormsModule ],
            providers: [ provideMockStore({ initialState }) ]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#retrieveCoordinates() should dispatch RetrieveCoordinatesAction', () => {
        const retrieveCoordinatesAction = new coneSearchActions.RetrieveCoordinatesAction('toto');
        const spy = spyOn(store, 'dispatch');
        testedComponent.retrieveCoordinates('toto');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(retrieveCoordinatesAction);
    });

    it('#updateConeSearch() should update cone search', () => {
        const addConeSearchAction = new coneSearchActions.AddConeSearchAction({ ra: 1, dec: 2, radius: 3 });
        const spy = spyOn(store, 'dispatch');
        testedComponent.updateConeSearch({ ra: 1, dec: 2, radius: 3 });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(addConeSearchAction);
    });

    it('#deleteResolver() should dispatch DeleteResolverAction', () => {
        const deleteResolverAction = new coneSearchActions.DeleteResolverAction();
        const spy = spyOn(store, 'dispatch');
        testedComponent.deleteResolver();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(deleteResolverAction);
    });
});
