/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector, createFeatureSelector } from '@ngrx/store';
import { FormControl, Validators } from '@angular/forms';

import * as coneSearch from './cone-search.reducer';
import { ConeSearch } from './model';
import { nanValidator, rangeValidator } from '../../validator';

export const getConeSearchState = createFeatureSelector<coneSearch.State>('coneSearch');

export const getResolverWip = createSelector(
    getConeSearchState,
    coneSearch.getResolverWip
);

export const getResolver = createSelector(
    getConeSearchState,
    coneSearch.getResolver
);

export const getConeSearch = createSelector(
    getConeSearchState,
    coneSearch.getConeSearch
);

export const getIsValidConeSearch = createSelector(
    getConeSearch,
    (coneSearch: ConeSearch) => {
        let raForm = new FormControl('', [Validators.required, nanValidator, rangeValidator(0, 360)]);
        raForm.setValue(coneSearch.ra);
        let decForm = new FormControl('', [Validators.required, nanValidator, rangeValidator(-90, 90)]);
        decForm.setValue(coneSearch.dec);
        let radiusForm = new FormControl('', [Validators.required, nanValidator, rangeValidator(0, 150)]);
        radiusForm.setValue(coneSearch.radius);
        if (raForm.errors || decForm.errors || radiusForm.errors) {
            return false;
        }
        return true;
    }
);
