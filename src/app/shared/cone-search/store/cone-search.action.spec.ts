import * as searchActions from '../store/cone-search.action';
import { ConeSearch, Resolver } from './model';

describe('[Shared][ConeSearch] Action', () => {
    it('should create RetrieveCoordinatesAction', () => {
        const action = new searchActions.RetrieveCoordinatesAction('toto');
        expect(action.type).toEqual(searchActions.RETRIEVE_COORDINATES);
        expect(action.payload).toEqual('toto');
    });

    it('should create RetrieveCoordinatesSuccessAction', () => {
        const resolver: Resolver = { name: 'toto', ra: 1, dec: 2 };
        const action = new searchActions.RetrieveCoordinatesSuccessAction(resolver);
        expect(action.type).toEqual(searchActions.RETRIEVE_COORDINATES_SUCCESS);
        expect(action.payload).toEqual(resolver);
    });

    it('should create RetrieveCoordinatesFailAction', () => {
        const action = new searchActions.RetrieveCoordinatesFailAction(null);
        expect(action.type).toEqual(searchActions.RETRIEVE_COORDINATES_FAIL);
        expect(action.payload).toBeNull();
    });

    it('should create DeleteResolverAction', () => {
        const action = new searchActions.DeleteResolverAction();
        expect(action.type).toEqual(searchActions.DELETE_RESOLVER);
    });

    it('should create AddConeSearchAction', () => {
        const coneSearch: ConeSearch = { ra: 1, dec: 2, radius: 3 };
        const action = new searchActions.AddConeSearchAction(coneSearch);
        expect(action.type).toEqual(searchActions.ADD_CONE_SEARCH);
        expect(action.payload).toEqual(coneSearch);
    });

    it('should create AddConeSearchFromUrlAction', () => {
        const coneSearch: ConeSearch = { ra: 1, dec: 2, radius: 3 };
        const action = new searchActions.AddConeSearchFromUrlAction(coneSearch);
        expect(action.type).toEqual(searchActions.ADD_CONE_SEARCH_FROM_URL);
        expect(action.payload).toEqual(coneSearch);
    });

    it('should create DeleteConeSearchAction', () => {
        const action = new searchActions.DeleteConeSearchAction();
        expect(action.type).toEqual(searchActions.DELETE_CONE_SEARCH);
    });
});
