import * as fromConeSearch from './cone-search.reducer';
import * as coneSearchActions from './cone-search.action';
import { ConeSearch, Resolver } from './model';

describe('[Shared][ConeSearch] Reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromConeSearch;
        const action = {} as coneSearchActions.Actions;
        const state = fromConeSearch.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should set resolverWip to true', () => {
        const { initialState } = fromConeSearch;
        const action = new coneSearchActions.RetrieveCoordinatesAction('toto');
        const state = fromConeSearch.reducer(initialState, action);

        expect(state.resolverWip).toBeTruthy();
        expect(state.resolver).toBeNull();
        expect(state.coneSearch.ra).toBeNull();
        expect(state.coneSearch.dec).toBeNull();
        expect(state.coneSearch.radius).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set resolver ans resolverWip to false', () => {
        const resolver: Resolver = { name: 'toto', ra: 1, dec: 2 };
        const initialState = {
            ...fromConeSearch.initialState,
            resolverWip: true
        };
        const action = new coneSearchActions.RetrieveCoordinatesSuccessAction(resolver);
        const state = fromConeSearch.reducer(initialState, action);

        expect(state.resolverWip).toBeFalsy();
        expect(state.resolver).toEqual(resolver);
        expect(state.coneSearch.ra).toBeNull();
        expect(state.coneSearch.dec).toBeNull();
        expect(state.coneSearch.radius).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set resolverWip to false', () => {
        const initialState = {
            ...fromConeSearch.initialState,
            resolverWip: true
        };
        const action = new coneSearchActions.RetrieveCoordinatesFailAction(null);
        const state = fromConeSearch.reducer(initialState, action);

        expect(state.resolverWip).toBeFalsy();
        expect(state.resolver).toBeNull();
        expect(state.coneSearch.ra).toBeNull();
        expect(state.coneSearch.dec).toBeNull();
        expect(state.coneSearch.radius).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should unset resolver', () => {
        const resolver: Resolver = { name: 'toto', ra: 1, dec: 2 };
        const initialState = {
            ...fromConeSearch.initialState,
            resolver
        };
        const action = new coneSearchActions.DeleteResolverAction();
        const state = fromConeSearch.reducer(initialState, action);

        expect(state.resolverWip).toBeFalsy();
        expect(state.resolver).toBeNull();
        expect(state.coneSearch.ra).toBeNull();
        expect(state.coneSearch.dec).toBeNull();
        expect(state.coneSearch.radius).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set coneSearch', () => {
        const coneSearch = { ra: 1, dec: 2, radius: 3 } as ConeSearch;
        const { initialState } = fromConeSearch;
        const action = new  coneSearchActions.AddConeSearchAction(coneSearch);
        const state = fromConeSearch.reducer(initialState, action);

        expect(state.resolverWip).toBeFalsy();
        expect(state.resolver).toBeNull();
        expect(state.coneSearch.ra).toEqual(1);
        expect(state.coneSearch.dec).toEqual(2);
        expect(state.coneSearch.radius).toEqual(3);
        expect(state).not.toEqual(initialState);
    });

    it('should unset coneSearch', () => {
        const coneSearch = { ra: 1, dec: 2, radius: 3 } as ConeSearch;
        const initialState = { ...fromConeSearch.initialState, coneSearch };
        const action = new  coneSearchActions.DeleteConeSearchAction();
        const state = fromConeSearch.reducer(initialState, action);

        expect(state.resolverWip).toBeFalsy();
        expect(state.resolver).toBeNull();
        expect(state.coneSearch.ra).toBeNull();
        expect(state.coneSearch.dec).toBeNull();
        expect(state.coneSearch.radius).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should get resolverWip', () => {
        const action = {} as  coneSearchActions.Actions;
        const state =  fromConeSearch.reducer(undefined, action);

        expect(fromConeSearch.getResolverWip(state)).toBeFalsy();
    });

    it('should get resolver', () => {
        const action = {} as coneSearchActions.Actions;
        const state = fromConeSearch.reducer(undefined, action);

        expect(fromConeSearch.getResolver(state)).toBeNull();
    });

    it('should get coneSearch', () => {
        const action = {} as coneSearchActions.Actions;
        const state = fromConeSearch.reducer(undefined, action);
        const expectedConeSearch: ConeSearch = { ra: null, dec: null, radius: null };

        expect(fromConeSearch.getConeSearch(state)).toEqual(expectedConeSearch);
    });
});
