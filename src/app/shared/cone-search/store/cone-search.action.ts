/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import { ConeSearch, Resolver } from './model';

export const RETRIEVE_COORDINATES = '[ConeSearch] Retrieve Coordinates';
export const RETRIEVE_COORDINATES_SUCCESS = '[ConeSearch] Retrieve Coordinates Success';
export const RETRIEVE_COORDINATES_FAIL = '[ConeSearch] Retrieve Coordinates Fail';
export const DELETE_RESOLVER = '[ConeSearch] Delete Resolver';
export const ADD_CONE_SEARCH = '[ConeSearch] Add Cone Search';
export const ADD_CONE_SEARCH_FROM_URL = '[ConeSearch] Add Cone Search From Url';
export const DELETE_CONE_SEARCH = '[ConeSearch] Delete Cone Search';


/**
 * @class
 * @classdesc RetrieveCoordinatesAction action.
 * @readonly
 */
export class RetrieveCoordinatesAction implements Action {
    readonly type = RETRIEVE_COORDINATES;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc RetrieveCoordinatesSuccessAction action.
 * @readonly
 */
export class RetrieveCoordinatesSuccessAction implements Action {
    readonly type = RETRIEVE_COORDINATES_SUCCESS;

    constructor(public payload: Resolver) { }
}

/**
 * @class
 * @classdesc RetrieveCoordinatesFailAction action.
 * @readonly
 */
export class RetrieveCoordinatesFailAction implements Action {
    readonly type = RETRIEVE_COORDINATES_FAIL;

    constructor(public payload: string | null) { }
}

/**
 * @class
 * @classdesc DeleteResolverAction action.
 * @readonly
 */
export class DeleteResolverAction implements Action {
    readonly type = DELETE_RESOLVER;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc AddConeSearchAction action.
 * @readonly
 */
export class AddConeSearchAction implements Action {
    readonly type = ADD_CONE_SEARCH;

    constructor(public payload: ConeSearch) { }
}

/**
 * @class
 * @classdesc AddConeSearchFromUrlAction action.
 * @readonly
 */
export class AddConeSearchFromUrlAction implements Action {
    readonly type = ADD_CONE_SEARCH_FROM_URL;

    constructor(public payload: ConeSearch) { }
}

/**
 * @class
 * @classdesc DeleteConeSearchAction action.
 * @readonly
 */
export class DeleteConeSearchAction implements Action {
    readonly type = DELETE_CONE_SEARCH;

    constructor(public payload: {} = null) { }
}

export type Actions
    = RetrieveCoordinatesAction
    | RetrieveCoordinatesSuccessAction
    | RetrieveCoordinatesFailAction
    | DeleteResolverAction
    | AddConeSearchAction
    | AddConeSearchFromUrlAction
    | DeleteConeSearchAction;
