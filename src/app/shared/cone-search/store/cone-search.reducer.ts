/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from './cone-search.action';
import { Resolver, ConeSearch } from './model';

/**
 * Interface for cone search state.
 *
 * @interface State
 */
export interface State {
    resolverWip: boolean;
    resolver: Resolver;
    coneSearch: ConeSearch;
}

export const initialState: State = {
    resolverWip: false,
    resolver: null,
    coneSearch: { ra: null, dec: null, radius: null }
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.RETRIEVE_COORDINATES:
            return {
                ...state,
                resolverWip: true
            };

        case actions.RETRIEVE_COORDINATES_SUCCESS:
            return {
                ...state,
                resolverWip: false,
                resolver: action.payload
            };

        case actions.RETRIEVE_COORDINATES_FAIL:
            return {
                ...state,
                resolverWip: false
            };

        case actions.DELETE_RESOLVER:
            return {
                ...state,
                resolver: null
            };

        case actions.ADD_CONE_SEARCH:
            return {
                ...state,
                coneSearch: action.payload
            };

        case actions.DELETE_CONE_SEARCH:
            return {
                ...state,
                coneSearch: { ra: null, dec: null, radius: null }
            };

        default:
            return state;
    }
}

export const getResolverWip = (state: State) => state.resolverWip;
export const getResolver = (state: State) => state.resolver;
export const getConeSearch = (state: State) => state.coneSearch;
