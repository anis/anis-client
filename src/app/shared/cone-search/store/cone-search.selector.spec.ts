import * as coneSearchSelector from './cone-search.selector';
import * as fromConeSearch from './cone-search.reducer';
import { ConeSearch } from './model';

describe('[Shared][ConeSearch] Selector', () => {
    it('should get resolverWip', () => {
        const state = { coneSearch: { ...fromConeSearch.initialState }};
        expect(coneSearchSelector.getResolverWip(state)).toBeFalsy();
    });

    it('should get resolver', () => {
        const state = { coneSearch: { ...fromConeSearch.initialState }};
        expect(coneSearchSelector.getResolver(state)).toBeNull();
    });

    it('should get coneSearch', () => {
        const state = { coneSearch: { ...fromConeSearch.initialState }};
        const expectedConeSearch: ConeSearch = { ra: null, dec: null, radius: null };
        expect(coneSearchSelector.getConeSearch(state)).toEqual(expectedConeSearch);
    });

    it('should get isValidConeSearch', () => {
        let state = { coneSearch: {
            ...fromConeSearch.initialState,
            coneSearch: { ra: 1, dec: 2, radius: 3 } as ConeSearch
        }};
        expect(coneSearchSelector.getIsValidConeSearch(state)).toBeTruthy();
        state = { coneSearch: {
            ...fromConeSearch.initialState,
            coneSearch: { ra: null, dec: 2, radius: 3 } as ConeSearch
        }};
        expect(coneSearchSelector.getIsValidConeSearch(state)).toBeFalsy();
        state = { coneSearch: {
            ...fromConeSearch.initialState,
            coneSearch: { ra: 1, dec: null, radius: 3 } as ConeSearch
        }};
        expect(coneSearchSelector.getIsValidConeSearch(state)).toBeFalsy();
        state = { coneSearch: {
            ...fromConeSearch.initialState,
            coneSearch: { ra: 1, dec: 2, radius: null } as ConeSearch
        }};
        expect(coneSearchSelector.getIsValidConeSearch(state)).toBeFalsy();
    });
});