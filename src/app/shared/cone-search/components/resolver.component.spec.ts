// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { Component, ViewChild } from '@angular/core';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//
// import { ResolverComponent } from './resolver.component';
// import { Resolver } from "../store/model";
//
// describe('[Shared][ConeSearch] Component: ResolverComponent', () => {
//     @Component({
//         selector: `app-host`,
//         template: `<app-resolver [disabled]="disabled" [resolverWip]="resolverWip" [resolver]="resolver"></app-resolver>`
//     })
//     class TestHostComponent {
//         @ViewChild(ResolverComponent, { static: false })
//         testedComponent: ResolverComponent;
//         disabled: boolean = false;
//         resolverWip: boolean = false;
//         resolver: Resolver = undefined;
//     }
//
//     let testHostComponent: TestHostComponent;
//     let testHostFixture: ComponentFixture<TestHostComponent>;
//     let testedComponent: ResolverComponent;
//
//     beforeEach(async(() => {
//         TestBed.configureTestingModule({
//             declarations: [
//                 ResolverComponent,
//                 TestHostComponent
//             ],
//             imports: [FormsModule, ReactiveFormsModule]
//         });
//         testHostFixture = TestBed.createComponent(TestHostComponent);
//         testHostComponent = testHostFixture.componentInstance;
//         testHostFixture.detectChanges();
//         testedComponent = testHostComponent.testedComponent;
//     }));
//
//     it('should create the component', () => {
//         expect(testedComponent).toBeTruthy();
//     });
//
//     it('should display search button when resolverWip is false', () => {
//         const template = testHostFixture.nativeElement;
//         expect(template.querySelector('#btn-search')).toBeTruthy();
//         expect(template.querySelector('#btn-wip')).toBeFalsy();
//     });
//
//     it('should display WIP button when resolverWip is true', () => {
//         testHostComponent.resolverWip = true;
//         testHostFixture.detectChanges();
//         const template = testHostFixture.nativeElement;
//         expect(template.querySelector('#btn-search')).toBeFalsy();
//         expect(template.querySelector('#btn-wip')).toBeTruthy();
//     });
//
//     it('should disable resolver or not depending on disabled input', () => {
//         expect(testedComponent.field.enabled).toBeTruthy();
//         testHostComponent.disabled = true;
//         testHostFixture.detectChanges();
//         expect(testedComponent.field.disabled).toBeTruthy();
//     });
//
//     it('should init resolver field depending on resolver input', () => {
//         expect(testedComponent.field.value).toBeNull();
//         testHostComponent.resolver = { name: 'toto', ra: 1, dec: 2 };
//         testHostFixture.detectChanges();
//         expect(testedComponent.field.value).toEqual('toto');
//         testHostComponent.resolver = null;
//         testHostFixture.detectChanges();
//         expect(testedComponent.field.value).toBeNull();
//     });
// });
