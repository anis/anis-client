import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DecComponent } from './dec.component';
import { ConeSearch, Resolver } from '../store/model';

describe('[Shared][ConeSearch] Component: DecComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-dec [disabled]='disabled' [coneSearch]='coneSearch' [resolver]='resolver' [unit]='unit'></app-dec>`
    })
    class TestHostComponent {
        @ViewChild(DecComponent, { static: false })
        testedComponent: DecComponent;
        disabled: boolean = false;
        coneSearch: ConeSearch = { ra: null, dec: null, radius: null };
        resolver: Resolver = undefined;
        unit = 'degree';
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: DecComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DecComponent,
                TestHostComponent
            ],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#initFields() should init state of decDegree, decH, decM and decS fields', () => {
        testedComponent.isDisabled = true;
        testedComponent.initFields();
        expect(testedComponent.decDegree.disabled).toBeTruthy();
        expect(testedComponent.decH.disabled).toBeTruthy();
        expect(testedComponent.decM.disabled).toBeTruthy();
        expect(testedComponent.decS.disabled).toBeTruthy();
        testedComponent.isDisabled = false;
        testedComponent.isDegree = true;
        testedComponent.initFields();
        expect(testedComponent.decDegree.enabled).toBeTruthy();
        expect(testedComponent.decH.disabled).toBeTruthy();
        expect(testedComponent.decM.disabled).toBeTruthy();
        expect(testedComponent.decS.disabled).toBeTruthy();
        testedComponent.isDisabled = false;
        testedComponent.isDegree = false;
        testedComponent.initFields();
        expect(testedComponent.decDegree.disabled).toBeTruthy();
        expect(testedComponent.decH.enabled).toBeTruthy();
        expect(testedComponent.decM.enabled).toBeTruthy();
        expect(testedComponent.decS.enabled).toBeTruthy();
    });

    it('#decDegree2HMS(value) convert DEC from degree to HH:MM:SS', () => {
        testedComponent.decDegree2HMS(32.87);
        expect(testedComponent.decH.value).toBe(32);
        expect(testedComponent.decM.value).toBe(52);
        expect(parseFloat(testedComponent.decS.value)).toBe(12);
    });

    it('#decHMS2Degree() convert DEC from HH:MM:SS to degree', () => {
        testedComponent.decH.setValue(32);
        testedComponent.decM.setValue(52);
        testedComponent.decS.setValue(12);
        testedComponent.decHMS2Degree();
        expect(testedComponent.decDegree.value).toBe(32.87);
        testedComponent.decH.setValue(-32);
        testedComponent.decHMS2Degree();
        expect(testedComponent.decDegree.value).toBe(-32.87);
    });

    it('#changeFocus() should save which element is focused', () => {
        testedComponent.changeFocus('dech', true);
        expect(testedComponent.decHFocused).toBeTruthy();
        testedComponent.changeFocus('decm', true);
        expect(testedComponent.decMFocused).toBeTruthy();
        testedComponent.changeFocus('decs', true);
        expect(testedComponent.decSFocused).toBeTruthy();
    });

    it('#decChange() should convert to hms and emit cone search if valid', () => {
        testedComponent.updateConeSearch.subscribe((event: ConeSearch) => expect(event).toEqual({ ra: null, dec: 32.87, radius: null }));
        testedComponent.decDegree.setValue(32.87);
        testedComponent.decChange();
        expect(testedComponent.decH.value).toBe(32);
        expect(testedComponent.decM.value).toBe(52);
        expect(parseFloat(testedComponent.decS.value)).toBe(12);
    });

    it('#decChange() should reset DEC hms and emit cone search if not valid', () => {
        testedComponent.updateConeSearch.subscribe((event: any) => expect(event).toEqual({ ra: null, dec: 'toto', radius: null }));
        testedComponent.decDegree.setValue('toto');
        testedComponent.decChange();
        expect(testedComponent.decH.value).toBeNull();
        expect(testedComponent.decM.value).toBeNull();
        expect(testedComponent.decS.value).toBeNull();
    });

    it('#decChange() should convert to degree and emit cone search if valid', () => {
        testedComponent.isDegree = false;
        testedComponent.initFields();
        testedComponent.updateConeSearch.subscribe((event: ConeSearch) => expect(event).toEqual({ ra: null, dec: 32.87, radius: null }));
        testedComponent.decH.setValue(32);
        testedComponent.decM.setValue(52);
        testedComponent.decS.setValue(12);
        testedComponent.decChange();
        expect(testedComponent.decDegree.value).toBe(32.87);
    });

    it('#decChange() should reset DEC degree if not valid', () => {
        testedComponent.isDegree = false;
        testedComponent.decH.setValue(32);
        testedComponent.decM.setValue(52);
        testedComponent.decS.setValue('toto');
        testedComponent.decChange();
        expect(testedComponent.decDegree.value).toBeNull();
    });

    it('#setToDefaultValue() should set to 0 DEC hms empty fields', () => {
        testedComponent.isDegree = false;
        testedComponent.initFields();
        testedComponent.decChange();
        expect(testedComponent.decH.value).toEqual(0);
        expect(testedComponent.decM.value).toEqual(0);
        expect(testedComponent.decS.value).toEqual(0);
    });

    it('#resetResolver() should emit deleteResolver if resolver value not equal to dec value', () => {
        testHostComponent.resolver = { name: 'toto', ra: 1, dec: 32.87 };
        testHostFixture.detectChanges();
        testedComponent.decDegree.setValue(2);
        testedComponent.deleteResolver.subscribe((event: null) => expect(event).toBeUndefined());
        testedComponent.resetResolver();
    });

    it('should set RA, radius and DEC fields value from coneSearch', () => {
        expect(testedComponent.ra).toBeNull();
        expect(testedComponent.radius).toBeNull();
        expect(testedComponent.decDegree.value).toBeNull();
        expect(testedComponent.decH.value).toBeNull();
        expect(testedComponent.decM.value).toBeNull();
        expect(testedComponent.decS.value).toBeNull();
        testHostComponent.coneSearch = { ra: null, dec: 32.87, radius: null };
        testHostFixture.detectChanges();
        expect(testedComponent.ra).toBeNull();
        expect(testedComponent.radius).toBeNull();
        expect(testedComponent.decDegree.value).toEqual(32.87);
        expect(testedComponent.decH.value).toBe(32);
        expect(testedComponent.decM.value).toBe(52);
        expect(parseFloat(testedComponent.decS.value)).toBe(12);
    });

    it('should set DEC from resolver', () => {
        testHostComponent.resolver = { name: 'toto', ra: 1, dec: 32.87 };
        testHostFixture.detectChanges();
        expect(testedComponent.decDegree.value).toEqual(32.87);
        expect(testedComponent.decH.value).toBe(32);
        expect(testedComponent.decM.value).toBe(52);
        expect(parseFloat(testedComponent.decS.value)).toBe(12);
    });

    it('should set isDegree', () => {
        expect(testedComponent.isDegree).toBeTruthy();
        testHostComponent.unit = 'hms';
        testHostFixture.detectChanges();
        expect(testedComponent.isDegree).toBeFalsy();
    });
});
