import { Component, OnChanges, ViewEncapsulation, Input, SimpleChanges } from '@angular/core';

import * as d3 from 'd3';

import { Dataset } from 'src/app/metamodel/model';

import { environment } from '../../../../environments/environment';

/**
 * @class
 * @classdesc Cone Search Plot component.
 *
 * @implements OnChanges
 */
@Component({
    selector: 'app-cone-search-plot',
    templateUrl: './cone-search-plot.component.html',
    styleUrls: ['./cone-search-plot.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class ConeSearchPlotComponent implements OnChanges {
    @Input() dataset: Dataset;
    @Input() data: {x: number, y: number}[];
    @Input() backgroundList: {id: number, name: string, display: number, stretch: string, pmin: number, pmax: number}[];
    @Input() selectedBackground: number;
    @Input() ra: number;
    @Input() dec: number;
    @Input() rad: number;
    @Input() width: number;
    @Input() height: number;
    @Input() fitscut: string;

    // Interactive variables intialisation
    margin = { top: 50, right: 50, bottom: 50 , left: 50 };
    degTorad = 0.0174532925;
    x: d3.ScaleLinear<number, number>;
    y: d3.ScaleLinear<number, number>;

    ngOnChanges(changes: SimpleChanges) {
        if (changes.data) {
            if (changes.data.previousValue != changes.data.currentValue) {
                this.conesearch_plot();
            }
        }
    }

    getHrefBackgroundImage() {
        const background = this.backgroundList.find(b => b.id === this.selectedBackground);
        if (background.name === 'SDSS DR16') {
            const scale = this.width / (this.rad * 2);
            return `http://skyserver.sdss.org/dr16/SkyServerWS/ImgCutout/getjpeg?TaskName=Skyserver.Chart.Image
                &ra=${this.ra}
                &dec=${this.dec}
                &scale=${scale}
                &width=${this.width}
                &height=${this.height}`;
        } else {
            return `${environment.servicesUrl}/fits-cut-to-png/${this.dataset.name}?filename=${background.name}`
            + `&ra=${this.ra}&dec=${this.dec}&radius=${this.rad}&stretch=${background.stretch}&pmin=${background.pmin}&pmax=${background.pmax}&axes=false`;
        }
    }

    /**
     * Creates the graph.
     */
    conesearch_plot(): void {
        if (!this.data || this.data.length === 0) {
            return;
        }

        d3.select('#plot').select('svg').remove();

        // Init SVG
        const svg = d3.select('#plot').append('svg')
            .attr('id', 'plot')
            .attr('width', this.width + this.margin.left + this.margin.right)
            .attr('height', this.height + this.margin.top + this.margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

        // Set Domain
        this.x = d3.scaleLinear().range([this.width, 0]);
        this.y = d3.scaleLinear().range([this.height, 0]);
        this.x.domain([this.ra - (this.rad / 3600) / Math.cos(this.dec * this.degTorad), this.ra + (this.rad / 3600) / Math.cos(this.dec * this.degTorad)]);
        this.y.domain([this.dec - (this.rad / 3600), this.dec + (this.rad / 3600)]);

        // Add fitscut png as backgroung
        if (this.backgroundList.length > 0) {
            svg.append('image')
                .attr('xlink:href', this.getHrefBackgroundImage())
                .attr('width', this.width)
                .attr('height', this.height);
        }

        // Add X axe
        svg.append("g")
            .attr("transform", "translate(0," + this.height + ")")
            .call(d3.axisBottom(this.x));

        // Add Y axe
        svg.append("g")
            .call(d3.axisLeft(this.y));

        // Add Dataset
        svg.selectAll("circle")
            .data(this.data)
            .enter()
            .append("rect")
            .attr('class','star')
            .attr("x", d => this.x(d.x) - 3)
            .attr("y", d => this.y(d.y) - 3)
            .attr('width', 6)
            .attr('height', 6)
            .on('mouseover', function (d) {
                const tooltip = d3.select('body')
                    .append('div')
                    .attr('class', 'tooltip')
                    .style('opacity', 1);
                tooltip.html('Ra: ' + d.x + '<br> Dec: ' + d.y)
                    .style('left', (d3.event.pageX + 10) + 'px')
                    .style('top', (d3.event.pageY - 5) + 'px');
            })
            .on('mouseout', () => { d3.select('.tooltip').remove(); });
    }
}
