/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Resolver } from '../store/model';

@Component({
    selector: 'app-resolver',
    templateUrl: 'resolver.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Resolver component.
 */
export class ResolverComponent {
    @Input()
    set disabled(disabled: boolean) {
        if (disabled) {
            this.field.disable();
        } else {
            this.field.enable();
        }
    }
    @Input() resolverWip: boolean;
    @Input()
    set resolver(resolver: Resolver) {
        if (resolver) {
            this.field.setValue(resolver.name);
        } else {
            this.field.reset();
        }
    }
    @Output() resolveName: EventEmitter<string> = new EventEmitter();

    field = new FormControl('');
}
