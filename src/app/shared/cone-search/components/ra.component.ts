/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { nanValidator, rangeValidator } from '../../validator';
import { ConeSearch, Resolver } from '../store/model';

@Component({
    selector: 'app-ra',
    templateUrl: 'ra.component.html',
    styleUrls: ['input-group.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc RA component.
 */
export class RaComponent {
    /**
     * Disables RA fields.
     *
     * @param  {boolean} disabled - If the field has to be disabled.
     */
    @Input()
    set disabled(disabled: boolean) {
        this.isDisabled = disabled;
        this.initFields();
    }
    /**
     * Sets RA, DEC and radius from cone search.
     *
     * @param  {ConeSearch} coneSearch - The cone search.
     */
    @Input()
    set coneSearch(coneSearch: ConeSearch) {
        this.dec = coneSearch.dec;
        this.radius = coneSearch.radius;
        if (coneSearch.ra) {
            this.raDegree.setValue(coneSearch.ra);
            if (this.raDegree.valid && !this.raHFocused && !this.raMFocused && !this.raSFocused) {
                this.raDegree2HMS(coneSearch.ra);
            }
        } else {
            this.raDegree.reset();
            this.raH.reset();
            this.raM.reset();
            this.raS.reset();
        }
        this.initFields();
    }
    /**
     * Sets RA from resolver.
     *
     * @param  {Resolver} resolver - The resolver.
     */
    @Input()
    set resolver(resolver: Resolver) {
        this.resolvedRa = null;
        if (resolver) {
            this.resolvedRa = resolver.ra;
            this.raDegree.setValue(resolver.ra);
            this.raDegree2HMS(resolver.ra);
        }
    }
    /**
     * Sets isDegree.
     *
     * @param  {string} unit - The unit.
     */
    @Input()
    set unit(unit: string) {
        unit === 'degree' ? this.isDegree = true : this.isDegree = false;
        this.initFields();
    }
    @Output() updateConeSearch: EventEmitter<ConeSearch> = new EventEmitter();
    @Output() deleteResolver: EventEmitter<null> = new EventEmitter();

    dec: number = null;
    radius: number = null;
    isDisabled = false;
    isDegree = true;
    resolvedRa: number;
    raHFocused: boolean = false;
    raMFocused: boolean = false;
    raSFocused: boolean = false;

    raDegree = new FormControl('', [Validators.required, nanValidator, rangeValidator(0, 360, 'RA')]);
    raH = new FormControl('', [nanValidator, rangeValidator(0, 24, 'Hours')]);
    raM = new FormControl('', [nanValidator, rangeValidator(0, 60, 'Minutes')]);
    raS = new FormControl('', [nanValidator, rangeValidator(0, 60, 'Seconds')]);

    /**
     * Sets RA fields.
     */
    initFields(): void {
        if (this.isDisabled) {
            this.raDegree.disable();
            this.raH.disable();
            this.raM.disable();
            this.raS.disable();
        } else if (this.isDegree) {
            this.raDegree.enable();
            this.raH.disable();
            this.raM.disable();
            this.raS.disable();
        } else {
            this.raDegree.disable();
            this.raH.enable();
            this.raM.enable();
            this.raS.enable();
        }
    }

    /**
     * Converts RA hour minute second from degree and sets RA HMS fields.
     *
     * @param  {number} value - The degree value.
     */
    raDegree2HMS(value: number): void {
        let tmp = value / 15;
        const hh = Math.trunc(tmp);
        tmp = (tmp - hh) * 60;
        const mm = Math.trunc(tmp);
        tmp = (tmp - mm) * 60;
        const ss = +tmp.toFixed(2);
        this.raH.setValue(hh);
        this.raM.setValue(mm);
        this.raS.setValue(ss);
    }

    /**
     * Sets RA degree from hour minute second and sets RA degree field.
     */
    raHMS2Degree(): void {
        const hh = +this.raH.value;
        const mm = +this.raM.value;
        const ss = +this.raS.value;
        const deg = +(((((ss / 60) + mm) / 60) + hh) * 15).toFixed(8);
        this.raDegree.setValue(deg);
    }

    /**
     * Changes fields focus.
     *
     * @param  {string} field - The field.
     * @param  {boolean} isFocused - Is the field is focused.
     */
    changeFocus(field: string, isFocused: boolean): void {
        switch (field) {
            case 'rah':
                this.raHFocused = isFocused;
                break;
            case 'ram':
                this.raMFocused = isFocused;
                break
            case 'ras':
                this.raSFocused = isFocused;
                break;
        }
    }

    /**
     * Manages RA value change.
     */
    raChange(): void {
        if (this.isDegree) {
            if (this.raDegree.valid) {
                this.raDegree2HMS(this.raDegree.value);
            } else {
                this.raH.reset();
                this.raM.reset();
                this.raS.reset();
            }
            this.updateConeSearch.emit({ ra: this.raDegree.value, dec: this.dec, radius: this.radius } as ConeSearch);
        } else {
            if (this.raH.valid && this.raM.valid && this.raS.valid) {
                this.setToDefaultValue();
                this.raHMS2Degree();
                this.updateConeSearch.emit({ ra: this.raDegree.value, dec: this.dec, radius: this.radius } as ConeSearch);
            } else {
                this.raDegree.reset();
            }
        }
        this.resetResolver();
    }

    /**
     * Sets RA hour minute second fields to default value if not valid.
     */
    setToDefaultValue(): void {
        if (this.raH.value === '' || this.raH.value === null) {
            this.raH.setValue(0);
        }
        if (this.raM.value === '' || this.raM.value === null) {
            this.raM.setValue(0);
        }
        if (this.raS.value === '' || this.raS.value === null) {
            this.raS.setValue(0);
        }
    }

    /**
     * Emits reset resolver event.
     */
    resetResolver(): void {
        if (this.resolvedRa && this.resolvedRa !== this.raDegree.value) {
            this.deleteResolver.emit();
        }
    }
}
