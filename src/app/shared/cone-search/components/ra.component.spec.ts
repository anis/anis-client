import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RaComponent } from './ra.component';
import { ConeSearch, Resolver } from '../store/model';

describe('[Shared][ConeSearch] Component: RaComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-ra [disabled]='disabled' [coneSearch]='coneSearch' [resolver]='resolver' [unit]='unit'></app-ra>`
    })
    class TestHostComponent {
        @ViewChild(RaComponent, { static: false })
        testedComponent: RaComponent;
        disabled: boolean = false;
        coneSearch: ConeSearch = { ra: null, dec: null, radius: null };
        resolver: Resolver = undefined;
        unit = 'degree';
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: RaComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                RaComponent,
                TestHostComponent
            ],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#initFields() should init state of raDegree, raH, raM and raS fields', () => {
        testedComponent.isDisabled = true;
        testedComponent.initFields();
        expect(testedComponent.raDegree.disabled).toBeTruthy();
        expect(testedComponent.raH.disabled).toBeTruthy();
        expect(testedComponent.raM.disabled).toBeTruthy();
        expect(testedComponent.raS.disabled).toBeTruthy();
        testedComponent.isDisabled = false;
        testedComponent.isDegree = true;
        testedComponent.initFields();
        expect(testedComponent.raDegree.enabled).toBeTruthy();
        expect(testedComponent.raH.disabled).toBeTruthy();
        expect(testedComponent.raM.disabled).toBeTruthy();
        expect(testedComponent.raS.disabled).toBeTruthy();
        testedComponent.isDisabled = false;
        testedComponent.isDegree = false;
        testedComponent.initFields();
        expect(testedComponent.raDegree.disabled).toBeTruthy();
        expect(testedComponent.raH.enabled).toBeTruthy();
        expect(testedComponent.raM.enabled).toBeTruthy();
        expect(testedComponent.raS.enabled).toBeTruthy();
    });

    it('#raDegree2HMS(value) convert RA from degree to HH:MM:SS', () => {
        testedComponent.raDegree2HMS(78.2);
        expect(testedComponent.raH.value).toBe(5);
        expect(testedComponent.raM.value).toBe(12);
        expect(parseFloat(testedComponent.raS.value)).toBe(48);
    });

    it('#raHMS2Degree() convert RA from HH:MM:SS to degree', () => {
        testedComponent.raH.setValue(5);
        testedComponent.raM.setValue(12);
        testedComponent.raS.setValue(48);
        testedComponent.raHMS2Degree();
        expect(testedComponent.raDegree.value).toBe(78.2);
    });

    it('#changeFocus() should save which element is focused', () => {
        testedComponent.changeFocus('rah', true);
        expect(testedComponent.raHFocused).toBeTruthy();
        testedComponent.changeFocus('ram', true);
        expect(testedComponent.raMFocused).toBeTruthy();
        testedComponent.changeFocus('ras', true);
        expect(testedComponent.raSFocused).toBeTruthy();
    });

    it('#raChange() should convert to hms and emit cone search if valid', () => {
        testedComponent.updateConeSearch.subscribe((event: ConeSearch) => expect(event).toEqual({ ra: 78.2, dec: null, radius: null }));
        testedComponent.raDegree.setValue(78.2);
        testedComponent.raChange();
        expect(testedComponent.raH.value).toBe(5);
        expect(testedComponent.raM.value).toBe(12);
        expect(parseFloat(testedComponent.raS.value)).toBe(48);
    });

    it('#raChange() should reset RA hms and emit cone search if not valid', () => {
        testedComponent.updateConeSearch.subscribe((event: any) => expect(event).toEqual({ ra: 'toto', dec: null, radius: null }));
        testedComponent.raDegree.setValue('toto');
        testedComponent.raChange();
        expect(testedComponent.raH.value).toBeNull();
        expect(testedComponent.raM.value).toBeNull();
        expect(testedComponent.raS.value).toBeNull();
    });

    it('#raChange() should convert to degree and emit cone search if valid', () => {
        testedComponent.isDegree = false;
        testedComponent.initFields();
        testedComponent.updateConeSearch.subscribe((event: ConeSearch) => expect(event).toEqual({ ra: 78.2, dec: null, radius: null }));
        testedComponent.raH.setValue(5);
        testedComponent.raM.setValue(12);
        testedComponent.raS.setValue(48);
        testedComponent.raChange();
        expect(testedComponent.raDegree.value).toBe(78.2);
    });

    it('#raChange() should reset RA degree if not valid', () => {
        testedComponent.isDegree = false;
        testedComponent.raH.setValue(5);
        testedComponent.raM.setValue(12);
        testedComponent.raS.setValue('toto');
        testedComponent.raChange();
        expect(testedComponent.raDegree.value).toBeNull();
    });

    it('#setToDefaultValue() should set to 0 RA hms empty fields', () => {
        testedComponent.isDegree = false;
        testedComponent.initFields();
        testedComponent.raChange();
        expect(testedComponent.raH.value).toEqual(0);
        expect(testedComponent.raM.value).toEqual(0);
        expect(testedComponent.raS.value).toEqual(0);
    });

    it('#resetResolver() should emit deleteResolver if resolver value not equal to ra value', () => {
        testHostComponent.resolver = { name: 'toto', ra: 78.2, dec: 2 };
        testHostFixture.detectChanges();
        testedComponent.raDegree.setValue(2);
        testedComponent.deleteResolver.subscribe((event: null) => expect(event).toBeUndefined());
        testedComponent.resetResolver();
    });

    it('should set DEC, radius and RA fields value from coneSearch', () => {
        expect(testedComponent.dec).toBeNull();
        expect(testedComponent.radius).toBeNull();
        expect(testedComponent.raDegree.value).toBeNull();
        expect(testedComponent.raH.value).toBeNull();
        expect(testedComponent.raM.value).toBeNull();
        expect(testedComponent.raS.value).toBeNull();
        testHostComponent.coneSearch = { ra: 78.2, dec: null, radius: null };
        testHostFixture.detectChanges();
        expect(testedComponent.dec).toBeNull();
        expect(testedComponent.radius).toBeNull();
        expect(testedComponent.raDegree.value).toEqual(78.2);
        expect(testedComponent.raH.value).toBe(5);
        expect(testedComponent.raM.value).toBe(12);
        expect(parseFloat(testedComponent.raS.value)).toBe(48);
    });

    it('should set RA from resolver', () => {
        testHostComponent.resolver = { name: 'toto', ra: 78.2, dec: 2 };
        testHostFixture.detectChanges();
        expect(testedComponent.raDegree.value).toEqual(78.2);
        expect(testedComponent.raH.value).toBe(5);
        expect(testedComponent.raM.value).toBe(12);
        expect(parseFloat(testedComponent.raS.value)).toBe(48);
    });

    it('should set isDegree', () => {
        expect(testedComponent.isDegree).toBeTruthy();
        testHostComponent.unit = 'hms';
        testHostFixture.detectChanges();
        expect(testedComponent.isDegree).toBeFalsy();
    });
});
