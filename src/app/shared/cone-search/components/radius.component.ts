/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { rangeValidator } from '../../validator';
import { ConeSearch } from '../store/model';

@Component({
    selector: 'app-radius',
    templateUrl: 'radius.component.html',
    styleUrls: ['radius.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Radius component.
 */
export class RadiusComponent {
    /**
     * Sets RA, DEC and radius from cone search.
     *
     * @param  {ConeSearch} coneSearch - The cone search.
     */
    @Input()
    set coneSearch(coneSearch: ConeSearch) {
        this.ra = coneSearch.ra;
        this.dec = coneSearch.dec;
        if (coneSearch.radius) {
            this.radiusField.setValue(coneSearch.radius);
            this.radiusRange.setValue(coneSearch.radius);
        } else {
            this.radiusRange.setValue(0);
            this.radiusField.setValue(0);
        }
    }
    /**
     * Disables radius fields.
     *
     * @param  {boolean} disabled - If the field has to be disabled.
     */
    @Input()
    set disabled(disabled: boolean) {
        if (disabled) {
            this.radiusField.disable();
            this.radiusRange.disable();
        } else {
            this.radiusField.enable();
            this.radiusRange.enable();
        }
    }
    @Output() updateConeSearch: EventEmitter<ConeSearch> = new EventEmitter();

    ra: number;
    dec: number;
    radiusRange = new FormControl('');
    radiusField = new FormControl('', [rangeValidator(0, 150, 'Radius')]);

    /**
     * Sets radius value form inputs and emits cone search event.
     *
     * @param  {string} value - The value of radius.
     *
     * @fires EventEmitter<ConeSearch>
     */
    radiusChange(value: string): void {
        this.radiusField.setValue(+value);
        this.radiusRange.setValue(+value);
        this.updateConeSearch.emit({ ra: this.ra, dec: this.dec, radius: +value } as ConeSearch);
    }
}
