import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, ViewChild } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RadiusComponent } from './radius.component';
import { ConeSearch } from '../store/model';

describe('[Shared][ConeSearch] Component: RadiusComponent', () => {
    @Component({
        selector: `app-host`,
        template: `<app-radius [coneSearch]='coneSearch' [disabled]='disabled'></app-radius>`
    })
    class TestHostComponent {
        @ViewChild(RadiusComponent, { static: false })
        testedComponent: RadiusComponent;
        coneSearch: ConeSearch = { ra: null, dec: null, radius: null };
        disabled: boolean = false
    }

    let testHostComponent: TestHostComponent;
    let testHostFixture: ComponentFixture<TestHostComponent>;
    let testedComponent: RadiusComponent;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                RadiusComponent,
                TestHostComponent
            ],
            imports: [FormsModule, ReactiveFormsModule]
        });
        testHostFixture = TestBed.createComponent(TestHostComponent);
        testHostComponent = testHostFixture.componentInstance;
        testHostFixture.detectChanges();
        testedComponent = testHostComponent.testedComponent;
    }));

    it('should create the component', () => {
        expect(testedComponent).toBeTruthy();
    });

    it('#radiusChange(value) should put same value for radius fields and emit cone search', () => {
        testedComponent.updateConeSearch.subscribe((event: ConeSearch) => expect(event).toEqual({ ra: null, dec: null, radius: 17}));
        testedComponent.radiusChange('17');
        expect(testedComponent.radiusRange.value).toBe(17);
        expect(testedComponent.radiusField.value).toBe(17);
    });

    it('should set RA, DEC and radius fields value from coneSearch', () => {
        expect(testedComponent.ra).toBeNull();
        expect(testedComponent.dec).toBeNull();
        expect(testedComponent.radiusRange.value).toEqual(0);
        expect(testedComponent.radiusField.value).toEqual(0);
        testHostComponent.coneSearch = { ra: null, dec: null, radius: 3 };
        testHostFixture.detectChanges();
        expect(testedComponent.ra).toBeNull();
        expect(testedComponent.dec).toBeNull();
        expect(testedComponent.radiusRange.value).toEqual(3);
        expect(testedComponent.radiusField.value).toEqual(3);
    });

    it('should disable radius fields or not depending on disabled input', () => {
        expect(testedComponent.radiusRange.enabled).toBeTruthy();
        expect(testedComponent.radiusField.enabled).toBeTruthy();
        testHostComponent.disabled = true;
        testHostFixture.detectChanges();
        expect(testedComponent.radiusRange.disabled).toBeTruthy();
        expect(testedComponent.radiusField.disabled).toBeTruthy();
    });
});
