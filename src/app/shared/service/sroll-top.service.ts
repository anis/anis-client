/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';

@Injectable()
/**
 * @class
 * @classdesc Manages automatic scroll to top page.
 */
export class ScrollTopService {
    constructor(
        @Inject(PLATFORM_ID) private platformId: object, private router: Router) {
    }

    /**
     * Scrolls to page top.
     */
    setScrollTop(): void {
        if (isPlatformBrowser(this.platformId)) {
            this.router.events.subscribe((event: NavigationEnd) => {
                window.scroll(0, 0);
            });
        }
    }
}
