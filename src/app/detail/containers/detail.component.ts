/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromDetail from '../store/detail.reducer';
import * as detailActions from '../store/detail.action';
import * as detailSelector from '../store/detail.selector';
import * as metamodelSelector from '../../metamodel/selectors';
import { Attribute, Category, Family } from '../../metamodel/model';

/**
 * Interface for store state.
 *
 * @interface StoreState
 */
interface StoreState {
    detail: fromDetail.State;
}

@Component({
    selector: 'app-detail-page',
    templateUrl: 'detail.component.html'
})
/**
 * @class
 * @classdesc Detail container.
 *
 * @implements OnInit
 * @implements OnDestroy
 */
export class DetailComponent implements OnInit, OnDestroy {
    public pristine: Observable<boolean>;
    public datasetName: Observable<string>;
    public attributeList: Observable<Attribute[]>;
    public outputFamilyList: Observable<Family[]>;
    public categoryList: Observable<Category[]>;
    public objectIsLoading: Observable<boolean>;
    public objectIsLoaded: Observable<boolean>;
    public object: Observable<any>;
    public spectraIsLoading: Observable<boolean>;
    public spectraIsLoaded: Observable<boolean>;
    public spectraCSV: Observable<string>;

    constructor(private location: Location, private store: Store<StoreState>) {
        this.pristine = this.store.select(detailSelector.getSearchIsPristine);
        this.datasetName = this.store.select(detailSelector.getDatasetName);
        this.outputFamilyList = this.store.select(metamodelSelector.getOutputFamilyList);
        this.categoryList = this.store.select(metamodelSelector.getCategoryList);
        this.objectIsLoading = this.store.select(detailSelector.getObjectIsLoading);
        this.objectIsLoaded = this.store.select(detailSelector.getObjectIsLoaded);
        this.object = this.store.select(detailSelector.getObject);
        this.spectraIsLoading = this.store.select(detailSelector.getSpectraIsLoading);
        this.spectraIsLoaded = this.store.select(detailSelector.getSpectraIsLoaded);
        this.spectraCSV = this.store.select(detailSelector.getSpectraCSV);
    }

    ngOnInit() {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(new detailActions.InitDetailAction()));
        this.datasetName.subscribe(datasetName => {
            if (datasetName) {
                this.attributeList = this.store.select(metamodelSelector.getAttributeList, { dname: datasetName });
            }
        });
    }

    /**
     * Returns the object type.
     *
     * @param  {Attribute[]} attributeList - The attribute list.
     *
     * @return string
     */
    getObjectType(attributeList: Attribute[]): string {
        const spectrumAttribute: Attribute =  attributeList
            .filter(a => a.detail)
            .find(attribute => attribute.search_flag === 'SPECTRUM_1D');
        if (spectrumAttribute) {
            return 'spectra';
        }
        return 'default';
    }

    /**
     * Gets back to result page.
     */
    goBackToResult(): void {
        this.location.back();
    }

    /**
     * Dispatches action to retrieve spectra file.
     *
     * @param  {string} spectraFile - The spectra file name.
     */
    getSpectraCSV(spectraFile: string): void {
        this.store.dispatch(new detailActions.RetrieveSpectraAction(spectraFile));
    }

    /**
     * Resets detail information.
     */
    ngOnDestroy() {
        this.store.dispatch(new detailActions.DestroyDetailAction());
    }
}
