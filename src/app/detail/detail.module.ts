/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '../shared/shared.module';
import { MetamodelModule } from '../metamodel/metamodel.module';
import { DetailEffects } from './store/detail.effects';
import { DetailService } from './store/detail.service';
import { DetailRoutingModule, routedComponents } from './detail-routing.module';
import { dummiesComponents } from './components';
import { reducer } from './store/detail.reducer';

@NgModule({
    imports: [
        SharedModule,
        MetamodelModule,
        DetailRoutingModule,
        StoreModule.forFeature('detail', reducer),
        EffectsModule.forFeature([DetailEffects])
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ],
    providers: [
        DetailService
    ]
})
/**
 * @class
 * @classdesc Detail module.
 */
export class DetailModule { }
