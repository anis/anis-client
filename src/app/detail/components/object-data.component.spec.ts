import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { ObjectDataComponent } from './object-data.component';
import { ATTRIBUTE_LIST, CATEGORY_LIST } from '../../../settings/test-data';

describe('[Detail] Component: ObjectDataComponent', () => {
    let component: ObjectDataComponent;
    let fixture: ComponentFixture<ObjectDataComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [ObjectDataComponent],
            imports: [AccordionModule.forRoot()]
        });
        fixture = TestBed.createComponent(ObjectDataComponent);
        component = fixture.componentInstance;
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getCategoryByFamilySortedByDisplay() should return sorted categories for the given family', () => {
        component.categoryList = CATEGORY_LIST;
        expect(component.getCategoryByFamilySortedByDisplay(1).length).toEqual(2);
    });

    it('#getAttributesVisibleByCategory() should return visible attributes for the given category', () => {
        component.attributeList = ATTRIBUTE_LIST;
        expect(component.getAttributesVisibleByCategory(1).length).toEqual(1);
        expect(component.getAttributesVisibleByCategory(1)[0].id).toEqual(1);
    });

    it('#getAttributesVisible() should return visible attributes', () => {
        component.attributeList = ATTRIBUTE_LIST;
        expect(component.getAttributesVisible().length).toEqual(2);
    });

    it('#getAttributeBySearchFlag() should return attribute for the given search_flag', () => {
        component.attributeList = ATTRIBUTE_LIST;
        expect(component.getAttributeBySearchFlag('ID').name).toEqual('name_one');
    });
});
