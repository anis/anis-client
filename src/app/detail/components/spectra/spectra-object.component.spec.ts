import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { SpectraObjectComponent } from './spectra-object.component';
import { Attribute, Category, Family } from '../../../metamodel/model';
import { ATTRIBUTE_LIST, OBJECT_DETAIL } from '../../../../settings/test-data';

describe('[Detail][Spectra] Component: SpectraObjectComponent', () => {
    @Component({ selector: 'app-object-data', template: '' })
    class ObjectDataStubComponent {
        @Input() datasetName: string;
        @Input() outputFamilyList: Family[];
        @Input() categoryList: Category[];
        @Input() attributeList: Attribute[];
        @Input() object: any;
    }

    let component: SpectraObjectComponent;
    let fixture: ComponentFixture<SpectraObjectComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                SpectraObjectComponent,
                ObjectDataStubComponent
            ]
        });
        fixture = TestBed.createComponent(SpectraObjectComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#ngOnInit() should emit getSpectraCSV event if an attribute have spectra_graph renderer_detail', () => {
        component.attributeList = ATTRIBUTE_LIST;
        component.object = OBJECT_DETAIL;
        component.getSpectraCSV.subscribe((event: string) => expect(event).toEqual('spec1d'));
        component.ngOnInit();
        // To prevent `Spec has no expectation' bug
        expect(true).toBeTruthy();
    });

    it('#getSpectra() should return url to download spectra', () => {
        component.datasetName = 'dname';
        component.attributeList = ATTRIBUTE_LIST;
        component.object = OBJECT_DETAIL;
        expect(component.getSpectra()).toEqual('http://localhost:8080/download-file/dname/spec1d');
    });

    it('#getAttributeSpectraGraph() should return attribute for the spectra_graph renderer_detail', () => {
        component.attributeList = ATTRIBUTE_LIST;
        expect(component.getAttributeSpectraGraph().name).toEqual('name_two');
    });

    it('#getZ() should return Z value', () => {
        component.attributeList = ATTRIBUTE_LIST;
        component.object = OBJECT_DETAIL;
        expect(component.getZ()).toEqual(0);
        const attributeZ: Attribute = {
            id: 3,
            name: 'name_three',
            table_name: 'table_2',
            label: 'label_three',
            form_label: 'form_label_three',
            description: 'description_three',
            output_display: 1,
            criteria_display: 1,
            search_flag: 'Z',
            search_type: 'field',
            operator: '=',
            type: '',
            min: null,
            max: null,
            placeholder_min: '',
            placeholder_max: '',
            renderer: '',
            renderer_config: null,
            display_detail: 1,
            selected: true,
            order_by: true,
            order_display: 1,
            detail: true,
            renderer_detail: '',
            options: null,
            vo_utype: '',
            vo_ucd: '',
            vo_unit: '',
            vo_description: '',
            vo_datatype: '',
            vo_size: 1,
            id_criteria_family: 2,
            id_output_category: 2
        };
        component.attributeList = [...ATTRIBUTE_LIST, attributeZ];
        expect(component.getZ()).toEqual(4);
    });
});
