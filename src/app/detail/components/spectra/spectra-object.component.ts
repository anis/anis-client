/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter, OnInit } from '@angular/core';

import { Attribute, Category, Family } from '../../../metamodel/model';
import { getHost } from '../../../shared/utils';

@Component({
    selector: 'app-spectra-object',
    templateUrl: 'spectra-object.component.html',
    styleUrls: ['spectra-object.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Detail spectra object component.
 *
 * @implements OnInit
 */
export class SpectraObjectComponent implements OnInit {
    @Input() datasetName: string;
    @Input() outputFamilyList: Family[];
    @Input() categoryList: Category[];
    @Input() attributeList: Attribute[];
    @Input() object: any;
    @Input() spectraIsLoading: boolean;
    @Input() spectraIsLoaded: boolean;
    @Input() spectraCSV: string;
    @Output() getSpectraCSV: EventEmitter<string> = new EventEmitter();

    ngOnInit() {
        const attributeSpectraGraph = this.getAttributeSpectraGraph();
        if (attributeSpectraGraph) {
            Promise.resolve(null).then(() => this.getSpectraCSV.emit(this.object[attributeSpectraGraph.label]));
        }
    }

    /**
     * Returns spectra file URL.
     *
     * @return string
     */
    getSpectra(): string {
        const spectraAttribute: Attribute = this.attributeList
            .filter(a => a.detail)
            .find(attribute => attribute.search_flag === 'SPECTRUM_1D');
        return getHost() + '/download-file/' + this.datasetName + '/' + this.object[spectraAttribute.label];
    }

    /**
     * Returns detail rendered spectra graph attribute.
     *
     * @return Attribute
     */
    getAttributeSpectraGraph(): Attribute {
        return this.attributeList
            .filter(a => a.detail)
            .find(attribute => attribute.renderer_detail === 'spectra_graph');
    }

    /**
     * Returns Z.
     *
     * @return number
     */
    getZ(): number {
        const attributeZ = this.attributeList
            .filter(a => a.detail)
            .find(attribute => attribute.search_flag === 'Z');
        if (attributeZ) {
            return +this.object[attributeZ.label];
        } else {
            return 0;
        }
    }
}
