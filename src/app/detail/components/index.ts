import { SpectraObjectComponent } from './spectra/spectra-object.component';
import { SpectraGraphComponent } from './spectra/spectra-graph/spectra-graph.component';
import { DefaultObjectComponent } from './default-object.component';
import { ObjectDataComponent } from './object-data.component';

export const dummiesComponents = [
    SpectraObjectComponent,
    SpectraGraphComponent,
    DefaultObjectComponent,
    ObjectDataComponent
];
