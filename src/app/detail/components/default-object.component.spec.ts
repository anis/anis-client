import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { DefaultObjectComponent } from './default-object.component';
import { Attribute, Category, Family } from '../../metamodel/model';

describe('[Detail] Component: DefaultObjectComponent', () => {
    @Component({ selector: 'app-object-data', template: '' })
    class ObjectDataStubComponent {
        @Input() datasetName: string;
        @Input() outputFamilyList: Family[];
        @Input() categoryList: Category[];
        @Input() attributeList: Attribute[];
        @Input() object: any;
    }

    let component: DefaultObjectComponent;
    let fixture: ComponentFixture<DefaultObjectComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DefaultObjectComponent,
                ObjectDataStubComponent
            ]
        });
        fixture = TestBed.createComponent(DefaultObjectComponent);
        component = fixture.componentInstance;
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
