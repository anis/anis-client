/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Attribute, Category, Family } from '../../metamodel/model';
import { sortByDisplay } from '../../shared/utils';
import { getHost } from '../../shared/utils';

@Component({
    selector: 'app-object-data',
    templateUrl: 'object-data.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Detail object data component.
 */
export class ObjectDataComponent {
    @Input() datasetName: string;
    @Input() outputFamilyList: Family[];
    @Input() categoryList: Category[];
    @Input() attributeList: Attribute[];
    @Input() object: any;

    /**
     * Returns category list sorted by display, for the given output family ID.
     *
     * @param  {number} idFamily - The output family ID.
     *
     * @return Category[]
     */
    getCategoryByFamilySortedByDisplay(idFamily: number): Category[] {
        return this.categoryList
            .filter(category => category.id_output_family === idFamily)
            .sort(sortByDisplay);
    }

    /**
     * Returns attribute list sorted by detail display, for the given output category ID.
     *
     * @param  {number} idCategory - The output category ID.
     *
     * @return Attribute[]
     */
    getAttributesVisibleByCategory(idCategory: number): Attribute[] {
        return this.attributeList
            .filter(a => a.detail)
            .filter(a => a.id_output_category === idCategory)
            .sort((a, b) => a.display_detail - b.display_detail);
    }

    /**
     * Returns attribute list sorted by detail display.
     *
     * @return Attribute[]
     */
    getAttributesVisible(): Attribute[] {
        return this.attributeList
            .filter(a => a.detail)
            .sort((a, b) => a.display_detail - b.display_detail);
    }

    /**
     * Returns attribute for the given search flag.
     *
     * @param  {string} searchFlag - The search flag.
     *
     * @return Attribute
     */
    getAttributeBySearchFlag(searchFlag: string): Attribute {
        return this.getAttributesVisible().find(attribute => attribute.search_flag === searchFlag);
    }

    getDownloadHref(value: string) {
        return getHost() + '/download-file/' + this.datasetName + '/' + value;
    }
}
