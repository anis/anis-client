/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Attribute, Category, Family } from '../../metamodel/model';

@Component({
    selector: 'app-default-object',
    templateUrl: 'default-object.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
/**
 * @class
 * @classdesc Detail default object component.
 */
export class DefaultObjectComponent {
    @Input() datasetName: string;
    @Input() outputFamilyList: Family[];
    @Input() categoryList: Category[];
    @Input() attributeList: Attribute[];
    @Input() object: any;
}
