import * as fromDetail from './detail.reducer';
import * as detailActions from './detail.action';

describe('[Detail] Reducer', () => {
    it('should return init state', () => {
        const { initialState } = fromDetail;
        const action = {} as detailActions.Actions;
        const state = fromDetail.reducer(undefined, action);

        expect(state).toBe(initialState);
    });

    it('should set objectIsLoading to true', () => {
        const { initialState } = fromDetail;
        const action = new detailActions.InitDetailAction();
        const state = fromDetail.reducer(initialState, action);

        expect(state.searchIsPristine).toBeNull();
        expect(state.datasetName).toBeNull();
        expect(state.objectIsLoading).toBeTruthy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.object).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set searchIsPristine', () => {
        const { initialState } = fromDetail;
        const action = new detailActions.SetSearchIsPristineAction(true);
        const state = fromDetail.reducer(initialState, action);

        expect(state.searchIsPristine).toBeTruthy();
        expect(state.datasetName).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.object).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set datasetName', () => {
        const { initialState } = fromDetail;
        const action = new detailActions.SetDatasetNameAction('toto');
        const state = fromDetail.reducer(initialState, action);

        expect(state.searchIsPristine).toBeNull();
        expect(state.datasetName).toEqual('toto');
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.object).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set object, objectIsLoaded to true and objectIsLoading to false', () => {
        const initialState = { ...fromDetail.initialState, objectIsLoading: true };
        const action = new detailActions.RetrieveObjectSuccessAction(['object']);
        const state = fromDetail.reducer(initialState, action);

        expect(state.searchIsPristine).toBeNull();
        expect(state.datasetName).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeTruthy();
        expect(state.object).toContain('object');
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set objectIsLoading and objectIsLoaded to false by object fail', () => {
        const initialState = {
            ...fromDetail.initialState,
            objectIsLoading: true,
            objectIsLoaded: true
        };
        const action = new detailActions.RetrieveObjectFailAction();
        const state = fromDetail.reducer(initialState, action);

        expect(state.searchIsPristine).toBeNull();
        expect(state.datasetName).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.object).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set spectraIsLoading to true', () => {
        const spectraFile: string = 'toto';
        const { initialState } = fromDetail;
        const action = new detailActions.RetrieveSpectraAction(spectraFile);
        const state = fromDetail.reducer(initialState, action);

        expect(state.searchIsPristine).toBeNull();
        expect(state.datasetName).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.object).toBeNull();
        expect(state.spectraIsLoading).toBeTruthy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should set spectraCSV, spectraIsLoaded to true and spectraIsLoading to false', () => {
        const spectraCSV: string = 'csv';
        const initialState = { ...fromDetail.initialState, spectraIsLoading: true };
        const action = new detailActions.RetrieveSpectraSuccessAction(spectraCSV);
        const state = fromDetail.reducer(initialState, action);

        expect(state.searchIsPristine).toBeNull();
        expect(state.datasetName).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.object).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeTruthy();
        expect(state.spectraCSV).toContain('csv');
        expect(state).not.toEqual(initialState);
    });

    it('should set spectraIsLoading to false', () => {
        const initialState = { ...fromDetail.initialState, spectraIsLoading: true };
        const action = new detailActions.RetrieveSpectraFailAction();
        const state = fromDetail.reducer(initialState, action);

        expect(state.searchIsPristine).toBeNull();
        expect(state.datasetName).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.object).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should reset state', () => {
        const initialState = {
            searchIsPristine: true,
            datasetName: 'dname',
            objectIsLoading: true,
            objectIsLoaded: true,
            object: 'toto',
            spectraIsLoading: true,
            spectraIsLoaded: true,
            spectraCSV: 'csv'
        };
        const action = new detailActions.DestroyDetailAction();
        const state = fromDetail.reducer(initialState, action);

        expect(state.searchIsPristine).toBeNull();
        expect(state.datasetName).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.object).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state).not.toEqual(initialState);
    });

    it('should get searchIsPristine', () => {
        const action = {} as detailActions.Actions;
        const state = fromDetail.reducer(undefined, action);

        expect(fromDetail.getSearchIsPristine(state)).toBeNull();
    });

    it('should get datasetName', () => {
        const action = {} as detailActions.Actions;
        const state = fromDetail.reducer(undefined, action);

        expect(fromDetail.getDatasetName(state)).toBeNull();
    });

    it('should get objectIsLoading', () => {
        const action = {} as detailActions.Actions;
        const state = fromDetail.reducer(undefined, action);

        expect(fromDetail.getObjectIsLoading(state)).toBeFalsy();
    });

    it('should get objectIsLoaded', () => {
        const action = {} as detailActions.Actions;
        const state = fromDetail.reducer(undefined, action);

        expect(fromDetail.getObjectIsLoaded(state)).toBeFalsy();
    });

    it('should get object', () => {
        const action = {} as detailActions.Actions;
        const state = fromDetail.reducer(undefined, action);

        expect(fromDetail.getObject(state)).toBeNull();
    });

    it('should get spectraIsLoading', () => {
        const action = {} as detailActions.Actions;
        const state = fromDetail.reducer(undefined, action);

        expect(fromDetail.getSpectraIsLoading(state)).toBeFalsy();
    });

    it('should get spectraIsLoaded', () => {
        const action = {} as detailActions.Actions;
        const state = fromDetail.reducer(undefined, action);

        expect(fromDetail.getSpectraIsLoaded(state)).toBeFalsy();
    });

    it('should get spectraCSV', () => {
        const action = {} as detailActions.Actions;
        const state = fromDetail.reducer(undefined, action);

        expect(fromDetail.getSpectraCSV(state)).toBeNull();
    });
});
