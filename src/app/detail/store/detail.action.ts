/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';


export const INIT_DETAIL = '[Detail] Init Detail';
export const SET_SEARCH_IS_PRISTINE = '[Detail] Set Search Is Pristine';
export const SET_DATASET_NAME = '[Detail] Set Dataset Name';
export const RETRIEVE_OBJECT = '[Detail] Retrieve Object';
export const RETRIEVE_OBJECT_SUCCESS = '[Detail] Retrieve Object Success';
export const RETRIEVE_OBJECT_FAIL = '[Detail] Retrieve Object Fail';
export const RETRIEVE_SPECTRA = '[Detail] Retrieve Spectra';
export const RETRIEVE_SPECTRA_SUCCESS = '[Detail] Retrieve Spectra Success';
export const RETRIEVE_SPECTRA_FAIL = '[Detail] Retrieve Spectra Fail';
export const DESTROY_DETAIL = '[Detail] Destroy detail';

/**
 * @class
 * @classdesc InitDetailAction action.
 * @readonly
 */
export class InitDetailAction implements Action {
    readonly type = INIT_DETAIL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc SetDatasetNameAction action.
 * @readonly
 */
export class SetDatasetNameAction implements Action {
    readonly type = SET_DATASET_NAME;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc SetSearchIsPristineAction action.
 * @readonly
 */
export class SetSearchIsPristineAction implements Action {
    readonly type = SET_SEARCH_IS_PRISTINE;

    constructor(public payload: boolean) { }
}

/**
 * @class
 * @classdesc RetrieveObjectAction action.
 * @readonly
 */
export class RetrieveObjectAction implements Action {
    readonly type = RETRIEVE_OBJECT;

    constructor(public payload: { datasetName: string, idAttribute: number, id: string } = null) { }
}

/**
 * @class
 * @classdesc RetrieveObjectSuccessAction action.
 * @readonly
 */
export class RetrieveObjectSuccessAction implements Action {
    readonly type = RETRIEVE_OBJECT_SUCCESS;

    constructor(public payload: any) { }
}

/**
 * @class
 * @classdesc RetrieveObjectFailAction action.
 * @readonly
 */
export class RetrieveObjectFailAction implements Action {
    readonly type = RETRIEVE_OBJECT_FAIL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc RetrieveSpectraAction action.
 * @readonly
 */
export class RetrieveSpectraAction implements Action {
    readonly type = RETRIEVE_SPECTRA;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc RetrieveSpectraSuccessAction action.
 * @readonly
 */
export class RetrieveSpectraSuccessAction implements Action {
    readonly type = RETRIEVE_SPECTRA_SUCCESS;

    constructor(public payload: string) { }
}

/**
 * @class
 * @classdesc RetrieveSpectraFailAction action.
 * @readonly
 */
export class RetrieveSpectraFailAction implements Action {
    readonly type = RETRIEVE_SPECTRA_FAIL;

    constructor(public payload: {} = null) { }
}

/**
 * @class
 * @classdesc DestroyDetailAction action.
 * @readonly
 */
export class DestroyDetailAction implements Action {
    readonly type = DESTROY_DETAIL;

    constructor(public payload: {} = null) { }
}

export type Actions
    = InitDetailAction
    | SetSearchIsPristineAction
    | SetDatasetNameAction
    | RetrieveObjectAction
    | RetrieveObjectSuccessAction
    | RetrieveObjectFailAction
    | RetrieveSpectraAction
    | RetrieveSpectraSuccessAction
    | RetrieveSpectraFailAction
    | DestroyDetailAction;
