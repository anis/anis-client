import * as detailActions from './detail.action';

describe('[Detail] Action', () => {
    it('should InitDetailAction', () => {
        const action = new detailActions.InitDetailAction();
        expect(action.type).toEqual(detailActions.INIT_DETAIL);
    });

    it('should SetSearchIsPristineAction', () => {
        const action = new detailActions.SetSearchIsPristineAction(true);
        expect(action.type).toEqual(detailActions.SET_SEARCH_IS_PRISTINE);
        expect(action.payload).toBeTruthy();
    });

    it('should SetDatsetNameAction', () => {
        const action = new detailActions.SetDatasetNameAction('toto');
        expect(action.type).toEqual(detailActions.SET_DATASET_NAME);
        expect(action.payload).toEqual('toto');
    });

    it('should RetrieveObjectAction', () => {
        const payload: { datasetName: string, idAttribute: number, id: string } = {
            datasetName: 'toto',
            idAttribute: 1,
            id: 'one'
        };
        const action = new detailActions.RetrieveObjectAction(payload);
        expect(action.type).toEqual(detailActions.RETRIEVE_OBJECT);
        expect(action.payload).toEqual(payload);
    });

    it('should RetrieveObjectSuccessAction', () => {
        const action = new detailActions.RetrieveObjectSuccessAction('toto');
        expect(action.type).toEqual(detailActions.RETRIEVE_OBJECT_SUCCESS);
        expect(action.payload).toEqual('toto');
    });

    it('should RetrieveObjectFailAction', () => {
        const action = new detailActions.RetrieveObjectFailAction();
        expect(action.type).toEqual(detailActions.RETRIEVE_OBJECT_FAIL);
    });

    it('should RetrieveSpectraAction', () => {
        const action = new detailActions.RetrieveSpectraAction('toto');
        expect(action.type).toEqual(detailActions.RETRIEVE_SPECTRA);
        expect(action.payload).toEqual('toto');
    });

    it('should RetrieveSpectraSuccessAction', () => {
        const action = new detailActions.RetrieveSpectraSuccessAction('toto');
        expect(action.type).toEqual(detailActions.RETRIEVE_SPECTRA_SUCCESS);
        expect(action.payload).toEqual('toto');
    });

    it('should RetrieveSpectraFailAction', () => {
        const action = new detailActions.RetrieveSpectraFailAction();
        expect(action.type).toEqual(detailActions.RETRIEVE_SPECTRA_FAIL);
    });

    it('should DestroyDetailAction', () => {
        const action = new detailActions.DestroyDetailAction();
        expect(action.type).toEqual(detailActions.DESTROY_DETAIL);
    });
});
