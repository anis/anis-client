/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as detail from './detail.reducer';

export const getDetailState = createFeatureSelector<detail.State>('detail');

export const getSearchIsPristine = createSelector(
    getDetailState,
    detail.getSearchIsPristine
);

export const getDatasetName = createSelector(
    getDetailState,
    detail.getDatasetName
);

export const getObjectIsLoading = createSelector(
    getDetailState,
    detail.getObjectIsLoading
);

export const getObjectIsLoaded = createSelector(
    getDetailState,
    detail.getObjectIsLoaded
);

export const getObject = createSelector(
    getDetailState,
    detail.getObject
);

export const getSpectraIsLoading = createSelector(
    getDetailState,
    detail.getSpectraIsLoading
);

export const getSpectraIsLoaded = createSelector(
    getDetailState,
    detail.getSpectraIsLoaded
);

export const getSpectraCSV = createSelector(
    getDetailState,
    detail.getSpectraCSV
);
