/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as fromRouter from '@ngrx/router-store';
import { of } from 'rxjs';
import { switchMap, map, catchError, tap, withLatestFrom } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as fromDetail from './detail.reducer';
import * as detailActions from './detail.action';
import { DetailService } from './detail.service';
import * as fromMetamodel from '../../metamodel/reducers';
import * as attributeActions from '../../metamodel/action/attribute.action';
import * as outputActions from '../../metamodel/action/output.action';
import * as fromSearch from '../../search/store/search.reducer';
import * as utils from '../../shared/utils';

/**
 * @class
 * @classdesc Detail effects.
 */
interface State {
    router: fromRouter.RouterReducerState<utils.RouterStateUrl>;
    metamodel: fromMetamodel.State;
    search: fromSearch.State;
    detail: fromDetail.State;
}

@Injectable()
export class DetailEffects {
    constructor(
        private actions$: Actions,
        private detailService: DetailService,
        private toastr: ToastrService,
        private store$: Store<State>
    ) { }

    /**
     * Calls actions to initiate detail.
     */
    @Effect()
    initDetailAction$ = this.actions$.pipe(
        ofType(detailActions.INIT_DETAIL),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const datasetName = state.router.state.params.dname;
            let actions: Action[] = [new detailActions.SetDatasetNameAction(datasetName)];

            if (!state.search || state.search.pristine) {
                actions.push(new attributeActions.LoadAttributeListAction(datasetName));
                actions.push(new outputActions.LoadOutputSearchMetaAction(datasetName));
                actions.push(new detailActions.SetSearchIsPristineAction(true));
            } else {
                actions.push(new detailActions.RetrieveObjectAction());
                actions.push(new detailActions.SetSearchIsPristineAction(false));
            }

            return actions;
        })
    );

    /**
     * Calls action to retrieve object details if attribute list is loaded.
     */
    @Effect()
    loadAttributeSuccessAction$ = this.actions$.pipe(
        ofType(attributeActions.LOAD_ATTRIBUTE_LIST_SUCCESS),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            if (state.router.state.url.includes('/detail/')) {
                return of(new detailActions.RetrieveObjectAction());
            } else {
                return of({ type: '[No Action] ' + attributeActions.LOAD_ATTRIBUTE_LIST_SUCCESS });
            }
        })
    );

    /**
     * Retrieves object details with given parameters.
     */
    @Effect()
    retrieveObjectAction$ = this.actions$.pipe(
        ofType(detailActions.RETRIEVE_OBJECT),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const dname = state.router.state.params.dname;
            const objectSelected = state.router.state.params.objectSelected;
            const attributes = state.metamodel.attribute.entities[dname].attributeList;
            const criterionId = attributes.find(a => a.search_flag === 'ID');
            const outputList = attributes.filter(a => a.detail).map(a => a.id);
            return this.detailService.retrieveObject(dname, criterionId.id, objectSelected, outputList).pipe(
                map((object: any[]) => new detailActions.RetrieveObjectSuccessAction(object[0])),
                catchError(() => of(new detailActions.RetrieveObjectFailAction()))
            );
        })
    );

    /**
     * Displays retrieves object details error notification.
     */
    @Effect({ dispatch: false })
    retrieveObjectFailAction$ = this.actions$.pipe(
        ofType(detailActions.RETRIEVE_OBJECT_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'The object loading failed'))
    );

    /**
     * Retrieves spectra.
     */
    @Effect()
    retrieveSpectraAction$ = this.actions$.pipe(
        ofType(detailActions.RETRIEVE_SPECTRA),
        withLatestFrom(this.store$),
        switchMap(([action, state]) => {
            const dname = state.router.state.params.dname;
            const retrieveSpectraAction = action as detailActions.RetrieveSpectraAction;
            return this.detailService.retrieveSpectra(dname, retrieveSpectraAction.payload).pipe(
                map((spectraCSV: string) => new detailActions.RetrieveSpectraSuccessAction(spectraCSV)),
                catchError(_ => of(new detailActions.RetrieveSpectraFailAction()))
            );
        })
    );

    /**
     * Displays retrieves spectra error notification.
     */
    @Effect({ dispatch: false })
    retrieveSpectraFailAction$ = this.actions$.pipe(
        ofType(detailActions.RETRIEVE_SPECTRA_FAIL),
        tap(_ => this.toastr.error('Loading Failed!', 'The spectra CSV loading failed'))
    );
}
