/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable()
/**
 * @class
 * @classdesc Detail service.
 */
export class DetailService {
    private API_PATH: string = environment.apiUrl + '/search/';
    private SERVICES_PATH: string = environment.servicesUrl;

    constructor(private http: HttpClient) { }

    /**
     * Retrieves object details for the given parameters.
     *
     * @param  {string} dname - The dataset name.
     * @param  {number} criterionId - The criterion ID.
     * @param  {string} objectSelected - The selected object ID.
     * @param  {number[]} outputList - The output list.
     *
     * @return Observable<any[]>
     */
    retrieveObject(dname: string, criterionId: number, objectSelected: string, outputList: number[]): Observable<any[]> {
        const query = dname + '?c=' + criterionId + '::eq::' + objectSelected + '&a=' + outputList.join(';');
        return this.http.get<any[]>(this.API_PATH + query);
    }

    /**
     * Retrieves object details for the given parameters.
     *
     * @param  {string} spectraFile - The spectra file name.
     *
     * @return Observable<string>
     */
    retrieveSpectra(dname: string, spectraFile: string): Observable<string> {
        return this.http.get(this.SERVICES_PATH + '/spectra-to-csv/' + dname + '?filename=' + spectraFile, {responseType: 'text'});
    }
}
