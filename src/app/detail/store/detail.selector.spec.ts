import * as detailSelector from './detail.selector';
import * as fromDetail from './detail.reducer';

describe('[Detail] Selector', () => {
    it('should get searchIsPristine', () => {
        const state = { detail: { ...fromDetail.initialState }};
        expect(detailSelector.getSearchIsPristine(state)).toBeNull();
    });

    it('should get datasetName', () => {
        const state = { detail: { ...fromDetail.initialState }};
        expect(detailSelector.getDatasetName(state)).toBeNull();
    });
   
    it('should get objectIsLoading', () => {
        const state = { detail: { ...fromDetail.initialState }};
        expect(detailSelector.getObjectIsLoading(state)).toBeFalsy();
    });
   
    it('should get objectIsLoaded', () => {
        const state = { detail: { ...fromDetail.initialState }};
        expect(detailSelector.getObjectIsLoaded(state)).toBeFalsy();
    });
   
    it('should get object', () => {
        const state = { detail: { ...fromDetail.initialState }};
        expect(detailSelector.getObject(state)).toBeNull();
    });
   
    it('should get spectraIsLoading', () => {
        const state = { detail: { ...fromDetail.initialState }};
        expect(detailSelector.getSpectraIsLoading(state)).toBeFalsy();
    });
   
    it('should get spectraIsLoaded', () => {
        const state = { detail: { ...fromDetail.initialState }};
        expect(detailSelector.getSpectraIsLoaded(state)).toBeFalsy();
    });
   
    it('should get spectraCSV', () => {
        const state = { detail: { ...fromDetail.initialState }};
        expect(detailSelector.getSpectraCSV(state)).toBeNull();
    });
});