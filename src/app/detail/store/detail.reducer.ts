/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as actions from './detail.action';

/**
 * Interface for detail state.
 *
 * @interface State
 */
export interface State {
    searchIsPristine: boolean
    datasetName: string;
    objectIsLoading: boolean;
    objectIsLoaded: boolean;
    object: any;
    spectraIsLoading: boolean;
    spectraIsLoaded: boolean;
    spectraCSV: string;
}

export const initialState: State = {
    searchIsPristine: null,
    datasetName: null,
    objectIsLoading: false,
    objectIsLoaded: false,
    object: null,
    spectraIsLoading: false,
    spectraIsLoaded: false,
    spectraCSV: null
};

/**
 * Reduces state.
 *
 * @param  {State} state - The state.
 * @param  {actions} action - The action.
 *
 * @return State
 */
export function reducer(state: State = initialState, action: actions.Actions): State {
    switch (action.type) {
        case actions.INIT_DETAIL:
            return {
                ...state,
                objectIsLoading: true
            };

        case actions.SET_SEARCH_IS_PRISTINE:
            return {
                ...state,
                searchIsPristine: action.payload
            };

        case actions.SET_DATASET_NAME:
            return {
                ...state,
                datasetName: action.payload
            };

        case actions.RETRIEVE_OBJECT_SUCCESS:
            return {
                ...state,
                objectIsLoading: false,
                objectIsLoaded: true,
                object: action.payload
            };

        case actions.RETRIEVE_OBJECT_FAIL:
            return {
                ...state,
                objectIsLoading: false,
                objectIsLoaded: false
            };

        case actions.RETRIEVE_SPECTRA:
            return {
                ...state,
                spectraIsLoading: true
            };

        case actions.RETRIEVE_SPECTRA_SUCCESS:
            return {
                ...state,
                spectraIsLoading: false,
                spectraIsLoaded: true,
                spectraCSV: action.payload
            };

        case actions.RETRIEVE_SPECTRA_FAIL:
            return {
                ...state,
                spectraIsLoading: false
            };

        case actions.DESTROY_DETAIL:
            return initialState;

        default:
            return state;
    }
}

export const getSearchIsPristine = (state: State) => state.searchIsPristine;
export const getDatasetName = (state: State) => state.datasetName;
export const getObjectIsLoading = (state: State) => state.objectIsLoading;
export const getObjectIsLoaded = (state: State) => state.objectIsLoaded;
export const getObject = (state: State) => state.object;
export const getSpectraIsLoading = (state: State) => state.spectraIsLoading;
export const getSpectraIsLoaded = (state: State) => state.spectraIsLoaded;
export const getSpectraCSV = (state: State) => state.spectraCSV;
