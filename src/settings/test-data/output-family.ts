import { Family } from '../../app/metamodel/model';

export const OUTPUT_FAMILY: Family = {
    id: 2,
    type: 'output',
    label: 'Default output family',
    display: 10
};
