import { Input, Directive } from '@angular/core';

@Directive({
    selector: '[routerLink]'
})
export class RouterLinkDirectiveStub {
    @Input('routerLink') linkParams: any;
    @Input('queryParams') queryParams: any;
    navigatedTo: any = null;
}