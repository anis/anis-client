import { Category } from '../../app/metamodel/model';

export const CATEGORY: Category = {
    id: 1,
    label: 'Default output category',
    display: 10,
    id_output_family: 1
};
