import { Dataset } from '../../app/metamodel/model';

export const DATASET: Dataset = {
    name: 'cat_1',
    table_ref: 'table_1',
    label: 'Cat 1',
    description: 'Description of cat 1',
    display: 10,
    count: 10000,
    vo: false,
    survey_name: 'survey_1',
    id_dataset_family: 1,
    public: true,
    config: {
        images: [],
        cone_search: {
            enabled: true,
            opened: true,
            column_ra: 2,
            column_dec: 3,
            plot_enabled: false,
            sdss_enabled: false,
            sdss_display: 10,
            background: []
        },
        download: {
            enabled: true,
            opened: true,
            csv: true,
            ascii: false,
            vo: false,
            archive: true
        },
        summary: {
            enabled: true,
            opened: true
        },
        server_link: {
            enabled: true,
            opened: true
        },
        samp: {
            enabled: true,
            opened: true
        },
        datatable: {
            enabled: true,
            opened: true,
            selectable_row: true
        }
    }
};
