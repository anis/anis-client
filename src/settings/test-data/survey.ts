import { Survey } from '../../app/metamodel/model';

export const SURVEY: Survey = {
    name: 'survey_1',
    label: 'Survey 1',
    description: 'Description of survey 1',
    link: '',
    manager: '',
    id_database: 1
};
