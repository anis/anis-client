import { Family } from '../../app/metamodel/model';

export const CRITERIA_FAMILY_LIST: Family[] = [
    {
        id: 3,
        type: 'criteria',
        label: 'The last criteria family',
        display: 30
    },
    {
        id: 1,
        type: 'criteria',
        label: 'Default criteria family',
        display: 10
    },
    {
        id: 2,
        type: 'criteria',
        label: 'Another criteria family',
        display: 20
    }
];
