import { Family } from '../../app/metamodel/model';

export const OUTPUT_FAMILY_LIST: Family[] = [
    {
        id: 2,
        type: 'output',
        label: 'Another output family',
        display: 10
    },
    {
        id: 1,
        type: 'output',
        label: 'Default output family',
        display: 10
    }
];
