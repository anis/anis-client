import { Category } from '../../app/metamodel/model';

export const CATEGORY_LIST: Category[] = [
    {
        id: 1,
        label: 'Another output category',
        display: 20,
        id_output_family: 1
    },
    {
        id: 3,
        label: 'The last output category',
        display: 10,
        id_output_family: 2
    },
    {
        id: 2,
        label: 'Default output category',
        display: 10,
        id_output_family: 1
    }
];
