import { Dataset } from '../../app/metamodel/model';

export const DATASET_LIST: Dataset[] = [
    {
        name: 'cat_1',
        table_ref: '',
        label: 'Cat 1',
        description: 'Description of cat 1',
        display: 10,
        count: 10000,
        vo: false,
        survey_name: 'survey_1',
        id_dataset_family: 1,
        public: true,
        config: {
            images: [],
            cone_search: {
                enabled: true,
                opened: true,
                column_ra: 2,
                column_dec: 3,
                plot_enabled: false,
                sdss_enabled: false,
                sdss_display: 10,
                background: []
            },
            download: {
                enabled: true,
                opened: true,
                csv: true,
                ascii: false,
                vo: true,
                archive: true
            },
            summary: {
                enabled: true,
                opened: true
            },
            server_link: {
                enabled: true,
                opened: true
            },
            samp: {
                enabled: true,
                opened: true
            },
            datatable: {
                enabled: true,
                opened: true,
                selectable_row: true
            }
        }
    },
    {
        name: 'cat_3',
        table_ref: '',
        label: 'Cat 3',
        description: 'Description of cat 3',
        display: 30,
        count: 30000,
        vo: false,
        survey_name: 'survey_2',
        id_dataset_family: 2,
        public: false,
        config: {
            images: [],
            cone_search: {
                enabled: false,
                opened: false,
                column_ra: 2,
                column_dec: 3,
                plot_enabled: false,
                sdss_enabled: false,
                sdss_display: 10,
                background: []
            },
            download: {
                enabled: true,
                opened: true,
                csv: true,
                ascii: false,
                vo: true,
                archive: true
            },
            summary: {
                enabled: true,
                opened: true
            },
            server_link: {
                enabled: true,
                opened: true
            },
            samp: {
                enabled: true,
                opened: true
            },
            datatable: {
                enabled: true,
                opened: true,
                selectable_row: true
            }
        }
    },
    {
        name: 'cat_2',
        table_ref: '',
        label: 'Cat 2',
        description: 'Description of cat 2',
        display: 20,
        count: 20000,
        vo: false,
        survey_name: 'survey_1',
        id_dataset_family: 2,
        public: false,
        config: {
            images: [],
            cone_search: {
                enabled: false,
                opened: false,
                column_ra: 2,
                column_dec: 3,
                plot_enabled: false,
                sdss_enabled: false,
                sdss_display: 10,
                background: []
            },
            download: {
                enabled: true,
                opened: true,
                csv: false,
                ascii: false,
                vo: true,
                archive: true
            },
            summary: {
                enabled: true,
                opened: true
            },
            server_link: {
                enabled: false,
                opened: false
            },
            samp: {
                enabled: true,
                opened: true
            },
            datatable: {
                enabled: true,
                opened: false,
                selectable_row: true
            }
        }
    }
];
