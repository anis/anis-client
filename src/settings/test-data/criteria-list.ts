import { Criterion, FieldCriterion } from '../../app/search/store/model';

export const CRITERIA_LIST: Criterion[] = [
    {id: 1, type: 'field', operator: 'eq', value: 'fd_crit_1'} as FieldCriterion,
    {id: 2, type: 'field', operator: 'eq', value: 'fd_crit_2'} as FieldCriterion
];
