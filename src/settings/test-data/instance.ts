import { Instance } from '../../app/metamodel/model';

export const INSTANCE: Instance = {
    name: 'instance',
    label: 'Instance',
    client_url: 'http://localhost.com',
    nb_dataset_families: 2,
    nb_datasets: 3,
    config: {
        authentication: {
            allowed: true
        },
        search: {
            allowed: true
        },
        search_multiple: {
            allowed: true,
            all_datasets_selected: true,
        },
        documentation: {
            allowed: false
        }
    }
};
