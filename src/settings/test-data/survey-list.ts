import { Survey } from '../../app/metamodel/model';

export const SURVEY_LIST: Survey[] = [
    {
        name: 'survey_2',
        label: 'Survey 2',
        description: 'Description of survey 2',
        link: '',
        manager: '',
        id_database: 1
    },
    {
        name: 'survey_1',
        label: 'Survey 1',
        description: 'Description of survey 1',
        link: '',
        manager: '',
        id_database: 1
    }
];
