import { Family } from '../../app/metamodel/model';

export const DATASET_FAMILY_LIST: Family[] = [
    {
        id: 3,
        type: 'dataset',
        label: 'The last dataset family',
        display: 30
    },
    {
        id: 1,
        type: 'dataset',
        label: 'Default dataset family',
        display: 10
    },
    {
        id: 2,
        type: 'dataset',
        label: 'Another dataset family',
        display: 20
    }
];
