/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export const environment = {
    production: true,
    apiUrl: '/server',
    servicesUrl: '/services',
    instanceName: 'default',
    baseHref: '/',
    ssoAuthUrl: 'https://keycloak.lam.fr/auth/',
    ssoRealm: 'anis',
    ssoClientId: 'anis-dev'
};
