# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.6.0] - 2021-xx
### Added
 - #160 => Add image renderer (datatable)

### Fixed
 - #163 => Bug datatable order_by
 - #157 => Sort results by order (ascending or descending)
 - #161 => The user can change the number of items per page

### Changed
- #155 => Display dataset label instead of dataset name in result page
- #159 => Update dependencies (Angular v11, ngrx v11, ...)

## [3.5.0] - 2020-12
### Added
- #140 => Add description tooltip on search multiple datasets page
- #136 => Add detail view for spectra type object and default object

### Fixed
- #142 => Fix bug datatable: attribute disappeared in specific circonstances

### Changed
- #151 => Display authentication actions buttons if instance configuration allows it
- #145 => Display public datasets only when no user logged in
- #134 => Result search summary into accordion
- #139 => Datasets selected in search multiple is a configurable option in anis-admin
- #138 => Sort attributes and put scrollbar if table too long in Documentation module
- #133 => Change typo if only one dataset

### Security
- #127 => Restrict navigation depending on instance configuration


## [3.4.0] - 2020-10
### Added
- #132 => Add reset cone search button in search multiple
- #122 => Add download results buttons to search multiple
- #97 => Add a new search multiple module
- #21 => Add module lazy loading
- #109 => Add help tooltip to like and not like operators
- #112 => Add domain names on documentation urls

### Fixed
- #116 => Fix search type list placeholder
- #115 => Fix between criterion that didn't fill correctly on page reload
- #113: Fix output list didn't change on summary box
- #111: Fix search lost when back to dataset page

### Changed
- #129 => By default in search multiple there is no selected dataset
- #130 => Sort datasets in search multiple by dataset family rather than project
- #126 => Download results in different formats are now a configurable option in anis-admin
- #124 => Navigation links are now a configurable option in anis-admin
- #125 => Display datasets with cone search only on search multiple
- #118 => Refactoring metamodel
- #81 => Opened result datatable is now a configurable option in anis-admin
- #81 => Display 'Direct link to the result' is now a configurable option in anis-admin
- #106 => Design update for summary box in result page
- #114 => Design update for outputs in summary box
- #110 => Design update for detail page
- #107 => Change select box for cone search to input text
- #99 => Improve cone search design for small screens and mobiles
- #100 => Improve outputs design on search summary


## [3.3.0] - 2020-06
### Added
- #88 => Search by cone search
- #89 => Name resolver added to cone search
- #90 => Dynamic documentation to explain how to export results via server urls
- #95 => Search by list
- #109 => Add tooltips on like and not like operators to help user
- #21 => Add lazy loading for features
- #81 => Section `Direct link to the result` as configurable option on result page
- #66 => Folded / unfolded datatable section as configurable option on result page
- #97 => Add multiple search

### Changed
- #91 => Download results buttons colors can be changed
- #92 => Improvement of sonarqube quality
- #93 => Auto select dataset if only one present
- #94 => Home redirection to project base href
- #96 => Improve search summary design
- #107 => Change field design for cone search hh:mm:ss coordinates
- #110 => Change design outputs on detail page
- #114 => Change design of outputs summary on result page for better readability
- #106 => Change summary design on result page
- #6 => README file more detailed

### Fixed
- #111 => Fix bug search lost when back to datasets page
- #113 => Fix bug output list not updated
- #115 => Fix bug on between criterion
- #116 => Fix bug placeholder for list criterion


## [3.2.0] - 2020-04
### Added
- #69 => User can change the number of object displayable (10, 20, 50, 100) in result datatable
- #70 => User can sort columns in result datatable
- #71 => User can dowload results in CSV and ASCII formats
- #72 => Display information about available criteria and outputs in summary box

### Fixed
- #32 => Criteria components with default value did not work
- #79 => Repair and improve component checkbox + test
- #80 => Search type field + IN or NOT IN didn't work
- #63 => If no outputs selectionned, request cannot work

### Changed
- #76 => Adding label min and max for the between component
- #73 => Result page design improved
- #83 => Upgrade dependencies
- #86 => Renderers in result section optimizedS

### Security
- Migrate to Angular v8 to v9
- Migrate from anis-server v3.1 to 3.2
- Migrate from anis-auth v3.1 to 3.2


## [3.1.0] - 2020-03
### Added
- Unit tests added
- Scroll to top on new page added
- Operators are visible on search form

### Changed
- Detail-link and detail-btn renderers refactored
- Update datasets display to accordion
- Password fields can be visible
- Search form design updated to integrate operators

### Fixed
- Migrate from anis-server v3.0.3 to 3.1
- Migrate to anis-auth v3.1

## [3.0.3] - 2019-10
### Added
- Display anis-client and anis-server versions
- Add detail page for an object

### Changed
- Fix ordering by diplay for options in Criteria step
- Design ouputs selection

### Fixed
- Fix bug renderer external links