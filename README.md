# Anis-client 

[![pipeline status](https://gitlab.lam.fr/anis/anis-client/badges/develop/pipeline.svg)](https://gitlab.lam.fr/anis/anis-client/-/commits/develop) [![coverage report](https://gitlab.lam.fr/anis/anis-client/badges/develop/coverage.svg)](https://gitlab.lam.fr/anis/anis-client/-/commits/develop)

## Introduction

AstroNomical Information System is a generic web tool that aims to facilitate the provision of data (Astrophysics), accessible from a database, for the scientific community.

This software allows you to control one or more databases related to astronomical projects and allows access to datasets via URLs.

This repository is the `anis-client` sub-project. It offers a web interface to search and retrieve astronomical data.

ANIS is protected by the CeCILL licence (see LICENCE file at the software root).

## Authors

Here is the list of people involved in the development:

* `François Agneray` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Chrystel Moreau` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Tifenn Guillas` : Laboratoire d'Astrophysique de Marseille (CNRS)

## More resources:

* [Website](https://anis.lam.fr)
* [Documentation](https://anis.lam.fr/doc/)

## Installing and starting the application

Anis-client contains a Makefile that helps the developer to install and start the application.

To list all operations availables just type `make` in your terminal at the root of this application.

- To install all dependancies : `make install`
- To start/stop/restart/status all services : `make start|stop|restart|status`
- To execute tests suite : `make test`
- To execute tests suite on every file change : `make test-watch`
- To generate anis-client application : `make build`
- To display logs for all services : `make logs`
- To open a shell command into anis-client container : `make shell`

## Web interface

After the start the interface is available at: [http://localhost:4200](http://localhost:4200)