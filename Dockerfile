FROM nginx

COPY dist/anis-client /usr/share/nginx/html
COPY conf-dev/nginx.conf /etc/nginx/conf.d/default.conf