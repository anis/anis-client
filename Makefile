UID := 1000
GID := 1000
NAME_APP=anis-client

list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  install      > install node modules dependancies (node_modules)"
	@echo "  start        > run a dev server for $(NAME_APP) application (in memory)"
	@echo "  stop         > stop the dev server for $(NAME_APP) application"
	@echo "  restart      > restart the dev server for $(NAME_APP) (container)"
	@echo "  status       > display $(NAME_APP) container status"
	@echo "  test         > run $(NAME_APP) tests"
	@echo "  test-watch   > run $(NAME_APP) tests on every file change"
	@echo "  report       > open the code coverage report in a browser (only available for Linux)"
	@echo "  build        > generate the angular dist application (html, css, js)"
	@echo "  logs         > display $(NAME_APP) container logs"
	@echo "  shell        > shell into $(NAME_APP) container"
	@echo ""

install:
	@docker build -t anis-node conf-dev && docker run --init -it --rm --user $(UID):$(GID) \
	-v $(CURDIR):/project \
	-w /project anis-node yarn install

start:
	@docker build -t anis-node conf-dev && docker run --init -it --rm --user $(UID):$(GID) \
	--name $(NAME_APP) \
	-p 4200:4200 \
	-p 9876:9876 \
	-v $(CURDIR):/project -d \
	-w /project anis-node ng serve --host=0.0.0.0 --disable-host-check --port 4200

restart: stop start

stop:
	@docker stop $(NAME_APP)

restart: stop start

status:
	@docker ps -f name=$(NAME_APP)

test:
	@docker exec $(NAME_APP) ng test --no-watch --code-coverage

test-watch:
	@docker exec $(NAME_APP) ng test

report:
	xdg-open var/coverage/index.html

build:
	@docker build -t anis-node conf-dev && docker run --init -it --rm --user $(UID):$(GID) \
	-v $(CURDIR):/project \
	-w /project anis-node ng build --prod --base-href /

logs:
	@docker logs -f -t $(NAME_APP)

shell:
	@docker exec -ti $(NAME_APP) bash
